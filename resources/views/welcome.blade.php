<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/cover.css') }}" rel="stylesheet">
</head>
<body class="d-flex h-100 text-center text-white bg-black">
<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="mb-auto">
        <div class="float-md-end">
            <ul class="nav px-3 nav-locales">
                @foreach (config()->get('app.locales') as $locale)
                    <li class="nav-item{{ $locale == app()->getLocale() ? " active" : "" }}">
                        <a class="nav-link"
                           href="/setlocale/{{ $locale }}"><img class="img-flag"
                                                                src="{{ asset("images/flags/$locale.png") }}"></a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div>
            <h3 class="float-md-start mb-0">{{ config('app.name', 'Laravel') }}</h3>
            <nav class="nav nav-masthead justify-content-center float-md-end">
                @if (Route::has('login'))
                    @auth
                        <a href="{{ url('/dashboard') }}" class="nav-link">{{__('Dashboard')}}</a>
                    @else
                        <a href="{{ route('login') }}" class="nav-link">{{__('Log in')}}</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="nav-link">{{__('Register')}}</a>
                        @endif
                    @endauth
                @endif
            </nav>
        </div>
    </header>
    <main class="px-3"></main>
    <footer class="mt-auto text-white-50">
        <p>{{date('Y')}}</p>
    </footer>
</div>
</body>
</html>


