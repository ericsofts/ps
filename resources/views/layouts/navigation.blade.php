<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse sidebar-menu">
    <div class="position-sticky pt-3">
        <ul class="nav flex-column">
            <li class="nav-item">
                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mb-1 text-muted">
                    {{ Auth::user()->name }}
                </h6>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('dashboard')) active @endif" href="{{route('dashboard')}}">
                    <i class="bi bi-house"></i>
                    {{__('Dashboard')}}
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('input_invoice.history')) active @endif @if($count_input_invoice_not_paid) has-active-invoice @endif"
                   href="{{route('input_invoice.history')}}">
                    <i class="bi bi-card-list"></i>
                    {{__('Top-Up history')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('balance.history')) active @endif" href="{{route('balance.history')}}">
                    <i class="bi bi-card-list"></i>
                    {{__('Balance history')}}
                </a>
            </li>
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link @if(request()->routeIs('payment2partner.create')) active @endif" href="{{route('payment2partner.create')}}">--}}
{{--                    <i class="bi bi-box-arrow-in-right"></i>--}}
{{--                    {{__('Payment to the partner')}}--}}
{{--                </a>--}}
{{--            </li>--}}
        </ul>
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>{{__('GROW Token')}}</span>
        </h6>
        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('input_invoice.create')) active @endif @if($count_input_invoice_not_paid) has-active-invoice @endif"
                   href="{{route('input_invoice.create')}}">
                    <i class="bi bi-cart"></i>
                    {{__('Buy token')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('grow_token.exchange')) active @endif" href="{{route('grow_token.exchange')}}">
                    <i class="bi bi-cart"></i>
                    {{__('Exchange token')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('grow_token.withdrawal')) active @endif" href="{{route('grow_token.withdrawal')}}">
                    <i class="bi bi-box-arrow-in-right"></i>
                    {{__('Token withdrawal')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('input_invoice.history.addition')) active @endif @if($count_input_invoice_not_paid) has-active-invoice @endif"
                   href="{{route('input_invoice.history.addition')}}">
                    <i class="bi bi-card-list"></i>
                    {{__('Buy history token')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('grow_token.withdrawal.index')) active @endif" href="{{route('grow_token.withdrawal.index')}}">
                    <i class="bi bi-card-list"></i>
                    {{__('Token withdrawal history')}}
                </a>
            </li>
        </ul>
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>{{__('Settings')}}</span>
            <i class="bi bi-gear"></i>
        </h6>
        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('user.profile')) active @endif" href="{{route('user.profile')}}">
                    <i class="bi bi-person"></i>
                    {{__('Profile')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('2fa.index')) active @endif" href="{{route('2fa.index')}}">
                    <i class="bi bi-shield-lock"></i>
                    {{__('Security options')}}
                </a>
            </li>
        </ul>
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1">
            <span>{{__('Contact us')}}:</span>
        </h6>
        <ul class="nav flex-column mb-2">
            <li class="">
                <a class="nav-link" href="mailto:{{config('app.support_email')}}">
                    {{config('app.support_email')}}
                </a>
            </li>
        </ul>
    </div>
</nav>
