<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}"></script>
    </head>
    <body class="bg-app @yield('body-class')">
    @include('layouts.header')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.navigation')
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                @yield('content')
            </main>
        </div>
    </div>
    @if(session()->has('message'))
        <script>
            window.onload = function(e){
                toastr.options.progressBar = true;
                var type = "{{ session()->get('alert-type', 'info') }}";
                switch(type){
                    case 'info':
                        toastr.info("{{ session()->get('message') }}");
                        break;

                    case 'warning':
                        toastr.warning("{{ session()->get('message') }}");
                        break;

                    case 'success':
                        toastr.success("{{ session()->get('message') }}");
                        break;

                    case 'error':
                        toastr.error("{{ session()->get('message') }}");
                        break;
                }
            }
        </script>
    @endif
    @yield('script-bottom')
    </body>
</html>
