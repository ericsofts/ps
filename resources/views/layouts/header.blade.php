<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="/">{{ config('app.name', 'Laravel') }}</a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="d-flex justify-content-end w-100">
        <ul class="nav px-3 nav-locales">
            @foreach (config()->get('app.locales') as $locale)
                <li class="nav-item{{ $locale == app()->getLocale() ? " active" : "" }}">
                    <a class="nav-link"
                       href="/setlocale/{{ $locale }}"><img class="img-flag"
                                                            src="{{ asset("images/flags/$locale.png") }}"></a>
                </li>
            @endforeach
        </ul>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <button class="btn btn-link text-muted" type="submit">{{__('Log out')}}</button>
                </form>
            </li>
        </ul>
    </div>
</header>
