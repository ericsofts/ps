@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Buy history token')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Buy history token') }}
    </h2>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        <div class="col">
            <div class="card text-white bg-dark mb-3 h-100">
                <div class="card-body">
                    <h5 class="card-title">{{__('Balance')}} GROW</h5>
                    <p class="card-text fs-2">GRT {{price_format($balanceTotalGrow, 6)}}</p>
                </div>
            </div>
        </div>
    </div>
    @if($invoices)
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{__('Number')}}</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Token')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            {{debug($invoices)}}
            @foreach($invoices as $i => $item)
                <tr>
                    <th scope="row">{{$i+1+(($invoices->currentPage()-1)*$invoices->perPage())}}</th>
                    <td>{{$item->id}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{$fmt->formatCurrency(price_format($item->amount), "USD")}}</td>
                    <td>{{price_format($item->grow_token_amount2pay, 6)}}</td>
                    <td>{{input_invoice_status($item->status)}}</td>
                    <td>
                        @if($item->payment_system == 2 && in_array($item->status, [\App\Models\InputInvoice::STATUS_CREATED]))
                            <a href="{{route('input_invoice.chatex_lbc_contact_confirm_show', $item->id)}}">{{__('Details')}}</a>
                        @endif
                        @if($item->payment_system == 3 && in_array($item->status, [\App\Models\InputInvoice::STATUS_CREATED]))
                            <a href="{{route('input_invoice.usdt', $item->id)}}">{{__('Details')}}</a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $invoices->links() }}</div>
    @endif
@endsection
