@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Buy token')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Buy token') }}
    </h2>
    <form method="POST" action="{{ route('input_invoice.payment_method') }}">
        @csrf
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="mb-3">
            <label for="amount" class="form-label">{{__('Amount')}}</label>
            <div class="input-group">
                <span class="input-group-text">$</span>
                <input type="text" class="form-control @error('amount') is-invalid @enderror" id="amount" name="amount" placeholder="{{__('Amount')}}"
                       value="{{old('amount')}}" required autofocus
                >
                @error('amount')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <span class="form-label">{{__('Current rate')}}: {{price_format($grtRate, 6)}}</span>
        </div>
        <div class="mb-3">
            <span class="form-label">{{__('Current commission')}}:<span class="fee-value"></span></span>
        </div>
        <div class="mb-3">
            <span class="form-label">{{__('Current amount token')}}:<span class="current-amount"></span></span>
        </div>

        <div class="mb-3">
            <label for="payment_method" class="form-label">{{__('Payment method')}}</label>
            <select class="form-select @error('payment_method') is-invalid @enderror" id="payment_method" name="payment_method" required>
                <option value=""></option>
                @if(in_array('bank_card', $ps))
                    <option value="bank_card">{{__('Bank Card')}}</option>
                @endif
                @if(in_array('usdt_erc20', $ps))
                    <option value="usdt_erc20">{{__('USDT ERC20')}}</option>
                @endif
            </select>
            @error('payment_method')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        <div class="mb-3 float-end">
            <button type="submit" class="btn btn-grow" onclick="this.disabled=true;this.form.submit();">
                {{__('Next')}}
                <i class="bi bi-arrow-right"></i>
            </button>
        </div>
    </form>
@endsection
@section('script-bottom')
    <script>
        var amountEvent = function () {
            var roundAccurately = (number, decimalPlaces) => Number(Math.round(Number(number + "e" + decimalPlaces)) + "e" + decimalPlaces * -1);
            var rate = {{$grtRate}};
            var fee = {{$fee}};
            $(document).on("change", '#amount', function (e) {
                $('.current-amount').html(roundAccurately(($(this).val() - ($(this).val() * fee / 100)) / rate, 6));
                $('.fee-value').html(roundAccurately(($(this).val() * fee / 100), 2) + '$');
            });
        }

        if (window.addEventListener)
        {
            window.addEventListener('load', amountEvent, false);
        }
        else if (window.attachEvent)
        {
            window.attachEvent('onload', amountEvent);
        }
    </script>
@endsection
