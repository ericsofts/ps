@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Top-Up history')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Top-Up history') }}
    </h2>
    @if($invoices)
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{__('Number')}}</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            {{debug($invoices)}}
            @foreach($invoices as $i => $item)
                <tr>
                    <th scope="row">{{$i+1+(($invoices->currentPage()-1)*$invoices->perPage())}}</th>
                    <td>{{$item->id}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{$fmt->formatCurrency(price_format($item->amount), "USD")}}</td>
                    <td>{{input_invoice_status($item->status)}}</td>
                    <td>
                        @if($item->payment_system == 2 && in_array($item->status, [\App\Models\InputInvoice::STATUS_CREATED]))
                            <a href="{{route('input_invoice.chatex_lbc_contact_confirm_show', $item->id)}}">{{__('Details')}}</a>
                        @endif
                        @if($item->payment_system == 3 && in_array($item->status, [\App\Models\InputInvoice::STATUS_CREATED]))
                            <a href="{{route('input_invoice.usdt', $item->id)}}">{{__('Details')}}</a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $invoices->links() }}</div>
    @endif
@endsection
