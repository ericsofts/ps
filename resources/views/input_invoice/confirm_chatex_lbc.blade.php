@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('input_invoice.create')}}">{{__('Buy token')}}</a></li>
            <li class="breadcrumb-item active">{{__('Bank Card')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Buy token') }}. {{ __('Bank Card') }}
    </h2>
    @if(!$deal_accepted)
        <div class="form-loading">
            <p class="text-center">{{__('Request processing ...')}}</p>
            <p class="text-center">{{__('Please wait for the payment details to appear. If you click the "Cancel" button, you will have to generate the payment again.')}}</p>
            <form class="form-confirm1" method="POST" action="{{ route('input_invoice.chatex_lbc_contact_confirm') }}">
                @csrf
                <input type="hidden" name="id" value="{{ $id }}">
                @if ($errors->any())
                    <div class="alert alert-grow alert-dismissible fade show" role="alert">
                        {{ __('Whoops! Something went wrong.') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if($status = session('status'))
                    <div class="alert alert-grow alert-dismissible fade show" role="alert">
                        {{ $status }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="mb-3 d-flex justify-content-center">
                    <button type="submit" class="btn btn-grow" name="action" value="0">
                        {{__('Cancel')}}
                    </button>
                </div>
            </form>
        </div>
    @endif
    <form class="form-confirm" method="POST" action="{{ route('input_invoice.chatex_lbc_contact_confirm') }}" enctype="multipart/form-data"
          style="@if(!$deal_accepted) display:none; @endif">
        @csrf
        <input type="hidden" name="id" value="{{ $id }}">
        <input type="hidden" name="action" value="0">
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="mb-3 mt-4 d-flex justify-content-center">
            <dl class="row">
                <dt class="col-sm-4">{{__('Amount')}}:</dt>
                <dd class="col-sm-8">{{$amount}} {{$currency}}</dd>
                <dt class="col-sm-4">{{__('Current rate')}}:</dt>
                <dd class="col-sm-8">{{price_format($grtRate, 6)}}</dd>
                <dt class="col-sm-4">{{__('Current commission')}}:</dt>
                <dd class="col-sm-8">{{price_format(($invoice->amount * $fee / 100), 2)}}$</dd>
                <dt class="col-sm-4">{{__('Amount Token')}}:</dt>
                <dd class="col-sm-8">{{price_format($invoice->grow_token_amount2pay, 6)}} Token</dd>
                <dt class="col-sm-4">{{__('Bank')}}:</dt>
                <dd class="col-sm-8">{{$bank_name}}</dd>
                <dt class="col-sm-4">{{__('Card number')}}:</dt>
                <dd class="col-sm-8 account_info">{{join(" ", str_split($clean_card_number, 4))}}</dd>
            </dl>
        </div>
        <p class="text-center mb-0">
            {{__('Please send the specified amount to this card number. The amount must be exactly what is shown on the screen.')}}
        </p>
        <p class="text-center">{{__('Please add an image with your payment confirmation. Adding an image will allow us to process your payment much faster.')}}</p>
        <div class="mb-3 min-w-320 m-auto">
            <input class="form-control @error('document') is-invalid @enderror"
                   type="file"
                   id="document"
                   name="document">
            @error('document')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        <div class="mb-3 d-flex justify-content-center">
            <button type="submit" class="btn btn-grow mx-2 btn-confirm" name="action" value="1">
                {{__('Payment completed')}}
            </button>
            <button type="submit" class="btn btn-grow btn-cancel" name="action" value="0">
                {{__('Cancel')}}
            </button>
        </div>
        <div class="text-center">
            <p class="fw-bold text-danger mb-0">{{__('If you made a payment and did not click the "Payment completed" button, it is not guaranteed to be credited.')}}</p>
            <p class="fw-bold text-danger">{{__('If you made a payment and clicked the "Cancel" button, the payment will not be credited.')}}</p>
        </div>
    </form>
@endsection

@section('script-bottom')
    <script>
        var deal_accepted = false;
        var form_clicked = false;
        window.onbeforeunload = function (e) {
            if(deal_accepted && !form_clicked){
                return true;
            }
        };
        window.onload = function () {
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })
                $(document).on("click", ".btn-cancel", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form.form-confirm');
                    var html = "{{__('If you made a payment and clicked the "Cancel" button, the payment will not be credited.')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Cancel payment')}}',
                        cancelButtonText: '{{__('Return to payment')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form_clicked = true;
                            form.find('input[name="action"]').val(0);
                            form.submit();
                        }
                    })
                    return false;
                });
                $(document).on("click", ".btn-confirm", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form.form-confirm');
                    var html = "{{__('Press the "Payment completed" button only after the actual payment. Otherwise, the company reserves the right to block your account.')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Payment completed')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form_clicked = true;
                            form.find('input[name="action"]').val(1);
                            form.submit();
                        }
                    })
                    return false;
                });
                @if(!$deal_accepted)
                var interval1 = setInterval(function () {
                    $.ajax({
                        url: '{{route('input_invoice.chatex_lbc_contact_check', $id)}}',
                        type: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}"
                        }
                    }).done(function (data) {
                        if (data.status) {
                            if(data.account_info){
                                $('.account_info').html(data.account_info)
                            }
                            $('.form-loading').remove();
                            $('.form-confirm').show();
                            deal_accepted = true;
                            clearInterval(interval1)
                        }
                    }).fail(function () {
                        clearInterval(interval1)
                    })
                }, 5000)
                @else
                    deal_accepted = true;
                @endif
            });
        }
    </script>
@endsection

