@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('input_invoice.create')}}">{{__('Account Top-Up')}}</a></li>
            <li class="breadcrumb-item active">{{__('USDT ERC20')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Account Top-Up') }}. {{ __('USDT ERC20') }}
    </h2>
    <div class="row">
        <div class="col-md-6 order-md-1 order-sm-2">
            <p class="fw-bold">{{__('Invoice #')}} {{$invoice->id}}</p>
            <P>{{__('Invoice Total')}}: {{$fmt->formatCurrency(price_format($invoice->amount), "USD")}}</P>
            <p>{{__('USDT ERC20 Address')}}: <b class="text-break">{{$invoice->usdt_address->usdt_address}}</b></p>
            <p>{{__('USDT ERC20 Amount')}}: <b>{{price_format($invoice->ps_amount, 6)}}</b></p>
            <p>{{__('Time Left')}}: <span id="time"></span></p>
        </div>
        <div class="col-md-6 order-sm-1 order-md-2">
            <p>{{__('Sending a coin or token other than USDT to this address may result in loss of funds.')}}</p>
            <p>{{__('Please make only one transfer to the specified address. The second transfer will not be automatically accepted.')}}</p>
            <p>{{__('You can transfer any amount in USDT. It will be converted to USD at the current exchange rate and credited to your account.')}}</p>
        </div>
    </div>

    <hr>
    <img src="{{route('usdt.qr', $invoice->id)}}">
    <div class="mt-3">
        <form method="POST" action="{{ route('usdt.cancel',$invoice->id) }}">
            @csrf
            <button type="submit" class="btn btn-grow mx-2 btn-cancel">
                {{__('Cancel')}}
            </button>
        </form>
    </div>


@endsection

@section('script-bottom')
    <script>
        function startTimer(duration, display) {
            var timer = duration;
            var si = setInterval(function () {
                var hours = Math.floor(timer / (60 * 60));
                var minutes = Math.floor((timer % (60 * 60)) / (60));
                var seconds = Math.floor(timer % 60);

                if (hours < 10) hours = '0' + hours;
                if (minutes < 10) minutes = '0' + minutes;
                if (seconds < 10) seconds = '0' + seconds;

                display.textContent = hours + ":"
                    + minutes + ":" + seconds + "";

                if (--timer < 0) {
                    clearInterval(si);
                }
            }, 1000);
        }

        window.onload = function () {
            startTimer({{$expired}}, document.querySelector('#time'));
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })
                $(document).on("click", ".btn-cancel", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form');
                    swalWithBootstrapButtons.fire({
                        html: "{{__('Are you sure?')}}",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Ok')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form.submit();
                        }
                    })
                    return false;
                });
            })
        }
    </script>
@endsection

