@extends('layouts.guest')
@section('body-class', 'd-flex h-100 text-white bg-black')
@section('html-class', 'h-100')

@section('content')
    <main class="form-1">
        <div class="text-center">
            <img class="mb-4" src="{{ asset('images/logo.png') }}">
            <h1 class="h3 mb-3 fw-normal">{{__('Payment')}}</h1>
        </div>
        @if($error)
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if ($errors->any())
            <ul class="mt-3 list-disc list-inside text-sm text-red-600">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <div class="text-center">
            <ul class="list-inline mt-1">
                <li class="list-inline-item">
                    <a href="{{url()->previous()}}" class="text-muted">
                        {{ __('Back') }}
                    </a>
                </li>
            </ul>
        </div>
        <p class="mt-5 mb-3 text-muted text-center">&copy; 2020–{{date('Y')}}</p>
    </main>
@endsection

