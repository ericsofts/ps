@extends('layouts.api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))

@section('script-head')
{{--    <script src="{{ mix('js/mask.js') }}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.11.2/jquery.mask.min.js"></script>
    <script>
        $(document).ready(function() {
            $('input[data-mask]').mask('9999 9999 9999 9999', {placeholder: "9999 9999 9999 9999"});
        });
    </script>
@endsection

@section('css-whitelabel')
    <style>
        @include('payment.invoice.v2.post.style')
    </style>
@endsection

@section('content')
    <main class="m-auto main w-100">
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @if(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @elseif(!empty($error))
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @if(!empty($canCancel))
                <div class="col text-center">
                    <a href="{{\Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.checkout.cancel', $invoice->id)}}"
                       class="btn text-center btn-dark">{{__('Cancel')}}</a>
                </div>
            @elseif(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @elseif($error = session()->pull('error'))
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @if(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @else
            @if(!empty($warning))
                <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                    {{ $warning }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
        <div class="content d-flex align-center align-items-center flex-column">
            <div class="text-center">
            <span>
                {{ $whiteLabelModel->getPageText('header_text')}}
            </span>
            </div>

            <form  class="contentIn w-100 d-flex flex-column align-items-start"
                method="POST" action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.checkout.confirm', $invoice->id) }}">
                @csrf
                <input type="hidden" name="payment_withdrawal_id" value="{{ $invoice->id }}">
                @if ($errors->any())
                    <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                        {{ __('Whoops! Something went wrong.') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <a href="#" class="logo m-auto d-flex flex-column">
                    <img src="{{ $whiteLabelModel->getImageLogo($merchant->viewVersion) }}" alt="" class="m-auto">
                    @if(!$merchant->isDisableShowName())
                    <span class="mt-2">{{$merchant->name}}</span>
                    @endif
                </a>

                <div class="numberBlock w-100 mt-5">

                    <div class="numberBlockOrder d-flex justify-content-between align-items-center">
                        <span class="numberBlockOrder__name">{{__('Order #')}}</span>
                        <span class="numberBlockOrder__card">{{$invoice->merchant_order_id}}</span>
                    </div>

                    <div class="numberBlockOrderTop d-flex justify-content-between">
                        <span class="numberBlockOrderTop__num">{{__('Internal order #')}}</span>
                        <span>{{$invoice->id}}</span>
                    </div>

                    <div class="numberBlockOrder d-flex justify-content-between mt-2">
                        <dt class="col-sm-6">{{__('Amount')}}:</dt>
                        <span class="numberBlockOrder__name">{{__('Amount')}}</span>
                        <span class="numberBlockOrder__total">
                        @if($invoice->input_currency == 'USD')
                                {{$fmt->formatCurrency(price_format($invoice->amount), "USD")}}
                            @else
                                @if(!$invoice->currency2currency)
                                    {{$fmt->formatCurrency(price_format($invoice->amount), "USD")}}
                                    /{{$fmt->formatCurrency(price_format($invoice->input_amount_value), $invoice->input_currency)}}
                                @else
                                    {{$fmt->formatCurrency(price_format($invoice->input_amount_value), $invoice->input_currency)}}
                                @endif
                            @endif
                        </span>
                    </div>

                        @if($invoice->input_currency != 'USD')
                            @if(!$invoice->currency2currency)
                            <div class="numberBlockOrder d-flex justify-content-between align-items-center mt-3">
                                <span class="numberBlockOrder__name">{{__('Rate')}}</span>
                                <span class="numberBlockOrder__card">{{$invoice->input_rate}}</span>
                            </div>
                            @endif
                        @endif
                </div>

                <p class="desc mt-3">
                    @if(!empty($invoice->merchant_order_desc))
                        {{$invoice->merchant_order_desc}}
                    @endif
                </p>

                <div class="selects mt-3 w-100">

                    <div class="selects__name">{{__('Select currency')}}</div>

                    <select class="form-select w-100 mt-2 @error('currency') is-invalid @enderror" id="currency" name="currency" required>
                    @foreach($currencies as $currency)
                        <option value="{{$currency}}"
                                @if($defaultFiat == $currency) selected @endif
                        >
                            {{$currency}}
                        </option>
                        @endforeach
                    </select>
                        @error('currency')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                </div>

                <div class="selects mt-4 w-100 mb-4">

                    <div class="selects__name">{{__('Select payment system')}}</div>
                    <div class="desc">{{__('Please choose from the list only the bank whose card you have and to which you plan to receive funds.')}}
                    </div>

                    <select class="form-select w-100 mt-2 @error('payment_system_id') is-invalid @enderror" id="payment_system_id" name="payment_system_id" required>
                        @foreach($buy_coins[$currencies[$defaultFiatKey]] as $item)
                            <option value="{{$item['ad_id']}}">{{$item['bank_name']}} - {{price_format($item['gps_temp_amount'])}} {{$item['currency']}}</option>
                        @endforeach
                    </select>
                    @error('payment_system_id')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>

                <div class="cardInput w-100 mb-3">
                    @if($defaultFiat == 'INR')
                        @php $label_message = __('Enter payment info') @endphp
                    @else
                        @php $label_message = __('Enter bank card number') @endphp
                    @endif
                    <label for="message">{{$label_message}}</label>
                    <input type="text"  class="w-100 @error('message') is-invalid @enderror message-input" id="message" name="message" placeholder="{{$label_message}}"
                           value="{{old('message')}}" required data-mask>
                        @error('message')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                </div>

                <button type="submit" class="btn btn-dark m-auto btnSend"
                        name="action"
                        value="1"
                        onclick="this.disabled=true;this.form.submit();"
                >
                    {{__('Confirm')}}
                </button>
                <a href="{{\Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.checkout.cancel', $invoice->id)}}"
                   class="btnCancel m-auto pt-3 text-decoration-none">{{__('Cancel')}}</a>

                <p class="desc pt-2">

                </p>
            </form>


        </div>
                <footer class="footer">
                    <ul class="languages d-flex justify-content-between m-auto pt-5">
                        @foreach (config()->get('app.locales') as $locale)
                            <li>
                                <a href="/setlocale/{{ $locale }}" class="{{ $locale == app()->getLocale() ? " languages__active" : "" }}">{{ $locale }}</a>
                            </li>

                        @endforeach
                    </ul>

                    <div class="copyright text-center m-auto pt-3 pb-3">Grow Payments System 2020–{{date('Y')}}</div>
                </footer>
        @endif
    </main>
@endsection

@section('script-bottom')
    @if (isset($buy_coins))
        <script>
            var message_labels = {
                'bc' :'{{__('Enter bank card number')}}',
                'pi' :'{{__('Enter payment info')}}'
            }
            $(function () {
                $(document).on('change', '#currency', function (event) {
                    if($(this).val() === 'INR'){
                        $('.message-label').text(message_labels.pi);
                        $('.message-input').attr('placeholder', message_labels.pi);
                    }else{
                        $('.message-label').text(message_labels.bc);
                        $('.message-input').attr('placeholder', message_labels.bc);
                    }
                })
            });

            var FiatsArray = [];
            @foreach ($buy_coins as $k => $v)
            var estimates_arr = [];
            @foreach ($v as $item)
                estimates_arr[estimates_arr.length] = {
                payment_system_id: '{{ $item['ad_id'] }}',
                payment_system_name: '{{ $item['bank_name'] }}',
                fiat_amount: '{{ price_format($item['gps_temp_amount']) }} {{$item['currency']}}'
            };
            @endforeach
                FiatsArray['{{ $k }}'] = estimates_arr;
            @endforeach
        </script>
    @endif
@endsection
