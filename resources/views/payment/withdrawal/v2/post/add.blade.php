@extends('layouts.api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))

@section('css-whitelabel')
    <style>
        @include('payment.invoice.v2.post.style')
    </style>
@endsection

@section('content')
    <main class="m-auto main w-100">
        <div class="content d-flex align-center align-items-center flex-column justify-content-between">
            <form class="contentIn w-100 d-flex flex-column align-items-start">
                <a href="#" class="logo m-auto d-flex flex-column">
                    <img src="{{ $whiteLabelModel->getImageLogo($merchant->viewVersion) }}" alt="" class="m-auto">
                </a>


                <div class="request w-100">
                    <p class="text-center request_desc request__title_modif">
                    @if($error)
                            {{ $error }}
                    @endif
                    @if ($errors->any())
                        <ul class="mt-3 list-disc list-inside text-sm text-red-600">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    @if($status = session('status'))
                            {{ $status }}
                        @endif
                    </p>
                </div>


                <button onclick="location.href={{request()->headers->get('referer')}}; return false" class="btn m-auto btnCancelBtn">{{__('Return to site')}}</button>
            </form>


        </div>
        <footer class="footer">
            <ul class="languages d-flex justify-content-between m-auto pt-5">
                @foreach (config()->get('app.locales') as $locale)
                    <li>
                        <a href="/setlocale/{{ $locale }}" class="{{ $locale == app()->getLocale() ? " languages__active" : "" }}">{{ $locale }}</a>
                    </li>

                @endforeach
            </ul>

            <div class="copyright text-center m-auto pt-3 pb-3">Grow Payments System 2020–{{date('Y')}}</div>
        </footer>
    </main>
@endsection

