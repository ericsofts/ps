@extends('layouts.api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))
@section('css-whitelabel')
    <style>
        @include('payment.invoice.v2.post.style')
    </style>
@endsection

@section('content')
    <main class="m-auto main w-100">
        <div class="content d-flex align-center align-items-center flex-column">
            <div class="contentIn w-100 d-flex flex-column align-items-start">
            <a href="#" class="logo m-auto d-flex flex-column">
                <img src="{{ $whiteLabelModel->getImageLogo($merchant->viewVersion) }}" alt="" class="m-auto">
            </a>

    @if(!$deal_accepted || !$payment_completed)
             <div class="request w-100 form-loading">
                        <div class="request__title text-center  mb-4">{{__('Request processing ...')}}</div>
                        <img src="/images/logoGif.gif" alt="" class="d-block m-auto logo-gif">

                        <p class="text-center request_desc">{{__('Дождитесь исполнения вашего запроса или отмените текущую операцию, чтобы вернуться на сайт поставщика услуг')}}</p>

            <form class="form-confirm1" method="POST"
                  action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.checkout.status') }}">
                @csrf
                <input type="hidden" name="id" value="{{ $id }}">
                <input type="hidden" name="action" value="0">
                @if ($errors->any())
                    <div class="alert alert-grow alert-dismissible fade show" role="alert">
                        {{ __('Whoops! Something went wrong.') }}
                        <button type="button" class="border-white bg-white btn-secondary mx-2 btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if($status = session('status'))
                    <div class="alert alert-grow alert-dismissible fade show" role="alert">
                        {{ $status }}
                        <button type="button" class="border-white bg-white btn-secondary mx-2 btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(!$deal_accepted)
                        <button type="submit" class="btn m-auto btnCancelBtn btn-cancel"  style="display: block" name="action" value="0">
                            {{__('Cancel')}}
                        </button>
                @endif
            </form>
        </div>
        </div>
    @endif
    <form class="form-confirm" method="POST" action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.checkout.status') }}"
          enctype="multipart/form-data"
          style="@if(!$payment_completed) display:none; @endif">
        @csrf
        <input type="hidden" name="id" value="{{ $id }}">
        <input type="hidden" name="action" value="0">
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                <button type="button" class="btn-close border-white bg-white btn-secondary mx-2" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close border-white bg-white btn-secondary mx-2" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="request w-100 mt-4">
            <div class="request__title text-center">{{__('The transfer has been made.')}}</div>
            <p class="text-center request_desc" @if(!$attachments->count())) style="display: none;" @endif>
                {{__('Verify receipt of the money and only then acknowledge receipt.')}}
            </p>
        </div>

        <p class="text-center mb-0 attachments-empty" @if($attachments->count()) style="display: none;" @endif>{{__('Please wait to receive an image of the receipt to verify the payment details')}}</p>



        <div class="payment-received" @if(!$attachments->count()) style="display: none;" @endif>
            <div class="text-center">
                <button type="submit" class="btn btn-dark m-auto btnSend btn-confirm" name="action" value="1">{{__('Payment received')}}</button>
            </div>

            <div class="text-center">
            <p class="text-center request_desc request__title_modif">{{__('Не подтверждайте получение оплаты, если средства не поступили на вашу карту. По любым вопросам обращайтесь в с службу поддержки')}}
            </p>
            </div>
        </div>
    </form>
    <div class="mt-5 container">
        <p class="text-center request_desc" @if(!$attachments->count()) style="display: none;" @endif>{{__('Check the image of the receipt with your data and press the "Receive payment" button only after a full match')}}</p>
        <div class="row list-attachments">
            @foreach($attachments as $attachment)
                <div class="col-12 col-sm-6 col-lg-3">
                    <a href="{{\Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.attachment', $attachment->id, now()->addMonth())}}" target="_blank">
                        <img src="{{\Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.attachment', $attachment->id, now()->addMonth())}}"
                             class="float-start img-thumbnail w-100">
                    </a>
                </div>
            @endforeach
        </div>
    </div>
        </div>


        </div>
        <footer class="footer">
            <ul class="languages d-flex justify-content-between m-auto pt-5">
                @foreach (config()->get('app.locales') as $locale)
                    <li>
                        <a href="/setlocale/{{ $locale }}" class="{{ $locale == app()->getLocale() ? " languages__active" : "" }}">{{ $locale }}</a>
                    </li>

                @endforeach
            </ul>

            <div class="copyright text-center m-auto pt-3 pb-3">Grow Payments System 2020–{{date('Y')}}</div>
        </footer>
        <template id="popupTemplate">
            <swal-html>
                <form class="form-confirm" method="POST" action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.checkout.status') }}"
                      enctype="multipart/form-data"
                      >
                    @csrf
                    <input type="hidden" name="id" value="{{ $id }}">
                    <input type="hidden" name="action" value="1">

                    <a href="#" class="popupLogo">
                        <img src="/images/icon3.svg" alt="">
                    </a>
                    <p class="popupDesc red">{{__('Не подтверждайте получение оплаты, если средства не поступили на вашу карту. По любым вопросам обращайтесь в с службу поддержки.')}}</p>

                    <button type="submit" class="btn btn-dark m-auto btnSend d-block">{{__('Payment received')}}</button>
                    <a href="#" onclick="Swal.close(); return false;" class="btnCancel m-auto text-decoration-none">{{__('Cancel')}}</a>
                </form>
            </swal-html>
            <swal-param
                name="customClass"
                value='{ "popup": "swalWithButton" }'/>
        </template>
    </main>
@endsection

@section('script-bottom')
    <script>
        var deal_accepted = @if($deal_accepted) true;
        @else false;
        @endif
        var payment_completed = @if($payment_completed) true;
        @else false;
        @endif
        var form_clicked = false;
        window.onbeforeunload = function (e) {
            if (deal_accepted && !form_clicked) {
                return true;
            }
        };
        window.onload = function () {
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })
                $(document).on("click", ".btn-cancel", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form');
                    var html = "{{__('Are you sure you want to cancel the operation?')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Cancel withdraw')}}',
                        cancelButtonText: '{{__('Return to withdraw')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form_clicked = true;
                            form.find('input[name="action"]').val(0);
                            form.submit();
                        }
                    })
                    return false;
                });
                $(document).on("click", ".btn-confirm", function (e) {
                    e.preventDefault();
                    Swal.fire({
                        template: '#popupTemplate',
                        showCloseButton: true,
                    })
                    form_clicked = true;

                    {{--var form = $(this).closest('form.form-confirm');--}}
                    {{--var html = "{{__('If you have confirmed receipt of the money, but it is not on the card, crediting is not guaranteed.')}}";--}}
                    {{--swalWithBootstrapButtons.fire({--}}
                    {{--    html: html,--}}
                    {{--    icon: 'warning',--}}
                    {{--    showCancelButton: true,--}}
                    {{--    confirmButtonText: '{{__('Payment received')}}',--}}
                    {{--    cancelButtonText: '{{__('Cancel')}}'--}}
                    {{--}).then((result) => {--}}
                    {{--    if (result.isConfirmed) {--}}
                    {{--        form_clicked = true;--}}
                    {{--        form.find('input[name="action"]').val(1);--}}
                    {{--        form.submit();--}}
                    {{--    }--}}
                    {{--})--}}
                    return false;
                });
                var interval1 = setInterval(function () {
                    $.ajax({
                        url: '{{route('payment.withdrawal.post.checkout.status.check')}}',
                        type: 'POST',
                        data: {
                            "id":{{$id}},
                            "_token": "{{ csrf_token() }}"
                        }
                    }).done(function (data) {
                        if (data.status == -1) {
                            location.reload();
                        }
                        if (data.status == 1) {
                            deal_accepted = true;
                            $('.btn-cancel').remove();
                        }
                        if (data.status == 2) {
                            payment_completed = true;
                            $('.form-loading').remove();
                            $('.form-confirm').show();
                            if(data.attachments.length > 0){
                                $('.attachments-text').show();
                                $('.attachments-empty').remove();
                                $('.payment-received').show();
                            }
                        }
                        if(data.new_attachments.length > 0){
                            $.each( data.new_attachments, function( key, value ) {
                                $('.list-attachments').append('<div class="col-12 col-sm-6 col-lg-3">'+
                                    '<a href="'+value+'" target="_blank">'+
                                    '<img src="'+value+'" class="float-start img-thumbnail w-100">'+
                                    '</a>'+
                            '</div>');
                            });
                        }

                    }).fail(function () {
                        //clearInterval(interval1)
                    })
                }, 5000)
            });
        }
    </script>
@endsection

