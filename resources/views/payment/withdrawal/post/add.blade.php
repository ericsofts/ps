@extends('layouts.api')
@section('body-class', 'd-flex h-100 api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))

@section('css-whitelabel')
    {!! $whiteLabelModel->getCss() !!}
@endsection

@section('content')
    <main class="form-1">
        <div class="text-center">
            <img class="mb-4 img-fluid" src="{{ $whiteLabelModel->getImageLogo() }}">
            <h1 class="h3 mb-3 fw-normal">{{__('Payment')}}</h1>
        </div>
        @if($error)
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
            </div>
        @endif
        @if ($errors->any())
            <ul class="mt-3 list-disc list-inside text-sm text-red-600">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
            </div>
        @endif
        <div class="text-center">
            <a href="{{request()->headers->get('referer')}}" class="btn btn-link">{{__('Return to site')}}</a>
        </div>
        <p class="mt-5 mb-3 text-muted text-center">2020–{{date('Y')}}</p>
    </main>
@endsection

