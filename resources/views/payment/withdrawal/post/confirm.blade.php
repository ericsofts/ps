@extends('layouts.api')
@section('body-class', 'd-flex h-100 api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))
@section('css-whitelabel')
    {!! $whiteLabelModel->getCss() !!}
@endsection

@section('content')

    @if(!$deal_accepted || !$payment_completed)
        <div class="form-loading">
            <p class="text-center">{{__('Request processing ...')}}</p>
            <form class="form-confirm1" method="POST"
                  action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.checkout.status') }}">
                @csrf
                <input type="hidden" name="id" value="{{ $id }}">
                <input type="hidden" name="action" value="0">
                @if ($errors->any())
                    <div class="alert alert-grow alert-dismissible fade show" role="alert">
                        {{ __('Whoops! Something went wrong.') }}
                        <button type="button" class="border-white bg-white btn-secondary mx-2 btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if($status = session('status'))
                    <div class="alert alert-grow alert-dismissible fade show" role="alert">
                        {{ $status }}
                        <button type="button" class="border-white bg-white btn-secondary mx-2 btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if(!$deal_accepted)
                    <div class="mb-3 d-flex justify-content-center">
                        <button type="submit" class="btn btn-secondary mx-2 btn-cancel btn-secondary-whitelabel" name="action" value="0">
                            {{__('Cancel')}}
                        </button>
                    </div>
                @endif
            </form>
        </div>
    @endif
    <form class="form-confirm" method="POST" action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.checkout.status') }}"
          enctype="multipart/form-data"
          style="@if(!$payment_completed) display:none; @endif">
        @csrf
        <input type="hidden" name="id" value="{{ $id }}">
        <input type="hidden" name="action" value="0">
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                <button type="button" class="btn-close border-white bg-white btn-secondary mx-2" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close border-white bg-white btn-secondary mx-2" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <p class="text-center">{{__('The transfer has been made.')}}</p>
        <p class="text-center mb-0 payment-received" @if(!$attachments->count())) style="display: none;" @endif>
            {{__('Verify receipt of the money and only then acknowledge receipt.')}}
        </p>
        <p class="text-center mb-0 attachments-empty" @if($attachments->count()) style="display: none;" @endif>{{__('Please wait to receive an image of the receipt to verify the payment details')}}</p>
        <div class="payment-received" @if(!$attachments->count()) style="display: none;" @endif>
            <div class="mb-3 mt-3 d-flex justify-content-center">
                <button type="submit" class="btn btn-secondary mx-2 btn-confirm btn-primary-whitelabel" name="action" value="1">
                    {{__('Payment received')}}
                </button>
            </div>
            <div class="text-center">
                <p class="fw-bold mb-0 danger-whitelable-text">{{__('If you have confirmed receipt of the money, but it is not on the card, crediting is not guaranteed.')}}</p>
            </div>
        </div>
    </form>
    <div class="mt-5 container">
        <p class="attachments-text" @if(!$attachments->count()) style="display: none;" @endif>
            {{__('Check the image of the receipt with your data and press the "Receive payment" button only after a full match')}}
        </p>
        <div class="row list-attachments">
            @foreach($attachments as $attachment)
                <div class="col-12 col-sm-6 col-lg-3">
                    <a href="{{\Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.attachment', $attachment->id, now()->addMonth())}}" target="_blank">
                        <img src="{{\Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.attachment', $attachment->id, now()->addMonth())}}"
                             class="float-start img-thumbnail w-100">
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('script-bottom')
    <script>
        var deal_accepted = @if($deal_accepted) true;
        @else false;
        @endif
        var payment_completed = @if($payment_completed) true;
        @else false;
        @endif
        var form_clicked = false;
        window.onbeforeunload = function (e) {
            if (deal_accepted && !form_clicked) {
                return true;
            }
        };
        window.onload = function () {
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })
                $(document).on("click", ".btn-cancel", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form');
                    var html = "{{__('Are you sure you want to cancel the operation?')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Cancel withdraw')}}',
                        cancelButtonText: '{{__('Return to withdraw')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form_clicked = true;
                            form.find('input[name="action"]').val(0);
                            form.submit();
                        }
                    })
                    return false;
                });
                $(document).on("click", ".btn-confirm", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form.form-confirm');
                    var html = "{{__('If you have confirmed receipt of the money, but it is not on the card, crediting is not guaranteed.')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Payment received')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form_clicked = true;
                            form.find('input[name="action"]').val(1);
                            form.submit();
                        }
                    })
                    return false;
                });
                var interval1 = setInterval(function () {
                    $.ajax({
                        url: '{{route('payment.withdrawal.post.checkout.status.check')}}',
                        type: 'POST',
                        data: {
                            "id":{{$id}},
                            "_token": "{{ csrf_token() }}"
                        }
                    }).done(function (data) {
                        if (data.status == -1) {
                            location.reload();
                        }
                        if (data.status == 1) {
                            deal_accepted = true;
                            $('.btn-cancel').remove();
                        }
                        if (data.status == 2) {
                            payment_completed = true;
                            $('.form-loading').remove();
                            $('.form-confirm').show();
                            if(data.attachments.length > 0){
                                $('.attachments-text').show();
                                $('.attachments-empty').remove();
                                $('.payment-received').show();
                            }
                        }
                        if(data.new_attachments.length > 0){
                            $.each( data.new_attachments, function( key, value ) {
                                $('.list-attachments').append('<div class="col-12 col-sm-6 col-lg-3">'+
                                    '<a href="'+value+'" target="_blank">'+
                                    '<img src="'+value+'" class="float-start img-thumbnail w-100">'+
                                    '</a>'+
                            '</div>');
                            });
                        }

                    }).fail(function () {
                        //clearInterval(interval1)
                    })
                }, 5000)
            });
        }
    </script>
@endsection

