@extends('layouts.api')
@section('body-class', 'd-flex h-100 api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))

@section('css-whitelabel')
    {!! $whiteLabelModel->getCss() !!}
@endsection

@section('content')
    <main class="form-1">
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @if(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @elseif(!empty($error))
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @if(!empty($canCancel))
                <div class="col text-center">
                    <a href="{{\Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.checkout.cancel', $invoice->id)}}"
                       class="btn text-center btn-cancel btn-link">{{__('Cancel')}}</a>
                </div>
            @elseif(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @elseif($error = session()->pull('error'))
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @if(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @else
            @if(!empty($warning))
                <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                    {{ $warning }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="text-center">
            <span>
                {{ $whiteLabelModel->getPageText('header_text')}}
            </span>
            </div>
            <div class="text-center">
                <img class="mb-4 img-fluid" src="{{ $whiteLabelModel->getImageLogo() }}">
                @if(!$merchant->isDisableShowName())
                <h2 class="h3 mb-3 fw-normal">{{$merchant->name}}</h2>
                @endif
            </div>
            <div class="mb-3 mt-4 d-flex justify-content-center">
                <dl class="row mx-3">
                    <dt class="col-sm-6">{{__('Internal order #')}}:</dt>
                    <dd class="col-sm-6">{{$invoice->id}}</dd>
                    @if(!empty($invoice->merchant_order_id))
                        <dt class="col-sm-6">{{__('Order #')}}:</dt>
                        <dd class="col-sm-6">{{$invoice->merchant_order_id}}</dd>
                    @endif
                    @if(!empty($invoice->merchant_order_desc))
                        <dt class="col-sm-6"></dt>
                        <dd class="col-sm-6">{{$invoice->merchant_order_desc}}</dd>
                    @endif
                    <dt class="col-sm-6">{{__('Amount')}}:</dt>
                    @if($invoice->input_currency == 'USD')
                        <dd class="col-sm-6">{{$fmt->formatCurrency(price_format($invoice->amount), "USD")}}</dd>
                    @else
                        @if(!$invoice->currency2currency)
                            <dd class="col-sm-6">{{$fmt->formatCurrency(price_format($invoice->amount), "USD")}}
                                /{{$fmt->formatCurrency(price_format($invoice->input_amount_value), $invoice->input_currency)}}</dd>
                            <dt class="col-sm-6">{{__('Rate')}}:</dt>
                            <dd class="col-sm-6">{{$invoice->input_rate}}</dd>
                        @else
                            <dd class="col-sm-6">{{$fmt->formatCurrency(price_format($invoice->input_amount_value), $invoice->input_currency)}}</dd>
                        @endif
                    @endif
                </dl>
            </div>
            <form method="POST" action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.checkout.confirm', $invoice->id) }}">
                @csrf
                <input type="hidden" name="payment_withdrawal_id" value="{{ $invoice->id }}">
                @if ($errors->any())
                    <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                        {{ __('Whoops! Something went wrong.') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="mb-3">
                    <label for="currency" class="form-label">{{__('Select currency')}}</label>
                    <select class="form-select @error('currency') is-invalid @enderror" id="currency" name="currency" required>
                        @foreach($currencies as $currency)
                            <option value="{{$currency}}"
                                @if($defaultFiat == $currency) selected @endif
                            >
                                {{$currency}}
                            </option>
                        @endforeach
                    </select>
                    @error('currency')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="payment_system_id" class="form-label">{{__('Select payment system')}}</label>
                    <select class="form-select @error('payment_system_id') is-invalid @enderror" id="payment_system_id" name="payment_system_id" required>
                        @foreach($buy_coins[$currencies[$defaultFiatKey]] as $item)
                            <option value="{{$item['ad_id']}}"
                                    data-type="{{$item['payment_system_type']}}"
                            >
                                {{$item['bank_name']}} - {{price_format($item['gps_temp_amount'])}} {{$item['currency']}}
                            </option>
                        @endforeach
                    </select>
                    @error('payment_system_id')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                    @if(!empty($defaultAd) && $defaultAd['payment_system_type'] == \App\Models\PaymentSystemType::IBAN)
                        @php $message_text = __('Please choose from the list only the bank where you plan to receive funds.') @endphp
                        @php $label_message = __('Enter IBAN') @endphp
                    @else
                        @php $message_text = __('Please choose from the list only the bank whose card you have and to which you plan to receive funds.') @endphp
                        @if(!empty($defaultAd) && $defaultAd['payment_system_type'] == \App\Models\PaymentSystemType::UPI)
                            @php $label_message = __('Enter payment info') @endphp
                        @elseif(!empty($defaultAd) && $defaultAd['payment_system_type'] == \App\Models\PaymentSystemType::QIWI)
                            @php $label_message = __('Enter phone number') @endphp
                        @else
                            @php $label_message = __('Enter bank card number') @endphp
                        @endif
                    @endif
                    <div class="form-text secondary-whitelable-text payment_system_id_text">
                        {{$message_text}}
                    </div>
                </div>
                <div class="mb-3">
                    <label for="message" class="form-label message-label">{{$label_message}}</label>
                    <input type="text" class="form-control @error('message') is-invalid @enderror message-input" id="message" name="message" placeholder="{{$label_message}}"
                           value="{{old('message')}}"
                    >
                    @error('message')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
                <div class="mb-3 payment-info" style="@if(!empty($defaultAd) && $defaultAd['payment_system_type'] != \App\Models\PaymentSystemType::IBAN) display:none; @endif">
                    <label for="payment_info" class="form-label">{{__("Recipient's full name")}}</label>
                    <input type="text" class="form-control @error('payment_info') is-invalid @enderror" id="payment_info" name="payment_info" placeholder="{{__("Recipient's full name")}}"
                           value="{{old('payment_info')}}"
                    >
                    @error('payment_info')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
                <div class="mb-3 mt-3 d-flex justify-content-center">
                    <button type="submit" class="btn btn-secondary mx-2 btn-confirm btn-primary-whitelabel"
                            name="action"
                            value="1"
                            onclick="this.disabled=true;this.form.submit();"
                    >
                        {{__('Confirm')}}
                    </button>
                    <a href="{{\Illuminate\Support\Facades\URL::signedRoute('payment.withdrawal.post.checkout.cancel', $invoice->id)}}"
                       class="btn btn-cancel btn-link">{{__('Cancel')}}</a>
                </div>
            </form>
            <div class="text-center">
                <span>
                    {{ $whiteLabelModel->getPageText('footer_text')}}
                </span>
            </div>
        @endif

        <p class="mt-5 mb-3 text-muted text-center">2020–{{date('Y')}}</p>
    </main>
@endsection

@section('script-bottom')
    @if (isset($buy_coins))
        <script>
            var message_labels = {
                'bc' :'{{__('Enter bank card number')}}',
                'pi' :'{{__('Enter payment info')}}',
                'ib' :'{{__('Enter IBAN')}}',
                'qw' :'{{__('Enter Qiwi phone number')}}',
            }
            var message_text = {
                'bc' :'{{__('Please choose from the list only the bank whose card you have and to which you plan to receive funds.')}}',
                'ib' :'{{__('Please choose from the list only the bank where you plan to receive funds.')}}',
                'qw' :'{{__('')}}'
            }
            $(function () {
                $(document).on('change', '#payment_system_id', function (event) {
                    var type = $(this).find(':selected').data('type');
                    if(type === 'iban') {
                        $('.payment_system_id_text').text(message_text.ib);
                        $('.message-label').text(message_labels.ib);
                        $('.message-input').attr('placeholder', message_labels.ib);
                        $('.payment-info').show();
                    }else if(type === 'upi'){
                        $('.message-label').text(message_labels.pi);
                        $('.message-input').attr('placeholder', message_labels.pi);
                        $('.payment_system_id_text').text(message_text.bc);
                        $('.payment-info').hide();
                    }else if(type === 'qiwi'){
                        $('.message-label').text(message_labels.qw);
                        $('.message-input').attr('placeholder', message_labels.qw);
                        $('.payment_system_id_text').text(message_text.qw);
                        $('.payment-info').hide();
                    }else{
                        $('.message-label').text(message_labels.bc);
                        $('.message-input').attr('placeholder', message_labels.bc);
                        $('.payment_system_id_text').text(message_text.bc);
                        $('.payment-info').hide();
                    }
                })
            });

            var FiatsArray = [];
            @foreach ($buy_coins as $k => $v)
            var estimates_arr = [];
            @foreach ($v as $item)
                estimates_arr[estimates_arr.length] = {
                payment_system_id: '{{ $item['ad_id'] }}',
                payment_system_name: '{{ $item['bank_name'] }}',
                payment_system_type: '{{ $item['payment_system_type'] }}',
                fiat_amount: '{{ price_format($item['gps_temp_amount']) }} {{$item['currency']}}'
            };
            @endforeach
                FiatsArray['{{ $k }}'] = estimates_arr;
            @endforeach
        </script>
    @endif
@endsection
