@extends('layouts.api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))

@section('css-whitelabel')
    <style>
        @include('payment.invoice.v2.post.style')
    </style>
@endsection

@section('content')

    <main class="m-auto main w-100 contentIn">
        <div class="content d-flex align-center align-items-center flex-column justify-content-between">
            <form class="contentIn w-100 d-flex flex-column align-items-start">
            <a href="#" class="logo m-auto d-flex flex-column">
                <img src="{{ $whiteLabelModel->getImageLogo($merchant->viewVersion) }}" alt="" class="m-auto">
            </a>

            <div class="request w-100">
                @if(!empty($isCompleted))
                    <p class="text-center request_desc request__title_modif">{{__('The payment is complete')}}</p>
                @elseif(!empty($isCanceled))
                    <p class="text-center request_desc request__title_modif">{{__('The payment is cancelled')}}</p>
                @else
                    <p class="text-center request_desc request__title_modif">{{__('Payment in progress ...')}}</p>
                @endif
            </div>

            @if($invoice->merchant_return_url)
                <div class="text-center w-100">
                    <a href="{{$invoice->merchant_return_url}}" class="btn m-auto btnCancelBtn">{{__('Return to site')}}</a>
                </div>
            @endif

        </form>
        </div>
        <footer class="footer">
            <ul class="languages d-flex justify-content-between m-auto pt-5">
                @foreach (config()->get('app.locales') as $locale)
                    <li>
                        <a href="/setlocale/{{ $locale }}" class="{{ $locale == app()->getLocale() ? " languages__active" : "" }}">{{ $locale }}</a>
                    </li>
                @endforeach
            </ul>

            <div class="copyright text-center m-auto pt-3 pb-3">Grow Payments System 2020-{{date('Y')}}</div>
        </footer>
    </main>

@endsection

@if(empty($isCanceled))
    @section('script-head')
        <script>
            setTimeout(function(){
                window.location.reload(true);
            }, 5000);

        </script>
    @endsection
@endif
