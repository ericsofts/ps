@extends('layouts.api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))

@section('css-whitelabel')
<style>
    @include('payment.invoice.v2.post.style')
</style>
@endsection

@section('content')
    <main class="m-auto main w-100">
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
            </div>
            @if(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @elseif(!empty($error))
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
            </div>
            @if(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @elseif($error = session()->pull('error'))
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
            </div>
            @if(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @else
            @if(!empty($warning))
                <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                    {{ $warning }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
        <div class="content d-flex align-center align-items-center flex-column">
            <form method="POST"
                  class="contentIn w-100 d-flex flex-column align-items-start"
                  action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.confirm', $invoice->id) }}">
                @csrf
                <input type="hidden" name="id" value="{{ $invoice->id }}">

                <a href="#" class="logo m-auto d-flex flex-column">
                    <img src="{{ $whiteLabelModel->getImageLogo($merchant->viewVersion) }}" alt="" class="m-auto">
                    @if(!$merchant->isDisableShowName())
                    <span class="mt-2">{{$merchant->name}}</span>
                    @endif
                </a>

                <div class="numberBlock w-100 mt-5">
                    <div class="numberBlockOrder d-flex justify-content-between align-items-center">
                        <span class="numberBlockOrder__name">{{__('Internal order #')}}</span>
                        <span class="numberBlockOrder__card">{{$invoice->invoice_number}}</span>
                    </div>

                    <div class="numberBlockOrderTop d-flex justify-content-between">
                        @if(!empty($invoice->merchant_order_id))
                            <span class="numberBlockOrderTop__num">{{__('Order #')}}</span>
                            <span>{{$invoice->merchant_order_id}}</span>
                        @endif
                    </div>


                        @if($invoice->input_currency == 'USD')
                        <div class="numberBlockOrder d-flex justify-content-between mt-2">
                            <span class="numberBlockOrder__name">{{__('Amount')}}</span>
                            <span class="numberBlockOrder__total">{{$fmt->formatCurrency(price_format($invoice->amount), "USD")}}</span>
                             </div>
                        @else
                            @if(!$invoice->currency2currency)
                            <div class="numberBlockOrder d-flex justify-content-between mt-2">
                                <span class="numberBlockOrder__name">{{__('Amount')}}</span>
                                <span class="numberBlockOrder__total">{{$fmt->formatCurrency(price_format($invoice->amount), "USD")}}
                                    / {{$fmt->formatCurrency(price_format($invoice->input_amount_value), $invoice->input_currency)}}</span>
                                </div>
                                <div class="numberBlockOrder d-flex justify-content-between align-items-center mt-3">
                                    <span class="numberBlockOrder__name">{{__('Rate')}}</span>
                                    <span class="numberBlockOrder__card">{{$invoice->input_rate}}</span>
                                </div>
                            @else
                            <div class="numberBlockOrder d-flex justify-content-between mt-2">
                                <span class="numberBlockOrder__name">{{__('Amount')}}</span>
                                 <span class="numberBlockOrder__total">{{$fmt->formatCurrency(price_format($invoice->input_amount_value), $invoice->input_currency)}}</span>
                                 </div>
                            @endif
                        @endif
        @if($invoice->grow_token_amount)
            <div class="numberBlockOrder d-flex justify-content-between align-items-center mt-3">
                <span class="numberBlockOrder__name">{{__('GROW Tokens Purchase')}}</span>
                <span class="numberBlockOrder__card">{{$invoice->grow_token_amount}}</span>
            </div>
        @endif

                </div>
                @if(!empty($invoice->merchant_order_desc))
                    <p class="desc mt-3">
                    {{$invoice->merchant_order_desc}}</p>
                @endif

                    @if ($errors->any())
                        <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                            {{ __('Whoops! Something went wrong.') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif


                <div class="selects mt-3 w-100">
                    <div class="selects__name">{{__('Select currency')}}</div>

                    <select class="form-select @error('currency') is-invalid @enderror" id="currency" name="currency"
                            required>
                        @foreach($currencies as $currency)
                            <option value="{{$currency}}"
                                    @if($defaultFiat == $currency) selected @endif
                            >
                                {{$currency}}
                            </option>
                        @endforeach
                    </select>
                    @error('currency')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>

                <div class="selects mt-4 w-100 mb-4">
                    <div class="selects__name">{{__('Select payment system')}}</div>
                    <div class="desc">{{__('Please choose from the list only the bank whose card you have and from which you plan to pay.')}}</div>
                    <select class="form-select w-100 mt-2 @error('payment_system_id') is-invalid @enderror" id="payment_system_id"
                            name="payment_system_id" required>
                        @foreach($sell_coins[$currencies[$defaultFiatKey]] as $item)
                            <option value="{{$item['ad_id']}}" data-name="{{$item['bank_name']}}">{{$item['bank_name']}}
                                - {{price_format($item['gps_temp_amount'])}} {{$item['currency']}}</option>
                        @endforeach
                    </select>
                    @error('payment_system_id')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-dark m-auto btnSend"
                        name="action"
                        value="1"
                >{{__('Confirm')}}</button>

                <a href="{{\Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.cancel', $invoice->id)}}" class="btnCancel m-auto pt-3 text-decoration-none">{{__('Cancel')}}</a>

                <p class="desc pt-2">

                    {{ $whiteLabelModel->getPageText('footer_text')}}

                <br/>
                    {{__('It is strictly forbidden to write in-app messages when sending money.')}}

                    <br/>
                    {{__('Payment must be made exactly for the amount indicated, including all decimal places.')}}
                    <br/>
                    {{__('Funds must be sent strictly by intra-bank transfer, the sending bank and the receiving bank must be the same. Otherwise, payment will not be guaranteed.')}}

                </p>
            </form>

        </div>
                <footer class="footer">
                    <ul class="languages d-flex justify-content-between m-auto pt-5">
                        @foreach (config()->get('app.locales') as $locale)
                            <li>
                                <a href="/setlocale/{{ $locale }}" class="{{ $locale == app()->getLocale() ? " languages__active" : "" }}">{{ $locale }}</a>
                            </li>

                        @endforeach
                    </ul>

                    <div class="copyright text-center m-auto pt-3 pb-3">Grow Payments System 2020–{{date('Y')}}</div>
                </footer>
        @endif

    </main>
@endsection

@section('script-head')
    @if (isset($sell_coins))
        <script>
            var FiatsArray = [];
            @foreach ($sell_coins as $k => $v)
            var estimates_arr = [];
            @foreach ($v as $item)
                estimates_arr[estimates_arr.length] = {
                payment_system_id: '{{ $item['ad_id'] }}',
                payment_system_name: '{{ $item['bank_name'] }}',
                fiat_amount: '{{ price_format($item['gps_temp_amount']) }} {{$item['currency']}}'
            };
            @endforeach
                FiatsArray['{{ $k }}'] = estimates_arr;
            @endforeach
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })

                $(document).on("click", ".btnSend", function (e) {
                    e.preventDefault();
                    $(this).prop('disabled', true);
                    var form = $(this).closest('form');
                    if($('#currency').val() === 'INR'){
                        form.submit();
                        return;
                    }
                    var html = "{{__('I confirm that the transfer will be made from the bank card bank_name, I will not write any comments to the recipient')}}";
                    var bank_name = $('#payment_system_id :selected').data('name').replace(/\*$/g, '');
                    html = html.replace(/bank_name/g, bank_name);
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Confirm')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form.submit();
                        }else{
                            $(this).prop('disabled', false);
                        }
                    })
                    return false;
                });
            });
        </script>
    @endif
@endsection
