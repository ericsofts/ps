@extends('layouts.api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))

@section('css-whitelabel')
    <style>
        @include('payment.invoice.v2.post.style')
    </style>
@endsection
@section('script-head')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
@section('content')

    @if(!$deal_accepted)
        <main class="m-auto main w-100 form-loading contentIn" style="padding-top: 20px;">
            <div class="content d-flex align-center align-items-center flex-column justify-content-between ">
                <a href="#" class="logo m-auto d-flex flex-column">
                    <img src="{{ $whiteLabelModel->getImageLogo($merchant->viewVersion) }}" alt="" class="m-auto">
                </a>


                <div class="request w-100">
                    <div class="request__title text-center  mb-4">{{__('Request processing ...')}}</div>
                    <img src="/images/logoGif.gif" alt="" class="d-block m-auto logo-gif">

                    <p class="text-center request_desc">{{__('Please wait for the payment details to appear. If you click the "Cancel" button, you will have to generate the payment again.')}}</p>
                </div>

                <form method="POST"
                      action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.status') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $id }}">
                    @if ($errors->any())
                        <div class="alert alert-grow alert-dismissible fade show" role="alert">
                            {{ __('Whoops! Something went wrong.') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    @if($status = session('status'))
                        <div class="alert alert-grow alert-dismissible fade show" role="alert">
                            {{ $status }}
                            <button type="button" class="btn m-auto btnCancelBtn" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                        </div>
                    @endif

                    <button type="submit" class="btn m-auto btnCancelBtn" name="action" value="0">
                        {{__('Cancel')}}
                    </button>

                </form>


            </div>
            <footer class="footer">
                <ul class="languages d-flex justify-content-between m-auto pt-5">
                    @foreach (config()->get('app.locales') as $locale)
                        <li>
                            <a href="/setlocale/{{ $locale }}"
                               class="{{ $locale == app()->getLocale() ? " languages__active" : "" }}">{{ $locale }}</a>
                        </li>

                    @endforeach
                </ul>

                <div class="copyright text-center m-auto pt-3 pb-3">Grow Payments System 2020–{{date('Y')}}</div>
            </footer>
        </main>

    @endif
    <form class="form-confirm" method="POST"
          action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.status') }}"
          enctype="multipart/form-data"
          style="@if(!$deal_accepted) display:none; @endif">
        @csrf
        <input type="hidden" name="id" value="{{ $id }}">
        <input type="hidden" name="action" value="0">

        <main class="m-auto main w-100" style="padding-top: 20px;">
            @if ($errors->any())
                <div class="alert alert-grow alert-dismissible fade show" role="alert">
                    {{ __('Whoops! Something went wrong.') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if($status = session('status'))
                <div class="alert alert-grow alert-dismissible fade show" role="alert">
                    {{ $status }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="content d-flex align-center align-items-center flex-column">
                <a href="#" class="logo m-auto d-flex flex-column">
                    <img src="{{ $whiteLabelModel->getImageLogo($merchant->viewVersion) }}" alt="" class="m-auto">
                </a>

                <div class="title text-center w-100">{{__('Request accepted.')}}</div>

                <p class="description text-center mt-3"> {{__('Please send the specified amount to this card number. The amount must be exactly what is shown on the screen.')}}</p>

                <div class="visaCard m-auto w-100">
                    <div class="visaCardTop d-flex justify-content-between">
                        <div class=" visaCardTop__name text-uppercase">{{$bank_name}}</div>
                        <img src="/images/card.svg" alt="">
                    </div>

                    <div class="visaCardBlock d-flex justify-content-between mt-2">
                        <div class="visaCardBlockTotal w-100">
                            <div class="visaCardBlockTotal__name">{{__('Amount')}}</div>
                            <div class="visaCardBlockTotal__transfer">{{$amount}} {{$currency}}</div>
                        </div>
                    </div>

                    @if($payment_system_type != \App\Models\PaymentSystemType::CRYPTO)
                    <div class="visaCardBlock d-flex justify-content-between mt-2">
                        <div class="visaCardBlockTotal w-100">
                            @if($clean_card_number)
                                <div class="visaCardBlockTotal__name">{{__('Card number')}}</div>
                                <div
                                    class="visaCardBlockTotal__transfer">{!! implode('<span>&nbsp;</span>', str_split($clean_card_number, 4)) !!}</div>
                            @else
                                <div class="visaCardBlockTotal__name">{{__('Payment Info')}}</div>
                                <div class="visaCardBlockTotal__transfer">{{$account_info}}</div>
                            @endif

                        </div>
                    </div>
                    @else
                        <div class="visaCardBlock d-flex justify-content-between mt-2">
                            <div class="w-100">
                                <div class="visaCardBlockTotal__name">{{__('Payment Info')}}</div>
                                <div class="d-flex justify-content-center">
                                    <img class="img-fluid" src="{{\Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.qr', $id) }}">
                                </div>
                                <div class="d-flex justify-content-center"><span>{{$address}}</span></div>
                            </div>

                        </div>
                        <div class="visaCardBlock d-flex justify-content-between mt-2">
                            <div class="w-100">
                                <div class="visaCardBlockTotal__name">{{__('Amount received')}}:<span class="amount-recived">0</span> {{$currency}}</div>
                                <div class="visaCardBlockTotal__name">{{__('Amount to pay')}}:<span class="amount-to-pay">{{$amount}}</span> {{$currency}}</div>
                            </div>
                        </div>
                    @endif
                    <p class="warning mb-0">{{__('It is strictly forbidden to write in-app messages when sending money.')}}</p>
                </div>

                <div class="time m-auto pb-3 pt-4">
                    <div class="time__title text-center">{{__('You can pay within:')}}</div>
                    <div class="time__seconds"><span id="time"></span></div>
                </div>
                @if($payment_system_type != \App\Models\PaymentSystemType::CRYPTO)
                <button class="btn btn-dark m-auto -btnSend btn-confirm" onclick="">{{__('Payment completed')}}</button>
                @endif
                <button type="submit" style="display: none;" class="btnSendT" name="action"
                        value="1">{{__('Payment completed')}}</button>
                <a href="#" onclick="$('.btnSendT').val('0').click(); return false;"
                   class="btnCancel m-auto pt-3 text-decoration-none"> {{__('Cancel')}}</a>


                <ol class="list mt-3 w-100 m-auto">
                    @if($payment_system_type != \App\Models\PaymentSystemType::CRYPTO)
                    <li>
                        <span>{{__('If you made a payment and did not click the "Payment completed" button, it is not guaranteed to be credited.')}}</span>
                    </li>
                    @endif
                    <li>
                        <span>{{__('If you made a payment and clicked the "Cancel" button, the payment will not be credited.')}}</span>
                    </li>
                        @if($payment_system_type != \App\Models\PaymentSystemType::CRYPTO)
                    <li>
                        <span>{{__('When sending money to the specified details it is strictly forbidden to leave comments in the description of payment')}}</span>
                    </li>
                        @endif
                </ol>
                <template id="popupTemplate">
                    <swal-html>
                        <form class="form-confirm" method="POST"
                              action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.status') }}"
                              enctype="multipart/form-data"
                        >
                            @csrf
                            <input type="hidden" name="id" value="{{ $id }}">
                            <input type="hidden" name="action" value="0">
                            <a href="#" class="popupLogo">
                                <img src="/images/icon3.svg" alt="">
                            </a>

                            <p class="popupDesc">{{__('Please add an image with your payment confirmation. Adding an image will allow us to process your payment much faster.')}}</p>

                            <div class="position-relative">
                                <input class="form-control" type="file" id="document" name="document">
                                @error('document')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>

                            <p class="popupDesc red">{{__('Press the "Payment completed" button only after the actual payment. Otherwise, the company reserves the right to block your account.')}}</p>

                            <button type="submit" class="btn btn-dark m-auto btnSend d-block" name="action"
                                    value="1">{{__('Payment completed')}}</button>

                            <a href="#" onclick="Swal.close(); return false;"
                               class="btnCancel m-auto pt-3 text-decoration-none"> {{__('Cancel')}}</a>
                        </form>
                    </swal-html>
                    <swal-param
                        name="customClass"
                        value='{ "popup": "swalWithButton" }'/>
                </template>


            </div>
                <footer class="footer">
                    <ul class="languages d-flex justify-content-between m-auto pt-5">
                        @foreach (config()->get('app.locales') as $locale)
                            <li>
                                <a href="/setlocale/{{ $locale }}"
                                   class="{{ $locale == app()->getLocale() ? " languages__active" : "" }}">{{ $locale }}</a>
                            </li>

                        @endforeach
                    </ul>

                    <div class="copyright text-center m-auto pt-3 pb-3">Grow Payments System 2020–{{date('Y')}}</div>
                </footer>
        </main>
    </form>
@endsection

@section('script-bottom')
    <script>

        function showPopup() {
            const {value: file} = Swal.fire({
                template: '#popupTemplate',
                showCloseButton: true,
                customClass: "swalWithButton",
            })
        }

        var deal_accepted = false;
        var form_clicked = false;
        window.onbeforeunload = function (e) {
            if (deal_accepted && !form_clicked) {
                return true;
            }
        };
        window.onload = function () {
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })
                $(document).on("click", ".btn-cancel", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form.form-confirm');
                    var html = "{{__('If you made a payment and clicked the "Cancel" button, the payment will not be credited.')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Cancel payment')}}',
                        cancelButtonText: '{{__('Return to payment')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form_clicked = true;
                            form.find('input[name="action"]').val(0);
                            form.submit();
                        }
                    })
                    return false;
                });
                $(document).on("click", ".btn-confirm", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form.form-confirm');
                    var html = "{{__('Press the "Payment completed" button only after the actual payment. Otherwise, the company reserves the right to block your account.')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Payment completed')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form_clicked = true;
                            form.find('input[name="action"]').val(1);
                            //form.submit();
                            Swal.fire({
                                template: '#popupTemplate',
                                showCloseButton: true,
                            });
                            return false;
                        }
                    })
                    return false;
                });
                @if(!$deal_accepted)
                var interval1 = setInterval(function () {
                    $.ajax({
                        url: '{{route('payment.invoice.post.checkout.status.check')}}',
                        type: 'POST',
                        data: {
                            "invoice_id": {{$id}},
                            "_token": "{{ csrf_token() }}"
                        }
                    }).done(function (data) {
                        if (data.status == -1) {
                            location.reload();
                        } else if (data.status == 1) {
                            $('.form-loading').remove();
                            $('.form-confirm').show();
                            deal_accepted = true;
                            startTimer(data.expired, document.querySelector('#time'), function () {
                                $('.time-text').html('{{__('Payment time is over, if you have not already paid, it is not recommended to do so under this invoice')}}')
                            });
                            clearInterval(interval1)
                        }
                    }).fail(function () {
                        clearInterval(interval1)
                    })
                }, 5000)
                @else
                    deal_accepted = true;
                startTimer({{$expired}}, document.querySelector('#time'), function () {
                    $('.time-text').html('{{__('Payment time is over, if you have not already paid, it is not recommended to do so under this invoice')}}')
                });
                var amount = {{$amount}};
                var interval2 = setInterval(function () {
                    $.ajax({
                        url: '{{route('payment.invoice.post.checkout.status.check')}}',
                        type: 'POST',
                        data: {
                            "invoice_id":{{$id}},
                            "_token": "{{ csrf_token() }}"
                        }
                    }).done(function (data) {
                        if (data.status == -1) {
                            form_clicked = true;
                            location.reload();
                        }else if((data.status == 1) && (data.address_amount)){
                            var toPay = amount-data.address_amount;
                            if(toPay < 0){
                                toPay = 0
                            }
                            $('.amount-recived').html(data.address_amount);
                            $('.amount-to-pay').html(amount-data.address_amount);
                        }else if(data.status == 2){
                            form_clicked = true;
                            window.location.replace("{{\Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.complete', ['payment_invoice_id' => $id]) }}")
                            clearInterval(interval2)
                        }
                    }).fail(function () {
                        clearInterval(interval2)
                    })
                }, 5000)
                @endif
            });
        }
    </script>
@endsection

