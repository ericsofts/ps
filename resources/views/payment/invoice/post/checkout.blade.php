@extends('layouts.api')
@section('body-class', 'd-flex h-100 api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))

@section('css-whitelabel')
    {!! $whiteLabelModel->getCss() !!}
@endsection

@section('content')
    <main class="form-1">
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
            </div>
            @if(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @elseif(!empty($error))
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
            </div>
            @if(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @elseif($error = session()->pull('error'))
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
            </div>
            @if(!empty($invoice->merchant_return_url))
                <div class="text-center">
                    <a href="{{$invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
                </div>
            @endif
        @else
            @if(!empty($warning))
                <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                    {{ $warning }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="text-center">
            <span>
                {{ $whiteLabelModel->getPageText('header_text')}}
            </span>
            </div>
            <div class="text-center">
                <img class="mb-4 img-fluid" src="{{ $whiteLabelModel->getImageLogo() }}">
                @if(!$merchant->isDisableShowName())
                <h2 class="h3 mb-3 fw-normal">{{$merchant->name}}</h2>
                @endif
            </div>
            <div class="mb-3 mt-4 d-flex justify-content-center">
                <dl class="row">
                    <dt class="col-sm-6">{{__('Internal order #')}}:</dt>
                    <dd class="col-sm-6">{{$invoice->invoice_number}}</dd>
                    @if(!empty($invoice->merchant_order_id))
                        <dt class="col-sm-6">{{__('Order #')}}:</dt>
                        <dd class="col-sm-6">{{$invoice->merchant_order_id}}</dd>
                    @endif
                    @if(!empty($invoice->merchant_order_desc))
                        <dt class="col-sm-6"></dt>
                        <dd class="col-sm-6">{{$invoice->merchant_order_desc}}</dd>
                    @endif
                    <dt class="col-sm-6">{{__('Amount')}}:</dt>
                    @if($invoice->input_currency == 'USD')
                        <dd class="col-sm-6">{{$fmt->formatCurrency(price_format($invoice->amount), "USD")}}</dd>
                    @else
                        @if(!$invoice->currency2currency)
                        <dd class="col-sm-6">{{$fmt->formatCurrency(price_format($invoice->amount), "USD")}}
                            /{{$fmt->formatCurrency(price_format($invoice->input_amount_value), $invoice->input_currency)}}</dd>
                        <dt class="col-sm-6">{{__('Rate')}}:</dt>
                        <dd class="col-sm-6">{{$invoice->input_rate}}</dd>
                        @else
                            <dd class="col-sm-6">{{$fmt->formatCurrency(price_format($invoice->input_amount_value), $invoice->input_currency)}}</dd>
                        @endif
                    @endif
                    @if($invoice->grow_token_amount)
                        <dt class="col-sm-6">{{__('GROW Tokens Purchase')}}:</dt>
                        <dd class="col-sm-6">{{$invoice->grow_token_amount}}</dd>
                    @endif
                </dl>
            </div>
            <form method="POST"
                  action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.confirm', $invoice->id) }}">
                @csrf
                <input type="hidden" name="id" value="{{ $invoice->id }}">
                @if ($errors->any())
                    <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                        {{ __('Whoops! Something went wrong.') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="mb-3">
                    <label for="currency" class="form-label">{{__('Select currency')}}</label>
                    <select class="form-select @error('currency') is-invalid @enderror" id="currency" name="currency"
                            required>
                        @foreach($currencies as $currency)
                            <option value="{{$currency}}"
                                    @if($defaultFiat == $currency) selected @endif
                            >
                                {{$currency}}
                            </option>
                        @endforeach
                    </select>
                    @error('currency')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="payment_system_id" class="form-label">{{__('Select payment system')}}</label>
                    <select class="form-select @error('payment_system_id') is-invalid @enderror" id="payment_system_id"
                            name="payment_system_id" required>
                        @foreach($sell_coins[$currencies[$defaultFiatKey]] as $item)
                            <option value="{{$item['ad_id']}}"
                                    data-name="{{$item['bank_name']}}"
                                    data-type="{{$item['payment_system_type']}}"
                                    data-fiat_amount="{{price_format($item['gps_temp_amount'])}} {{$item['currency']}}"
                            >{{$item['bank_name']}}
                                - {{price_format($item['gps_temp_amount'])}} {{$item['currency']}}</option>
                        @endforeach
                    </select>
                    @error('payment_system_id')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                    @if(!empty($defaultAd) && $defaultAd['payment_system_type'] == \App\Models\PaymentSystemType::IBAN)
                        @php $label_message = __('Please choose from the list only the bank where you plan to make the payment.') @endphp
                    @elseif(!empty($defaultAd) && $defaultAd['payment_system_type'] == \App\Models\PaymentSystemType::CRYPTO)
                        @php $label_message = '' @endphp
                    @else
                        @php $label_message = __('Please choose from the list only the bank whose card you have and from which you plan to pay.') @endphp
                    @endif
                    <div class="form-text secondary-whitelable-text payment_system_id_text">
                        {{$label_message}}
                    </div>
                </div>
                <div class="mb-3 mt-3 d-flex justify-content-center">
                    <button type="submit" class="btn btn-secondary mx-2 btn-confirm btn-primary-whitelabel"
                            name="action"
                            value="1"
                    >
                        {{__('Confirm')}}
                    </button>
                    <a href="{{\Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.cancel', $invoice->id)}}"
                       class="btn btn-cancel btn-link">{{__('Cancel')}}</a>
                </div>
            </form>
            <div class="text-center">
                <p>
                    {{ $whiteLabelModel->getPageText('footer_text')}}
                </p>
                <p class="fw-bold mb-0 mt-2 danger-whitelable-text text-block-1" @if(!empty($defaultAd) && $defaultAd['payment_system_type'] == \App\Models\PaymentSystemType::CRYPTO) style="display: none;" @endif>
                    {{__('It is strictly forbidden to write in-app messages when sending money.')}}
                </p>
                <p class="fw-bold mb-0 mt-2 danger-whitelable-text text-block-2">
                    {{__('Payment must be made exactly for the amount indicated, including all decimal places.')}}
                </p>
                <p class="fw-bold mb-0 mt-2 danger-whitelable-text text-block-3" @if(!empty($defaultAd) && $defaultAd['payment_system_type'] == \App\Models\PaymentSystemType::CRYPTO) style="display: none;" @endif>
                    {{__('Funds must be sent strictly by intra-bank transfer, the sending bank and the receiving bank must be the same. Otherwise, payment will not be guaranteed.')}}
                </p>
            </div>
        @endif

        <p class="mt-5 mb-3 text-muted text-center">2020–{{date('Y')}}</p>
    </main>
@endsection

@section('script-head')
    @if (isset($sell_coins))
        <script>
            var message_labels = {
                'bc' :'{{__('Please choose from the list only the bank whose card you have and from which you plan to pay.')}}',
                'ib' :'{{__('Please choose from the list only the bank where you plan to make the payment.')}}',
                'cr' :''
            }
            $(function () {
                $(document).on('change', '#payment_system_id', function (event) {
                    var type = $(this).find(':selected').data('type');
                    if(type === 'iban') {
                        $('.payment_system_id_text').text(message_labels.ib);
                        $('.text-block-1, .text-block-3').show();
                    }else if(type === 'crypto'){
                        $('.payment_system_id_text').text(message_labels.cr);
                        $('.text-block-1, .text-block-3').hide();
                    }else{
                        $('.payment_system_id_text').text(message_labels.bc);
                        $('.text-block-1, .text-block-3').show();
                    }
                })
            });

            var FiatsArray = [];
            @foreach ($sell_coins as $k => $v)
            var estimates_arr = [];
            @foreach ($v as $item)
                estimates_arr[estimates_arr.length] = {
                payment_system_id: '{{ $item['ad_id'] }}',
                payment_system_name: '{{ $item['bank_name'] }}',
                payment_system_type: '{{ $item['payment_system_type'] }}',
                fiat_amount: '{{ price_format($item['gps_temp_amount']) }} {{$item['currency']}}'
            };
            @endforeach
                FiatsArray['{{ $k }}'] = estimates_arr;
            @endforeach
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })

                $(document).on("click", ".btn-confirm", function (e) {
                    e.preventDefault();
                    $(this).prop('disabled', true);
                    var form = $(this).closest('form');
                    var ps_type = $('#payment_system_id').find(':selected').data('type')
                    if(['upi', 'iban', 'qiwi'].indexOf(ps_type) != -1){
                        form.submit();
                        return;
                    }

                    if(ps_type == 'crypto'){
                        var html = "{{__('I confirm that I will send fiat_amount to the address indicated in the invoice. I accept that sending to another address or another cryptocurrency may result in loss of my funds.')}}";
                        var fiat_amount = $('#payment_system_id :selected').data('fiat_amount').replace(/\*$/g, '');
                        html = html.replace(/fiat_amount/g, fiat_amount);
                    }else{
                        var html = "{{__('I confirm that the transfer will be made from the bank card bank_name, I will not write any comments to the recipient')}}";
                        var bank_name = $('#payment_system_id :selected').data('name').replace(/\*$/g, '');
                        html = html.replace(/bank_name/g, bank_name);
                    }

                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Confirm')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form.submit();
                        }else{
                            $(this).prop('disabled', false);
                        }
                    })
                    return false;
                });
            });
        </script>
    @endif
@endsection
