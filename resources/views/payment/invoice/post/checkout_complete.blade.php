@extends('layouts.api')
@section('body-class', 'd-flex h-100 api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))

@section('css-whitelabel')
    {!! $whiteLabelModel->getCss() !!}
@endsection

@section('content')
    <main class="form-1">
        <div class="text-center">
            <img class="mb-4 img-fluid" src="{{ $whiteLabelModel->getImageLogo() }}">
            @if(!empty($isCompleted))
                <h1 class="h3 mb-3 fw-normal">{{__('The payment is complete')}}</h1>
            @elseif(!empty($isCanceled))
                <h1 class="h3 mb-3 fw-normal">{{__('The payment is cancelled')}}</h1>
            @else
                <h1 class="h3 mb-3 fw-normal">{{__('Payment in progress ...')}}</h1>
            @endif
        </div>
        @if($invoice->merchant_return_url)
            <div class="text-center">
                <a href="{{(is_null(parse_url($invoice->merchant_return_url, PHP_URL_HOST)) ? '//' : '') . $invoice->merchant_return_url}}" class="btn btn-link">{{__('Return to site')}}</a>
            </div>
        @endif
    </main>
@endsection

@if(empty($isCanceled) && empty($isCompleted))
    @section('script-head')
        <script>
            setTimeout(function(){
                window.location.reload(true);
            }, 5000);

        </script>
    @endsection
@endif
