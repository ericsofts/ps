@extends('layouts.api')
@section('body-class', 'd-flex h-100 api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant->id))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))

@section('css-whitelabel')
    {!! $whiteLabelModel->getCss() !!}
@endsection
@section('content')

    @if(!$deal_accepted)
        <div class="form-loading">

            <p class="text-center">{{__('Request processing ...')}}</p>
            <p class="text-center">{{__('Please wait for the payment details to appear. If you click the "Cancel" button, you will have to generate the payment again.')}}</p>
            <form class="form-confirm1" method="POST" action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.status') }}">
                @csrf
                <input type="hidden" name="id" value="{{ $id }}">
                @if ($errors->any())
                    <div class="alert alert-grow alert-dismissible fade show" role="alert">
                        {{ __('Whoops! Something went wrong.') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if($status = session('status'))
                    <div class="alert alert-grow alert-dismissible fade show" role="alert">
                        {{ $status }}
                        <button type="button" class="btn-close btn-secondary mx-2" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="mb-3 d-flex justify-content-center">
                    <button type="submit" class="btn btn-secondary mx-2 btn-secondary-whitelabel" name="action" value="0">
                        {{__('Cancel')}}
                    </button>
                </div>
            </form>
        </div>
    @endif
    <form class="form-confirm" method="POST" action="{{ \Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.status') }}" enctype="multipart/form-data"
          style="@if(!$deal_accepted) display:none; @endif">
        @csrf
        <input type="hidden" name="id" value="{{ $id }}">
        <input type="hidden" name="action" value="0">
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif

        <p class="text-center fw-bold mb-2 time-text">{{__('You can pay within:')}} <span id="time"></span></p>
        <div class="mb-3 mt-4 d-flex justify-content-center">
            <dl class="row mx-3">
                <dt class="col-sm-4">{{__('Amount')}}:</dt>
                <dd class="col-sm-8">{{$amount}} {{$currency}}</dd>
                @if($payment_system_type != \App\Models\PaymentSystemType::CRYPTO)
                    @if(!in_array($payment_system_type, [\App\Models\PaymentSystemType::UPI, \App\Models\PaymentSystemType::QIWI]))
                        <dt class="col-sm-4">{{__('Bank')}}:</dt>
                        <dd class="col-sm-8">{{$bank_name}}</dd>
                    @endif
                    @if($payment_system_type == \App\Models\PaymentSystemType::IBAN)
                        <dt class="col-sm-4">{{__('IBAN')}}:</dt>
                        <dd class="col-sm-8 account_info">{!! implode('<span>&nbsp;</span>', str_split($account_info, 4)) !!}</dd>
                    @elseif($payment_system_type == \App\Models\PaymentSystemType::UPI)
                        <dt class="col-sm-4">{{__('UPI')}}:</dt>
                        <dd class="col-sm-8 account_info">{{$account_info}}</dd>
                    @elseif($payment_system_type == \App\Models\PaymentSystemType::QIWI)
                        <dt class="col-sm-4">{{__('By wallet number / By nickname')}}:</dt>
                        <dd class="col-sm-8 account_info">{{$account_info}}</dd>
                    @else
                        <dt class="col-sm-4">{{__('Card number')}}:</dt>
                        <dd class="col-sm-8 account_info">{!! implode('<span>&nbsp;</span>', str_split($account_info, 4)) !!}</dd>
                    @endif
                @else
                    <dt class="col-sm-4">{{__('Amount received')}}:</dt>
                    <dd class="col-sm-8"><span class="amount-recived">0</span> {{$currency}}</dd>
                    <dt class="col-sm-4">{{__('Amount to pay')}}:</dt>
                    <dd class="col-sm-8"><span class="amount-to-pay">{{$amount}}</span> {{$currency}}</dd>
                @endif
            </dl>
        </div>
        @if($payment_system_type == \App\Models\PaymentSystemType::CRYPTO)
            <div class="mb-3 mt-4 d-flex justify-content-center">
                <img class="img-fluid" src="{{\Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.qr', $id) }}">
            </div>
            <div class="mb-3 mt-4 d-flex justify-content-center">
                <span class="img-fluid">{{$address}}</span>
            </div>
        @endif
        <p class="text-center mb-0">
            @if($payment_system_type == \App\Models\PaymentSystemType::CARD_NUMBER)
                {{__('Please send the specified amount to this card number. The amount must be exactly what is shown on the screen.')}}
            @else
                {{__('Send the specified amount to these details. The amount should be exactly the amount shown on the screen.')}}
            @endif
        </p>
        @if($payment_system_type != \App\Models\PaymentSystemType::CRYPTO)
            <p class="text-center">{{__('Please add an image with your payment confirmation. Adding an image will allow us to process your payment much faster.')}}</p>
        <div class="mb-3 min-w-320 m-auto">
            <input class="form-control @error('document') is-invalid @enderror"
                   type="file"
                   id="document"
                   name="document">
            @error('document')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        @endif
        <div class="mb-3 mb-t d-flex justify-content-center">
            @if($payment_system_type != \App\Models\PaymentSystemType::CRYPTO)
            <button type="submit" class="btn btn-secondary mx-2 btn-confirm btn-primary-whitelabel" name="action" value="1">
                {{__('Payment completed')}}
            </button>
            @endif
            <button type="submit" class="btn btn-secondary mx-2 btn-cancel btn-secondary-whitelabel" name="action" value="0">
                {{__('Cancel')}}
            </button>
        </div>
        <div class="text-center">
            @if($payment_system_type != \App\Models\PaymentSystemType::CRYPTO)
            <p class="fw-bold mb-0 danger-whitelable-text">{{__('If you made a payment and did not click the "Payment completed" button, it is not guaranteed to be credited.')}}</p>
            @endif
            <p class="fw-bold danger-whitelable-text">{{__('If you made a payment and clicked the "Cancel" button, the payment will not be credited.')}}</p>
            @if($payment_system_type != \App\Models\PaymentSystemType::CRYPTO)
            <p class="fw-bold mb-0 mt-2 danger-whitelable-text">
                {{__('When sending money to the specified details it is strictly forbidden to leave comments in the description of payment')}} 🚫
            </p>
            @endif
        </div>
    </form>
@endsection

@section('script-bottom')
    <script>
        var deal_accepted = false;
        var form_clicked = false;
        window.onbeforeunload = function (e) {
            if(deal_accepted && !form_clicked){
                return true;
            }
        };
        window.onload = function () {
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })
                $(document).on("click", ".btn-cancel", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form.form-confirm');
                    var html = "{{__('If you made a payment and clicked the "Cancel" button, the payment will not be credited.')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Cancel payment')}}',
                        cancelButtonText: '{{__('Return to payment')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form_clicked = true;
                            form.find('input[name="action"]').val(0);
                            form.submit();
                        }
                    })
                    return false;
                });
                $(document).on("click", ".btn-confirm", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form.form-confirm');
                    var html = "{{__('Press the "Payment completed" button only after the actual payment. Otherwise, the company reserves the right to block your account.')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Payment completed')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form_clicked = true;
                            form.find('input[name="action"]').val(1);
                            form.submit();
                        }
                    })
                    return false;
                });
                @if(!$deal_accepted)
                var interval1 = setInterval(function () {
                    $.ajax({
                        url: '{{route('payment.invoice.post.checkout.status.check')}}',
                        type: 'POST',
                        data: {
                            "invoice_id":{{$id}},
                            "_token": "{{ csrf_token() }}"
                        }
                    }).done(function (data) {
                        if (data.status == -1) {
                            location.reload();
                        }else if (data.status == 1) {
                            // location.reload();
                            if(data.account_info){
                                $('.account_info').html(data.account_info)
                            }
                            $('.form-loading').remove();
                            $('.form-confirm').show();
                            deal_accepted = true;
                            startTimer(data.expired, document.querySelector('#time'), function(){
                                $('.time-text').html('{{__('Payment time is over, if you have not already paid, it is not recommended to do so under this invoice')}}')
                            });
                            clearInterval(interval1)
                        }
                    }).fail(function () {
                        clearInterval(interval1)
                    })
                }, 5000)
                @else
                    deal_accepted = true;
                    startTimer({{$expired}}, document.querySelector('#time'), function(){
                        $('.time-text').html('{{__('Payment time is over, if you have not already paid, it is not recommended to do so under this invoice')}}')
                    });
                    var amount = {{$amount}};
                    var interval2 = setInterval(function () {
                    $.ajax({
                        url: '{{route('payment.invoice.post.checkout.status.check')}}',
                        type: 'POST',
                        data: {
                            "invoice_id":{{$id}},
                            "_token": "{{ csrf_token() }}"
                        }
                    }).done(function (data) {
                        if (data.status == -1) {
                            form_clicked = true;
                            location.reload();
                        }else if((data.status == 1) && (data.address_amount)){
                            var toPay = amount-data.address_amount;
                            if(toPay < 0){
                                toPay = 0
                            }
                            $('.amount-recived').html(data.address_amount);
                            $('.amount-to-pay').html(toPay);
                        }else if(data.status == 2){
                            form_clicked = true;
                            window.location.replace("{{\Illuminate\Support\Facades\URL::signedRoute('payment.invoice.post.checkout.complete', ['payment_invoice_id' => $id]) }}")
                            clearInterval(interval2)
                        }
                    }).fail(function () {
                        clearInterval(interval2)
                    })
                }, 5000)
                @endif
            });
        }
    </script>
@endsection

