@extends('layouts.guest')
@section('body-class', 'd-flex h-100 text-white bg-black')
@section('html-class', 'h-100')

@section('content')
    @if($invoice->merchant_response_url)
        <form name="response" method="post" action="{{$invoice->merchant_response_url}}" style="display: none;">
            @foreach($response as $k => $v)
                <input type="hidden" name="{{$k}}" value="{{$v}}">
            @endforeach
        </form>
    @endif
    <main class="form-1">
        <div class="text-center">
            <img class="mb-4" src="{{ asset('images/logo.png') }}">
            <h1 class="h3 mb-3 fw-normal">{{__('The payment is complete')}}</h1>
        </div>
    </main>
@endsection

@section('script-head')
    <script>
        window.onload = function () {
            document.forms["response"].submit();
        }
    </script>
@endsection

