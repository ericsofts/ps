@extends('layouts.guest')
@section('body-class', 'd-flex h-100 text-white bg-black')
@section('html-class', 'h-100')

@section('content')
    <main class="form-1">
        @if(!empty($error))
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @elseif($error = session()->pull('error'))
            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ $error }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @else
            <div class="text-center">
                <img class="mb-4" src="{{ asset('images/logo.png') }}">
                @if(!$merchant->isDisableShowName())
                <h2 class="h3 mb-3 fw-normal">{{$merchant->name}}</h2>
                @endif
            </div>
            <div class="mb-3 mt-4 d-flex justify-content-center">
                <dl class="row">
                    <dt class="col-sm-5">{{__('Internal order #')}}:</dt>
                    <dd class="col-sm-7">{{$invoice->invoice_number}}</dd>
                    @if(!empty($invoice->merchant_order_id))
                        <dt class="col-sm-5">{{__('Order #')}}:</dt>
                        <dd class="col-sm-7">{{$invoice->merchant_order_id}}</dd>
                    @endif
                    @if(!empty($invoice->merchant_order_desc))
                        <dt class="col-sm-5"></dt>
                        <dd class="col-sm-5">{{$invoice->merchant_order_desc}}</dd>
                    @endif
                    <dt class="col-sm-5">{{__('Amount')}}:</dt>
                    <dd class="col-sm-7">{{$fmt->formatCurrency(price_format($invoice->amount), "USD")}}</dd>
                </dl>
            </div>
            @if($enoughFunds)
                <form method="POST" action="{{ route('payment.checkout_confirm', $invoice->id) }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $invoice->id }}">
                    @if ($errors->any())
                        <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                            {{ __('Whoops! Something went wrong.') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    <p>{{__('Enter the OTP code from the email:')}}</p>
                    <div class="row g-3 align-items-center">
                        <div class="col-auto">
                            <label for="otp">{{__('OTP')}}</label>
                        </div>
                        <div class="col-auto">
                            <input type="text" class="form-control @error('otp') is-invalid @enderror" id="otp"
                                   name="otp"
                                   placeholder="{{__('OTP')}}"
                                   value="" required autofocus
                            >
                            @error('otp')
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3 mt-3 d-flex justify-content-center">
                        <button type="submit" class="btn border-white bg-white btn-secondary mx-2 btn-confirm"
                                name="action"
                                value="1">
                            {{__('Confirm')}}
                        </button>
    {{--                        <button type="submit" class="btn btn-grow" name="action" value="0">--}}
    {{--                            {{__('Cancel')}}--}}
    {{--                        </button>--}}
                    </div>
                </form>
                <div class="mb-3 text-center">
                    <button type="button" class="btn btn-grow resend-otp">
                        {{__('Resend OTP')}}
                    </button>
                </div>
            @else
                <p class="text-muted">
                    {{__('Not enough funds, your account:')}}
                    {{$fmt->formatCurrency(price_format($balanceTotal), "USD")}}
                </p>
            @endif
        @endif
        <p class="mt-5 mb-3 text-muted text-center">&copy; 2020–{{date('Y')}}</p>
    </main>
@endsection

@section('script-head')
    <script>
        window.onload = function () {
            $(function () {
                $(document).on("click", ".resend-otp", function (e) {
                    e.preventDefault();
                    var that = this;
                    $(that).attr('disabled', true);
                    $.ajax({
                        url: '{{route('otp.resend')}}',
                        type: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "invoice_id": "{{$invoice->id}}",
                            "invoice_type": "1"
                        }
                    }).done(function (data) {
                        if (data.status) {
                            toastr.success(data.message);
                        }else{
                            toastr.error(data.message);
                        }
                        $(that).attr('disabled', false);
                    }).fail(function () {
                        $(that).attr('disabled', false);
                    })
                    return false;
                })
            })
        }
    </script>
@endsection
