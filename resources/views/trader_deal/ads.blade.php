<?php
    use App\Models\TraderAd;
?>

@extends('layouts.app')
@section('content')

<nav aria-label="breadcrumb" class="pt-3">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
        <li class="breadcrumb-item active">{{__('Trader ads')}}</li>
    </ol>
</nav>
<div class="page-header">
    <h1>
        {{ __('Trader ads') }}
    </h1>
</div>

<form method="GET" action="{{ route('trader_deal.ads') }}" class="trader_ads-filter-form row g-3 mb-3">
    <div class="col-md-3">
        <div class="row">
            <label class="col-sm-12 col-form-label">{{__('Type')}}:</label>
            <div class="col-sm-12">
                <select class="form-select" name="type">
                    <option value="{{ TraderAd::TYPE_SELL }}" @if($type == TraderAd::TYPE_SELL) selected @endif>{{__('Deposit')}}</option>
                    <option value="{{ TraderAd::TYPE_BUY }}" @if($type == TraderAd::TYPE_BUY) selected @endif>{{__('Withdraw')}}</option>
                </select>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="row">
            <label class="col-sm-12 col-form-label">{{__('Fiat')}}:</label>
            <div class="col-sm-12">
                <select class="form-select @error('fiat_id') is-invalid @enderror"
                    name="fiat_id" id="fiat_id">
                    <option value="">{{__('Choose')}}</option>
                    @foreach ($all_fiats as $unit)
                        <option data-asset="{{$unit->asset}}" value="{{$unit->id}}" @if($fiat_id === $unit->id) selected @endif>{{$unit->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="row">
            <label class="col-sm-12 col-form-label">{{__('Payment System')}}:</label>
            <div class="col-sm-12">
                <select class="form-select @error('payment_system_id') is-invalid @enderror"
                    name="payment_system_id" id="payment_system_id"
                    value="{{$payment_system_id}}">
                    <option value="">{{__('Choose')}}</option>
                </select>
            </div>
        </div>
    </div>

    <div class="col-md-3 d-flex align-items-end">
        <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
    </div>
</form>

<table class="table">
    <thead>
        <tr>
            <th scope="col">{{__('ID')}}</th>
            <th scope="col">{{__('Currency')}}</th>
            <th scope="col">{{__('Fiat')}}</th>
            <th scope="col">{{__('Payment System')}}</th>
            <th scope="col">{{__('Min Amount')}}</th>
            <th scope="col">{{__('Max amount')}}</th>
            <th scope="col">{{__('Rate')}}</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($trader_ads as $i => $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->currency->name}}</td>
                <td>{{$item->fiat->name}}</td>
                <td>{{$item->payment_system->name}}</td>
                <td>{{round($item->min_amount, 2)}}</td>
                <td>{{round($item->max_amount, 2)}}</td>
                <td>{{round($item->rate, 2)}}</td>
                <td>
                    <form action="{{route('trader_deal.create')}}" method="post" class="input-group">
                        <input type="text" class="form-control @error('amount') is-invalid @enderror"
                            name="amount"
                            min="{{round($item->min_amount, 2)}}" max="{{round($item->max_amount, 2)}}"
                            step="0.01" style="width: 3em;">
                        @error('amount')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror

                        <input type="hidden" name="id" value="{{$item->id}}">
                        @csrf
                        <button class="btn btn-outline-secondary">Create Deal</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<script>
    var fiats = {!! json_encode($all_fiats) !!};
    var currencies = {!! json_encode($all_currencies) !!};
</script>
@endsection
