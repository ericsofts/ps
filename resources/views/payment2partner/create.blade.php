@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Payment to the partner')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Payment to the partner') }}
    </h2>
    <form method="POST" action="{{ route('payment2partner.confirm') }}">
        @csrf
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="mb-3">
            <p>{{__(('Balance'))}}: <b>${{price_format($balance_total)}}</b></p>
        </div>
        <div class="mb-3">
            <label for="amount" class="form-label">{{__('Amount')}}</label>
            <div class="input-group">
                <span class="input-group-text">$</span>
                <input type="number" max="{{$balance_total}}" class="form-control @error('amount') is-invalid @enderror" id="amount" name="amount" placeholder="{{__('Amount')}}"
                       value="{{old('amount')}}" required autofocus
                >
                @error('amount')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="partner" class="form-label">{{__('The partner')}}</label>
            <select class="form-select @error('partner') is-invalid @enderror" id="partner" name="partner" required>
                <option value=""></option>
                @foreach($merchants as $merchant)
                    <option @if(old('partner') == $merchant->id) selected @endif value="{{$merchant->id}}">{{$merchant->name}}</option>
                @endforeach
            </select>
            @error('partner')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        <div class="mb-3 float-end">
            <button type="submit" class="btn btn-grow btn-confirm">
                {{__('Confirm')}}
            </button>
        </div>
    </form>
@endsection

@section('script-bottom')
    <script>
        window.onload = function () {
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })
                $(document).on("click", ".btn-confirm", function (e) {
                    e.preventDefault();
                    var that = this;
                    $(that).attr('disabled', true);
                    var form = $(this).closest('form');
                    var amount = $(form).find('#amount').val();

                    if(amount <= 0){
                        swalWithBootstrapButtons.fire({
                            html: "{{__('The amount field is required.')}}",
                            icon: 'warning',
                            showCancelButton: false,
                            confirmButtonText: '{{__('Ok')}}',
                        }).then((result) => {
                            $(that).attr('disabled', false);
                        })
                        return false;
                    }

                    var html = "{{__('Please confirm the payment of $:amount. Once confirmed, the funds will be debited from your account.', [])}}".replace(':amount', amount)
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Confirm')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form.submit();
                        }
                        $(that).attr('disabled', false);
                    })
                    return false;
                });
            });
        }
    </script>
@endsection
