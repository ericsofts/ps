@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Profile')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Profile') }}
        </h1>
    </div>
    <div class="content-wrapper">
        <div class="p-3">
            <label for="exampleFormControlInput1" class="form-label">{{__('Account ID')}}</label>
            <input class="form-control" type="text" disabled value="{{request()->user()->account_id}}">
        </div>

    </div>
@endsection
