@extends('layouts.app')
@section('content')
    <div class="page-header">
        <h1>
            {{ __('Dashboard') }}
        </h1>
    </div>
    <p>{{__('Welcome')}}, {{request()->user()->name}}</p>

    <div class="row row-cols-1 row-cols-md-3 g-4">
        <div class="col">
            <div class="card text-white bg-dark mb-3 h-100">
                <div class="card-body">
                    <h5 class="card-title">{{__('Balance')}}</h5>
                    <p class="card-text fs-2">${{price_format($balanceTotal)}}</p>
                    @if($balanceTotalGrow)
                        <h5 class="card-title">{{__('Balance')}} GROW</h5>
                        <p class="card-text fs-2">GRT {{price_format($balanceTotalGrow, 8)}}</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card text-white bg-dark mb-3 h-100">
                <div class="card-body">
                    <h5 class="card-title">{{__(('Account ID'))}}</h5>
                    <p class="card-text fs-2">{{request()->user()->account_id}}</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card text-white bg-dark mb-3 h-100">
                <div class="card-body">
                    <h5 class="card-title">&nbsp;</h5>
                    <p class="card-text fs-2">
                        <a class="text-white" href="{{route('input_invoice.create')}}">
                            {{__('Buy token')}}
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    @if($balance_histories)
        <p class="mt-3">{{__('Balance history')}}</p>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Description')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($balance_histories as $i => $item)
                <tr>
                    <th scope="row">{{$i+1}}</th>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{price_format_currency($item->amount, $item->currency, $fmt)}}</td>
                    <td>{{$item->description}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="d-grid gap-2 col-12 mx-auto">
            <a href="{{route('balance.history')}}" class="btn btn-grow">{{__('More')}}</a>
        </div>
    @endif
@endsection
