@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Balance history')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Balance history') }}
        </h1>
    </div>
    @if($balance_histories)
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Description')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($balance_histories as $i => $item)
                <tr>
                    <th scope="row">{{$i+1}}</th>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{price_format_currency($item->amount, $item->currency, $fmt)}}</td>
                    <td>{{$item->description}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $balance_histories->links() }}</div>
    @endif
@endsection
