@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item">{{__('GROW Token')}}</li>
            <li class="breadcrumb-item active">{{__('Exchange GROW Token')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Exchange GROW Token') }}
    </h2>
    <form method="POST" action="{{ route('grow_token.exchange.confirm') }}">
        @csrf
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="mb-3">
            <p>{{__('Balance')}}: <b>${{price_format($balance_total)}}</b></p>
            <p>{{__('Balance')}} GROW: <b>GROW {{price_format($balance_total_grow, 8)}}</b></p>
            <p>{{__('Rate')}}: <b>{{$rate}}</b></p>
        </div>
        <div class="mb-3">
            <label for="amount" class="form-label">{{__('Amount')}}</label>
            <div class="input-group">
                <span class="input-group-text">$</span>
                <input type="number" max="{{$balance_total}}" class="form-control @error('amount') is-invalid @enderror"
                       id="amount" name="amount" placeholder="{{__('Amount')}}"
                       value="{{old('amount')}}" required autofocus
                >
                @error('amount')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mt-2">
                <span class="input-group-text">{{__('GROW')}}</span>
                <input type="number" max="{{$balance_total*$rate}}" class="form-control" id="grow_amount"
                       placeholder="{{__('GROW Amount')}}"
                       value="{{old('amount')*$rate ? old('amount')*$rate :''}}"
                >
            </div>
        </div>
        <div class="mb-3 float-end">
            <button type="submit" class="btn btn-grow btn-confirm">
                {{__('Confirm')}}
            </button>
        </div>
    </form>
@endsection

@section('script-bottom')
    <script>
        var roundAccurately = (number, decimalPlaces) => Number(Math.round(Number(number + "e" + decimalPlaces)) + "e" + decimalPlaces * -1);
        window.onload = function () {
            $(function () {
                var rate = {{$rate}};

                $(document).on("change",'#amount', function(e){
                    $('#grow_amount').val(roundAccurately($(this).val() / rate, 8));
                });

                $(document).on("change",'#grow_amount', function(e){
                    $('#amount').val(roundAccurately($(this).val() * rate, 2));
                });

                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })
                $(document).on("click", ".btn-confirm", function (e) {
                    e.preventDefault();
                    var that = this;
                    $(that).attr('disabled', true);
                    var form = $(this).closest('form');
                    var amount = $(form).find('#amount').val();
                    if(amount <= 0){
                        swalWithBootstrapButtons.fire({
                            html: "{{__('The amount field is required.')}}",
                            icon: 'warning',
                            showCancelButton: false,
                            confirmButtonText: '{{__('Ok')}}',
                        }).then((result) => {
                            $(that).attr('disabled', false);
                        })
                        return false;
                    }
                    var html = "{{__('Please confirm your purchase in the amount of $:amount. Once confirmed, the funds will be debited from your account.', [])}}".replace(':amount', amount)
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Confirm')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form.submit();
                        }
                        $(that).attr('disabled', false);
                    })
                    return false;
                });
            });
        }
    </script>
@endsection
