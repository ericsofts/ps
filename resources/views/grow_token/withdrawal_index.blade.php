@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item">{{__('GROW Token')}}</li>
            <li class="breadcrumb-item active">{{__('Token withdrawal history')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Token withdrawal history') }}
        </h1>
    </div>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        <div class="col">
            <div class="card text-white bg-dark mb-3 h-100">
                <div class="card-body">
                    <h5 class="card-title">{{__('Balance')}} GROW</h5>
                    <p class="card-text fs-2">GRT {{price_format($balance_total_grow, 6)}}</p>
                </div>
            </div>
        </div>
    </div>
    @if($invoices)
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoices as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{price_format_currency($item->amount, 'GRT')}}</td>
                    <td>{{grow_token_invoice_status($item->status) }}</td>
                    <td>
                        @if($item->status == \App\Models\GrowTokenInvoice::STATUS_CREATED)
                            <form method="POST" action="{{ route('grow_token.withdrawal.cancel', $item->id) }}">
                                @csrf
                                <button type="submit" class="btn btn-link mx-2 btn-cancel">
                                    {{__('Cancel')}}
                                </button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $invoices->links() }}</div>
    @endif
@endsection

@section('script-bottom')
    <script>

        window.onload = function () {
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })
                $(document).on("click", ".btn-cancel", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form');
                    swalWithBootstrapButtons.fire({
                        html: "{{__('Are you sure?')}}",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Ok')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form.submit();
                        }
                    })
                    return false;
                });
            })
        }
    </script>
@endsection

