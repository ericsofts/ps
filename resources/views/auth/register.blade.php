@extends('layouts.guest')
@section('body-class', 'd-flex h-100 text-white bg-black')
@section('html-class', 'h-100')

@section('content')
    <main class="form-register">
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="text-center">
                <img class="mb-4" src="{{ asset('images/logo.png') }}">
                <h1 class="h3 mb-3 fw-normal">{{__('Registration')}}</h1>
            </div>
            @if ($errors->any())
                <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                    {{ __('Whoops! Something went wrong.') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="mb-3">
                <label for="name" class="form-label">{{__('Name')}}</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="{{__('Name')}}"
                       value="{{old('name')}}" required autofocus
                >
                @error('name')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="name" class="form-label">{{__('Email')}}</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="{{__('Email')}}"
                       value="{{old('email')}}" required
                >
                @error('email')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="name" class="form-label">{{__('Password')}}</label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="{{__('Password')}}"
                       required autocomplete="new-password"
                >
                @error('password')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="password_confirmation" class="form-label">{{__('Confirm Password')}}</label>
                <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" name="password_confirmation" placeholder="{{__('Confirm Password')}}"
                       required
                >
                @error('password')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <button class="w-100 btn btn-lg btn-secondary fw-bold border-white bg-white" type="submit">{{__('Register')}}</button>
            <div class="text-center">
                <ul class="list-inline mt-1">
                    <li class="list-inline-item">
                        <a href="{{ route('login') }}" class="text-muted">
                            {{ __('Already registered?') }}
                        </a>
                    </li>
                </ul>
            </div>
            <p class="mt-5 mb-3 text-muted text-center">&copy; 2020–{{date('Y')}}</p>
        </form>
    </main>
@endsection
