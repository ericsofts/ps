@extends('layouts.guest')
@section('body-class', 'd-flex h-100 text-white bg-black')
@section('html-class', 'h-100')

@section('content')
    <main class="form-1">
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="text-center">
                <img class="mb-4" src="{{ asset('images/logo.png') }}">
                <p class="mb-3 fw-normal">{{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}</p>
            </div>
            @if($status = session('status'))
                <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                    {{ $status }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                    {{ __('Whoops! Something went wrong.') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="mb-3">
                <label for="name" class="form-label">{{__('Email')}}</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="{{__('Email')}}"
                       value="{{old('email')}}" required
                >
                @error('email')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>

            <button class="w-100 btn btn-lg btn-secondary fw-bold border-white bg-white" type="submit">{{__('Email Password Reset Link')}}</button>
            <div class="text-center">
                <ul class="list-inline mt-1">
                    <li class="list-inline-item">
                        <a href="{{ route('login') }}" class="text-muted">
                            {{ __('Log in') }}
                        </a>
                    </li>
                </ul>
            </div>
            <p class="mt-5 mb-3 text-muted text-center">&copy; 2020–{{date('Y')}}</p>
        </form>
    </main>
@endsection
