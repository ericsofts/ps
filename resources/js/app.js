// require('./bootstrap');
//
// require('alpinejs');

// import 'bootstrap';
import {Modal} from 'bootstrap';
import $ from 'jquery';
import Swal from 'sweetalert2'

window.toastr = require('toastr');
window.$ = window.jQuery = $;
window.Swal = Swal;

window.startTimer = function(duration, display, callback) {
    var timer = duration;
    var si = setInterval(function () {
        var hours = Math.floor(timer / (60 * 60));
        var minutes = Math.floor((timer % (60 * 60)) / (60));
        var seconds = Math.floor(timer % 60);

        if (hours < 10) hours = '0' + hours;
        if (minutes < 10) minutes = '0' + minutes;
        if (seconds < 10) seconds = '0' + seconds;

        display.textContent = hours + ":"
            + minutes + ":" + seconds + "";

        if (--timer < 0) {
            clearInterval(si);
            callback();
        }
    }, 1000);
}

$(function () {
    $(document).on('change', '.enable-2fa', function (event) {
        event.preventDefault();
        var that = this;
        if (this.checked) {
            $('.block-form-2fa').show();
            $('#enable_2fa').val(1);
        } else {
            $(that).attr('disabled', true);
            $.ajax({
                url: '/2fa/status',
                type: 'GET'
            }).done(function (data) {
                if (data.status) {
                    $(that).prop('checked', true);
                    var modal = new Modal(document.getElementById('modal-2fa-disable'))
                    modal.show()
                } else {
                    $('.block-form-2fa').hide();
                }
            }).always(function () {
                $(that).attr('disabled', false);
            })
        }
        return false;
    })

    $(document).on('submit', 'form#disable-2fa', function (event) {
        event.preventDefault();
        var form = $(this).closest('.modal-content').find('form');
        var that = this;
        $(that).attr('disabled', true);
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize()
        }).done(function (data) {
            if (data.status) {
                toastr.success(data.message);
                window.location.reload(true);
            } else {
                toastr.error(data.message);
                $.each(data.errors, function (index, value) {
                    form.find('#' + index).addClass('is-invalid');
                    form.find('#' + index).siblings('.invalid-feedback').text(value[0]);
                });
            }
        }).always(function () {
            $(that).attr('disabled', false);
            form.find("input[type=text]").val("");
        })
        return false;
    });

    $(document).on('click', '.btn-regenerate-2fa', function (event) {
        event.preventDefault();
        var modal = new Modal(document.getElementById('modal-2fa-generate'))
        modal.show()
        return false;
    })
    $(document).on('submit', 'form#2fa-generate', function (event) {
        event.preventDefault();
        var form = $(this).closest('.modal-content').find('form');
        var that = this;
        $(that).attr('disabled', true);
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize()
        }).done(function (data) {
            if (data.status) {
                toastr.success(data.message);
                window.location.reload(true);
            } else {
                toastr.error(data.message);
                $.each(data.errors, function (index, value) {
                    form.find('#' + index).addClass('is-invalid');
                    form.find('#' + index).siblings('.invalid-feedback').text(value[0]);
                });
            }
        }).always(function () {
            $(that).attr('disabled', false);
            form.find("input[type=text]").val("");
        })
        return false;
    });
    $('#fiat_name').change(function () {
        $('#payment_system_id').find('option').remove();
        var fiat = $(this).val();
        if (FiatsArray[fiat]) {
            FiatsArray[fiat].forEach(function (estimate) {
                $('#payment_system_id')
                    .append($("<option></option>")
                        .attr("value", estimate.payment_system_id)
                        .attr('data-name', estimate.payment_system_name)
                        .attr('data-type', estimate.payment_system_type)
                        .attr('data-fiat_amount', estimate.fiat_amount)
                        .text(estimate.payment_system_name + ' - ' + estimate.fiat_amount));
            });
        }
    });

    $('#currency').change(function () {
        $('#payment_system_id').find('option').remove();
        var fiat = $(this).val();
        if (FiatsArray[fiat]) {
            FiatsArray[fiat].forEach(function (estimate) {
                $('#payment_system_id')
                    .append($("<option></option>")
                        .attr("value", estimate.payment_system_id)
                        .attr('data-name', estimate.payment_system_name)
                        .attr('data-type', estimate.payment_system_type)
                        .attr('data-fiat_amount', estimate.fiat_amount)
                        .text(estimate.payment_system_name + ' - ' + estimate.fiat_amount));
            });
        }
        $('#payment_system_id').trigger('change');
    });

    var taf = $('.trader_ads-filter-form');
    if (taf) {
        taf.find('#fiat_id').on('change', function () {
            fiats.forEach(function (el) {
                if (el.id != $('#fiat_id').val()) {
                    return;
                }

                $('#payment_system_id').find('option:not([value=""])').remove();
                var buf = '';

                el.payment_systems.forEach(function (el) {
                    buf += '<option value="' + el.id + '">' + el.name + '</option>';
                });
                $('#payment_system_id').html(
                    $('#payment_system_id').html() + buf
                );

                $('#payment_system_id').find('option[value="' + $('#payment_system_id').attr('value') + '"]').attr('selected', true);
            });
        });
        taf.find('#fiat_id').trigger('change');
    }
})
