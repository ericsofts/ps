Ошибки Invalid parameters для PI, WI

P10001 - Merchant not found

P10002 - Invalid signature / Неверная подпись

P10003 - Unavailable currency / Недоступная валюта 

P10004 - Invalid min amount / Неверная минимальная сумма

P10005 - Invalid rate

P10006 - order_id exists / order_id существует

P10007 - Unavailable coin

I10000 - incorrect status / неправильный статус
I10001 - ad not found / объявление не найдено
I10002 - unavailable coin for merchant / недоступная крипта для мерчанта
I10003 - Ad without card number / Объявление без номера карты
I10004 - Unavailable Coin Rate / Недоступный курс валюты
I10005 - Course out of range / Курс вне диапазона допустимого значения
I10006 - Service provider response has error
I10007 - Insufficient funds on the merchant's balance / Недостаточно средств на балансе мерчанта

I10008 - Insufficient funds on the system (service provider)'s balance
I10009 - merchant not found
I10010 - Tried to close deal without attachments / пытались закрыть сделку без вложений
I10011 - Unavailable currency Rate / Недоступный курс валюты

I10099 - Unknown error / Неизвестная ошибка
    
