<?php

use App\Http\Controllers\Api3\WithdrawalController;
use App\Http\Controllers\Api5\PostController;
use App\Http\Controllers\Api5\WithdrawalPostController;
use App\Http\Controllers\BalanceHistoryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GrowTokenController;
use App\Http\Controllers\InputInvoiceController;
//use App\Http\Controllers\TestController;
use App\Http\Controllers\TwoFAController;
use App\Http\Controllers\UsdtController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TelegramController;
use App\Http\Controllers\TraderDealsController;
use App\Http\Controllers\TraderInputInvoiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Locale
Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, config()->get('app.locales'))) {
        session()->put('locale', $locale);
    }
    return redirect()->back()->cookie('locale', $locale, 60 * 24 * 360);
});

//Route::post('payment/checkout/redirect', [PaymentController::class, 'checkout_redirect'])->name('payment.checkout_redirect');
//Route::get('payment/checkout/redirect', function (){
//    abort(404);
//});

//Route::get('/trader/input_invoice/transact', [TraderInputInvoiceController::class, 'transact'])->name('trader_input_invoice.transact');
//Route::post('/trader/input_invoice/transact', [TraderInputInvoiceController::class, 'transact'])->name('trader_input_invoice.transact');

Route::middleware(['auth', 'verified'])->group(function () {

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/balance-history', [BalanceHistoryController::class, 'index'])->name('balance.history');

    Route::get('/create-invoice', [InputInvoiceController::class, 'create'])->name('input_invoice.create');
    Route::get('/create-invoice/{payment_method}', [InputInvoiceController::class, 'paymentMethod'])->name('input_invoice.payment_method');
    Route::post('/create-invoice', [InputInvoiceController::class, 'paymentMethod'])->name('input_invoice.payment_method');

    Route::post('/create-invoice/bank-card', [InputInvoiceController::class, 'chatex_lbc_contact_create'])->name('input_invoice.chatex_lbc_contact_create');
    Route::post('/create-invoice/bank-card/confirm', [InputInvoiceController::class, 'chatex_lbc_contact_confirm'])->name('input_invoice.chatex_lbc_contact_confirm');
    Route::get('/create-invoice/bank-card/confirm/{id}', [InputInvoiceController::class, 'chatex_lbc_contact_confirm_show'])->name('input_invoice.chatex_lbc_contact_confirm_show');
    Route::post('/create-invoice/bank-card/check/{id}', [InputInvoiceController::class, 'chatex_lbc_contact_check'])->name('input_invoice.chatex_lbc_contact_check');
    Route::get('/top-up/history', [InputInvoiceController::class, 'index'])->name('input_invoice.history');
    Route::get('/top-up/history-addition', [InputInvoiceController::class, 'history_addition'])->name('input_invoice.history.addition');
    Route::get('/top-up/usdt/{id}', [UsdtController::class, 'view'])->name('input_invoice.usdt');
    Route::get('/usdt/qr/{id}', [UsdtController::class, 'qr'])->name('usdt.qr');
    Route::post('/usdt/cancel/{id}', [UsdtController::class, 'cancel'])->name('usdt.cancel');

//    Route::get('/payment-to-partner/create-invoice', [PaymentToPartnerController::class, 'create'])->name('payment2partner.create');
//    Route::post('/payment-to-partner/create-invoice', [PaymentToPartnerController::class, 'confirm'])->name('payment2partner.confirm');

    Route::get('/grow-token/exchange', [GrowTokenController::class, 'exchange'])->name('grow_token.exchange');
    Route::post('/grow-token/exchange', [GrowTokenController::class, 'confirm'])->name('grow_token.exchange.confirm');
    Route::get('/grow-token/withdrawal', [GrowTokenController::class, 'withdrawal'])->name('grow_token.withdrawal');
    Route::post('/grow-token/withdrawal', [GrowTokenController::class, 'withdrawal_confirm'])->name('grow_token.withdrawal.confirm');
    Route::get('/grow-token/withdrawal/history', [GrowTokenController::class, 'withdrawal_index'])->name('grow_token.withdrawal.index');
    Route::post('/grow-token/withdrawal/cancel/{id}', [GrowTokenController::class, 'withdrawal_cancel'])->name('grow_token.withdrawal.cancel');

    Route::get('2fa', [TwoFAController::class, 'index'])->name('2fa.index');
    Route::get('2fa/status', [TwoFAController::class, 'status'])->name('2fa.status');
    Route::post('2fa/configure', [TwoFAController::class, 'configure'])->name('2fa.configure');
    Route::post('2fa/disable', [TwoFAController::class, 'disable'])->name('2fa.disable');
    Route::post('2fa/generate_new', [TwoFAController::class, 'generateNew'])->name('2fa.generate_new');

    Route::get('profile', [UserController::class, 'profile'])->name('user.profile');

//    Route::post('/payment/checkout/{payment_invoice_id}', [PaymentController::class, 'checkout_confirm'])->name('payment.checkout_confirm');
//    Route::get('/payment/checkout/{payment_invoice_id}/complete', [PaymentController::class, 'checkout_complete'])->name('payment.checkout_complete');
//    Route::get('/payment/checkout/{payment_invoice_id?}', [PaymentController::class, 'checkout'])->name('payment.checkout');
//
//    Route::post('/otp/resend', [OTPController::class, 'resend'])->name('otp.resend');
});

Route::get('/withdrawal/attachment/{id}', [WithdrawalController::class, 'attachment'])
    ->middleware(['signed'])
    ->name('withdrawal.attachment');

Route::post('payment/invoice/post/add', [PostController::class, 'invoicePostAdd'])->name('payment.invoice.post.add')->middleware('check.merchant.ip.security:byId,merchant_id');
Route::get('/payment/invoice/post/checkout/status', [PostController::class, 'invoicePostCheckoutStatus'])->name('payment.invoice.post.checkout.status')->middleware('signed');
Route::get('/payment/invoice/post/checkout/qr/{payment_invoice_id?}', [PostController::class, 'getQr'])->name('payment.invoice.post.checkout.qr')->middleware('signed');
Route::post('/payment/invoice/post/checkout/status', [PostController::class, 'invoicePostCheckoutStatus'])->name('payment.invoice.post.checkout.status')->middleware('signed');
Route::post('/payment/invoice/post/checkout/status-check', [PostController::class, 'invoicePostCheckoutStatusCheck'])->name('payment.invoice.post.checkout.status.check');
Route::get('/payment/invoice/post/checkout/{payment_invoice_id}/apply', [PostController::class, 'invoicePostCheckoutApply'])->name('payment.invoice.post.checkout.apply')->middleware('signed');
Route::get('/payment/invoice/post/checkout/{payment_invoice_id}/complete', [PostController::class, 'invoicePostCheckoutComplete'])->name('payment.invoice.post.checkout.complete')->middleware('signed');
Route::get('/payment/invoice/post/checkout/{payment_invoice_id}/cancel', [PostController::class, 'invoicePostCheckoutDecline'])->name('payment.invoice.post.checkout.cancel')->middleware('signed');
Route::get('/payment/invoice/post/checkout/{payment_invoice_id?}', [PostController::class, 'invoicePostCheckout'])->name('payment.invoice.post.checkout')->middleware('signed');
Route::post('/payment/invoice/post/checkout/{payment_invoice_id?}', [PostController::class, 'invoicePostCheckoutConfirm'])->name('payment.invoice.post.checkout.confirm')->middleware('signed');

Route::get('payment/invoice/post/add', function (){
    abort(404);
});

Route::post('payment/withdrawal/post/add', [WithdrawalPostController::class, 'withdrawalPostAdd'])->name('payment.withdrawal.post.add')->middleware('check.merchant.ip.security:byId,merchant_id');
Route::post('/payment/withdrawal/post/checkout/status-check', [WithdrawalPostController::class, 'withdrawalPostCheckoutStatusCheck'])->name('payment.withdrawal.post.checkout.status.check');
Route::get('/payment/withdrawal/post/attachment/{id}', [WithdrawalPostController::class, 'attachment'])->name('payment.withdrawal.post.attachment')->middleware('signed');;
Route::post('/payment/withdrawal/post/checkout/status', [WithdrawalPostController::class, 'withdrawalPostCheckoutStatus'])->name('payment.withdrawal.post.checkout.status')->middleware('signed');
Route::get('/payment/withdrawal/post/checkout/status', [WithdrawalPostController::class, 'withdrawalPostCheckoutStatus'])->name('payment.withdrawal.post.checkout.status')->middleware('signed');
Route::get('/payment/withdrawal/post/checkout/{payment_withdrawal_id}/apply', [WithdrawalPostController::class, 'withdrawalPostCheckoutApply'])->name('payment.withdrawal.post.checkout.apply')->middleware('signed');
Route::get('/payment/withdrawal/post/checkout/{payment_withdrawal_id}/cancel', [WithdrawalPostController::class, 'withdrawalPostCheckoutDecline'])->name('payment.withdrawal.post.checkout.cancel')->middleware('signed');
Route::get('/payment/withdrawal/post/checkout/{payment_withdrawal_id?}', [WithdrawalPostController::class, 'withdrawalPostCheckout'])->name('payment.withdrawal.post.checkout')->middleware('signed');
Route::post('/payment/withdrawal/post/checkout/{payment_withdrawal_id?}', [WithdrawalPostController::class, 'withdrawalPostCheckoutConfirm'])->name('payment.withdrawal.post.checkout.confirm')->middleware('signed');

Route::get('/payment/withdrawal/post/checkout/{payment_withdrawal_id}/complete', [WithdrawalPostController::class, 'withdrawalPostCheckoutComplete'])->name('payment.withdrawal.post.checkout.complete')->middleware('signed');

Route::get('/telegram/webhook', [TelegramController::class, 'webhook'])->name('telegram.webhook');
Route::post('/telegram/webhook', [TelegramController::class, 'webhook'])->name('telegram.webhook');

//Route::get('/test', [TestController::class, 'test'])->name('test');
Route::get('/trader/deal/ads', [TraderDealsController::class, 'ads'])->name('trader_deal.ads');
Route::post('/trader/deal/trader_deal.create', [TraderDealsController::class, 'create'])->name('trader_deal.create');
require __DIR__ . '/auth.php';
