<?php

use App\Http\Controllers\Api\MerchantController;
use App\Http\Controllers\Api\StatisticController;
use App\Http\Controllers\Api3\PaymentController as PaymentController3;
use App\Http\Controllers\Api6\PaymentController as PaymentController6;
use App\Http\Controllers\Api3\WithdrawalController;
use App\Http\Controllers\Api5\GetController;
use App\Http\Controllers\Api5\WithdrawalGetController;
use App\Http\Controllers\Crypto\InvoiceCryptoExtendedController;
use App\Http\Controllers\Crypto\PaymentInvoiceController;
use App\Http\Controllers\DealMessageAttachmentController;
use App\Http\Procedures\TraderProcedure;
use App\Http\Procedures\MerchantWithdrawalInvoiceProcedure;
use App\Http\Procedures\PaymentInvoiceProcedure;
use App\Http\Procedures\UserProcedure;
use App\Http\Procedures\UserWithdrawalInvoiceProcedure;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::group(['middleware' => ['check.merchant.token'], 'prefix' => 'v1'], function () {
//    Route::post('payment/request', [PaymentController::class, 'request']);
//    Route::post('payment/charge', [PaymentController::class, 'charge']);
//});

Route::group(['middleware' => ['check.merchant.token:api3_key', 'check.merchant.ip.security:token,api3_key'], 'prefix' => 'v3'], function () {
    Route::get('balance', [MerchantController::class, 'balance']);
    Route::post('payment/request', [PaymentController3::class, 'request']);
    Route::post('payment/create', [PaymentController3::class, 'create']);
    Route::post('payment/check/approved', [PaymentController3::class, 'check_approved']);
    Route::post('payment/confirm', [PaymentController3::class, 'confirm']);
    Route::post('payment/cancel', [PaymentController3::class, 'cancel']);
    Route::post('payment/check/status', [PaymentController3::class, 'check_status']);
    Route::post('payment/invoices', [GetController::class, 'invoices']);

    Route::post('withdrawal/request', [WithdrawalController::class, 'request']);
    Route::post('withdrawal/create', [WithdrawalController::class, 'create']);
    Route::post('withdrawal/confirm', [WithdrawalController::class, 'confirm']);
    Route::post('withdrawal/cancel', [WithdrawalController::class, 'cancel']);
    Route::post('withdrawal/check/status', [WithdrawalController::class, 'check_status']);
    Route::post('withdrawal/invoices', [WithdrawalGetController::class, 'invoices']);
});

Route::group(['middleware' => ['check.merchant.token:api5_key', 'check.merchant.ip.security:token,api5_key'], 'prefix' => 'v5'], function () {
    Route::get('balance', [MerchantController::class, 'balance']);
    Route::post('invoice/get', [GetController::class, 'invoiceGetAdd']);
    Route::post('invoice/status', [GetController::class, 'invoiceStatus']);
    Route::post('invoice/invoices', [GetController::class, 'invoices']);
    Route::post('withdrawal/get', [WithdrawalGetController::class, 'withdrawalGetAdd']);
    Route::post('withdrawal/status', [WithdrawalGetController::class, 'withdrawalStatus']);
    Route::post('withdrawal/invoices', [WithdrawalGetController::class, 'invoices']);
});

Route::group(['middleware' => ['check.internal.token'], 'prefix' => 'v1'], function () {
    Route::rpc('/', [
        UserProcedure::class,
        TraderProcedure::class,
        UserWithdrawalInvoiceProcedure::class,
    ])->name('rpc.api');
    Route::post('save/file', [DealMessageAttachmentController::class, 'save']);
    Route::get('get/file', [DealMessageAttachmentController::class, 'get']);
    Route::get('get/path', [DealMessageAttachmentController::class, 'path']);
});

Route::group(['middleware' => ['check.internal.token:'.config('app.api_statistic_internal_token')], 'prefix' => 'v1/statistic'], function () {
    Route::get('pending-fees', [StatisticController::class, 'pendingFees']);
});

Route::group(['middleware' => ['check.internal.token', 'check.internal.ip', 'log.internal.api.request'], 'prefix' => 'i/v1'], function () {
    Route::rpc('/', [
        PaymentInvoiceProcedure::class,
        MerchantWithdrawalInvoiceProcedure::class,
    ])->name('rpc.api');
});

Route::group(['middleware' => ['check.internal.ip', 'log.internal.api.request'], 'prefix' => 'i/v1'], function () {
    Route::post('/payment-invoice/crypto-callback', [PaymentInvoiceController::class, 'crypto_callback']);
    Route::post('/invoice-crypto-extend/crypto-callback', [InvoiceCryptoExtendedController::class, 'crypto_callback']);
});

Route::group(['middleware' => ['check.merchant.token:api6_key', 'check.merchant.ip.security:token,api6_key'], 'prefix' => 'v6'], function () {
    Route::get('balance', [MerchantController::class, 'balance']);
    Route::post('payment/request', [PaymentController6::class, 'request']);
    Route::post('payment/create', [PaymentController6::class, 'create']);
    Route::post('payment/check/approved', [PaymentController6::class, 'check_approved']);
    Route::post('payment/confirm', [PaymentController6::class, 'confirm']);
    Route::post('payment/cancel', [PaymentController6::class, 'cancel']);
    Route::post('payment/check/status', [PaymentController6::class, 'check_status']);
    Route::post('payment/invoices', [GetController::class, 'invoices']);

});
