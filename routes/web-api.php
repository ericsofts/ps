<?php

use App\Http\Controllers\Api5\PostController;

use Illuminate\Support\Facades\Route;


Route::post('payment/invoice/post/add', [PostController::class, 'invoicePostAdd'])->name('payment.invoice.post.add');
Route::get('/payment/invoice/post/checkout/status', [PostController::class, 'invoicePostCheckoutStatus'])->name('payment.invoice.post.checkout.status');
Route::post('/payment/invoice/post/checkout/status', [PostController::class, 'invoicePostCheckoutStatus'])->name('payment.invoice.post.checkout.status');
Route::post('/payment/invoice/post/checkout/status-check', [PostController::class, 'invoicePostCheckoutStatusCheck'])->name('payment.invoice.post.checkout.status.check');
Route::get('/payment/invoice/post/checkout/{payment_invoice_id}/apply', [PostController::class, 'invoicePostCheckoutApply'])->name('payment.invoice.post.checkout.apply');
Route::get('/payment/invoice/post/checkout/{payment_invoice_id}/complete', [PostController::class, 'invoicePostCheckoutComplete'])->name('payment.invoice.post.checkout.complete');
Route::get('/payment/invoice/post/checkout/{payment_invoice_id?}', [PostController::class, 'invoicePostCheckout'])->name('payment.invoice.post.checkout');
Route::post('/payment/invoice/post/checkout/{payment_invoice_id?}', [PostController::class, 'invoicePostCheckoutConfirm'])->name('payment.invoice.post.checkout.confirm');

Route::get('payment/invoice/post/add', function (){
    abort(404);
});

