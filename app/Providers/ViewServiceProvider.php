<?php

namespace App\Providers;

use App\Models\InputInvoice;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.navigation', function ($view) {
            $view->with(['count_input_invoice_not_paid' => request()->user()
                    ->input_invoices->whereIn('status', [InputInvoice::STATUS_CREATED, InputInvoice::STATUS_USER_CONFIRMED])->count()]);
        });
    }
}
