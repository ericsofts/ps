<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            [
                'payment.invoice.post.add',
                'payment.invoice.post.checkout',
                'payment.invoice.post.confirm_chatex_lbc',
                'payment.invoice.post.checkout_complete',
                'payment.withdrawal.post.add',
                'payment.withdrawal.post.checkout',
                'payment.withdrawal.post.confirm',
                'payment.invoice.post.checkout_complete',
                'payment.withdrawal.post.checkout_complete',
            ]
            , 'App\Http\ViewComposers\WhiteLabelComposer'
        );
    }

}
