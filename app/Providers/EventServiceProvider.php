<?php

namespace App\Providers;

use App\Models\CryptoPaymentInvoice;
use App\Models\GrowTokenInvoice;
use App\Models\InputInvoice;
use App\Models\InvoiceCryptoExtend;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\TraderDeal;
use App\Observers\CryptoPaymentInvoiceObserver;
use App\Observers\GrowTokenInvoiceObserver;
use App\Observers\InputInvoiceObserver;
use App\Observers\InvoiceCryptoExtendObserver;
use App\Observers\MerchantWithdrawalInvoiceObserver;
use App\Observers\TraderDealObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Models\PaymentInvoice;
use App\Observers\PaymentInvoiceObserver;
use App\Models\TraderInputInvoice;
use App\Observers\TraderInputInvoiceObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        PaymentInvoice::observe(PaymentInvoiceObserver::class);
        MerchantWithdrawalInvoice::observe(MerchantWithdrawalInvoiceObserver::class);
        TraderDeal::observe(TraderDealObserver::class);
        TraderInputInvoice::observe(TraderInputInvoiceObserver::class);
        InputInvoice::observe(InputInvoiceObserver::class);
        GrowTokenInvoice::observe(GrowTokenInvoiceObserver::class);
        CryptoPaymentInvoice::observe(CryptoPaymentInvoiceObserver::class);
        InvoiceCryptoExtend::observe(InvoiceCryptoExtendObserver::class);
    }
}
