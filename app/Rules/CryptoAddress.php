<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CryptoAddress implements Rule
{
    protected $cryptoprocessing;

    public function __construct(
        \App\Lib\Crypto\Cryptoprocessing $cryptoprocessing
    )
    {
        $this->cryptoprocessing = $cryptoprocessing;
    }
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = $this->cryptoprocessing->checkAddress(["TRX:USDT",$value]);
        return $result;
//        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The :attribute must contain only crypto addresses.');
    }
}
