<?php

namespace App\Observers;

use App\Jobs\PaymentWithdrawalSendResponse;
use App\Lib\Chatex\ChatexApiLBC;
use App\Lib\PaymentApiHelper;
use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\AgentBalance;
use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\Property;
use App\Models\ServiceProvidersBalance;
use App\Models\SystemBalance;

class MerchantWithdrawalInvoiceObserver
{
    public function updated(MerchantWithdrawalInvoice $invoice)
    {
        if($invoice->isDirty('status') && ($invoice->status == MerchantWithdrawalInvoice::STATUS_PAYED)){
            $merchant = Merchant::where('id', $invoice->merchant_id)->first();
            if($invoice->amount2service > 0){
                $this->addAccountingQueue(
                    $invoice,
                    AccountingEntrie::USER_TYPE_SERVICE,
                    $invoice->service_provider_id,
                    $invoice->amount2service,
                    AccountingEntrie::TYPE_CREDIT
                );
            }
            if($invoice->amount2agent > 0){
                if(!empty($merchant->agent_id)){
                    $this->addAccountingQueue(
                        $invoice,
                        AccountingEntrie::USER_TYPE_AGENT,
                        $merchant->agent_id,
                        $invoice->amount2agent,
                        AccountingEntrie::TYPE_CREDIT
                    );
                }
            }
            if($invoice->amount2grow > 0){
                $this->addAccountingQueue(
                    $invoice,
                    AccountingEntrie::USER_TYPE_GROW,
                    null,
                    $invoice->amount2grow,
                    AccountingEntrie::TYPE_CREDIT
                );
            }
            $this->addAccountingQueue(
                $invoice,
                AccountingEntrie::USER_TYPE_MERCHANT,
                $invoice->merchant_id,
                $invoice->amount,
                AccountingEntrie::TYPE_DEBIT
            );

            //if (!$invoice->merchant_response_at && $invoice->merchant_server_url) {
            if ($invoice->merchant_server_url) {
                $response = [
                    'invoice_id' => $invoice->id,
                    'merchant_id' => $invoice->merchant_id,
                    'order_id' => $invoice->merchant_order_id,
                    'amount' => round($invoice->amount, 2),
                    'amount_currency' => round($invoice->input_amount_value, 2),
                    'currency' => $invoice->input_currency,
                    'order_desc' => $invoice->merchant_order_desc,
                    'merchant_amount' => round($invoice->amount2pay, 2),
                    'account_info' => mask_credit_card($invoice->account_info),
                    'status' => 1
                ];

                $response['signature'] = PaymentApiHelper::generateSignature($response, $merchant->api5_key);
                PaymentWithdrawalSendResponse::dispatch($invoice, $response);

                $invoice->merchant_response_at = now();
                $invoice->saveQuietly();
            }
        }

        if ($invoice->isDirty('status')
            && MerchantWithdrawalInvoice::isCancelStatus($invoice->status)
            //&& !$invoice->merchant_cancel_at
            && $invoice->merchant_server_url
        ) {
            $response = [
                'invoice_id' => $invoice->id,
                'merchant_id' => $invoice->merchant_id,
                'order_id' => $invoice->merchant_order_id,
                'amount' => round($invoice->amount, 2),
                'amount_currency' => round($invoice->input_amount_value, 2),
                'currency' => $invoice->input_currency,
                'order_desc' => $invoice->merchant_order_desc,
                'merchant_amount' => round($invoice->amount2pay, 2),
                'account_info' => mask_credit_card($invoice->account_info),
                'status' => -1
            ];
            $merchant = Merchant::where('id', $invoice->merchant_id)->first();
            $response['signature'] = PaymentApiHelper::generateSignature($response, $merchant->api5_key);
            PaymentWithdrawalSendResponse::dispatch($invoice, $response);
            $invoice->merchant_cancel_at = now();
            $invoice->saveQuietly();
        }

        if($invoice->isDirty('status') && ($invoice->status == MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT)){
            $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
            $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
            $chatexLBC->contactMessagePost(
                $invoice['payment_id'],
                'Пожалуйста, прикрепите изображение с подтверждением перечисления. Без него будет невозможно подтвердить операцию.'
            );
        }
    }

    private function addAccountingQueue($invoice, $user_type, $user_id, $amount, $type){
        $accounting = AccountingQueue::create([
            'type' => $type,
            'user_type' => $user_type,
            'user_id' => $user_id,
            'amount' => $amount,
            'balance_after' => $this->getBalanceAfter($user_type, $invoice, $amount, $type),
            'invoice_id' => $invoice->id,
            'invoice_type' => AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
            'status' => AccountingEntrie::STATUS_CREATE
        ]);
    }

    private function getBalanceAfter($user_type, $invoice, $amount, $type){

        if($type == AccountingEntrie::TYPE_DEBIT){
            $amount = $amount*(-1);
        }

        switch ($user_type){
            case AccountingEntrie::USER_TYPE_SERVICE:
                $serviceBalance = ServiceProvidersBalance::firstOrCreate(['provider_id' => $invoice->service_provider_id]);
                $result = $serviceBalance->amount + $amount;
                break;
            case AccountingEntrie::USER_TYPE_AGENT:
                $merchant = Merchant::where(['id' => $invoice->merchant_id])->first();
                $agentBalance = AgentBalance::firstOrCreate(['agent_id' => $merchant->agent_id]);
                $result = $agentBalance->amount + $amount;
                break;
            case AccountingEntrie::USER_TYPE_GROW:
                $growBalance = SystemBalance::firstOrCreate(['name' => SystemBalance::NAME_GROW]);
                $result = $growBalance->amount + $amount;
                break;
            case AccountingEntrie::USER_TYPE_MERCHANT:
                $merchantBalance = MerchantBalance::firstOrCreate(['merchant_id' => $invoice->merchant_id]);
                $result = $merchantBalance->amount + $amount;
                break;
            default:
                $result = 0;
        }
        return $result;
    }
}
