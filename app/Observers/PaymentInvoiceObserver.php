<?php

namespace App\Observers;

use App\Jobs\PaymentInvoiceSendResponse;
use App\Lib\PaymentApiHelper;
use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\InvoiceCryptoExtend;
use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\PaymentInvoice;
use App\Models\AgentBalance;
use App\Models\ServiceProvidersBalance;
use App\Models\SystemBalance;

class PaymentInvoiceObserver
{

    public function updated(PaymentInvoice $paymentInvoice)
    {
        if ($paymentInvoice->isDirty('status') && ($paymentInvoice->status == PaymentInvoice::STATUS_PAYED)) {
            $this->commitInvoice($paymentInvoice, AccountingEntrie::TYPE_CREDIT);
        }

        if ($paymentInvoice->isDirty('status') && ($paymentInvoice->status == PaymentInvoice::STATUS_CANCELED_BY_REVERT)) {
            $this->commitInvoice($paymentInvoice, AccountingEntrie::TYPE_DEBIT);
        }

        if ($paymentInvoice->isDirty('status')
            && PaymentInvoice::isCancelStatus($paymentInvoice->status)
            //&& !$paymentInvoice->merchant_cancel_at
            && $paymentInvoice->merchant_server_url
        ) {
            $response = [
                'invoice_id' => $paymentInvoice->id,
                'merchant_id' => $paymentInvoice->merchant_id,
                'order_id' => $paymentInvoice->merchant_order_id,
                'amount' => round($paymentInvoice->amount, 2),
                'amount_currency' => round($paymentInvoice->input_amount_value, 2),
                'currency' => $paymentInvoice->input_currency,
                'order_desc' => $paymentInvoice->merchant_order_desc,
                'merchant_amount' => round($paymentInvoice->amount2pay, 2),
                'account_info' => mask_credit_card($paymentInvoice->account_info),
                'status' => -1
            ];
            $merchant = Merchant::where('id', $paymentInvoice->merchant_id)->first();
            $response['signature'] = PaymentApiHelper::generateSignature($response, $merchant->api5_key);
            PaymentInvoiceSendResponse::dispatch($paymentInvoice, $response);
            $paymentInvoice->merchant_cancel_at = now();
            $paymentInvoice->saveQuietly();
        }
    }

    private function addAccountingQueue($paymentInvoice, $user_type, $user_id, $amount, $type)
    {
        $accounting = AccountingQueue::create([
            'type' => $type,
            'user_type' => $user_type,
            'user_id' => $user_id,
            'amount' => $amount,
            'balance_after' => $this->getBalanceAfter($user_type, $paymentInvoice, $amount, $type),
            'invoice_id' => $paymentInvoice->id,
            'invoice_type' => AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
            'status' => AccountingEntrie::STATUS_CREATE
        ]);
    }

    private function getBalanceAfter($user_type, $paymentInvoice, $amount, $type)
    {

        if ($type == AccountingEntrie::TYPE_DEBIT) {
            $amount = $amount * (-1);
        }

        switch ($user_type) {
            case AccountingEntrie::USER_TYPE_SERVICE:
                $serviceBalance = ServiceProvidersBalance::firstOrCreate(['provider_id' => $paymentInvoice->service_provider_id]);
                $result = $serviceBalance->amount + $amount;
                break;
            case AccountingEntrie::USER_TYPE_AGENT:
                $merchant = Merchant::where(['id' => $paymentInvoice->merchant_id])->first();
                $agentBalance = AgentBalance::firstOrCreate(['agent_id' => $merchant->agent_id]);
                $result = $agentBalance->amount + $amount;
                break;
            case AccountingEntrie::USER_TYPE_GROW:
                $growBalance = SystemBalance::firstOrCreate(['name' => SystemBalance::NAME_GROW]);
                $result = $growBalance->amount + $amount;
                break;
            case AccountingEntrie::USER_TYPE_MERCHANT:
                $merchantBalance = MerchantBalance::firstOrCreate(['merchant_id' => $paymentInvoice->merchant_id]);
                $result = $merchantBalance->amount + $amount;
                break;
            default:
                $result = 0;
        }
        return $result;
    }

    private function commitInvoice(PaymentInvoice $paymentInvoice, $type){
        $merchant = Merchant::where('id', $paymentInvoice->merchant_id)->first();
        if ($paymentInvoice->amount2service > 0) {
            $this->addAccountingQueue(
                $paymentInvoice,
                AccountingEntrie::USER_TYPE_SERVICE,
                $paymentInvoice->service_provider_id,
                $paymentInvoice->amount2service,
                $type
            );
        }
        if ($paymentInvoice->amount2agent > 0) {
            if (!empty($merchant->agent_id)) {
                $this->addAccountingQueue(
                    $paymentInvoice,
                    AccountingEntrie::USER_TYPE_AGENT,
                    $merchant->agent_id,
                    $paymentInvoice->amount2agent,
                    $type
                );
            }
        }
        if ($paymentInvoice->amount2grow > 0) {
            $this->addAccountingQueue(
                $paymentInvoice,
                AccountingEntrie::USER_TYPE_GROW,
                null,
                $paymentInvoice->amount2grow,
                $type
            );
        }

        $this->addAccountingQueue(
            $paymentInvoice,
            AccountingEntrie::USER_TYPE_MERCHANT,
            $paymentInvoice->merchant_id,
            $paymentInvoice->amount2pay,
            $type
        );

        if (($type == AccountingEntrie::TYPE_CREDIT) && ($paymentInvoice->api_type == PaymentInvoice::API_TYPE_6)) {
            $invoiceCryptoExtend = InvoiceCryptoExtend::where('invoice_type', InvoiceCryptoExtend::getInvoiceTypeByInstance($paymentInvoice))
            ->where('invoice_id', $paymentInvoice->id)
            ->first();
            $invoiceCryptoExtend->status = InvoiceCryptoExtend::STATUS_TRADER_CONFIRMED;
            $invoiceCryptoExtend->save();
        }

        if ($paymentInvoice->merchant_server_url && ($type == AccountingEntrie::TYPE_CREDIT)) {
            $response = [
                'invoice_id' => $paymentInvoice->id,
                'merchant_id' => $paymentInvoice->merchant_id,
                'order_id' => $paymentInvoice->merchant_order_id,
                'amount' => round($paymentInvoice->amount, 2),
                'amount_currency' => round($paymentInvoice->input_amount_value, 2),
                'currency' => $paymentInvoice->input_currency,
                'order_desc' => $paymentInvoice->merchant_order_desc,
                'merchant_amount' => round($paymentInvoice->amount2pay, 2),
                'account_info' => mask_credit_card($paymentInvoice->account_info),
                'status' => 1
            ];

            $response['signature'] = PaymentApiHelper::generateSignature($response, $merchant->api5_key);
            PaymentInvoiceSendResponse::dispatch($paymentInvoice, $response);
            $paymentInvoice->merchant_response_at = now();
            $paymentInvoice->saveQuietly();
        }
    }
}
