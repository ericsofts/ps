<?php

namespace App\Observers;


use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\BalanceQueue;
use App\Models\InputInvoice;
use App\Models\ServiceProvidersBalance;
use App\Models\SystemBalance;

class InputInvoiceObserver
{

    public function updated(InputInvoice $inputInvoice)
    {
        if ($inputInvoice->isDirty('status') && ($inputInvoice->status == InputInvoice::STATUS_PAYED)) {

            if ($inputInvoice->amount2service > 0) {
                $this->addAccountingQueue(
                    $inputInvoice,
                    AccountingEntrie::USER_TYPE_SERVICE,
                    $inputInvoice->service_provider_id,
                    $inputInvoice->amount2service,
                    AccountingEntrie::TYPE_CREDIT
                );
            }

            if ($inputInvoice->amount2grow > 0) {
                $this->addAccountingQueue(
                    $inputInvoice,
                    AccountingEntrie::USER_TYPE_GROW,
                    null,
                    $inputInvoice->amount2grow,
                    AccountingEntrie::TYPE_CREDIT
                );
            }
            if($inputInvoice->payment_system == InputInvoice::PAYMENT_SYSTEM_USDT){
                $description = "Account Top-Up - USDT ERC20";
            }else{
                $description = "Account Top-Up - Bank Card";
            }

            if($inputInvoice->is_auto_grt){
                $amount = $inputInvoice->grow_token_amount2pay;
                $currency = BalanceQueue::CURRENCY_GROW_TOKEN;
            }else{
                $amount = $inputInvoice->amount;
                $currency = BalanceQueue::CURRENCY_USD;
            }

            $result = BalanceQueue::addToBalanceQueue(
                $inputInvoice->user_id,
                $amount,
                BalanceQueue::TYPE_CREDIT,
                $description,
                $inputInvoice->id,
                null,
                null,
                BalanceQueue::STATUS_WAITING,
                $currency
            );
        }

    }

    private function addAccountingQueue($inputInvoice, $user_type, $user_id, $amount, $type)
    {
        $accounting = AccountingQueue::create([
            'type' => $type,
            'user_type' => $user_type,
            'user_id' => $user_id,
            'amount' => $amount,
            'balance_after' => $this->getBalanceAfter($user_type, $inputInvoice, $amount, $type),
            'invoice_id' => $inputInvoice->id,
            'invoice_type' => AccountingEntrie::getInvoiceTypeByInstance($inputInvoice),
            'status' => AccountingEntrie::STATUS_CREATE
        ]);
    }

    private function getBalanceAfter($user_type, $inputInvoice, $amount, $type)
    {

        if ($type == AccountingEntrie::TYPE_DEBIT) {
            $amount = $amount * (-1);
        }

        switch ($user_type) {
            case AccountingEntrie::USER_TYPE_SERVICE:
                $serviceBalance = ServiceProvidersBalance::firstOrCreate(['provider_id' => $inputInvoice->service_provider_id]);
                $result = $serviceBalance->amount + $amount;
                break;
            case AccountingEntrie::USER_TYPE_GROW:
                $growBalance = SystemBalance::firstOrCreate(['name' => SystemBalance::NAME_GROW]);
                $result = $growBalance->amount + $amount;
                break;
            default:
                $result = 0;
        }
        return $result;
    }
}
