<?php

namespace App\Observers;

use App\Jobs\PaymentInvoiceSendResponse;
use App\Lib\PaymentApiHelper;
use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\InvoiceCryptoExtend;
use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\PaymentInvoice;
use App\Models\AgentBalance;
use App\Models\ServiceProvidersBalance;
use App\Models\SystemBalance;
use Illuminate\Support\Facades\Log;

class InvoiceCryptoExtendObserver
{

    protected $cryptoprocessing;

    public function __construct(
        \App\Lib\Crypto\Cryptoprocessing $cryptoprocessing
    )
    {
        $this->cryptoprocessing = $cryptoprocessing;
    }

    public function updated(InvoiceCryptoExtend $invoiceCryptoExtend)
    {
        if ($invoiceCryptoExtend->isDirty('status') && ($invoiceCryptoExtend->status == InvoiceCryptoExtend::STATUS_TRADER_CONFIRMED)) {
            $this->commitInvoice($invoiceCryptoExtend);
        }

        if ($invoiceCryptoExtend->isDirty('status')
            && InvoiceCryptoExtend::isCancelStatus($invoiceCryptoExtend->status)
        ) {

        }
    }

    private function commitInvoice(InvoiceCryptoExtend $invoiceCryptoExtend){
        $result = $this->cryptoprocessing->withdrawalInvoiceNew(["$invoiceCryptoExtend->id", $invoiceCryptoExtend->coin, "$invoiceCryptoExtend->crypto_address", $invoiceCryptoExtend->amount]);
    }
}
