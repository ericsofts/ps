<?php

namespace App\Observers;


use App\Models\TraderBalance;
use App\Models\TraderBalanceQueue;
use App\Models\TraderInputInvoice;

class TraderInputInvoiceObserver
{

    public function created(TraderInputInvoice $traderInputInvoice)
    {
        $balance = TraderBalance::find($traderInputInvoice->balance_id);
        $balanceAmount = $balance->amount + $traderInputInvoice->amount;
        $desc = __('Transact');

        TraderBalanceQueue::create([
            'amount' => $traderInputInvoice->amount,
            'balance' => $balanceAmount,
            'balance_id' => $traderInputInvoice->id,
            'balance_type' => TraderBalance::TYPE_NORMAL,
            'currency' => $traderInputInvoice->currency->asset,
            'currency_id' => $traderInputInvoice->currency_id,
            'description' => $desc,
            'invoice_id' => $traderInputInvoice->id,
            'invoice_type' => TraderBalanceQueue::DEAL,
            'status' => TraderBalanceQueue::STATUS_WAITING,
            'trader_id' => $traderInputInvoice->trader_id,
            'type' => TraderBalanceQueue::TYPE_DEBIT
        ]);
    }

}
