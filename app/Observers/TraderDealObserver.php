<?php

namespace App\Observers;


use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use App\Models\Trader;
use App\Models\TraderAd;
use App\Models\TraderFiat;
use App\Models\TraderCurrency;
use App\Models\TraderBalance;
use App\Models\TraderBalanceQueue;
use App\Models\TraderDeal;
use App\Models\TraderDealHistory;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Keyboard\Keyboard;

class TraderDealObserver
{

    private $history = null;

    public function updating(TraderDeal $traderDeal)
    {
        // @TODO история создаётся даже если статус не сменился при ошибке
        if ($traderDeal->isDirty('status')) {
            switch ($traderDeal->status) {
                case TraderDeal::STATUS_TRADER_FUNDED:
                    $result = $this->changeDeal($traderDeal, 'Funded Deal', 'funded');
                    break;
                case TraderDeal::STATUS_COMPLETED:
                    $result = $this->changeDeal($traderDeal, 'Complete Deal', 'complete');
                    break;
                case TraderDeal::STATUS_CANCELED:
                case TraderDeal::STATUS_CANCELED_SD:
                case TraderDeal::STATUS_CANCELED_TIMEOUT:
                case TraderDeal::STATUS_CANCELED_TRADER:
                case TraderDeal::STATUS_CANCELED_USER:
                    $result = $this->changeDeal($traderDeal, 'Cancel Deal', 'cancel');
                    break;
                case TraderDeal::STATUS_CANCELED_BY_REPLY_TIMEOUT:
                    $result = $this->changeDeal($traderDeal, 'Cancel Deal', 'cancel');
                    if(isset($result) && !($result instanceof \Exception) && $result){
                        $this->cancelAdsTrader($traderDeal);
                        $this->logCancelAdsTrader($traderDeal);
                    }
                    break;
                default:
                    $result = true;
                    break;
            }
            if(isset($result) && $result){
                $this->history = TraderDealHistory::create([
                    'deal_id' => $traderDeal->id,
                    'customer_id' => $traderDeal->customer_id ?? NULL,
                    'customer_type' => $traderDeal->customer_type ?? NULL,
                    'status' => $traderDeal->status,
                    'description' => $traderDeal->description ?? 'change status deal',
                ]);
                $traderDeal->last_history_id = $this->history->id;

            }
        }
        unset($traderDeal->description);
        unset($traderDeal->customer_id);
        unset($traderDeal->customer_type);
    }

    public function updated(TraderDeal $traderDeal) {
        if (config('telegram.on')) {
            $notyStatuses = [
                TraderDeal::STATUS_PAYMENT_COMPLETED_USER,
                TraderDeal::STATUS_COMPLETED,
                TraderDeal::STATUS_CANCELED_BY_REPLY_TIMEOUT,
                TraderDeal::STATUS_CANCELED_TIMEOUT,
                TraderDeal::STATUS_CANCELED_SD,
                TraderDeal::STATUS_CANCELED_TRADER,
                TraderDeal::STATUS_CANCELED_USER,
                TraderDeal::STATUS_CANCELED,
            ];
            if (in_array($traderDeal->status, $notyStatuses)) {
                $text = __('Deal Status Changed') . " #$traderDeal->id
                " . __('status_' . $traderDeal->status);
                $url = config('telegram.trader_host') . "/trader/deal/detail/".$traderDeal->id;

                $reply_markup = Keyboard::make([
                    'resize_keyboard' => true,
                    'one_time_keyboard' => false
                ])
                    ->inline()
                    ->row(
                        Keyboard::inlineButton(['text' => __("Go to"), 'url' => $url])
                    );

                $trader = Trader::find($traderDeal->trader_id);
                $trader->telegramNoty($text, $reply_markup);
            }
        }
    }

    public function creating(TraderDeal $traderDeal)
    {
        $this->history = TraderDealHistory::create([
            'deal_id' => 0,
            'status' => $traderDeal->status,
            'customer_id' => $traderDeal->customer_id ?? NULL,
            'customer_type' => $traderDeal->customer_type ?? NULL,
            'description' => $traderDeal->description ?? 'create deal',
        ]);
        $traderDeal->last_history_id = $this->history->id;
        unset($traderDeal->description);
        unset($traderDeal->customer_id);
        unset($traderDeal->customer_type);
    }

    public function created(TraderDeal $traderDeal)
    {
        $traderHistory = TraderDealHistory::find($traderDeal->last_history_id);
        $traderHistory->deal_id = $traderDeal->id;
        $traderHistory->save();

        if (config('telegram.on')) {
            $fiat = TraderFiat::find($traderDeal->fiat_id);
            $currency = TraderCurrency::find($traderDeal->currency_id);
            $currency->asset = explode('_', $currency->asset)[0];

            $text = [
                TraderDeal::TYPE_SELL => __('New Sell Deal Created'),
                TraderDeal::TYPE_BUY => __('New Buy Deal Created')
            ][$traderDeal->type] . " #$traderDeal->id
            " . [
                TraderDeal::TYPE_SELL => $traderDeal->amount_currency . ' ' . $currency->asset . ' → ' . $traderDeal->amount_fiat . ' ' . $fiat->asset,
                TraderDeal::TYPE_BUY => $traderDeal->amount_fiat . ' ' . $fiat->asset . ' → ' . $traderDeal->amount_currency . ' ' . $currency->asset
            ][$traderDeal->type];

            $url = config('telegram.trader_host') . "/trader/deal/detail/".$traderDeal->id;

            $reply_markup = Keyboard::make([
                'resize_keyboard' => true,
                'one_time_keyboard' => false
            ])
                ->inline()
                ->row(
                    Keyboard::inlineButton(['text' => __("Go to"), 'url' => $url])
                );

            $trader = Trader::find($traderDeal->trader_id);
            $trader->telegramNoty($text, $reply_markup);
        }

        $ad = TraderAd::find($traderDeal->ad_id);
        if($ad && $ad->auto_acceptance){
            $traderDeal->status = TraderDeal::STATUS_TRADER_FUNDED;
            $traderDeal->description = 'Auto accept Deal by Ad setting';
            $traderDeal->customer_type = TraderDeal::TRADER_USER;
            $traderDeal->customer_id = $traderDeal->trader_id;
            $traderDeal->save();
        }
    }

    private function changeDeal($traderDeal, $desc, $type): bool
    {
        $currency = TraderCurrency::all()->pluck('asset', 'id');
        $trader = Trader::find($traderDeal->trader_id);
        $totalNormalBalance = $trader->getTotalBalance($traderDeal->currency_id, TraderBalance::TYPE_NORMAL);

        $originalDeal = TraderDeal::findOrFail($traderDeal->id);

        switch ($type) {
            case 'complete':
                if ($traderDeal->type == TraderDeal::TYPE_SELL) {
                    if (TraderDeal::isCancelStatus($originalDeal->status)) {
                        $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_NORMAL);
                        $balanceAmount = $balance->amount - $traderDeal->amount_currency;

                        if ($totalNormalBalance - $traderDeal->amount_currency < 0) {
                            throw new \Exception("Balance amount error");
                        }
                        $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_NORMAL, $desc, TraderBalanceQueue::TYPE_DEBIT);
                    } else {
                        $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_FROZEN);
                        $balanceAmount = $balance->amount - $traderDeal->amount_currency;
                        $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_FROZEN, $desc, TraderBalanceQueue::TYPE_DEBIT);
                    }
                } elseif ($traderDeal->type == TraderDeal::TYPE_BUY) {
                    $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_NORMAL);
                    $balanceAmount = $balance->amount + $traderDeal->amount_currency;
                    $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_NORMAL, $desc, TraderBalanceQueue::TYPE_CREDIT);
                }
                break;
            case 'cancel':
                if ($traderDeal->type == TraderDeal::TYPE_SELL) {
                    if (TraderDealHistory::where(['deal_id' => $traderDeal->id, 'status' => TraderDeal::STATUS_TRADER_FUNDED])->first()) {
                        $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_FROZEN);
                        $balanceAmount = $balance->amount - $traderDeal->amount_currency;
                        $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_FROZEN, $desc, TraderBalanceQueue::TYPE_DEBIT);
                        $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_NORMAL);
                        $balanceAmount = $balance->amount + $traderDeal->amount_currency;
                        $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_NORMAL, $desc, TraderBalanceQueue::TYPE_CREDIT);
                    }
                }
                $this->cancelInvoice($traderDeal);
                break;
            case 'funded':
                if ($traderDeal->type == TraderDeal::TYPE_SELL) {
                    $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_NORMAL);
                    if ($totalNormalBalance < $traderDeal->amount_currency) {
                        throw new \Exception("Balance amount error");
                    }
                    $balanceAmount = $balance->amount - $traderDeal->amount_currency;
                    $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_NORMAL, $desc, TraderBalanceQueue::TYPE_DEBIT);
                    $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_FROZEN);
                    $balanceAmount = $balance->amount + $traderDeal->amount_currency;
                    $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_FROZEN, $desc, TraderBalanceQueue::TYPE_CREDIT);
                }
                break;
        }

        return true;
    }

    private function getBalance($traderDeal, $currency, $type)
    {
        return TraderBalance::firstOrCreate([
            'trader_id' => $traderDeal->trader_id,
            'type' => $type,
            'currency_id' => $traderDeal->currency_id,
            'currency' => $currency[$traderDeal->currency_id]
        ]);
    }

    private function addBalanceQueue($traderDeal, $amount, $balance, $typeBalance, $desc, $type)
    {
        TraderBalanceQueue::create([
            'amount' => $traderDeal->amount_currency,
            'balance' => $amount,
            'balance_id' => $balance->id,
            'balance_type' => $typeBalance,
            'currency' => $traderDeal->currency->asset,
            'currency_id' => $traderDeal->currency_id,
            'description' => $desc,
            'invoice_id' => $traderDeal->id,
            'invoice_type' => TraderBalanceQueue::DEAL,
            'status' => TraderBalanceQueue::STATUS_WAITING,
            'trader_id' => $traderDeal->trader_id,
            'type' => $type,
        ]);
    }

    private function cancelAdsTrader($traderDeal)
    {
        $traderAds = TraderAd::where('trader_id', '=', $traderDeal->trader_id)->where('status', '=', TraderAd::STATUS_ACTIVE);
        $traderAds->update(['status' => TraderAd::STATUS_INACTIVE]);

        if (config('telegram.on')) {

            $text = __('You have not responded to the deal. All your ads are disabled. Go over and turn it on again');

            $url = config('telegram.trader_host') . "/trader/ad/" . TraderAd::getRouteTypes()[$traderDeal->type];

            $reply_markup = Keyboard::make([
                'resize_keyboard' => true,
                'one_time_keyboard' => false
            ])
                ->inline()
                ->row(
                    Keyboard::inlineButton(['text' => __("Go to"), 'url' => $url])
                );

            $trader = Trader::find($traderDeal->trader_id);
            $trader->telegramNoty($text, $reply_markup);
        }
    }

    private function logCancelAdsTrader($traderDeal)
    {
        Log::channel('trader_ads_shutdown')->info('ADS|CANCELED_BY_REPLY_TIMEOUT|TRADER_ID|' . $traderDeal->trader_id, ['deal' => $traderDeal]);
    }

    private function cancelInvoice($traderDeal){
        $trader = Trader::where('id', $traderDeal->trader_id)->first();
        switch ($traderDeal->type) {
            case TraderDeal::TYPE_SELL:
                $invoice = PaymentInvoice::where([
                    'service_provider_id' => $trader->service_provider_id,
                    'payment_id' => $traderDeal->id
                ])->whereNotIn('status', PaymentInvoice::getCancelStatuses())
                    ->first();
                break;
            case TraderDeal::TYPE_BUY:
                $invoice = MerchantWithdrawalInvoice::where([
                    'service_provider_id' => $trader->service_provider_id,
                    'payment_id' => $traderDeal->id
                ])->whereNotIn('status', MerchantWithdrawalInvoice::getCancelStatuses())
                    ->first();
                break;

        }
        if (isset($invoice) && $invoice) {
            switch ($traderDeal->status) {
                case TraderDeal::STATUS_CANCELED:
                case TraderDeal::STATUS_CANCELED_SD:
                case TraderDeal::STATUS_CANCELED_TRADER:
                case TraderDeal::STATUS_CANCELED_USER:
                    $invoice->status = get_class($invoice)::STATUS_CANCELED;
                    break;
                case TraderDeal::STATUS_CANCELED_TIMEOUT:
                case TraderDeal::STATUS_CANCELED_BY_REPLY_TIMEOUT:
                    $invoice->status = get_class($invoice)::STATUS_CANCELED_BY_TIMEOUT;
                    break;
            }
            $invoice->save();
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::getInvoiceTypeByInstance($invoice),
                4,
                InvoicesHistory::getStatus(1),
                InvoicesHistory::getSource(3),
                InvoicesHistory::getSource(3),
                NULL,
                'Canceled by Trader Dial Cancel'
            );
        }
    }
}
