<?php

namespace App\Observers;

use App\Models\CryptoPaymentInvoice;
use App\Models\PaymentInvoice;
use App\Models\ServiceProvider;

class CryptoPaymentInvoiceObserver
{

    public function updated(CryptoPaymentInvoice $invoice)
    {
        if ($invoice->isDirty('status') && ($invoice->status == CryptoPaymentInvoice::STATUS_PAYED)) {
            $provider = ServiceProvider::where('type', '=' , 'crypto')->first();
            $paymentInvoice = PaymentInvoice::where('payment_id', $invoice->id)
                ->where('service_provider_id', $provider->id)
                ->first();
            if($paymentInvoice) {
                $paymentInvoice->status = PaymentInvoice::STATUS_PAYED;
                $paymentInvoice->payed = now();
                $paymentInvoice->save();
            }
        }

        if ($invoice->isDirty('status') && CryptoPaymentInvoice::isCancelStatus($invoice->status)){
            $provider = ServiceProvider::where('type', '=' , 'crypto')->first();
            $paymentInvoice = PaymentInvoice::where('payment_id', $invoice->id)
                ->where('service_provider_id', $provider->id)
                ->first();
            if($paymentInvoice){
                $paymentInvoice->status = PaymentInvoice::STATUS_CANCELED;
                $paymentInvoice->save();
            }

        }
    }
}
