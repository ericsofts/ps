<?php

namespace App\Observers;

use App\Models\BalanceQueue;
use App\Models\GrowTokenInvoice;

class GrowTokenInvoiceObserver
{

    public function created(GrowTokenInvoice $invoice){
        $result = BalanceQueue::addToBalanceQueue(
            $invoice->user_id,
            $invoice->amount*(-1),
            BalanceQueue::TYPE_DEBIT,
            'Withdrawal Invoice Create',
            $invoice->id,
            null,
            null,
            BalanceQueue::STATUS_WAITING,
            BalanceQueue::CURRENCY_GROW_TOKEN
        );
    }

    public function updating(GrowTokenInvoice $invoice)
    {
        if ($invoice->isDirty('status')) {
            switch ($invoice->status){
                case GrowTokenInvoice::STATUS_PAYED:
                    $this->invoicePayed($invoice);
                    break;
                case GrowTokenInvoice::STATUS_CANCELED:
                    $this->invoiceCancel($invoice);
                    break;
            }
        }
    }

    private function invoicePayed(GrowTokenInvoice $invoice){
        if($invoice->getOriginal('status') == GrowTokenInvoice::STATUS_CANCELED){
            $result = BalanceQueue::addToBalanceQueue(
                $invoice->user_id,
                $invoice->amount*(-1),
                BalanceQueue::TYPE_DEBIT,
                'Withdrawal Invoice Cancel to Pay',
                $invoice->id,
                null,
                null,
                BalanceQueue::STATUS_WAITING,
                BalanceQueue::CURRENCY_GROW_TOKEN
            );
        }
    }

    private function invoiceCancel(GrowTokenInvoice $invoice){
        $result = BalanceQueue::addToBalanceQueue(
            $invoice->user_id,
            $invoice->amount,
            BalanceQueue::TYPE_CREDIT,
            'Withdrawal Invoice Cancel',
            $invoice->id,
            null,
            null,
            BalanceQueue::STATUS_WAITING,
            BalanceQueue::CURRENCY_GROW_TOKEN
        );
    }

}
