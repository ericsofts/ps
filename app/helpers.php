<?php

use App\Lib\LuhnAlgorithm;
use App\Models\AccountsCurrency;
use App\Models\BalanceHistory;
use Illuminate\Support\Carbon;

if (!function_exists('price_format')) {
    function price_format($price, $decimals = 2)
    {
        return number_format($price, $decimals, '.', '');
    }
}

if (!function_exists('input_invoice_status')) {
    function input_invoice_status($status)
    {
        $status_name = '';
        switch ($status) {
            case 0:
                $status_name = __('Created');
                break;
            case 99:
                $status_name = __('Canceled');
                break;
            case 2:
                $status_name = __('Confirmed');
                break;
            case 1:
                $status_name = __('Paid');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('payment_invoice_status')) {
    function payment_invoice_status($status)
    {
        $status_name = '';
        switch ($status) {
            case 0:
                $status_name = __('Created');
                break;
            case 99:
                $status_name = __('Canceled');
                break;
            case 2:
                $status_name = __('Confirmed');
                break;
            case 1:
                $status_name = __('Paid');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('grow_token_invoice_status')) {
    function grow_token_invoice_status($status)
    {
        $status_name = '';
        switch ($status) {
            case 0:
                $status_name = __('Created');
                break;
            case 99:
                $status_name = __('Canceled');
                break;
            case 1:
                $status_name = __('Paid');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('datetimeFormat')) {
    function datetimeFormat($datetime = null, $format = null)
    {
        if (!$datetime) {
            return;
        }
        if (!$format) {
            $format = 'd.m.Y H:i:s';
        }
        try {
            return Carbon::parse($datetime)->setTimezone(config('app.default.timezone'))->format($format);
        } catch (Throwable $t) {
            return $datetime;
        }
    }
}

if (!function_exists('payment_system')) {
    function payment_system($ps)
    {
        switch ($ps) {
            case 1:
                $name = __('Chatex');
                break;
            case 2:
                $name = __('Chatex LBC');
                break;
            default:
                $name = $ps;
        }
        return $name;
    }
}

if (!function_exists('balance_currency')) {
    function balance_currency($id): string
    {
        switch ($id) {
            case BalanceHistory::CURRENCY_USD:
                $name = 'USD';
                break;
            case BalanceHistory::CURRENCY_GROW_TOKEN:
                $name = 'GRT';
                break;
            default:
                $name = '';
        }
        return $name;
    }
}

if (!function_exists('price_format_currency')) {
    function price_format_currency($amount, $currency = 'USD', $fmt = null): string
    {
        $currencies = AccountsCurrency::getAll();
        if($cur = $currencies->firstWhere('asset', $currency)){
            if($cur->type == 'fiat' && $fmt){
                return $fmt->formatCurrency(price_format($amount, $cur->precision), $currency);
            }
            return $currency . ' ' . price_format($amount, $cur->precision);
        }
        return $currency . ' ' . price_format($amount, 2);
    }
}

if (!function_exists('mask_credit_card')) {
    function mask_credit_card($string, $replacement = "*")
    {
        $regex = '/(?:\d[ \t-]*?){13,19}/m';
        $matches = [];
        preg_match_all($regex, $string, $matches);

        if (!isset($matches[0]) || empty($matches[0])) {
            return $string;
        }
        $luhn = new LuhnAlgorithm();
        foreach ($matches as $matchGroup) {
            foreach ($matchGroup as $match) {
                $strippedMatch = preg_replace('/[^\d]/', '', $match);
                if (false === $luhn->isValid($strippedMatch)) {
                    continue;
                }
                $cardLength = strlen($strippedMatch);
                $replacement_string = substr($strippedMatch, 0, 6) . str_pad('', $cardLength - 10, $replacement) . substr($strippedMatch, -4);
                $string = str_replace($match, $replacement_string, $string);
            }
        }
        return $string;
    }
}

