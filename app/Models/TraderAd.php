<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TraderAd extends Model
{
    const STATUS_ACTIVE = 1;
    const TYPE_BUY = 1;
    const TYPE_SELL = 0;
    const RATE_FIXED = 0;
    const RATE_PERCENTAGE = 1;
    const STATUS_INACTIVE = 0;

    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'trader_id', 'type', 'fiat_id', 'currency_id', 'payment_system_id', 'card_number', 'min_amount', 'max_amount', 'rate_type', 'rate', 'status',
        'rate_source_id', 'rate_stop', 'payment_system_type_id', 'payment_info', 'auto_acceptance'
    ];

    public function trader()
    {
        return $this->hasOne(Trader::class, 'id', 'trader_id');
    }

    public function fiat()
    {
        return $this->hasOne(TraderFiat::class, 'id', 'fiat_id');
    }

    public function payment_system()
    {
        return $this->hasOne(TraderPaymentSystem::class, 'id', 'payment_system_id');
    }

    public function currency()
    {
        return $this->hasOne(TraderCurrency::class, 'id', 'currency_id');
    }

    public function payment_system_type()
    {
        return $this->hasOne(PaymentSystemType::class, 'id', 'payment_system_type_id');
    }

    public static function getRouteTypes(): array
    {
        return [
            self::TYPE_BUY => 'buy',
            self::TYPE_SELL => 'sell',
        ];
    }

}
