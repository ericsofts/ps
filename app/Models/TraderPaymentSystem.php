<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderPaymentSystem extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'status'
    ];

    public function fiats()
    {
        return $this->belongsToMany(TraderFiat::class, 'trader_fiats_payment_systems', 'payment_system_id', 'fiat_id');
    }
}
