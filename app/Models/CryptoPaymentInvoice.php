<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CryptoPaymentInvoice extends Model
{
    use HasFactory;

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_USER_CONFIRMED = 2;
    const STATUS_TRADER_CONFIRMED = 5;
    const STATUS_CANCELED = 99;

    const CANCELED_BY_USER = 0;
    const CANCELED_BY_EXPIRED = 1;
    const CANCELED_BY_MERCHANT = 3;
    const CANCELED_BY_SD = 4;

    protected $fillable = [
        'amount', 'address', 'address_amount', 'address_status', 'currency_id', 'currency_rate',
        'addition_info', 'status', 'payed', 'checked_at', 'input_currency_id', 'input_rate', 'input_amount',
        'comment', 'cancelation_reason', 'expired_at'
    ];

    public function currency()
    {
        return $this->hasOne(CryptoCurrency::class, 'id', 'currency_id');
    }

    public function input_currency()
    {
        return $this->hasOne(CryptoCurrency::class, 'id', 'input_currency_id');
    }


    public static function isCancelStatus($status): bool
    {
        return in_array($status, [self::STATUS_CANCELED]);
    }

}
