<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountingEntrie extends Model
{
    use HasFactory;

    const INVOICE_TYPE_PAYMENT_INVOICE = 0;
    const INVOICE_TYPE_MERCHANT_WITHDRAWAL = 1;
    const INVOICE_TYPE_MERCHANT_INPUT_INVOICE = 2;
    const INVOICE_TYPE_AGENT_WITHDRAWAL = 3;
    const INVOICE_TYPE_SYSTEM_BALANCE_WITHDRAWAL = 4;
    const INVOICE_TYPE_SERVICE_BALANCE_WITHDRAWAL = 5;
    const INVOICE_TYPE_USER_INPUT_INVOICE = 6;
    const USER_TYPE_GROW = 0;
    const USER_TYPE_SERVICE = 1;
    const USER_TYPE_AGENT = 2;
    const USER_TYPE_MERCHANT = 3;
    const TYPE_DEBIT = 0;
    const TYPE_CREDIT = 1;
    const TYPE_SET = 2;

    const STATUS_CREATE = 0;
    const STATUS_DONE = 1;


    protected $fillable = [
        'type', 'user_type', 'user_id', 'amount', 'balance_after', 'invoice_id', 'invoice_type', 'status'
    ];

    public static function getInvoiceTypeByInstance($invoice): bool|int
    {
       switch ($invoice){
           case $invoice instanceof MerchantInputInvoice:
               return self::INVOICE_TYPE_MERCHANT_INPUT_INVOICE;
           case $invoice instanceof MerchantWithdrawalInvoice:
               return self::INVOICE_TYPE_MERCHANT_WITHDRAWAL;
           case $invoice instanceof PaymentInvoice:
               return self::INVOICE_TYPE_PAYMENT_INVOICE;
           case $invoice instanceof InputInvoice:
               return self::INVOICE_TYPE_USER_INPUT_INVOICE;
       }
       return false;
    }
}
