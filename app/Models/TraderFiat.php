<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderFiat extends Model
{
    use HasFactory;

    protected $fillable = [
        'asset', 'name', 'precision', 'status'
    ];

    public function payment_systems()
    {
        return $this->belongsToMany(TraderPaymentSystem::class, 'trader_fiats_payment_systems', 'fiat_id', 'payment_system_id');
    }
}
