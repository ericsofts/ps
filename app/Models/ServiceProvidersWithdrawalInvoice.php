<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceProvidersWithdrawalInvoice extends Model
{
    use HasFactory;

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_CANCELED = 99;

    const PAYMENT_SYSTEM_MANUALLY = 0;

    protected $fillable = [
        'service_provider_id', 'amount', 'addition_info', 'status', 'payed', 'sd_user_id', 'payment_id', 'payment_system', 'comment'
    ];

    public function service_balance()
    {
        return $this->hasOne(ServiceProvidersBalance::class, 'provider_id', 'service_provider_id');
    }

    public function sd_user()
    {
        return $this->hasOne(SdUser::class, 'id', 'sd_user_id');
    }
}
