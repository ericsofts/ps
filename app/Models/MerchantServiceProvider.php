<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerchantServiceProvider extends Model
{
    use HasFactory;

    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $fillable = [
        'merchant_id', 'service_provider_id', 'priority'
    ];

    public function service_provider()
    {
        return $this->hasOne(ServiceProvider::class, 'id', 'service_provider_id');
    }
}
