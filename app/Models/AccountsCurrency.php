<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class AccountsCurrency extends Model
{
    use HasFactory;

    protected $fillable = [
        'asset', 'name', 'type', 'precision'
    ];

    public static function getRender(){
        return self::orderBy('id', 'asc')->get()->pluck('name', 'asset')->toArray();
    }

    public static function getAll()
    {
        return Cache::remember('AccountsCurrencyGetAll', 60, function () {
            return self::orderBy('id', 'asc')->get();
        });
    }
}
