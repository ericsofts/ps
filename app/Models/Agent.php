<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Agent extends Model
{
    use HasFactory;


    protected $fillable = [
        'name', 'email', 'password', 'status', 'partner_id'
    ];

    public function balance()
    {
        //return $this->hasOne(AgentBalance::class, 'agent_id');
    }


}
