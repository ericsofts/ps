<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerchantInputInvoice extends Model
{
    use HasFactory;

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_CANCELED = 99;

    const PAYMENT_SYSTEM_MANUALLY = 0;

    protected $fillable = [
        'merchant_id', 'amount', 'addition_info', 'status', 'payed', 'sd_user_id', 'payment_id', 'payment_system', 'comment'
    ];

}
