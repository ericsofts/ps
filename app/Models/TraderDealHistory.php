<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TraderDealHistory extends Model
{
    use HasFactory;


    protected $fillable = [
        'deal_id',
        'description',
        'status',
        'customer_id',
        'customer_type'
    ];



}
