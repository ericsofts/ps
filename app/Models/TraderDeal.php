<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class TraderDeal extends Model
{
    const STATUS_CREATE = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_TRADER_FUNDED = 2;
    const STATUS_PAYMENT_COMPLETED_USER = 3;
    const STATUS_PAYMENT_COMPLETED_TRADER = 4;
    const STATUS_DISPUTED = 5;
    const STATUS_CANCELED_BY_REPLY_TIMEOUT = 94;
    const STATUS_CANCELED_TIMEOUT = 95;
    const STATUS_CANCELED_SD = 96;
    const STATUS_CANCELED_TRADER = 97;
    const STATUS_CANCELED_USER = 98;
    const STATUS_CANCELED = 99;

    const MERCHANT_USER = 0;
    const SD_USER = 1;
    const TRADER_USER = 2;
    const PS_USER = 3;
    const SYSTEM = 4;

    const TYPE_SELL = 0;
    const TYPE_BUY = 1;

    use HasFactory;

    protected $fillable = [
        'ad_id',
        'trader_id',
        'type',
        'fiat_id',
        'currency_id',
        'payment_system_id',
        'card_number',
        'payment_info',
        'amount_currency',
        'amount_fiat',
        'rate',
        'status',
        'ad_json',
        'customer_id',
        'customer_type',
        'description',
        'last_history_id',
        'checked_at',
        'comment'
    ];

    public static function getStatuses(): array
    {
        return [
            self::STATUS_CREATE => 'Create',
            self::STATUS_PAYMENT_COMPLETED_TRADER => 'Apply Trader',
            self::STATUS_PAYMENT_COMPLETED_USER => 'Apply User',
            self::STATUS_DISPUTED => 'Arbitrage',
            self::STATUS_COMPLETED => 'Complete',
            self::STATUS_CANCELED_USER => 'Cancel by User',
            self::STATUS_CANCELED_TRADER => 'Cancel by Trader',
            self::STATUS_CANCELED_SD => 'Cancel by SD',
            self::STATUS_CANCELED_TIMEOUT => 'Cancel by Timeout',
            self::STATUS_CANCELED => 'Cancel',
            self::STATUS_TRADER_FUNDED => 'Trader Accept Deal',
            self::STATUS_CANCELED_BY_REPLY_TIMEOUT => 'Cancel by Reply Timeout',
        ];
    }

    public static function getAvalableStatuses(): array
    {
        return [
            self::STATUS_CREATE => [
                TraderDealMessage::MERCHANT_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                    ],
                TraderDealMessage::SD_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::TRADER_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::PS_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
            ],
            self::STATUS_PAYMENT_COMPLETED_TRADER => [
                TraderDealMessage::MERCHANT_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::SD_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::TRADER_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::PS_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
            ],
            self::STATUS_PAYMENT_COMPLETED_USER => [
                TraderDealMessage::MERCHANT_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::SD_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::TRADER_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::PS_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
            ],
            self::STATUS_DISPUTED => [
                TraderDealMessage::MERCHANT_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::SD_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::TRADER_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::PS_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
            ],
            self::STATUS_COMPLETED => [
                TraderDealMessage::MERCHANT_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::SD_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::TRADER_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::PS_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
            ],
            self::STATUS_CANCELED_USER => [
                TraderDealMessage::MERCHANT_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::SD_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::TRADER_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::PS_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
            ],
            self::STATUS_CANCELED_TRADER => [
                TraderDealMessage::MERCHANT_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::SD_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::TRADER_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::PS_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
            ],
            self::STATUS_CANCELED_SD => [
                TraderDealMessage::MERCHANT_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::SD_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::TRADER_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::PS_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
            ],
            self::STATUS_CANCELED_TIMEOUT => [
                TraderDealMessage::MERCHANT_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::SD_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::TRADER_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::PS_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
            ],
            self::STATUS_CANCELED => [
                TraderDealMessage::MERCHANT_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::SD_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::TRADER_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::PS_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
            ],
            self::STATUS_TRADER_FUNDED => [
                TraderDealMessage::MERCHANT_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::SD_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::TRADER_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
                TraderDealMessage::PS_USER => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_PAYMENT_COMPLETED_USER,
                    self::STATUS_DISPUTED,
                    self::STATUS_COMPLETED,
                    self::STATUS_CANCELED_USER,
                    self::STATUS_CANCELED_TRADER,
                    self::STATUS_CANCELED_SD,
                    self::STATUS_CANCELED,
                    self::STATUS_TRADER_FUNDED,
                ],
            ],
        ];
    }

    public static function getCustomerType(){
        return [
            self::MERCHANT_USER => [
                'instance' => Merchant::class,
                'label' => 'Merchant'
            ],
            self::SD_USER => [
                'instance' => SdUser::class,
                'label' => 'SD User'
            ],
            self::TRADER_USER => [
                'instance' => Trader::class,
                'label' => 'Trader'
            ],
            self::PS_USER => [
                'instance' => User::class,
                'label' => 'PS User'
            ],
            self::SYSTEM => [
                'instance' => '',
                'label' => 'System'
            ],
        ];
    }

    public function currency()
    {
        return $this->hasOne(TraderCurrency::class, 'id', 'currency_id');
    }

    static public function getTraderDealsWaiting()
    {
        $check_interval = config('app.trader_deals_check_time');
        $datetime = Carbon::now()->addSeconds(-$check_interval);
        return self::whereIn('status', [self::STATUS_CREATE, self::STATUS_TRADER_FUNDED])->with('statusHistory')
            ->orderBy('id', 'asc')->where('checked_at', '<=', $datetime)
            ->orWhereNull('checked_at')->get();
    }

    public function statusHistory(){
        return $this->hasOne(TraderDealHistory::class, 'id', 'last_history_id');
    }

    public static function getCustomerTypeByUser($user){
        switch ($user::class){
            case Merchant::class:
                return self::MERCHANT_USER;
            case User::class:
                return self::PS_USER;
            case Trader::class:
                return self::TRADER_USER;
            case SdUser::class:
                return self::SD_USER;
        }
    }

    public static function isCancelStatus($status): bool
    {
        return in_array($status, [
            self::STATUS_CANCELED_BY_REPLY_TIMEOUT,
            self::STATUS_CANCELED_TIMEOUT,
            self::STATUS_CANCELED_SD,
            self::STATUS_CANCELED_TRADER,
            self::STATUS_CANCELED_USER,
            self::STATUS_CANCELED,
        ]);
    }

}
