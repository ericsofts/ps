<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerchantProperty extends Model
{
    use HasFactory;

    protected $fillable = [
        'merchant_id', 'property', 'value',
    ];

    public static function getProperty($merchant_id, $property, $default = null, $service_provider_id = null)
    {
        $where = ['merchant_id' => $merchant_id, 'property' => $property];
        if ($service_provider_id) {
            $where['service_provider_id'] = $service_provider_id;
        }
        $item = self::where($where)->first();
        return $item->value ?? $default;
    }

    /**
     * @param $merchant_id
     * @return array
     */
    public static function getProperties($merchant_id)
    {
        $properties = [];
        foreach (self::where(['merchant_id' => $merchant_id])->get() as $property) {
            $properties[$property->property] = $property->value;
        }
        return $properties;
    }

    public static function getPropertiesByName($merchant_id, $name)
    {
        $properties = [];
        foreach (self::where([
            'merchant_id' => $merchant_id,
            'property' => $name
        ])->get() as $property) {
            $properties[] = $property->value;
        }
        return $properties;
    }
}
