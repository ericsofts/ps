<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceCryptoExtend extends Model
{
    use HasFactory;

    const INVOICE_TYPE_PAYMENT_INVOICE = 0;
    const INVOICE_TYPE_MERCHANT_WITHDRAWAL = 1;
    const INVOICE_TYPE_MERCHANT_INPUT_INVOICE = 2;
    const INVOICE_TYPE_AGENT_WITHDRAWAL = 3;
    const INVOICE_TYPE_SYSTEM_BALANCE_WITHDRAWAL = 4;
    const INVOICE_TYPE_SERVICE_BALANCE_WITHDRAWAL = 5;
    const INVOICE_TYPE_USER_INPUT_INVOICE = 6;

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_USER_CONFIRMED = 2;
    const STATUS_TRADER_CONFIRMED = 5;
    const STATUS_CANCELED = 99;

    protected $fillable = [
        'invoice_id',
        'invoice_type',
        'crypto_address',
        'coin',
        'status',
        'currency_id',
        'address_amount',
        'additional_info',
        'amount',
        'cancelation_reason',
        'payed',
        'checked_at',
        'comment'
    ];

    public static function getInvoiceTypeByInstance($invoice): bool|int
    {
        switch ($invoice){
            case $invoice instanceof MerchantInputInvoice:
                return self::INVOICE_TYPE_MERCHANT_INPUT_INVOICE;
            case $invoice instanceof MerchantWithdrawalInvoice:
                return self::INVOICE_TYPE_MERCHANT_WITHDRAWAL;
            case $invoice instanceof PaymentInvoice:
                return self::INVOICE_TYPE_PAYMENT_INVOICE;
            case $invoice instanceof InputInvoice:
                return self::INVOICE_TYPE_USER_INPUT_INVOICE;
        }
        return false;
    }

    public static function isCancelStatus($status): bool
    {
        return in_array($status, [self::STATUS_CANCELED]);
    }

    public static function getCancelStatuses()
    {
        return [self::STATUS_CANCELED];
    }
}
