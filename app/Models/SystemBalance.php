<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemBalance extends Model
{
    use HasFactory;

    const NAME_GROW = 'grow';
    const NAME_SERVICE = 'service';

    protected $fillable = [
        'name', 'amount'
    ];
}
