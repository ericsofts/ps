<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class BalanceQueue extends Model
{
    use HasFactory;

    const TYPE_CREDIT = 0;
    const TYPE_DEBIT = 1;

    const CURRENCY_USD = 'USD';
    const CURRENCY_GROW_TOKEN = 'GRT';

    const STATUS_WAITING = 0; //waiting
    const STATUS_DONE = 1; //done
    const STATUS_PROCESSING = 2; //processing
    const STATUS_CONFIRMATION = 3; //confirmation
    const STATUS_FAILED = 99; //failed

    protected $fillable = [
        'transaction_id', 'user_id', 'merchant_id', 'amount', 'type', 'status', 'done', 'description', 'input_invoice_id', 'payment_invoice_id',
        'currency', 'balance_id'
    ];

    /**
     * @param $user_id
     * @param $amount
     * @param string $currency
     * @param $type
     * @param $description
     * @param null $input_invoice_id
     * @param null $payment_invoice_id
     * @param null $merchant_id
     * @param int $status
     * @return bool
     */
    static public function addToBalanceQueue($user_id, $amount, $type, $description, $input_invoice_id = null, $payment_invoice_id = null, $merchant_id = null, $status = self::STATUS_WAITING, $currency = self::CURRENCY_USD): bool
    {
        $transaction_id = Str::uuid();
        $message = __('Action: addToBalanceQueue
        [
            transaction_id =  :transaction_id
            status =  :status
            user_id = :user_id
            merchant_id = :merchant_id
            amount = :amount
            currency = :currency
            type = :type
            description = :description
            input_invoice_id = :input_invoice_id
            payment_invoice_id = :payment_invoice_id
        ]',
            [
                'transaction_id' => $transaction_id,
                'status' => $status,
                'user_id' => $user_id,
                'merchant_id' => $merchant_id,
                'amount' => $amount,
                'currency' => $currency,
                'type' => $type,
                'description' => $description,
                'input_invoice_id' => $input_invoice_id,
                'payment_invoice_id' => $payment_invoice_id,
            ]);
        Log::channel('balance_queue')->info('--------------------------------------------------------');
        Log::channel('balance_queue')->info($message);

        try {
            $balance = Balance::firstOrCreate(['user_id' => $user_id, 'currency' => $currency]);
            self::create([
                'transaction_id' => $transaction_id,
                'status' => $status,
                'user_id' => $user_id,
                'merchant_id' => $merchant_id,
                'input_invoice_id' => $input_invoice_id,
                'payment_invoice_id' => $payment_invoice_id,
                'amount' => $amount,
                'currency' => $currency,
                'type' => $type,
                'description' => $description,
                'balance_id' => $balance->id
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $message = __('Failed to queue request  TransactionID = :transaction_id ', ['transaction_id' => $transaction_id]);
            Log::channel('balance_queue')->info($message);
            Log::channel('balance_queue')->info($e->getMessage());
            Log::channel('balance_queue')->info('--------------------------------------------------------');
            return false;
        }
        return true;
    }

    static public function getBalanceQueueWaiting()
    {
        return self::where(['balance_queues.status' => self::STATUS_WAITING])
            ->orderBy('id', 'asc')
            ->first();
    }
}
