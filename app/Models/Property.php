<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;
    /**
     *  OBJECT_NAME
     */
    const OBJECT_NAME = 'system';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'obj_name', 'obj_id', 'property', 'property_value',
    ];

    /**
     * @param $object_name
     * @param $object_id
     * @param $property
     * @param $default
     * @return Property
     */
    public static function get($object_name, $object_id, $property)
    {
        return self::where(['obj_name' => $object_name, 'obj_id' => $object_id, 'property' => $property])->first();
    }

    /**
     * @param $object_name
     * @param $object_id
     * @param $property
     * @param $default
     * @return string|null|integer
     */
    public static function getProperty($object_name, $object_id, $property, $default = null)
    {
        $property = self::where(['obj_name' => $object_name, 'obj_id' => $object_id, 'property' => $property])->first();
        return ($property->property_value) ?? $default;
    }

    /**
     * @param $object_name
     * @param $object_id
     * @param $property
     * @param $new_value
     */
    public static function setProperty($object_name, $object_id, $property, $new_value = null)
    {
        $property = self::where(['obj_name' => $object_name, 'obj_id' => $object_id, 'property' => $property])->first();
        if ($property && $new_value != '' && !is_null($new_value) && $property->property_value != $new_value) {
            $property->property_value = $new_value;
            $property->save();
        }
    }

    /**
     * @param $object_name
     * @param $object_id
     * @return array
     */
    public static function getProperties($object_name, $object_id = 0)
    {
        $properties = [];
        foreach (self::where(['obj_name' => $object_name, 'obj_id' => $object_id])->get() as $property) {
            $properties[$property->property] = $property->property_value;
        }
        return $properties;
    }
}
