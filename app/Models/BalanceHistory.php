<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BalanceHistory extends Model
{
    use HasFactory;

    const TYPE_CREDIT = 0;
    const TYPE_DEBIT = 1;

    const CURRENCY_USD = 'USD';
    const CURRENCY_GROW_TOKEN = 'GRT';

    protected $fillable = [
        'transaction_id', 'user_id', 'merchant_id', 'amount', 'type', 'status', 'done', 'description', 'input_invoice_id', 'payment_invoice_id',
        'currency', 'balance', 'balance_id'
    ];
}
