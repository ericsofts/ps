<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TraderBalanceQueue extends Model
{
    use HasFactory;

    const DEAL = 0;
    const INPUT = 1;
    const OUTPUT = 2;

    const TYPE_CREDIT = 0;
    const TYPE_DEBIT = 1;

    const STATUS_WAITING = 0; //waiting
    const STATUS_DONE = 1; //done
    const STATUS_PROCESSING = 2; //processing
    const STATUS_FAILED = 99; //failed

    protected $fillable = [
        'amount',
        'balance',
        'balance_id',
        'balance_type',
        'currency', // currency asset
        'currency_id',
        'description',
        'invoice_id',
        'invoice_type',
        'status',
        'trader_id',
        'type',
    ];

    static public function getTraderBalanceQueueWaiting()
    {
        return self::where(['trader_balance_queues.status' => self::STATUS_WAITING])
            ->orderBy('id', 'asc')
            ->first();
    }

}
