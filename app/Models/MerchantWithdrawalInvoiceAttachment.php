<?php

namespace App\Models;

use App\Lib\Chatex\ChatexApiLBC;
use App\Services\ContractManager;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class MerchantWithdrawalInvoiceAttachment extends Model
{
    use HasFactory;


    protected $fillable = [
        'invoice_id', 'type', 'url'
    ];

    public function invoice()
    {
        return $this->hasOne(MerchantWithdrawalInvoice::class, 'id', 'invoice_id');
    }

    static public function addAttachments(ContractManager $contractManager, $invoice)
    {
        $messages = $contractManager->contactMessagesByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
        $new_attachments = new Collection();
        if (!empty($messages)) {
            $attachments = self::where(['invoice_id' => $invoice->id])->get()->pluck('url');
            foreach ($messages as $message) {
                if (!empty($message['attachment_url']) && str_contains($message['attachment_type'], 'image') && !$attachments->contains($message['attachment_url'])) {
                    $new_attachments->add(MerchantWithdrawalInvoiceAttachment::firstOrCreate([
                        'invoice_id' => $invoice->id,
                        'type' => $message['attachment_type'],
                        'url' => $message['attachment_url']
                    ]));
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                        7,
                        InvoicesHistory::getStatus(1),
                        InvoicesHistory::getSource(3),
                        InvoicesHistory::getSource(2),
                        json_encode($messages),
                        'Add Attachment'
                    );
                }
            }
        }
        return $new_attachments;
    }

}
