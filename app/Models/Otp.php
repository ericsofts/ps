<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JetBrains\PhpStorm\Pure;

class Otp extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'merchant_id', 'amount', 'key', 'code', 'expired_at', 'status', 'invoice_id', 'invoice_type'
    ];

    static public function generateCode($digits = 4)
    {
        $i = 0;
        $code = "";
        while($i < $digits){
            $code .= mt_rand(0, 9);
            $i++;
        }
        return $code;
    }
}
