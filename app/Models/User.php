<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    const STATUS_BLOCKED = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
        'account_id',
        'secret_2fa',
        'enable_2fa'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function balance($currency = BalanceQueue::CURRENCY_USD)
    {
        return Balance::where(['user_id' => $this->id, 'currency' => $currency])->first();
    }

    public function balance_to_day($to, $currency = BalanceQueue::CURRENCY_USD)
    {
        $balance =  BalanceHistory::where(['user_id' => $this->id, 'currency' => $currency])
            ->where('created_at', '<=', $to)->orderBy('id','desc')->first()->balance ?? 0;
        $balanceDebitQueue = BalanceQueue::where([
            'user_id' => $this->id,
            'currency' => $currency,
            'type' => BalanceQueue::TYPE_DEBIT,
        ])
            ->where('created_at', '<=', $to)
            ->whereNotIn('status', [BalanceQueue::STATUS_DONE, BalanceQueue::STATUS_FAILED])
            ->sum('amount');
        return round($balance + $balanceDebitQueue, 8);
    }

    public function balance_change($from, $to, $currency = BalanceQueue::CURRENCY_USD)
    {
        $balanceDebitQueue = BalanceQueue::where([
            'user_id' => $this->id,
            'currency' => $currency,
        ])
            ->where('created_at', '<=', $to)
            ->where('created_at', '>=', $from)
            ->whereNotIn('status', [BalanceQueue::STATUS_FAILED])
            ->sum('amount');
        return round($balanceDebitQueue, 8);
    }

    public function balance_grow_to_day($to)
    {
        $currency = BalanceQueue::CURRENCY_GROW_TOKEN;
        $balanceGrow =  BalanceHistory::where(['user_id' => $this->id, 'currency' => $currency])
            ->where('created_at', '<=', $to)->orderBy('id','desc')->first()->balance ?? 0;
        $balanceDebitQueueGT = BalanceQueue::where([
            'user_id' => $this->id,
            'currency' => $currency,
            'type' => BalanceQueue::TYPE_DEBIT,
        ])
            ->where('created_at', '<=', $to)
            ->whereNotIn('status', [BalanceQueue::STATUS_DONE, BalanceQueue::STATUS_FAILED])
            ->sum('amount');
        $growTokenInvoiceSum = GrowTokenInvoice::where([
            'user_id' => $this->id,
            'status' => GrowTokenInvoice::STATUS_CREATED
        ])
            ->where('created_at', '<=', $to)
            ->sum('amount');
        return round($balanceGrow + $balanceDebitQueueGT - $growTokenInvoiceSum, 8);
    }

    public function balance_grow_change($from, $to)
    {
        $currency = BalanceQueue::CURRENCY_GROW_TOKEN;
        $balanceDebitQueueGT = BalanceQueue::where([
            'user_id' => $this->id,
            'currency' => $currency,
        ])
            ->where('created_at', '<=', $to)
            ->where('created_at', '>=', $from)
            ->whereNotIn('status', [BalanceQueue::STATUS_FAILED])
            ->sum('amount');
        $growTokenInvoiceSum = GrowTokenInvoice::where([
            'user_id' => $this->id,
            'status' => GrowTokenInvoice::STATUS_CREATED
        ])
            ->where('created_at', '<=', $to)
            ->where('created_at', '>=', $from)
            ->sum('amount');
        return round($balanceDebitQueueGT - $growTokenInvoiceSum, 8);
    }

    public function balance_histories()
    {
        return $this->hasMany(BalanceHistory::class, 'user_id');
    }

    public function input_invoices()
    {
        return $this->hasMany(InputInvoice::class, 'user_id');
    }

    public function grow_token_invoices()
    {
        return $this->hasMany(GrowTokenInvoice::class, 'user_id');
    }

    public function balance_total($curency = BalanceQueue::CURRENCY_USD): float
    {

        $balance = $this->balance($curency)->amount ?? 0;
        $balanceDebitQueue = BalanceQueue::where([
            'user_id' => $this->id,
            'currency' => $curency,
            'type' => BalanceQueue::TYPE_DEBIT,
        ])
            ->whereNotIn('status', [BalanceQueue::STATUS_DONE, BalanceQueue::STATUS_FAILED])
            ->sum('amount');

        return round($balance + $balanceDebitQueue, 6);
    }

    public function balance_grow_token_total(): float
    {

        $balanceGrow = $this->balance('GRT')->amount ?? 0;
        $balanceDebitQueueGT = BalanceQueue::where([
            'user_id' => $this->id,
            'currency' => BalanceQueue::CURRENCY_GROW_TOKEN,
            'type' => BalanceQueue::TYPE_DEBIT,
        ])
            ->whereNotIn('status', [BalanceQueue::STATUS_DONE, BalanceQueue::STATUS_FAILED])
            ->sum('amount');

//        $growTokenInvoiceSum = GrowTokenInvoice::where([
//            'user_id' => $this->id,
//            'status' => GrowTokenInvoice::STATUS_CREATED
//        ])
//            ->sum('amount');
        return round($balanceGrow + $balanceDebitQueueGT, 8);
    }

    public static function generateAccountId(): int
    {
        $num = mt_rand(100000000000, 999999999999);
        if (self::where('account_id', '=', $num)->exists()) {
            return self::generateAccountId();
        }

        return $num;
    }


    public function getFeeCommission($amount)
    {
        $commissions =  json_decode(Property::getProperty('users', '0', 'user_commission', '[]'), true);

        $fee['service'] = [
            'amount' => $commissions['service'] * $amount / 100,
            'commission' => $commissions['service']
        ];
        $fee['grow'] = [
            'amount' => $commissions['grow'] * $amount / 100,
            'commission' => $commissions['grow']
        ];

        $fee['total'] = $fee['service']['amount'] + $fee['grow']['amount'];

        return $fee;
    }
}
