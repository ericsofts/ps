<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InputInvoice extends Model
{
    use HasFactory;

    const PAYMENT_SYSTEM_CHATEX = 1;
    const PAYMENT_SYSTEM_CHATEX_LBC = 2;
    const PAYMENT_SYSTEM_USDT = 3;
    const PAYMENT_SYSTEM_TRADER = 4;

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_USER_CONFIRMED = 2;
    const STATUS_USER_SELECTED = 5;
    const STATUS_TRADER_CONFIRMED = 3;
    const STATUS_CANCELED = 99;

    protected $fillable = [
        'user_id', 'amount', 'payment_system', 'payment_id', 'addition_info', 'status', 'payed', 'checked_at',
        'ps_amount', 'ps_currency', 'usdt_address_id', 'service_provider_id', 'fiat_amount', 'fiat_currency', 'amount2service',
        'amount2grow', 'commission_grow', 'commission_service', 'amount2pay', 'grow_token_amount', 'grow_token_rate',
        'input_currency', 'input_rate', 'input_amount_value', 'grow_token_amount2pay', 'is_auto_grt'
    ];

    public function usdt_address()
    {
        return $this->hasOne(UsdtAddress::class, 'id','usdt_address_id');
    }
}
