<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderCurrency extends Model
{
    use HasFactory;

    protected $fillable = [
        'asset', 'crypto_asset', 'name', 'precision', 'status', 'can_get_address'
    ];
}
