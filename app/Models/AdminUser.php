<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminUser extends Model
{
    use HasFactory;

    const STATUS_BLOCKED = 0;

    protected $fillable = [
        'name', 'email', 'password', 'status', 'email_verified_at'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

}
