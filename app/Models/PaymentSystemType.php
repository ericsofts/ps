<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PaymentSystemType extends Model
{
    use HasFactory;

    const CARD_NUMBER = 'card_number';
    const IBAN = 'iban';
    const UPI = 'upi';
    const CRYPTO = 'crypto';
    const QIWI = 'qiwi';

    protected $fillable = [
        'obj_name', 'name'
    ];

}
