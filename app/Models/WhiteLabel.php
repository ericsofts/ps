<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class WhiteLabel extends Model
{
    use HasFactory;

    const PREFIX = 'body';
    const POSTFIX = '.api';

    protected $defaultStyle = null;

    protected $merchantStyle = null;

    protected $fillable = [
        'style', 'enable', 'merchant_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->style = $model->generateDefaultStyle();
        });
    }

    public function getWhiteLabelById($id)
    {
        $model = $this->where('merchant_id', $id)->first();
        if($model){
            return $model;
        }
        return $this;
    }

    public function isEnable(){
        return $this->enable;
    }

    protected function getDefaultStyle(){
        if(!$this->defaultStyle){
            $this->defaultStyle = json_decode(config('whitelabel.default'), true);
        }
        return $this->defaultStyle;
    }

    protected function getMerchantStyle(){
        if(!$this->merchantStyle){
            $this->merchantStyle = json_decode($this->style, true);
        }
        return $this->merchantStyle;
    }

    protected function getPageStyle($style){
        if(isset($style['page'])){
            return $style['page'];
        }
        return [];
    }

    protected function getModalStyle($style){
        if(isset($style['modal'])){
            return $style['modal'];
        }
        return [];
    }

    public function getCss(){
        $css = '<style>';
        $css .= $this->getStyleItem('page', 'background_color', 'background','');
        $css .= $this->getStyleItem('page', 'primary_font_color', 'color','');
        $css .= $this->getStyleItem('page', 'secondary_font_color', 'color',' .secondary-whitelable-text');
        $css .= $this->getStyleItem('page', 'danger_font_color', 'color',' .danger-whitelable-text');
        $css .= $this->getStyleItem('page', 'btn_primary_font_color', 'color',' .btn-primary-whitelabel');
        $css .= $this->getStyleItem('page', 'btn_primary_background_color', 'background',' .btn-primary-whitelabel');
        $css .= $this->getStyleItem('page', 'btn_primary_border_color', 'border-color',' .btn-primary-whitelabel');
        $css .= $this->getStyleItem('page', 'btn_primary_hover_font_color', 'color',' .btn-primary-whitelabel:hover');
        $css .= $this->getStyleItem('page', 'btn_primary_hover_background_color', 'background',' .btn-primary-whitelabel:hover');
        $css .= $this->getStyleItem('page', 'btn_primary_hover_border_color', 'border-color',' .btn-primary-whitelabel:hover');
        $css .= $this->getStyleItem('page', 'btn_secondary_font_color', 'color',' .btn-secondary-whitelabel');
        $css .= $this->getStyleItem('page', 'btn_secondary_background_color', 'background',' .btn-secondary-whitelabel');
        $css .= $this->getStyleItem('page', 'btn_secondary_border_color', 'border-color',' .btn-secondary-whitelabel');
        $css .= $this->getStyleItem('page', 'btn_secondary_hover_font_color', 'color',' .btn-secondary-whitelabel:hover');
        $css .= $this->getStyleItem('page', 'btn_secondary_hover_background_color', 'background',' .btn-secondary-whitelabel:hover');
        $css .= $this->getStyleItem('page', 'btn_secondary_hover_border_color', 'border-color',' .btn-secondary-whitelabel:hover');
        $css .= $this->getStyleItem('page', 'alert_background', 'background',' .alert-grow');
        $css .= $this->getStyleItem('page', 'alert_border', 'border-color',' .alert-grow');
        $css .= $this->getStyleItem('page', 'alert_color', 'color',' .alert-grow');


        $css .= $this->getStyleItem('modal', 'background_color', 'background',' .swal2-popup');
        $css .= $this->getStyleItem('modal', 'warning_color', 'color',' .swal2-grow .swal2-icon.swal2-warning');
        $css .= $this->getStyleItem('modal', 'warning_color', 'border-color',' .swal2-grow .swal2-icon.swal2-warning');
        $css .= $this->getStyleItem('modal', 'danger_font_color', 'color',' .swal2-grow .text-danger', true);
        $css .= $this->getStyleItem('modal', 'btn_primary_font_color', 'color',' .btn-grow');
        $css .= $this->getStyleItem('modal', 'btn_primary_background_color', 'background',' .btn-grow');
        $css .= $this->getStyleItem('modal', 'btn_primary_border_color', 'border-color',' .btn-grow');
        $css .= $this->getStyleItem('modal', 'btn_primary_hover_font_color', 'color',' .btn-grow:hover');
        $css .= $this->getStyleItem('modal', 'btn_primary_hover_background_color', 'background',' .btn-grow:hover');
        $css .= $this->getStyleItem('modal', 'btn_primary_hover_border_color', 'border-color',' .btn-grow:hover');
        $css .= $this->getStyleItem('modal', 'btn_secondary_font_color', 'color',' .btn-link');
        $css .= $this->getStyleItem('modal', 'btn_secondary_background_color', 'background',' .btn-link');
        $css .= $this->getStyleItem('modal', 'btn_secondary_border_color', 'border-color',' .btn-link');
        $css .= $this->getStyleItem('modal', 'btn_secondary_hover_font_color', 'color',' .btn-link:hover');
        $css .= $this->getStyleItem('modal', 'btn_secondary_hover_background_color', 'background',' .btn-link:hover');
        $css .= $this->getStyleItem('modal', 'btn_secondary_hover_border_color', 'border-color',' .btn-link:hover');
        $css .= $this->getStyleItem('modal', 'btn_secondary_underline', 'text-decoration',' .btn-link');

        $css .= '</style>';
        return $css;
    }

    protected function getStyleItem($type, $index, $cssItem, $selector = '', $important = false)
    {
        if($type == 'page'){
            $merchantStyle = $this->getPageStyle($this->getMerchantStyle());
            $defaultStyle = $this->getPageStyle($this->getDefaultStyle());
        }else if($type == 'modal'){
            $merchantStyle = $this->getModalStyle($this->getMerchantStyle());
            $defaultStyle = $this->getModalStyle($this->getDefaultStyle());
        }else{
            return '';
        }
        if($this->isEnable()){
            $value = $merchantStyle[$index] ?: $defaultStyle[$index];
        }else{
            $value = $defaultStyle[$index];
        }
        $template = $this->getStyleTeplate($cssItem, $value, $important);
        $style = self::PREFIX.self::POSTFIX.$selector.$template;
        return $style;
    }

    protected function getStyleTeplate($cssItem, $value, $important){
        if($important){
            return __('{:type : :value !important;}', ['type' => $cssItem, 'value' => $value]);
        }
        return __('{:type : :value;}', ['type' => $cssItem, 'value' => $value]);
    }

    public function getPageText($index){
        $merchantText = $this->getPageStyle($this->getMerchantStyle());
        $defaultText = $this->getPageStyle($this->getDefaultStyle());
        if($this->isEnable()){
            if(isset($merchantText[$index])){
                return $merchantText[$index] ?: $defaultText[$index];
            }
            return $defaultText[$index];
        }
        return $defaultText[$index];
    }

    public function getModalValue($index){
        $merchantText = $this->getModalStyle($this->getMerchantStyle());
        $defaultText = $this->getModalStyle($this->getDefaultStyle());
        if(isset($merchantText[$index])){
            return $merchantText[$index] ?: $defaultText[$index];
        }
        return $defaultText[$index];

    }

    public function getPageValue($index){
        $merchantText = $this->getPageStyle($this->getMerchantStyle());
        $defaultText = $this->getPageStyle($this->getDefaultStyle());
        if(isset($merchantText[$index])){
            return $merchantText[$index] ?: $defaultText[$index];
        }
        return $defaultText[$index];
    }

    public function getImageLogo($viewTheme = '')
    {
        $merchantText = $this->getPageStyle($this->getMerchantStyle());
        $defaultText = $this->getPageStyle($this->getDefaultStyle());
        if ($viewTheme == 2) {
            $defaultText['image_logo'] = '/images/logo.svg';
        }

        if($this->isEnable()){
            if(isset($merchantText['image_logo']) && ($merchantText['image_logo'] != $defaultText['image_logo'])){
                return $merchantText['image_logo'] ? Storage::disk('whitelabel')->url($merchantText['image_logo']): $defaultText['image_logo'];
            }
            return $defaultText['image_logo'];
        }
        return $defaultText['image_logo'];
    }

    public function getImageLogoValue(){
        $merchantText = $this->getPageStyle($this->getMerchantStyle());
        $defaultText = $this->getPageStyle($this->getDefaultStyle());
        if(isset($merchantText['image_logo']) && ($merchantText['image_logo'] != $defaultText['image_logo'])){
           return $merchantText['image_logo'] ? Storage::disk('whitelabel')->url($merchantText['image_logo']): $defaultText['image_logo'];
        }
        return $defaultText['image_logo'];
    }

    public function generateDefaultStyle(){
        $config = json_decode(config('whitelabel.default'), true);
        unset($config['page']['image_logo']);
        return json_encode($config);
    }
}
