<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TraderInputInvoice extends Model
{
    use HasFactory;

    protected $fillable = [
        'trader_id', 'currency_id', 'amount', 'address_id', 'balance_id', 'additional_info'
    ];

    public function currency()
    {
        return $this->hasOne(TraderCurrency::class, 'id', 'currency_id');
    }

}
