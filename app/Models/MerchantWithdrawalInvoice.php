<?php

namespace App\Models;

use App\Lib\Chatex\ChatexApi;
use App\Traits\ServiceProviderTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class MerchantWithdrawalInvoice extends Model
{
    use HasFactory;

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_TRADER_CONFIRMED_PAYMENT = 2;
    const STATUS_TRADER_CONFIRMED = 3;
    const STATUS_USER_SELECTED = 5;
    const STATUS_IN_PROCESS = 4;
    const STATUS_CANCELED_BY_TIMEOUT = 98;
    const STATUS_CANCELED = 99;

    const PAYMENT_SYSTEM_MANUALLY = 0;
    const PAYMENT_SYSTEM_CHATEX_LBC = 1;

    const API_TYPE_0 = 0;
    const API_TYPE_3 = 3;
    const API_TYPE_5 = 5;

    protected $fillable = [
        'merchant_id', 'amount', 'amount2pay', 'amount2service', 'amount2agent', 'amount2grow', 'commission_grow', 'commission_agent',
        'commission_service', 'addition_info', 'status', 'payed', 'api_type', 'payment_id', 'payment_system', 'checked_at',
        'checked_at', 'ps_amount', 'ps_currency', 'comment', 'fiat_amount', 'fiat_currency', 'account_info', 'merchant_order_id',
        'merchant_order_desc', 'merchant_response_url', 'merchant_server_url', 'sd_user_id', 'merchant_expense',
        'merchant_cancel_url', 'merchant_response_at', 'merchant_cancel_at', 'input_currency', 'input_rate', 'input_amount_value',
        'currency2currency', 'merchant_return_url', 'service_provider_id', 'exact_currency'
    ];

    public function service_provider()
    {
        return $this->hasOne(ServiceProvider::class,'id', 'service_provider_id');
    }

    public static function isCancelStatus($status): bool
    {
        return in_array($status, [self::STATUS_CANCELED, self::STATUS_CANCELED_BY_TIMEOUT]);
    }

    public static function getCancelStatuses()
    {
        return [self::STATUS_CANCELED, self::STATUS_CANCELED_BY_TIMEOUT];
    }

    public function attachment()
    {
        return $this->hasOne(MerchantWithdrawalInvoiceAttachment::class, 'invoice_id')->latest();
    }
}
