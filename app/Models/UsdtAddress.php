<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsdtAddress extends Model
{
    use HasFactory;

    const STATUS_UNUSED = 'unused';
    const STATUS_ASSIGNED = 'assigned';
    const STATUS_USED = 'used';

    const CONTRACT_ADDRESS = '0xdac17f958d2ee523a2206206994597c13d831ec7';

    protected $guarded = [];

    protected $fillable = [
        'usdt_address', 'block', 'status', 'total_received_funds', 'amount_correction',
        'assigned_at', 'joined_at', 'xpub_id', 'addr_index', 'join_info', 'join_value'
    ];
}
