<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountingQueue extends Model
{
    use HasFactory;

    const STATUS_WAITING = 0; //waiting
    const STATUS_DONE = 1; //done
    const STATUS_PROCESSING = 2; //processing
    const STATUS_CONFIRMATION = 3; //confirmation
    const STATUS_FAILED = 99; //failed

    protected $fillable = [
        'type', 'user_type', 'user_id', 'amount', 'balance_after', 'invoice_id', 'invoice_type', 'status', 'created_at'
    ];

    static public function getAccountingQueueWaiting()
    {
        return self::where(['accounting_queues.status' => self::STATUS_WAITING])
            ->orderBy('id', 'asc')
            ->first();
    }
}
