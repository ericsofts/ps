<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentInvoice extends Model
{
    use HasFactory;

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_USER_CONFIRMED = 2;
    const STATUS_TRADER_CONFIRMED = 3;
    const STATUS_USER_SELECTED = 5;
    const STATUS_CANCELED_BY_REVERT = 97;
    const STATUS_CANCELED_BY_TIMEOUT = 98;
    const STATUS_CANCELED = 99;

    const STATUS_ALL_PAYED = 51;
    const STATUS_INVOICE_PAYED_CRYPYO_CREATE = 50;
    const STATUS_INVOICE_PAYED_CRYPYO_CANCEL = 59;
    const STATUS_INVOICE_PAYED_CRYPYO_PROCESS = 55;

    const API_TYPE_0 = 0;
    const API_TYPE_1 = 1;
    const API_TYPE_2 = 2;
    const API_TYPE_3 = 3;
    const API_TYPE_4 = 4;
    const API_TYPE_5 = 5;
    const API_TYPE_6 = 6;

    protected $fillable = [
        'invoice_number', 'user_id', 'amount', 'amount2pay', 'amount2merchant', 'amount2service', 'amount2agent', 'amount2grow', 'commission_grow',
        'commission_agent', 'commission_service', 'merchant_id', 'otp_id', 'addition_info', 'status', 'payed', 'merchant_order_id', 'merchant_order_desc',
        'merchant_response_url', 'merchant_server_url', 'api_type', 'payment_id', 'checked_at', 'ps_amount', 'ps_currency', 'fiat_amount', 'fiat_currency',
        'input_currency', 'input_rate', 'input_amount_value', 'comments','isMerchant', 'grow_token_amount', 'account_info',
        'merchant_cancel_url', 'merchant_response_at', 'merchant_cancel_at', 'currency2currency', 'merchant_return_url',
        'service_provider_id', 'exact_currency'
    ];

    public static function generateOrderNumber(): int
    {
        $num = mt_rand(100000000000, 999999999999);
        if (self::where('invoice_number', '=', $num)->exists()) {
            return self::generateOrderNumber();
        }

        return $num;
    }

    public static function isCancelStatus($status): bool
    {
        return in_array($status, [self::STATUS_CANCELED, self::STATUS_CANCELED_BY_TIMEOUT, self::STATUS_CANCELED_BY_REVERT]);
    }

    public static function getCancelStatuses()
    {
        return [self::STATUS_CANCELED, self::STATUS_CANCELED_BY_TIMEOUT, self::STATUS_CANCELED_BY_REVERT];
    }

    public function service_provider()
    {
        return $this->hasOne(ServiceProvider::class, 'id', 'service_provider_id');
    }

    public function crypto_address()
    {
        return $this->hasOne(CryptoPaymentInvoice::class, 'id', 'payment_id');
    }

    public function getStatus(){
        if(($this->api_type == self::API_TYPE_6) && ($this->status == self::STATUS_PAYED)){
            $invoiceCryptoExtend = InvoiceCryptoExtend::where('invoice_type', InvoiceCryptoExtend::getInvoiceTypeByInstance($this))
                ->where('invoice_id', $this->id)
                ->first();
            switch ($invoiceCryptoExtend->status){
                case InvoiceCryptoExtend::STATUS_CREATED:
                    $status = self::STATUS_INVOICE_PAYED_CRYPYO_CREATE;
                    break;
                case InvoiceCryptoExtend::STATUS_PAYED:
                    $status = self::STATUS_ALL_PAYED;
                    break;
                case InvoiceCryptoExtend::STATUS_CANCELED:
                    $status = self::STATUS_INVOICE_PAYED_CRYPYO_CANCEL;
                    break;
                default:
                    $status = self::STATUS_INVOICE_PAYED_CRYPYO_PROCESS;
                    break;
            }
            return $status;
        }
        return $this->status;
    }

}
