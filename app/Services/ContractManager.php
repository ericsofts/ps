<?php

namespace App\Services;


use App\Lib\Chatex\ChatexApi;
use App\Lib\Crypto\Cryptoprocessing;
use App\Lib\Crypto\KrakenAPI;
use App\Models\CryptoPaymentInvoice;
use App\Models\ServiceProvider;
use App\Models\TraderDeal;
use App\Models\TraderDealMessage;
use App\Traits\ChatexContract;
use App\Traits\CryptoContract;
use App\Traits\ServiceProviderTrait;
use App\Traits\TraderContract;

class ContractManager
{
    use ServiceProviderTrait;
    use ChatexContract;
    use TraderContract;
    use CryptoContract;

    private $chatexLBC;
    private $ad;

    const STATUS_CANCEL = 99;
    const STATUS_CLOSED = 98;
    const STATUS_COMPLETE = 1;
    const STATUS_FUNDED = 2;
    const STATUS_PAYMENT_COMPLETED = 3;
    const STATUS_DISPUTED = 5;
    const STATUS_DEFAULT = 0;

    private $dealMessageModel;
    private $cryptoPaymentInvoiceModel;
    private $cryptoprocessing;
    private $krakenAPI;

    public function __construct(
        TraderDealMessage $dealMessageModel,
        CryptoPaymentInvoice $cryptoPaymentInvoiceModel,
        Cryptoprocessing $cryptoprocessing,
        KrakenAPI $krakenAPI
    )
    {
        $this->dealMessageModel = $dealMessageModel;
        $this->cryptoPaymentInvoiceModel = $cryptoPaymentInvoiceModel;
        $this->cryptoprocessing = $cryptoprocessing;
        $this->krakenAPI = $krakenAPI;
    }

    public function getChatexLBCConnect()
    {
        if (!$this->chatexLBC) {
            $this->chatexLBC = $this->getChatexLBCConnection();
        }
        return $this->chatexLBC;
    }

    public function contractCreate($ad, $currency_amount, $user = false, $invoice = false, $message = null, $payment_info = null)
    {
        switch ($ad['service_provider_type']) {
            case ServiceProvider::TRADER_PROVIDER:
                return $this->createTraderContract($ad, $currency_amount, $user, $invoice, $message, $payment_info);
            case ChatexApi::OBJECT_NAME:
                return $this->createChatexContract($ad, $currency_amount, $user, $invoice, $message);
            case ServiceProvider::CRYPTO_PROVIDER:
                return $this->createCryptoContract($ad, $currency_amount, $user, $invoice, $message);
        }
        return false;
    }

    public function contractInfoByContract($contract)
    {
        switch ($contract['service_provider_type']) {
            case ServiceProvider::TRADER_PROVIDER:
                return $this->infoTraderContract($contract['id']);
            case ChatexApi::OBJECT_NAME:
                return $this->infoChatexContract($contract['id']);
            case ServiceProvider::CRYPTO_PROVIDER:
                return $this->infoCryptoContract($contract['id']);
        }
        return false;
    }

    public function contractCancelByContract($contract)
    {
        switch ($contract['service_provider_type']) {
            case ServiceProvider::TRADER_PROVIDER:
                return $this->cancelTraderContract($contract['id']);
            case ChatexApi::OBJECT_NAME:
                return $this->cancelChatexContract($contract['id']);
            case ServiceProvider::CRYPTO_PROVIDER:
                return $this->cancelCryptoContract($contract['id']);
        }
        return false;
    }

    public function contactMarkAsPaidByContract($contract)
    {
        switch ($contract['service_provider_type']) {
            case ServiceProvider::TRADER_PROVIDER:
                return $this->markAsPaidTraderContract($contract['id']);
            case ChatexApi::OBJECT_NAME:
                return $this->markAsPaidChatexContract($contract['id']);
            case ServiceProvider::CRYPTO_PROVIDER:
                return $this->markAsPaidCryptoContract($contract['id']);
        }
        return false;
    }

    public function contactReleaseByContract($contract)
    {
        switch ($contract['service_provider_type']) {
            case ServiceProvider::TRADER_PROVIDER:
                return $this->releaseTraderContract($contract['id']);
            case ChatexApi::OBJECT_NAME:
                return $this->releaseChatexContract($contract['id']);
            case ServiceProvider::CRYPTO_PROVIDER:
                return $this->releaseCryptoContract($contract['id']);
        }
        return false;
    }

    public function contactMessagePostByContract($contract)
    {
        switch ($contract['service_provider_type']) {
            case ServiceProvider::TRADER_PROVIDER:
                $path = $this->messagePostTraderContract($contract['document']);
                if ($path) {
                    TraderDealMessage::create([
                        'attachment_type' => 1,
                        'deal_id' => $contract['id'],
                        'msg' => $path,
                        'sender_id' => $contract['user_id'],
                        'sender_type' => TraderDeal::MERCHANT_USER,
                    ]);
                    return true;
                }

                return false;

            case ChatexApi::OBJECT_NAME:
                return $this->messagePostChatexContract($contract['id'], $contract['document']);
            case ServiceProvider::CRYPTO_PROVIDER:
                return $this->messagePostCryptoContract($contract['id'], $contract['document']);
        }
        return false;
    }

    public function contactMessagesByContract($contract)
    {
        switch ($contract['service_provider_type']) {
            case ServiceProvider::TRADER_PROVIDER:
                return $this->contactMessagesTraderContract($contract['id']);
            case ChatexApi::OBJECT_NAME:
                return $this->contactMessagesChatexContract($contract['id']);
            case ServiceProvider::CRYPTO_PROVIDER:
                return $this->contactMessagesCryptoContract($contract['id']);
        }
        return false;
    }


    public function contractInfoByProvider($contract)
    {
        $providerType = $this->getProviderById($contract['service_provider_id'])->type;
        return $this->contractInfoByContract(['id' => $contract['id'], 'service_provider_type' => $providerType]);
    }

    public function contractCancelByProvider($contract)
    {
        $providerType = $this->getProviderById($contract['service_provider_id'])->type;
        return $this->contractCancelByContract(['id' => $contract['id'], 'service_provider_type' => $providerType]);
    }

    public function contactMarkAsPaidByProvider($contract)
    {
        $providerType = $this->getProviderById($contract['service_provider_id'])->type;
        return $this->contactMarkAsPaidByContract(['id' => $contract['id'], 'service_provider_type' => $providerType]);
    }

    public function contactReleaseByProvider($contract)
    {
        $providerType = $this->getProviderById($contract['service_provider_id'])->type;
        return $this->contactReleaseByContract(['id' => $contract['id'], 'service_provider_type' => $providerType]);
    }

    public function contactMessagePostByProvider($contract)
    {
        $providerType = $this->getProviderById($contract['service_provider_id'])->type;
        return $this->contactMessagePostByContract([
            'id' => $contract['id'],
            'service_provider_type' => $providerType,
            'document' => $contract['document'],
            'user_id' => $contract['user_id'] ?? null,
        ]);
    }

    public function contactMessagesByProvider($contract)
    {
        $providerType = $this->getProviderById($contract['service_provider_id'])->type;
        return $this->contactMessagesByContract([
            'id' => $contract['id'],
            'service_provider_type' => $providerType
        ]);
    }

}
