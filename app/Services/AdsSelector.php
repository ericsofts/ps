<?php

namespace App\Services;

use App\Lib\Chatex\ChatexApi;
use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\Property;
use App\Models\ServiceProvider;
use App\Models\TraderAd;
use App\Traits\ChatexAds;
use App\Traits\CryptoAds;
use App\Traits\ServiceProviderTrait;
use App\Traits\TraderAds;
use Illuminate\Support\Facades\Log;

class AdsSelector
{

    use ChatexAds;
    use TraderAds;
    use CryptoAds;

    const TYPE_MERCHANT = 1;
    const TYPE_USER = 2;

    private $merchant;
    private $api;
    private $currency;
    private $amount;
    private $amount2pay;
    private $input_amount;
    private $type;
    private $currency2currency;
    private $exact_currency;
    private $expense;
    private $providers;
    private $currentProvider;
    private $chatexLBC;
    private $propertyLimit;
    private $user;
    private $userType;
    private $coin;

    public function __construct(Merchant $merchantModel)
    {
        $this->merchant = $merchantModel;
    }

    public function getChatexLBCConnect()
    {
        if (!$this->chatexLBC) {
            $this->chatexLBC = $this->getChatexLBCConnection();
        }
        return $this->chatexLBC;
    }

    public function getAdsByMerchant($merchant, $invoice_data){
        $this->merchant = $merchant;
        $this->userType = self::TYPE_MERCHANT;
        $this->setInvoiceData($invoice_data);
        $this->propertyLimit = json_decode(MerchantProperty::getProperty($this->merchant->id, 'provider_limit', '[]'), true);
        $providers = $this->getAvalableProviderByMerchant();
        return $this->getAdsByProviders($providers);
    }

    public function getAdsByUser($user, $invoice_data){
        $this->user = $user;
        $this->userType = self::TYPE_USER;
        $this->setInvoiceData($invoice_data);
        $providers = $this->getAvalableProviderByUser();
        return $this->getAdsByProviders($providers);
    }

    private function setInvoiceData($data){
        $this->api = $data['api'] ?? null;
        $this->currency = $data['currency'] ?? null;
        $this->amount = $data['amount'] ?? null;
        $this->input_amount = $data['input_amount'] ?? null;
        $this->amount2pay = $data['amount2pay'] ?? null;
        $this->type = $data['type'] ?? null;
        $this->currency2currency = $data['currency2currency'] ?? null;
        $this->expense = $data['expense'] ?? null;
        $this->exact_currency = $data['exact_currency'] ?? null;
        $this->coin = $data['coin'] ?? null;
    }

    private function getAdsByProviders($providers){
        $ads = [];
        foreach ($providers as $provider) {
            switch ($provider->type) {
                case ServiceProvider::TRADER_PROVIDER:
                    $this->currentProvider = $provider;
                    $ads[$provider->name] = $this->getTraderAvalableAds();
                    break;
                case ServiceProvider::CHATEX_PROVIDER:
                    $this->currentProvider = $provider;
                    $ads[$provider->name] = $this->getChatexAvalableAds();
                    break;
                case ServiceProvider::CRYPTO_PROVIDER:
                    $this->currentProvider = $provider;
                    $ads[$provider->name] = $this->getCryptoAvalableAds();
                    break;
            }
        }
        return $this->getAdsResult($ads);
    }

    private function getAvalableProviderByMerchant()
    {
        $this->providers = $this->getAvalableProviderQuery()
            ->where('merchant_service_providers.merchant_id', '=', $this->merchant->id)
            ->get();
        return $this->providers;
    }

    private function getAvalableProviderByUser()
    {
        $providerList = json_decode(Property::getProperty('users', '0', 'user_available_service_providers', '[]'), true);
        $this->providers = $this->getAvalableUserProviderQuery()
            ->whereIn('service_providers.name', $providerList)
            ->get();
        return $this->providers;
    }

    private function getAdsResult($ads)
    {
        $result = [];
        $control = [];
        $prefix = [];
        foreach ($this->providers as $value) {
            $prefix[$value->name] = $value->prefix;
        }
        foreach ($ads as $provider => $ad) {
            foreach ($ad as $currency => $item)
                foreach ($item as $record) {
                    if (!in_array($record['bank_name'], $control[$currency] ?? [])) {
                        $record['ad_id'] = $prefix[$provider] . '_' . ($record['ad_id'] ?? $record['id']);
                        $result[$currency][] = $record;
                        $control[$currency][] = $record['bank_name'];
                    }
                }
        }

        return $result;
    }


    public function getAd($adId, $invoice = false)
    {
        $providerType = $this->getProviderTypeByAdId($adId);
        switch ($providerType) {
            case ServiceProvider::TRADER_PROVIDER:
                $ad = $this->getTraderAd($this->getCleanAdId($adId), $invoice);
                return $ad;
            case ChatexApi::OBJECT_NAME:
                $ad = $this->getChatexAd($this->getCleanAdId($adId), $invoice);
                return $ad;
            case ServiceProvider::CRYPTO_PROVIDER:
                $ad = $this->getCryptoAd($this->getCleanAdId($adId), $invoice);
                return $ad;
        }
        return false;
    }

    private function filterProviders($providers){
        $limitProperty = json_decode(MerchantProperty::getProperty($this->merchant->id, 'provider_limit', '[]'), true);
        if(count($limitProperty)){

            foreach ($providers as $key=>$provider) {
                if($this->currency2currency){
                    $amount = $this->input_amount;
                    $currency = $this->currency;
                }else{
                    $amount = $this->amount;
                    $currency = 'USD';
                }
                if(isset($limitProperty[$provider->name]) &&
                    $limitProperty[$provider->name] &&
                    isset($limitProperty[$provider->name][$currency]) &&
                    $limitProperty[$provider->name][$currency]){
                    if($this->type == TraderAd::TYPE_SELL){
                        if(isset($limitProperty[$provider->name][$currency]['payment_invoice'])){
                            if(isset($limitProperty[$provider->name][$currency]['payment_invoice']['min']) &&
                                floatval($limitProperty[$provider->name][$currency]['payment_invoice']['min']) > $amount
                            ){
                                $providers->forget($key);
                                continue;
                            }
                            if(isset($limitProperty[$provider->name][$currency]['payment_invoice']['max']) &&
                                floatval($limitProperty[$provider->name][$currency]['payment_invoice']['max']) < $amount
                            ){
                                $providers->forget($key);
                            }

                        }
                    }
                    elseif ($this->type == TraderAd::TYPE_BUY){
                        if ($this->currency2currency) {
                            $fee = $this->merchant->getFeeCommission(
                                $this->merchant->id,
                                $amount,
                                $this->api,
                                MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                                $provider->id
                            );
                            if ($this->expense) {
                                $amount += $fee['total'];
                            } else {
                                $amount -=$fee['total'];
                            }
                        }

                        if(isset($limitProperty[$provider->name][$currency]['withdrawal_invoice']['min']) &&
                            floatval($limitProperty[$provider->name][$currency]['withdrawal_invoice']['min']) > $amount
                        ){
                            $providers->forget($key);
                            continue;
                        }
                        if(isset($limitProperty[$provider->name][$currency]['withdrawal_invoice']['max']) &&
                            floatval($limitProperty[$provider->name][$currency]['withdrawal_invoice']['max']) < $amount
                        ){
                            $providers->forget($key);
                        }
                    }
                }
            }
        }
        return $providers;
    }

    private function filterAdByLimit($data, $amount){
        if($data instanceof \stdClass){
            $data = (array)$data;
        }
        if(is_array($data)){
            $limitProperty = $this->propertyLimit;
            if(count($limitProperty)){
                $current = $limitProperty[$this->currentProvider->name][$data['currency']][$data['bank_name']][$this->getTypeInvoice()] ?? false;
                $default = $limitProperty[$this->currentProvider->name][$data['currency']]['__DEF__'][$this->getTypeInvoice()] ?? false;
                $isBankRecordIsset = (bool)(($limitProperty[$this->currentProvider->name][$data['currency']][$data['bank_name']] ?? false));
                if($current && isset($current['max'])){
                    if(floatval($current['max']) < $amount){
                        Log::channel('ads_filter_limit')->info('ADS|FILTER|CURRENT|MAX|TYPE|'. $this->getTypeInvoice() , ['data' => $data, 'provider' => $this->currentProvider->name, 'limitProperty' => $limitProperty, 'amount' => $amount]);
                        return true;
                    }
                }elseif ($default && isset($default['max']) && !$isBankRecordIsset && (floatval($default['max']) < $amount)){
                        Log::channel('ads_filter_limit')->info('ADS|FILTER|DEFAULT|MAX|TYPE|'. $this->getTypeInvoice() , ['data' => $data, 'provider' => $this->currentProvider->name, 'limitProperty' => $limitProperty, 'amount' => $amount]);
                    return true;
                }
                if($current && isset($current['min'])){
                    if(floatval($current['min']) > $amount){
                        Log::channel('ads_filter_limit')->info('ADS|FILTER|CURRENT|MIN|TYPE|'. $this->getTypeInvoice() , ['data' => $data, 'provider' => $this->currentProvider->name, 'limitProperty' => $limitProperty, 'amount' => $amount]);
                        return true;
                    }
                }elseif ($default && isset($default['min']) && !$isBankRecordIsset &&  (floatval($default['min']) > $amount)){
                    Log::channel('ads_filter_limit')->info('ADS|FILTER|DEFAULT|MIN|TYPE|'. $this->getTypeInvoice() , ['data' => $data, 'provider' => $this->currentProvider->name, 'limitProperty' => $limitProperty, 'amount' => $amount]);
                    return true;
                }
            }
        }
        return false;
    }

    private function getTypeInvoice(){
        if($this->type == TraderAd::TYPE_SELL){
            return 'payment_invoice';
        }else{
            return 'withdrawal_invoice';
        }
    }
}
