<?php
namespace App\Http\Libs\Api;


use Illuminate\Support\Facades\Http;

class Base
{
    protected $httpClient = null;

    public function __construct($token)
    {
        $this->httpClient = Http::withToken($token)->withHeaders([
            'Accept' => 'application/json'
        ]);
    }

}
