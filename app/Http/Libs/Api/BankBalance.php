<?php
namespace App\Http\Libs\Api;

class BankBalance extends Base
{
    public function __construct($token = null)
    {
        if (is_null($token))
            $token = config('app.bank_host_token');
        parent::__construct($token);
    }


    public function getAccounts()
    {
        $response = $this->httpClient->post(
            config('app.bank_host') . '/backoffice/api/dbalance/accounts',
            ['platform_id' => 2]
        );
        if ($response->ok()) {
            $result = $response->json();
            if ($result['success']) {
                return $result['data'];
            }
        } else {
            abort(404);
        }
        return false;
    }

    public function getAssetsInfo()
    {
        $response = $this->httpClient->get(
            config('app.bank_host') . '/backoffice/api/dbalance/assets_info'
        );
        if ($response->ok()) {
            $result = $response->json();
            if ($result['success']) {
                return $result['data'];
            }
        } else {
            return false;
        }
        return false;
    }

    public function getCalcExchange($param = [])
    {
        $response = $this->httpClient->post(
            config('app.bank_host') . '/backoffice/api/dbalance/calc_exchange',
            array_merge($param, ['platform_id' => 2])
        );
        if ($response->ok()) {
            $result = $response->json();
            if ($result['success']) {
                return $result['data'];
            }
        } else {
            abort(404);
        }
        return false;
    }

    public function getCompanyBalance()
    {
        $response = $this->httpClient->post(
            config('app.bank_host') . '/backoffice/api/dbalance/company_accounts',
            ['platform_id' => 2]
        );
        if ($response->ok()) {
            $result = $response->json();
            if ($result['success']) {
                return $result['data'];
            }
        } else {
            abort(404);
        }
        return false;
    }

    public function changeCompanyBalance($param = [])
    {
        $response = $this->httpClient->post(
            config('app.bank_host') . '/backoffice/api/dbalance/company_account_change',
            array_merge($param, ['platform_id' => 2])
        );
        if ($response->ok()) {
            $result = $response->json();
            if ($result['success']) {
                return $result['success'];
            }
        } else {
            abort(404);
        }
        return false;
    }

}
