<?php

namespace App\Http\Resources\Api;

use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PaginationCollection extends ResourceCollection
{
    private array $pagination;

    public function __construct($resource)
    {
        $this->pagination = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'per_page' => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request = null)
    {

        $collection = $this->collection->map(function ($item) {
            if($item instanceof PaymentInvoice){
                $item->amount = round($item->amount, 2);
                $item->merchant_amount = round($item->merchant_amount, 2);
                $item->amount_currency = round($item->amount_currency, 2);
                $item->account_info = mask_credit_card($item->account_info);
                if(PaymentInvoice::isCancelStatus($item->status)){
                    $item->status = -1;
                }
            }elseif($item instanceof MerchantWithdrawalInvoice){
                $item->amount = round($item->amount, 2);
                $item->merchant_amount = round($item->merchant_amount, 2);
                $item->amount_currency = round($item->amount_currency, 2);
                $item->account_info = mask_credit_card($item->account_info);
                if(MerchantWithdrawalInvoice::isCancelStatus($item->status)){
                    $item->status = -1;
                }
            }

            return $item;
        });
        return [
            'data' => $collection,
            'pagination' => $this->pagination
        ];
    }
}
