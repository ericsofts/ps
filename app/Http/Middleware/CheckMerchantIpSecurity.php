<?php

namespace App\Http\Middleware;

use App\Models\Merchant;
use App\Models\MerchantProperty;
use Closure;
use Illuminate\Support\Facades\Log;

class CheckMerchantIpSecurity
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param string $field
     * @return mixed
     */
    public function handle($request, Closure $next, $type, $option )
    {
        if($request->input('merchant') && ($request->input('merchant') instanceof Merchant)){

            if(!$this->isPermitted($request->input('merchant'), $request->ip())){
                Log::channel('ip_security')->info('MIDDLWARE|IP SECURITY|REJECTED|' . $request->input('merchant')->id . '|' . $request->ip());
                abort(403, 'Unauthorized action.');
            }

        }elseif($type == 'token'){
            $token = $request->input('token');

            if(!$token){
                $token = $request->bearerToken();
            }

            if (!$token) {
                abort(403, 'Unauthorized action.');
            }

            $merchant = Merchant::where([
                $option => $token,
                'status' => 1
            ])->first();

            if(!$merchant){
                abort(403, 'Unauthorized action.');
            }

            if(!$this->isPermitted($merchant, $request->ip())){
                Log::channel('ip_security')->info('MIDDLWARE|IP SECURITY|REJECTED|' . $merchant->id . '|' . $request->ip());
                abort(403, 'Unauthorized action.');
            }

        }elseif ($type == 'byId'){

            $merchantId = $request->input($option);
            if (!$merchantId) {
                abort(403, 'Unauthorized action.');
            }

            $merchant = Merchant::where([
                'id' => $merchantId,
                'status' => 1
            ])->first();

            if(!$merchant){
                abort(403, 'Unauthorized action.');
            }

            if(!$this->isPermitted($merchant, $request->ip())){
                Log::channel('ip_security')->info('MIDDLWARE|IP SECURITY|REJECTED|' . $merchant->id . '|' . $request->ip());
                abort(403, 'Unauthorized action.');
            }

        }


        return $next($request);
    }

    protected function isPermitted($merchant, $ip){

        if(MerchantProperty::getProperty($merchant->id, 'enable_ip_list')){
            $permitted = explode(';',MerchantProperty::getProperty($merchant->id, 'permitted_ip_list'));
            if($permitted){
                if(in_array($ip, $permitted)){
                    return true;
                }
                return false;
            }
            return false;
        }
        return true;
    }
}
