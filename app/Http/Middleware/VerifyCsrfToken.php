<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'chatex/*',
        'trader/input_invoice/transact',
        'payment/checkout/redirect',
        'payment/invoice/post/add',
        'payment/withdrawal/post/add',
        'telegram/webhook'
    ];
}
