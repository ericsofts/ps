<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CheckInternalApiIpSecurity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $ips = explode(';', config('app.api_internal_ips'));
        if(!in_array($request->ip(), $ips)){
            Log::channel('api_internal_ip_security')->info('MIDDLEWARE|IP SECURITY|REJECTED|' . $request->ip());
            abort(404);
        }
        return $next($request);
    }
}
