<?php

namespace App\Http\Middleware;

use App\Models\Merchant;
use App\Models\MerchantProperty;
use Closure;

class CheckMerchantToken
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param string $field
     * @return mixed
     */
    public function handle($request, Closure $next, $field = 'token')
    {
        $token = $request->input('token');

        if(!$token){
            $token = $request->bearerToken();
        }

        if (!$token) {
            abort(403, 'Unauthorized action.');
        }

        $merchant = Merchant::where([
            $field => $token,
            'status' => 1
        ])->first();

        if(!$merchant){
            abort(403, 'Unauthorized action.');
        }

        if(($field == 'api6_key') && !$merchant->api6_available){
            abort(403, 'Unauthorized action.');
        }

        $request->merge(['merchant' => $merchant]);

        return $next($request);
    }
}
