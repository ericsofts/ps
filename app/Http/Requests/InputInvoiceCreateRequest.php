<?php

namespace App\Http\Requests;

use App\Models\Property;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class InputInvoiceCreateRequest extends FormRequest
{
    protected $redirectRoute = 'input_invoice.create';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ps = Property::getProperty('system', 0, 'available_payment_systems');
        return [
            'amount' => ['required', 'numeric'],
            'payment_method' => ['required', Rule::in(json_decode($ps, true))],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'amount' => $this->input('amount', $this->old('amount')),
            'payment_method' => $this->input('payment_method', $this->old('payment_method'))
        ]);
    }
}
