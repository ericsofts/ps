<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Balance;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|iunique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        $email_domain = explode('@', $request->email)[1] ?? null;
        if ($email_domain) {
            $validator = Validator::make(['email' => $email_domain], [
                'email' => 'iunique:blocked_email_domains,domain',
            ], [
                    'iunique' => __('Invalid email')
                ]
            );
            $validator->validate();
        }

        Auth::login($user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'status' => true,
            'account_id' => User::generateAccountId()
        ]));

        Balance::create(['user_id' => $user->id]);

        event(new Registered($user));

        return redirect(RouteServiceProvider::HOME);
    }
}
