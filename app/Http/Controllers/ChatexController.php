<?php

namespace App\Http\Controllers;

use App\Lib\Chatex\ChatexApi;
use App\Models\BalanceQueue;
use App\Models\InputInvoice;
use App\Models\Property;
use App\Models\ServiceProvidersProperty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ChatexController extends Controller
{
    public function callback(Request $request)
    {
        Log::channel('callback_chatex')->info('--------------------------------------------------------');
        Log::channel('callback_chatex')->info(__('Callback from Chatex'));
        Log::channel('callback_chatex')->info($request->all());
        Log::channel('callback_chatex')->info('from ip: ' . $request->getClientIp());


        $id = $request->input('id');
        $status = $request->input('status');
        //$token = $request->input('token');
        if ($status == ChatexApi::ORDER_STATUS_COMPLETED || $status == ChatexApi::ORDER_STATUS_CANCELED) {
            try {
                $chatexProperties = ServiceProvidersProperty::getProperties(ChatexApi::OBJECT_NAME);
                $chatex = new ChatexApi($chatexProperties['base_url'], $chatexProperties['api_token']);
                $response = $chatex->get_invoice($id);
                if ($response['status'] == ChatexApi::ORDER_STATUS_COMPLETED) {
                    Log::channel('callback_chatex')->info(__('Invoice id = :id - COMPLETED (response from get_invoice)', ['id' => $id]));
                    $inputInvoice = InputInvoice::where(['payment_id' => $id])->first();
                    if (!$inputInvoice) {
                        Log::channel('callback_chatex')->info(__('payment_id = :id not found in DB', ['id' => $id]));
                        return;
                    }

                    Log::channel('callback_chatex')->info('Current status: ' . $inputInvoice->status);
                    if ($inputInvoice->status != InputInvoice::STATUS_PAYED) {
                        $description = "Account Top-Up - Bank Card";
                        $result = BalanceQueue::addToBalanceQueue(
                            $inputInvoice->user_id,
                            $inputInvoice->amount,
                            BalanceQueue::TYPE_CREDIT,
                            $description,
                            $inputInvoice->id
                        );

                        if ($result) {
                            $current = __('User balance user_id=:id replenished in the amount :amount ', ['id' => $inputInvoice->user_id, 'amount' => $inputInvoice->amount]);
                            Log::channel('callback_chatex')->info($current);
                        } else {
                            $current = __('Failed to top-up user balance');
                            Log::channel('callback_chatex')->info($current);
                        }

                        $inputInvoice->status = InputInvoice::STATUS_PAYED;
                        $inputInvoice->payed = now();

                        if ($inputInvoice->save()) {
                            $current = __('Заявка на пополнение счета подтвержен в БД. Статус :status', ['status' => InputInvoice::STATUS_PAYED]);
                            Log::channel('callback_chatex')->info($current);
                        } else {
                            $current = __('Не удалось  изменить статус заявки на пополнение счета.');
                            Log::channel('callback_chatex')->info($current);
                        }
                    }
                }
                #TODO  if status == ChatexApi::ORDER_STATUS_CANCELED

            } catch (\Exception $exception) {
                $current = __('Произошла ошибка при выполнении запроса к Chatex');
                Log::channel('callback_chatex')->info($current);
                Log::channel('callback_chatex')->info('Message: ' . $exception->getMessage());
                Log::channel('callback_chatex')->info('Line: ' . $exception->getLine());
                Log::channel('callback_chatex')->info('Http code: ' . $exception->getCode());
            }

        }
    }
}
