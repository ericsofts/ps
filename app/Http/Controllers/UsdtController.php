<?php

namespace App\Http\Controllers;

use App\Lib\Chatex\ChatexApiLBC;
use App\Models\InputInvoice;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use BaconQrCode\Writer as BaconQrCodeWriter;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use NumberFormatter;

class UsdtController extends Controller
{
    public function view(Request $request, $id)
    {
        $invoice = InputInvoice::where([
            'id' => $id,
            'user_id' => $request->user()->id,
            'payment_system' => InputInvoice::PAYMENT_SYSTEM_USDT,
            'status' => InputInvoice::STATUS_CREATED
        ])
            ->with('usdt_address')
            ->firstOrFail();
        if(empty($invoice->usdt_address->usdt_address)){
            abort(404);
        }
        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
        $expired_at = $invoice->created_at->addHours(config('app.usdt_erc20_input_invoice_expiry'));
        return view('input_invoice.usdt', [
            'invoice' => $invoice,
            'fmt' => $fmt,
            'expired' => now()->diffInSeconds($expired_at)
        ]);
    }

    public function cancel(Request $request, $id)
    {
        $invoice = InputInvoice::where([
            'id' => $id,
            'user_id' => $request->user()->id,
            'payment_system' => InputInvoice::PAYMENT_SYSTEM_USDT,
            'status' => InputInvoice::STATUS_CREATED
        ])
            ->with('usdt_address')
            ->firstOrFail();
        $invoice->status = InputInvoice::STATUS_CANCELED;
        $invoice->save();
        return redirect()->route('dashboard');
    }

    public function qr(Request $request, $id)
    {
        $invoice = InputInvoice::where([
            'id' => $id,
            'user_id' => $request->user()->id,
            'payment_system' => InputInvoice::PAYMENT_SYSTEM_USDT,
            'status' => InputInvoice::STATUS_CREATED
        ])
            ->with('usdt_address')
            ->firstOrFail();

        if(empty($invoice->usdt_address->usdt_address)){
            abort(404);
        }
        $size = 200;
        $imageBackEnd = new ImagickImageBackEnd();
        $renderer = new ImageRenderer(
            (new RendererStyle($size))->withSize($size),
            $imageBackEnd
        );

        $bacon = new Writer($renderer);

        $data = $bacon->writeString(
            $invoice->usdt_address->usdt_address,
            'utf-8'
        );

        $response = Response::make($data, 200);
        $response->header("Content-Type", "image/png");
        return $response;
    }
}
