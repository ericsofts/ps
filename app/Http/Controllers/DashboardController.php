<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use NumberFormatter;

class DashboardController extends Controller
{
    public function index(Request $request){
        $balance_histories = $request->user()
            ->balance_histories()
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();
        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);

        $total = $request->user()->balance_total();
        $totalGrow = $request->user()->balance_grow_token_total();

        return view('dashboard', [
            'balance_histories' => $balance_histories,
            'fmt' => $fmt,
            'balanceTotal' => $total,
            'balanceTotalGrow' => $totalGrow
        ]);
    }
}
