<?php

namespace App\Http\Controllers;

use App\Models\BalanceQueue;
use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\MerchantProperty;
use App\Models\PaymentInvoice;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PaymentToPartnerController extends Controller
{

    private $merchantModel;

    public function __construct(Merchant $merchantModel)
    {
        $this->merchantModel = $merchantModel;
    }

    /**
     * Display the create view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Request $request)
    {

        $balance = $request->user()->balance()->amount ?? 0;
        $balanceDebitQueue = BalanceQueue::where([
            'user_id' => $request->user()->id,
            'currency' => BalanceQueue::CURRENCY_USD,
            'type' => BalanceQueue::TYPE_DEBIT,
        ])
            ->where('status', '!=', BalanceQueue::STATUS_DONE)
            ->sum('amount');

        $total = round($balance + $balanceDebitQueue, 6);

        $merchants = Merchant::where([
            'status' => 1,
            'type' => Merchant::TYPE_1
        ])->get();

        return view('payment2partner.create', [
            'balance_total' => $total,
            'merchants' => $merchants
        ]);
    }

    public function confirm(Request $request)
    {

        $balance = $request->user()->balance()->amount ?? 0;
        $balanceDebitQueue = BalanceQueue::where([
            'user_id' => $request->user()->id,
            'currency' => BalanceQueue::CURRENCY_USD,
            'type' => BalanceQueue::TYPE_DEBIT,
        ])
            ->where('status', '!=', BalanceQueue::STATUS_DONE)
            ->sum('amount');

        $total = round($balance + $balanceDebitQueue, 6);
        $merchantId = $request->input('partner');
        $min_payment_invoice_amount = MerchantProperty::getProperty($merchantId, 'min_payment_invoice_amount', config('app.min_payment_invoice_amount'));

        $rules = [
            'partner' => [
                'required',
                'numeric',
                Rule::exists('merchants', 'id')->where(function ($query) {
                    return $query->where([
                        'type' => Merchant::TYPE_1,
                        'status' => 1
                    ]);
                })
            ],
            'amount' => ['required', 'numeric', 'gt:0', 'lte:' . $total, 'gte:' . $min_payment_invoice_amount, 'regex:/^\d+(\.\d{1,2})?$/']
        ];

        $request->validate($rules, [], [
            'partner' => __('partner'),
            'amount' => __('amount'),
        ]);
        $amount = $request->input('amount');
        $fee = $this->merchantModel->getFeeCommission($merchantId, $amount, PaymentInvoice::API_TYPE_4);
        $amount2pay = $amount - $fee['total'];
        $merchant = Merchant::where([
            'id' => $merchantId
        ])->first();

        $invoice = PaymentInvoice::create([
            'invoice_number' => PaymentInvoice::generateOrderNumber(),
            'user_id' => $request->user()->id,
            'merchant_id' => $merchantId,
            'amount' => $amount,
            'amount2pay' => $amount2pay,
            'amount2service' => $fee['service']['amount'],
            'amount2agent' => $fee['agent']['amount'],
            'amount2grow' => $fee['grow']['amount'],
            'commission_grow' => $fee['grow']['commission'],
            'commission_agent' => $fee['agent']['commission'],
            'commission_service' => $fee['service']['commission'],
            'payed' => now(),
            'status' => PaymentInvoice::STATUS_CREATED,
            'api_type' => PaymentInvoice::API_TYPE_4
        ]);

        $invoice->status = PaymentInvoice::STATUS_PAYED;
        $invoice->save();

        $description = "Account Charge - {$merchant->name}";
        BalanceQueue::addToBalanceQueue(
            $request->user()->id,
            -$amount,
            BalanceQueue::TYPE_DEBIT,
            $description,
            null,
            $invoice->id,
            $merchant->id
        );

        return redirect()->route('payment2partner.create')->with('status',
            __('The payment is complete')
        );
    }
}
