<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\TraderInputInvoice;
use App\Models\TraderCurrencyAddress;
use App\Models\TraderBalance;

class TraderInputInvoiceController extends Controller
{
    public function transact(Request $request)
    {
        $message = json_encode($request->all());
        Log::channel('trader_input_invoice')->info($message);

        // [{"curCode":"TRX:USDT","address":"TNzEjeAJQaiuT22t63yQMD9boScZrjFFHH","value":"125.5"}]
        $json = json_decode($request->getContent());

        $rAmount = $json[0]->value ?? '';
        $rAddress = $json[0]->address ?? '';

        if (!$rAmount || !$rAddress) {
            return response('not ok: missing params', 400);
        }

        $address = TraderCurrencyAddress::where('address', $rAddress)->first();
        if (!$address) {
            return response('not ok: wrong arrdess', 400);
        }

        $balance = TraderBalance::firstOrCreate([
            'trader_id' => $address->trader_id,
            'currency_id' => $address->currency_id,
            'currency' => $address->currency->asset,
            'type' => TraderBalance::TYPE_NORMAL
        ]);

        TraderInputInvoice::create([
            'trader_id' => $address->trader_id,
            'currency_id' => $address->currency_id,
            'amount' => $rAmount,
            'address_id' => $address->id,
            'balance_id' => $balance->id,
            'additional_info' => json_encode($request->getContent())
        ]);

        return response('ok', 200);
    }

}