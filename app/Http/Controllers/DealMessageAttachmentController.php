<?php

namespace App\Http\Controllers;

use App\Models\TraderDealMessage;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DealMessageAttachmentController extends Controller
{

    private $dealMessageModel;

    public function __construct(
        TraderDealMessage $dealMessageModel
    )
    {
        $this->dealMessageModel = $dealMessageModel;
    }

    public function save(Request $request)
    {
        $file = $request->file('file');
        $path = $this->dealMessageModel->saveFile($file);
        if($path){
            return response()->json(['success' => true, 'path' => $path]);
        }
        return response()->json(['success' => false]);
    }

    public function get(Request $request)
    {
        $name = $request->input('name');
        $file = $this->dealMessageModel->getFile($name);

        if($file){
            return response()->file($file);
        }
        return false;
    }

    public function path(Request $request)
    {
        $name = $request->input('name');
        $path = $this->dealMessageModel->getFile($name);

        if($path){
            return response()->json(['success' => true, 'path' => $path]);
        }
        return response()->json(['success' => false]);
    }
}
