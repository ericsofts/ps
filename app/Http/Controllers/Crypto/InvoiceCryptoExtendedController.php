<?php


namespace App\Http\Controllers\Crypto;


use App\Lib\Crypto\Cryptoprocessing;
use App\Lib\Crypto\KrakenAPI;
use App\Models\InvoiceCryptoExtend;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class InvoiceCryptoExtendedController
{

    public function crypto_callback(Request $request, KrakenAPI $krakenAPI)
    {
        Log::channel('crypto_callback')->info('request', ['request' => $request->input()]);
        $data = $request->input();
        if ($data) {
            foreach ($data as $item) {
                if (!empty($item['extId']) && !empty($item['status'])) {
                    $invoice = InvoiceCryptoExtend::where(['id' => $item['extId']])
                        ->first();
                    if ($invoice) {

                        if ($item['status'] == Cryptoprocessing::INVOICE_STATUS_DONE) {
                            $invoice->status = InvoiceCryptoExtend::STATUS_PAYED;
                            $invoice->address_amount = $invoice->amount;
                            $invoice->payed = now();
                        } elseif ($item['status'] == Cryptoprocessing::INVOICE_STATUS_EXPIRED) {
                            $invoice->status = InvoiceCryptoExtend::STATUS_CANCELED;
                            $invoice->cancelation_reason = InvoiceCryptoExtend::STATUS_CANCELED;
                        }
                        $invoice->save();
                    } else {
                        Log::channel('crypto_callback')->info('invoice not found', ['item' => $item]);
                    }
                }
            }
        }

        return response('ok', 200);
    }
}
