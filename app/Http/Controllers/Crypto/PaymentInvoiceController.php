<?php


namespace App\Http\Controllers\Crypto;


use App\Lib\Crypto\Cryptoprocessing;
use App\Lib\Crypto\KrakenAPI;
use App\Models\CryptoPaymentInvoice;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class PaymentInvoiceController
{

    public function crypto_callback(Request $request, KrakenAPI $krakenAPI)
    {
        Log::channel('crypto_callback')->info('request', ['request' => $request->input()]);
        $data = $request->input();
        if ($data) {
            foreach ($data as $item) {
                if (!empty($item['id']) && !empty($item['status']) && isset($item['value'])) {
                    $invoice = CryptoPaymentInvoice::where(['id' => $item['id']])
                        ->with(['input_currency', 'currency'])
                        ->first();
                    if ($invoice) {
                        $invoice->address_amount = $item['value'];
                        if ($item['status'] == Cryptoprocessing::INVOICE_STATUS_DONE) {
                            $invoice->status = CryptoPaymentInvoice::STATUS_PAYED;
                        } elseif ($item['status'] == Cryptoprocessing::INVOICE_STATUS_EXPIRED) {
                            $invoice->status = CryptoPaymentInvoice::STATUS_CANCELED;
                            $invoice->cancelation_reason = CryptoPaymentInvoice::CANCELED_BY_EXPIRED;
                        }
                        $invoice->save();
                    } else {
                        Log::channel('crypto_callback')->info('invoice not found', ['item' => $item]);
                    }
                }
            }
        }

        return response('ok', 200);
    }
}
