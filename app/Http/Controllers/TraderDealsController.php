<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\TraderAd;
use App\Models\TraderDeal;
use App\Models\TraderRate;
use App\Models\TraderFiat;
use App\Models\TraderCurrency;

class TraderDealsController extends Controller
{
    public function ads(Request $request)
    {
        $type = $request->input('type') ?? TraderAd::TYPE_SELL;
        $fiat_id = $request->input('fiat_id');
        $payment_system_id = $request->input('payment_system_id');

        $query = TraderAd::where([
            'status' => 1
        ]);

        if (!is_null($type) && in_array($type, [0, 1])) {
            $query->where('type', '=', $type);
        }
        if (!is_null($fiat_id)) {
            $query->where('fiat_id', '=', $fiat_id);
        }
        if (!is_null($payment_system_id)) {
            $query->where('payment_system_id', '=', $payment_system_id);
        }

        $query->with(['fiat', 'payment_system', 'currency']);
        $query->orderBy('updated_at', 'desc');
        $trader_ads = $query->get();

        $rate_rates = TraderRate::all();

        foreach ($trader_ads as $item) {
            if ($item->rate_type == TraderAd::RATE_PERCENTAGE) {
                foreach ($rate_rates as $unit) {
                    if ($unit->rate_source_id == $item->rate_source_id
                        && $unit->currency_id == $item->currency_id
                        && $unit->fiat_id == $item->fiat_id
                    ) {
                        $item->rate = $unit->rate * (1 + $item->rate / 100);

                        if ($item->rate_stop) {
                            if ($item->type == TraderAd::TYPE_BUY) {
                                $item->rate = min($item->rate, $item->rate_stop);
                            }
                            else {
                                $item->rate = max($item->rate, $item->rate_stop);
                            }
                        }
                    }
                }
            }
        }

        return view('trader_deal.ads', [
            'type' => $type,
            'fiat_id' => (int) $fiat_id,
            'payment_system_id' => (int) $payment_system_id,
            'trader_ads' => $trader_ads,
            'all_fiats' => TraderFiat::with('payment_systems')->where('status', '1')->get(),
            'all_currencies' => TraderCurrency::where('status', 1)->get()
        ]);
    }

    public function create(Request $request)
    {
        $id = $request->input('id');
        $trader_ad = TraderAd::where(['id' => $id, 'status' => TraderAd::STATUS_ACTIVE])->firstOrFail();

        $rules = [
            'amount' => 'required|numeric|regex:/^\d+(\.\d{1,2})?$/',
        ];

        $messages = [];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $amount = $request->input('amount');

        $rate_rates = TraderRate::all();

        $rate = $trader_ad->rate;
        if ($trader_ad->rate_type == TraderAd::RATE_PERCENTAGE) {
            foreach ($rate_rates as $unit) {
                if ($unit->rate_source_id == $trader_ad->rate_source_id
                    && $unit->currency_id == $trader_ad->currency_id
                    && $unit->fiat_id == $trader_ad->fiat_id
                ) {
                    $rate = $unit->rate * (1 + $trader_ad->rate / 100);
                }
            }
        }

        if (!$rate) {
            abort(500);
        }

        $amount_currency = $amount;
        $amount_fiat = $amount_currency * $rate;

        $request->amount_currency = $amount_currency;
        $request->amount_fiat = $amount_fiat;
        $request->rate = $rate;

        if ((float) $trader_ad->rate_stop) {
            if ($trader_ad->type == TraderAd::TYPE_BUY) {
                $request->rate = max($rate, $trader_ad->rate_stop);
            }
            else {
                $request->rate = min($rate, $trader_ad->rate_stop);
            }
        }

        //dd($amount_currency, $amount_fiat, $rate);

        $trader_ad = $this->createTraderDeal($trader_ad, $request);
        if($trader_ad){
            return redirect()->route('trader_deal.ads')->with(['alert-type' => 'success', 'message' => __('The Trader deal Created')]);
        }

        return redirect()->route('trader_deal.ads')->with(['alert-type' => 'error', 'message' => __('The Trader deal Create Error')]);
    }

    protected function createTraderDeal($trader_ad, $request)
    {
        $data = [
            'description' => 'create deal',
            'customer_id' => Auth::user()->id,

            'ad_id' => $trader_ad->id,
            'trader_id' => $trader_ad->trader_id,
            'type' => $trader_ad->type,
            'fiat_id' => $trader_ad->fiat_id,
            'currency_id' => $trader_ad->currency_id,
            'payment_system_id' => $trader_ad->payment_system_id,
            'card_number' => $trader_ad->card_number,
            'amount_currency' => $request->amount_currency,
            'amount_fiat' => $request->amount_fiat,
            'rate' => $request->rate,
            'status' => TraderDeal::STATUS_CREATE,
            'ad_json' => json_encode($trader_ad),
            'customer_type' => TraderDeal::TRADER_USER
        ];
        $trader_ad = TraderDeal::create($data);

        return $trader_ad;
    }
}
