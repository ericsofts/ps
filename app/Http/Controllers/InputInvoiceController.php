<?php

namespace App\Http\Controllers;

use App\Http\Libs\Api\BankBalance;
use App\Http\Requests\InputInvoiceCreateRequest;
use App\Lib\Chatex\ChatexApi;
use App\Lib\Chatex\ChatexApiLBC;
use App\Models\AccountingEntrie;
use App\Models\InputInvoice;
use App\Models\InvoicesHistory;
use App\Models\PaymentSystemType;
use App\Models\Property;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use App\Models\TraderAd;
use App\Models\UsdtAddress;
use App\Models\User;
use App\Services\AdsSelector;
use App\Services\ContractManager;
use App\Traits\Contract;
use App\Traits\Invoice;
use App\Traits\ServiceProviderTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use NumberFormatter;

class InputInvoiceController extends Controller
{

    use ServiceProviderTrait;
    use Contract;
    use Invoice;

    private $adsService;
    private $contractManager = null;
    private $userModel;
    private $growBankApi;

    public function __construct(
        AdsSelector $adsService,
        User $userModel,
        ContractManager $contractManager,
        BankBalance $growBankApi
    )
    {
        $this->adsService = $adsService;
        $this->growBankApi = $growBankApi;
        $this->contractManager = $contractManager;
        $this->userModel = $userModel;
    }

    public function index(Request $request)
    {
        $invoicesQuery = InputInvoice::where([
            'user_id' => $request->user()->id
        ])
            ->orderBy('id', 'desc');

        $invoices = $invoicesQuery->paginate(config('app.default.pagination_limit'));
        return view('input_invoice.index', [
            'invoices' => $invoices,
            'fmt' => new NumberFormatter('en_US', NumberFormatter::CURRENCY)
        ]);
    }

    public function history_addition(Request $request)
    {
        $invoicesQuery = InputInvoice::where([
            'user_id' => $request->user()->id
        ])
            ->where('is_auto_grt',true)
            ->orderBy('id', 'desc');
        $balance = $request->user()->balance_grow_token_total();
        $invoices = $invoicesQuery->paginate(config('app.default.pagination_limit'));
        return view('input_invoice.history_addition', [
            'balanceTotalGrow' => $balance,
            'invoices' => $invoices,
            'fmt' => new NumberFormatter('en_US', NumberFormatter::CURRENCY)
        ]);
    }

    /**
     * Display the create view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Request $request)
    {
        $ps = Property::getProperty('system', 0, 'available_payment_systems');
        $ps = json_decode($ps, true);
//        if (($key = array_search('bank_card', $ps)) !== false) {
//            unset($ps[$key]);
//        }
        $grtRate = $this->getGrowTokenRate();
        $fee = $this->userModel->getFeeCommission(100);
        return view('input_invoice.create', [
            'ps' => $ps ?? [],
            'grtRate' => $grtRate,
            'fee' => $fee['total']
        ]);
    }

    public function paymentMethod(InputInvoiceCreateRequest $request)
    {
        $rules = [
            'amount' => 'required|numeric|gt:0|regex:/^\d+(\.\d{1,2})?$/',
            'payment_method' => 'required'
        ];

        $validator = Validator::make($request->input(), $rules, [], [
            'amount' => __('amount'),
            'payment_method' => __('payment method')
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $payment_method = $request->input('payment_method');
        $amount = $request->input('amount');
        if ($payment_method == ChatexApiLBC::PM_NAME) { // chatex_lbc

        $sell_coins = $this->adsService->getAdsByUser($request->user(), [
                'amount' => $amount,
                'type' => TraderAd::TYPE_SELL,
            ]);

        if (!$sell_coins) {
            return back()->with('status',
                    __('There are no payment systems available.')
                );
        }

        $currencies = array_keys($sell_coins);
        $grtRate = $this->getGrowTokenRate();
        $fee = $this->userModel->getFeeCommission($amount);
        $feePercent = $this->userModel->getFeeCommission(100);
        $amount2pay = $amount - $fee['total'];
        $amount2payGrow = $amount2pay / $grtRate;
        return view('input_invoice.create_chatex_lbc', [
            'currencies' => $currencies,
            'sell_coins' => $sell_coins,
            'amount' => $amount,
            'amount2pay' => $amount2payGrow,
            'fee' => $feePercent['total'],
            'grtRate' => $grtRate,
        ]);
        }
        if ($payment_method == 'usdt_erc20') {
            $rate = Property::get('coinmarketcap', 0, 'coin_rate');
            if (empty($rate->property_value)) {
                return back()->with('status',
                    __('Whoops! Something went wrong. Contact Support')
                );
            }
            $coin_amount = round($amount * $rate->property_value, 6);
            $usdt_addr = DB::transaction(function () {
                $usdt_addr = UsdtAddress::where(['status' => UsdtAddress::STATUS_UNUSED])
                    ->orderBy('addr_index', 'asc')
                    ->lockForUpdate()
                    ->first();
                if ($usdt_addr) {
                    $usdt_addr->status = UsdtAddress::STATUS_ASSIGNED;
                    $usdt_addr->assigned_at = now();
                    $usdt_addr->save();
                }
                return $usdt_addr;
            });

            if (!$usdt_addr) {
                return back()->with('status',
                    __('Whoops! Something went wrong. Contact Support')
                );
            }
            $inputInvoice = InputInvoice::create([
                'user_id' => $request->user()->id,
                'amount' => $amount,
                'payment_system' => InputInvoice::PAYMENT_SYSTEM_USDT,
                'usdt_address_id' => $usdt_addr->id,
                'ps_amount' => $coin_amount,
                'ps_currency' => 'usdt',
                'addition_info' => json_encode([
                    'amount' => $amount,
                    'ps_amount' => $coin_amount
                ]),
                'status' => InputInvoice::STATUS_CREATED
            ]);
            if ($inputInvoice) {
                return redirect()->route('input_invoice.usdt', ['id' => $inputInvoice->id]);
            }
        }
        return back()->with('status',
            __('There are no payment systems available')
        );
    }

    public function chatex_lbc_contact_create(Request $request)
    {
        $data['amount'] = $request->input('amount');
        $data['payment_system_id'] = $request->input('payment_system_id');
        $data['currency'] = $request->input('currency');

        $rules = [
            'payment_system_id' => 'required|string',
            'currency' => 'required',
            'amount' => 'required|numeric|gt:0|regex:/^\d+(\.\d{1,2})?$/'
        ];

        $validator = Validator::make($data, $rules, [], [
            'currency' => __('currency'),
            'payment_system_id' => __('payment system')
        ]);

        if ($validator->fails()) {
            $request->merge(['payment_method' => ChatexApiLBC::PM_NAME]);
            return redirect()->route('input_invoice.payment_method', [ChatexApiLBC::PM_NAME])
                ->withErrors($validator)
                ->withInput();
        }

        $inputInvoice = InputInvoice::where([
            'user_id' => $request->user()->id
        ])->whereIn('payment_system',[InputInvoice::PAYMENT_SYSTEM_CHATEX_LBC, InputInvoice::PAYMENT_SYSTEM_TRADER])
            ->whereIn('status',[InputInvoice::STATUS_CREATED, InputInvoice::STATUS_USER_SELECTED])
            ->first();

        if ($inputInvoice) {
            $logChanel = $this->getLogChannel($inputInvoice);
            $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $inputInvoice->payment_id, 'service_provider_id' => $inputInvoice->service_provider_id]);
            if ($contractInfo) {
                if (!$this->isContractCancel($contractInfo, $inputInvoice)
                    && !$this->isContractClosed($contractInfo, $inputInvoice)
                    && !$this->isContractComplete($contractInfo, $inputInvoice)
                ) {
                    Log::channel($logChanel)->info('CONTACT_CREATE|EXISTS|USER_ID|' . $request->user()->id . '|INPUT_INVOICE|' . $inputInvoice->id, [$contractInfo]);
                    return redirect()->route('input_invoice.chatex_lbc_contact_confirm_show', $inputInvoice->id)
                        ->with('status',
                            __('Please confirm or cancel the previous request')
                        );
                }
            } else {
                Log::channel($logChanel)->info('CONTACT_CREATE|EXISTS|ERROR|USER_ID|' . $request->user()->id . '|INPUT_INVOICE|' . $inputInvoice->id, [$contractInfo]);
            }
        }

        $ad = $this->adsService->getAd($data['payment_system_id'], $inputInvoice);
        if (!$ad || $ad['type'] != 'SELL') {
            Log::channel('chatex_lbc_requests')->info('AD|NOT_FOUND|ERROR|USER_ID|' . $request->user()->id, $data);
            Log::channel('chatex_lbc_requests')->info('AD|NOT_FOUND|ERROR|USER_ID|' . $request->user()->id, [$ad]);
            return redirect()->route('input_invoice.create')->with('status',
                __('Whoops! Something went wrong.')
            );
        }

        $available_coins = $this->getAvailableUserCoins();
        if (!$this->isAvailableCoins($ad, $available_coins, $inputInvoice)) {
            return back()->with('status',
                __('Whoops! Something went wrong. Contact Support')
            );
        }

        if (!$this->isAvalableCoinRate($ad, $inputInvoice)) {
            return back()->with('status',
                __('Whoops! Something went wrong. Contact Support')
            );
        }

        $coin_amount = $data['amount'] * $this->getCoinRate(strtolower($ad['coin']));
        $currency_amount = round($ad['temp_price'] * $coin_amount, 2);

        $contract = $this->contractManager->contractCreate($ad, $currency_amount, $request->user(), $inputInvoice);

        if ($contract) {
            $contractInfo = $this->contractManager->contractInfoByContract($contract);
            if ($contractInfo) {
                $fee = $this->userModel->getFeeCommission($data['amount']);
                $grtRate = $this->getGrowTokenRate();
                $amount2pay = $data['amount'] - $fee['total'];
                $ps_amount = $contractInfo['amount_btc'] ?? null;
                $paymentSystem = $this->getPaymentSystem($ad['service_provider_id']);
                $inputInvoice = InputInvoice::create([
                    'user_id' => $request->user()->id,
                    'amount' => $data['amount'],
                    'payment_system' => $paymentSystem,
                    'payment_id' => $contractInfo['id'],
                    'ps_amount' => $ps_amount,
                    'ps_currency' => strtolower($ad['coin']),
                    'addition_info' => json_encode([
                        'contact' => $contractInfo,
                        'ad' => $ad
                    ]),
                    'service_provider_id' => $ad['service_provider_id'],
                    'is_auto_grt' => true,
                    'fiat_amount' => $contractInfo['amount'],
                    'fiat_currency' => $contractInfo['currency'],
                    'amount2service' => $fee['service']['amount'],
                    'amount2grow' => $fee['grow']['amount'],
                    'commission_service' => $fee['service']['commission'],
                    'commission_grow' => $fee['grow']['commission'],
                    'amount2pay' => $amount2pay,
                    'grow_token_amount' => $data['amount'] / $grtRate,
                    'grow_token_amount2pay' => $amount2pay / $grtRate,
                    'grow_token_rate' => $grtRate,
                    'input_rate' => $this->getCurrencyRate($data['currency']),
                    'input_currency' => $data['currency'],
                    'input_amount_value' => $request->input('amount'),
                    'status' => InputInvoice::STATUS_USER_SELECTED
                ]);
                return redirect()->route('input_invoice.chatex_lbc_contact_confirm_show', $inputInvoice->id);
            }
        }
        Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|ERROR|USER_ID|' . $request->user()->id, $data);
        Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|ERROR|USER_ID|' . $request->user()->id, ['amount' => $currency_amount]);
        Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|ERROR|USER_ID|' . $request->user()->id, [$contract]);
        return redirect()->route('input_invoice.create')->with('status',
            __('Whoops! Something went wrong.')
        );
    }

    public function chatex_lbc_contact_confirm_show(Request $request, $input_invoice_id)
    {
        $invoice = InputInvoice::where([
            'id' => $input_invoice_id,
            'user_id' => $request->user()->id
        ])->whereIn('payment_system',[InputInvoice::PAYMENT_SYSTEM_CHATEX_LBC, InputInvoice::PAYMENT_SYSTEM_TRADER])
            ->whereIn('status',[InputInvoice::STATUS_CREATED, InputInvoice::STATUS_USER_SELECTED, InputInvoice::STATUS_TRADER_CONFIRMED])
            ->first();

        if (!$invoice) {
            return redirect()->route('input_invoice.create')->with('status',
                __('Whoops! Something went wrong.')
            );
        }
        $logChanel = $this->getLogChannel($invoice);
        $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
        $deal_accepted = false;
        if ($contractInfo) {
            if ($this->isContractCancel($contractInfo, $invoice)) {
                return redirect()->route('input_invoice.create')->with('status',
                    __('Payment canceled')
                );
            }
            if ($this->isContractComplete($contractInfo, $invoice)) {
                return redirect()->route('input_invoice.create')->with('status',
                    __('Payment closed successfully')
                );
            }
            if ($this->isContractClosed($contractInfo, $invoice)) {
                return redirect()->route('input_invoice.create')->with('status',
                    __('Payment closed')
                );
            }
            if (!empty($contractInfo['funded_at'])) {
                $deal_accepted = true;
            }
            $addition_info = json_decode($invoice['addition_info'], true);
            $grtRate = $this->getGrowTokenRate();
            $feePercent = $this->userModel->getFeeCommission(100);
            return view('input_invoice.confirm_chatex_lbc', [
                'id' => $invoice->id,
                'invoice' => $invoice,
                'deal_accepted' => $deal_accepted,
                'amount' => $contractInfo['amount'],
                'clean_card_number' => $contractInfo['account_info'] ?? null,
                'bank_name' => $addition_info['ad']['bank_name'] ?? null,
                'currency' => $addition_info['ad']['currency'] ?? null,
                'fee' => $feePercent['total'],
                'grtRate' => $grtRate,
            ]);
        } else {
            Log::channel($logChanel)->info('CONTACT_CONFIRM_SHOW|ERROR|USER_ID|' . $request->user()->id . '|INPUT_INVOICE|' . $invoice->id, [$contractInfo]);
        }
        return redirect()->route('input_invoice.create')->with('status',
            __('Whoops! Something went wrong.')
        );
    }

    public function chatex_lbc_contact_confirm(Request $request)
    {
        $messages = [
            'document.max' => __('The maximum file size is over 2Mb.'),
            'document.uploaded' => __('The maximum file size is over 2Mb.')
        ];

        $rules = [
            'id' => 'required|numeric',
            'action' => 'required|numeric',
            'document' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048'
        ];

        $validator = Validator::make($request->only(['id', 'action', 'document']), $rules, $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $invoice = InputInvoice::where([
            'id' => $request->input('id'),
            'user_id' => $request->user()->id,
        ])->whereIn('payment_system',[InputInvoice::PAYMENT_SYSTEM_CHATEX_LBC, InputInvoice::PAYMENT_SYSTEM_TRADER])
            ->whereIn('status',[InputInvoice::STATUS_CREATED, InputInvoice::STATUS_USER_SELECTED, InputInvoice::STATUS_TRADER_CONFIRMED])
            ->first();

        if (!$invoice) {
            return back()->with('status',
                __('Whoops! Something went wrong.')
            );
        }
        $logChanel = $this->getLogChannel($invoice);
        $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
        if ($contractInfo) {
            Log::channel($logChanel)->info('CONTACT_CONFIRM|CHECK|USER_ID|' . $request->user()->id . '|INPUT_INVOICE|' . $invoice->id, [$contractInfo]);
            if ($this->isContractCancel($contractInfo, $invoice)) {
                return redirect()->route('input_invoice.create')->with('status',
                    __('Payment canceled')
                );
            }

            if ($this->isContractComplete($contractInfo, $invoice)) {
                return redirect()->route('input_invoice.create')->with('status',
                    __('Payment closed successfully')
                );
            }

            if ($this->isContractClosed($contractInfo, $invoice)) {
                return redirect()->route('input_invoice.create')->with('status',
                    __('Payment closed')
                );
            }

            if ($request->input('action') == 0) {
                $res = $this->contractManager->contractCancelByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($res) {
                    $message = __('Payment canceled');
                    $invoice->status = InputInvoice::STATUS_CANCELED;
                    $invoice->save();
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::getInvoiceTypeByInstance($invoice),
                        4,
                        InvoicesHistory::getStatus(1),
                        InvoicesHistory::getSource(0),
                        InvoicesHistory::getSource(2),
                        json_encode($res),
                        'Cancel Invoice'
                    );
                    Log::channel($logChanel)->info('CONTACT_CONFIRM|CANCEL|USER_ID|' . $request->user()->id . '|INPUT_INVOICE|' . $invoice->id, [$res]);
                    return redirect()->route('input_invoice.create')->with('status', $message);
                } else {
                    Log::channel($logChanel)->info('CONTACT_CONFIRM|CANCEL|ERROR||USER_ID|' . $request->user()->id . '|INPUT_INVOICE|' . $invoice->id, [$res]);
                }
            }

            if (!$this->isContractFunded($contractInfo)) {
                return back()->with('status',
                    __('Request processing ...')
                );
            }

            if ($request->input('action') == 1) {
                $res = $this->contractManager->contactMarkAsPaidByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($res) {
                    $invoice->status = InputInvoice::STATUS_USER_CONFIRMED;
                    $invoice->save();
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::getInvoiceTypeByInstance($invoice),
                        6,
                        InvoicesHistory::getStatus(1),
                        InvoicesHistory::getSource(0),
                        InvoicesHistory::getSource(2),
                        json_encode($res),
                        'User Confirm'
                    );
                    $message = __('Payment in progress ...');
                    Log::channel($logChanel)->info('CONTACT_CONFIRM|MARK_AS_PAID|SUCCESS|USER_ID|' . $request->user()->id . '|INPUT_INVOICE|' . $invoice->id, [$res]);
                    if ($request->hasFile('document')) {
                        $requestMessage = $this->contractManager->contactMessagePostByProvider([
                            'id' => $invoice['payment_id'],
                            'service_provider_id' => $invoice['service_provider_id'],
                            'document' => $request->file('document'),
                            'user_id' =>  $invoice->user_id
                        ]);
                        $requestMessageResult = $requestMessage ? 1 : 0;
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                            7,
                            InvoicesHistory::getStatus($requestMessageResult),
                            InvoicesHistory::getSource(3),
                            InvoicesHistory::getSource(2),
                            json_encode($requestMessage),
                            'Attachment'
                        );
                    }
                } else {
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::getInvoiceTypeByInstance($invoice),
                        6,
                        InvoicesHistory::getStatus(0),
                        InvoicesHistory::getSource(0),
                        InvoicesHistory::getSource(2),
                        json_encode($res),
                        'Service Provider response incorrect'
                    );
                    Log::channel($logChanel)->info('CONTACT_CONFIRM|MARK_AS_PAID|ERROR|USER_ID|' . $request->user()->id . '|INPUT_INVOICE|' . $invoice->id, [$res]);
                }
            }
            if (empty($message)) {
                $message = __('Whoops! Something went wrong.');
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::getInvoiceTypeByInstance($invoice),
                    8,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(2),
                    json_encode($contractInfo),
                    'Error'
                );
            }
            return redirect()->route('input_invoice.create')->with('status', $message);
        }
        InvoicesHistory::addHistoryRecord(
            __METHOD__,
            $invoice->id,
            AccountingEntrie::getInvoiceTypeByInstance($invoice),
            8,
            InvoicesHistory::getStatus(0),
            InvoicesHistory::getSource(0),
            InvoicesHistory::getSource(2),
            json_encode($contractInfo),
            'Service Provider response incorrect'
        );
        Log::channel($logChanel)->info('CONTACT_CONFIRM|ERROR|USER_ID|' . $request->user()->id . '|INPUT_INVOICE|' . $invoice->id, [$contractInfo]);
        return back()->with('status',
            __('Whoops! Something went wrong.')
        );
    }

    public function chatex_lbc_contact_check(Request $request, $input_invoice_id)
    {
        $result = ['status' => 0];
        $invoice = InputInvoice::where([
            'id' => $input_invoice_id,
            'user_id' => $request->user()->id,
        ])->whereIn('status', [InputInvoice::STATUS_CREATED, InputInvoice::STATUS_USER_SELECTED, InputInvoice::STATUS_TRADER_CONFIRMED])
            ->whereIn('payment_system',[InputInvoice::PAYMENT_SYSTEM_CHATEX_LBC, InputInvoice::PAYMENT_SYSTEM_TRADER])
            ->first();

        if ($invoice) {
            $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
            if ($contractInfo) {
                if ($this->isContractCancel($contractInfo)) {
                    $result['status'] = -1;
                }
                if (!empty($contractInfo['funded_at']) && !$this->isContractCancel($contractInfo)) {
                    $result['status'] = 1;
                    if($contractInfo['account_info'] ?? null){
                        $result['account_info'] = join(" ", str_split($contractInfo['account_info'], 4));
                    }
                }
            }
        }
        return response()->json($result);
    }

    private function getPaymentSystem($pamentSystemId){
        $serviceProvider = $this->getProviderById($pamentSystemId);
        switch ($serviceProvider->type){
            case ServiceProvider::CHATEX_PROVIDER:
                return InputInvoice::PAYMENT_SYSTEM_CHATEX_LBC;

            case ServiceProvider::TRADER_PROVIDER:
                return InputInvoice::PAYMENT_SYSTEM_TRADER;

        }
        return false;
    }
}
