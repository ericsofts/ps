<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use NumberFormatter;

class BalanceHistoryController extends Controller
{
    public function index(Request $request){
        $balance_histories = $request->user()
            ->balance_histories()
            ->orderBy('id', 'desc')
            ->paginate(20);

        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);

        return view('balance_history.index', [
            'balance_histories' => $balance_histories,
            'fmt' => $fmt
        ]);
    }
}
