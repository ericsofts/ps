<?php

namespace App\Http\Controllers\Api5;

use App\Http\Resources\Api\PaginationCollection;
use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\Merchant;
use App\Http\Controllers\Controller;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\PaymentInvoice;
use App\Models\Property;
use App\Traits\Invoice;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class GetController extends Controller
{

    use Invoice;

    private $merchantModel;
    private $count_service_provider;
    private $service_provider;

    public function __construct(Merchant $merchantModel)
    {
        $this->merchantModel = $merchantModel;
    }

    public function invoices(Request $request)
    {
        $rules = [
            'from_date' => ['sometimes', 'required', 'integer'],
            'to_date' => ['sometimes', 'required', 'integer'],
            'status' => ['sometimes', 'required', Rule::in([-1, 0, 1, 2])]
        ];

        $response['status'] = 0;
        $merchant = $request->input('merchant');
        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            $response['error'] = $validator->errors();
            return $response;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');
        if (!empty($from_date)) {
            try {
                $from_date = Carbon::createFromTimestamp($from_date);
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::createFromTimestamp($to_date);
            } catch (Exception $e) {
                $to_date = null;
            }
        }

        if ($from_date && $from_date->isBefore(now()->subDays(30))) {
            $validator->after(function ($validator) {
                $validator->errors()->add('from_date', __('from_date cannot be greater than 30 days'));
            });
        }

        if ($to_date && $from_date->isBefore(now()->subDays(30))) {
            $validator->after(function ($validator) {
                $validator->errors()->add('to_date', __('to_date cannot be greater than 30 days'));
            });
        }

        if ($validator->fails()) {
            $response['error'] = $validator->errors();
            return $response;
        }

        $query = PaymentInvoice::where([
            'merchant_id' => $merchant->id
        ])
            ->select([
                'id as invoice_id',
                'status',
                'amount',
                'amount2pay as merchant_amount',
                'input_amount_value as amount_currency',
                'input_currency as currency',
                'merchant_order_id as order_id',
                'merchant_order_desc as order_desc',
                'merchant_response_url as response_url',
                'merchant_cancel_url as cancel_url',
                'merchant_server_url as server_url',
                //'api_type',
                'account_info',
                'created_at'
            ])
            ->orderBy('created_at', 'DESC');

        if (!is_null($status)) {
            if ($status == -1) {
                $status = PaymentInvoice::getCancelStatuses();
            } else {
                $status = [$status];
            }
            $query->whereIn('status', $status);
        }

        if (!$from_date) {
            $from_date = now()->subDays(30);
        }
        $query->where('created_at', '>=', $from_date);
        if ($to_date) {
            $query->where('created_at', '<=', $to_date);
        }

        $invoices = new PaginationCollection($query->paginate(config('app.default.merchant_pagination_limit')));

        return array_merge(['status' => 1], $invoices->toArray());
    }

    public function invoiceStatus(Request $request)
    {
        $response['status'] = 0;
        $merchant = $request->input('merchant');

        $rules = [
            'invoice_id' => 'required_without:order_id|numeric',
            'order_id' => 'required_without:invoice_id'
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            $response['error'] = $validator->errors();
            return $response;
        }

        $query = PaymentInvoice::where([
            'merchant_id' => $merchant->id
        ]);

        if ($invoice_id = $request->input('invoice_id')) {
            $query->where('id', '=', $invoice_id);
        } elseif ($order_id = $request->input('order_id')) {
            $query->where('merchant_order_id', '=', $order_id);
        }

        $invoice = $query->first();

        if (!$invoice) {
            $response['error']['invoice_id'][] = __('Payment not found');
            return $response;
        }
        $status = [];
        switch ($invoice->status) {
            case PaymentInvoice::STATUS_CREATED:
            case PaymentInvoice::STATUS_TRADER_CONFIRMED:
            case PaymentInvoice::STATUS_USER_SELECTED:
                $status = [
                    'status' => 0,
                    'message' => __('Payment is being processed')
                ];
                break;
            case PaymentInvoice::STATUS_CANCELED:
            case PaymentInvoice::STATUS_CANCELED_BY_REVERT:
            case PaymentInvoice::STATUS_CANCELED_BY_TIMEOUT:
                $status = [
                    'status' => -1,
                    'message' => __('Payment canceled')
                ];
                break;
            case PaymentInvoice::STATUS_PAYED:
                $status = [
                    'status' => 1,
                    'message' => __('Payment paid'),
                    'merchant_amount' => price_format($invoice->amount2pay),
                    'amount' => price_format($invoice->amount)
                ];
                break;
            case PaymentInvoice::STATUS_USER_CONFIRMED:
                $status = [
                    'status' => 2,
                    'message' => __('Payment confirmed')
                ];
                break;
        }

        $response['status'] = 1;
        $response['data'] = $status;
        $response['data']['invoice'] = [
            'invoice_id' => $invoice->id,
            'merchant_id' => $invoice->merchant_id,
            'order_id' => $invoice->merchant_order_id,
            'amount' => round($invoice->amount, 2),
            'amount_currency' => round($invoice->input_amount_value, 2),
            'currency' => $invoice->input_currency,
            'order_desc' => $invoice->merchant_order_desc,
            'merchant_amount' => round($invoice->amount2pay, 2),
            'account_info' => mask_credit_card($invoice->account_info)
        ];

        return $response;
    }

    public function invoiceGetAdd(Request $request)
    {

        $merchant = $request->input('merchant');
        $response = [
            'status' => 0,
            'message' => __('Invalid parameters')
        ];
        $this->service_provider = $merchant->default_service_provider();
        $this->count_service_provider = $merchant->service_providers()->count();
        $validRequest = $this->getRequestAddValid($request, $merchant);
        if ($validRequest instanceof \Illuminate\Contracts\Validation\Validator) {
            $response['errors'] = $validRequest->errors();
            return $response;
        }
        if (!$validRequest) {
            return $response;
        }

        if($invoiceExists = PaymentInvoice::where(['merchant_id' => $merchant->id, 'merchant_order_id' => $request->input('order_id')])->first()){
            Log::channel('api5_payment_invoice')->info('order_id exists', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id,
                'invoiceExists' => $invoiceExists,
                'method' => 'POST'
            ]);
            $response['errors']['order_id'][] = __('Error: :error', ['error' => 'P10006']);
            return $response;
        }

        $currency2currency = $exact_currency = false;
        $amount = floatval($request->input('amount'));
        if (!$request->input('currency')) {
            $currency = 'USD';
        } else {
            $currency = $request->input('currency');
            $currency2currency = MerchantProperty::getProperty($merchant->id, 'currency2currency', false);
            if($request->has('exact_currency')){
                $exact_currency = (bool)$request->input('exact_currency');
            }else{
                $exact_currency = (bool)MerchantProperty::getProperty($merchant->id, 'exact_currency', false);
            }
        }
        $amount_usd = round($amount * $this->getCurrencyRate($currency), 2);
        if($currency2currency){
            $amount = 0;
        }else{
            $amount = $amount_usd;
        }

        $grow_token_amount = null;
        if ($request->input('grow_token')) {
            $rate = Property::getProperties('grow_token_usd2token');
            $coin_rate = $rate['coin_rate'] ?? 0;
            $correction = $rate['correction'] ?? 0;
            $grow_rate = round($coin_rate + ($coin_rate * $correction / 100), 8);
            $grow_token_amount = round($amount_usd / $grow_rate, 8);
        }

        if ($currency2currency) {
            $amount2pay = 0;
            $amount2service = 0;
            $amount2agent = 0;
            $amount2grow = 0;
            $commission_grow = null;
            $commission_agent = null;
            $commission_service = null;
        } else {
            $fee = $this->merchantModel->getFeeCommission(
                $merchant->id,
                $amount,
                PaymentInvoice::API_TYPE_5,
                MerchantCommission::PROPERTY_TYPE_COMMISSION,
                $this->service_provider->id
            );
            $amount2pay = $amount - $fee['total'];
            $amount2service = $fee['service']['amount'];
            $amount2agent = $fee['agent']['amount'];
            $amount2grow = $fee['grow']['amount'];
            $commission_grow = $fee['grow']['commission'];
            $commission_agent = $fee['agent']['commission'];
            $commission_service = $fee['service']['commission'];
        }

        $invoice = PaymentInvoice::create([
            'invoice_number' => PaymentInvoice::generateOrderNumber(),
            'merchant_id' => $merchant->id,
            'amount' => $amount,
            'amount2pay' => $amount2pay,
            'amount2service' => $amount2service,
            'amount2agent' => $amount2agent,
            'amount2grow' => $amount2grow,
            'commission_grow' => $commission_grow,
            'commission_agent' => $commission_agent,
            'commission_service' => $commission_service,
            'merchant_order_id' => $request->input('order_id'),
            'merchant_order_desc' => $request->input('order_desc'),
            'merchant_response_url' => $request->input('response_url'),
            'merchant_cancel_url' => $request->input('cancel_url'),
            'merchant_server_url' => $request->input('server_url'),
            'merchant_return_url' => $request->input('return_url'),
            'status' => PaymentInvoice::STATUS_CREATED,
            'input_currency' => $currency,
            'input_rate' => $this->getCurrencyRate($currency),
            'input_amount_value' => floatval($request->input('amount')),
            'api_type' => PaymentInvoice::API_TYPE_5,
            'grow_token_amount' => $grow_token_amount,
            'currency2currency' => $currency2currency,
            'exact_currency' => $exact_currency
        ]);
        if ($invoice) {
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                0,
                InvoicesHistory::getStatus(1),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(3),
                NULL,
                'Create Invoice Record'
            );
            return [
                'url' => URL::signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id]),
                'merchant_id' => $merchant->id,
                'invoice_id' => $invoice->id,
                'order_id' => $invoice->merchant_order_id,
                'amount' => round($invoice->amount, 2),
                'amount_currency' => round($invoice->input_amount_value, 2),
                'currency' => $invoice->input_currency,
                'order_desc' => $invoice->merchant_order_desc,
                'merchant_amount' => round($invoice->amount2pay, 2),
                'status' => 1
            ];
        }

        return $response;
    }


    private function getRequestAddValid(Request $request, $merchant)
    {
        $rules = [
            'amount' => 'required|numeric|gt:0|regex:/^\d*(\.\d{1,2})?$/',
            'order_id' => 'required'
        ];

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            Log::channel('api5_payment_invoice')->info('Validation fails', [
                'errors' => $validator->errors()->toArray(),
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id ?? null
            ]);
            return $validator;
        }
        if (!$merchant) {
            Log::channel('api5_payment_invoice')->info('merchant fail', [
                'inputs' => $request->input()
            ]);
            $validator->after(function ($validator) {
                $validator->errors()->add('merchant_id', 'P10001');
            });
        }
        if ($validator->fails()) {
            return $validator;
        }
        if (!$request->input('currency')) {
            $currency = 'USD';
        } else {
            $currency = $request->input('currency');
        }
        if (!$this->isAvalableCurrency($currency)) {
            Log::channel('api5_payment_invoice')->info('isAvailableCurrency fail', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $validator->after(function ($validator) {
                $validator->errors()->add('currency', 'P10003');
            });
        }

        $amount = floatval($request->input('amount'));
        $amount = round($amount * $this->getCurrencyRate($currency), 2);

        $min_invoice_amount = config('app.min_payment_invoice_amount');
        if($this->count_service_provider == 1){
            $min_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_payment_invoice_amount', $min_invoice_amount, $this->service_provider->id);
        }else{
            $props_min_amount = MerchantProperty::getPropertiesByName($merchant->id, 'min_payment_invoice_amount');
            foreach ($props_min_amount as $value) {
                $min_invoice_amount = ($value < $min_invoice_amount) ? $value : $min_invoice_amount;
            }
        }

        if ($amount < $min_invoice_amount) {
            Log::channel('api5_payment_invoice')->info('min_payment_invoice_amount fail', [
                'inputs' => $request->input(),
                'amount' => $amount,
                'min_payment_invoice_amount' => $min_invoice_amount,
                'merchant_id' => $merchant->id
            ]);
            $validator->after(function ($validator) {
                $validator->errors()->add('amount', 'P10004');
            });
        }

        if ($validator->fails()) {
            return $validator;
        }

        return true;
    }

}
