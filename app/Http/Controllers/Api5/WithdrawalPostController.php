<?php

namespace App\Http\Controllers\Api5;

use App\Http\Controllers\Controller;
use App\Lib\Chatex\ChatexApi;
use App\Lib\Chatex\ChatexApiLBC;
use App\Lib\PaymentApiHelper;
use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\MerchantWithdrawalInvoiceAttachment;
use App\Models\PaymentSystemType;
use App\Models\Property;
use App\Models\WhiteLabel;
use App\Models\ServiceProvider;
use App\Models\TraderAd;
use App\Services\AdsSelector;
use App\Services\ContractManager;
use App\Models\ServiceProvidersProperty;
use App\Traits\Contract;
use App\Traits\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use NumberFormatter;

class WithdrawalPostController extends Controller
{
    use Invoice;
    use Contract;

    private $viewTheme = 'payment.withdrawal.post.';
    private $viewVersion = 1;

    /** @var  Merchant */
    private $merchant = null;

    private Merchant $merchantModel;
    private $adsService;
    protected $contractManager = null;

    public function __construct(
        Merchant $merchantModel,
        AdsSelector $adsService,
        ContractManager $contractManager
    )
    {
        $this->merchantModel = $merchantModel;
        $this->adsService = $adsService;
        $this->contractManager = $contractManager;
    }

    private function getMerchant($merchantId = null, $status = null)
    {
        $where = [
            'id' => $merchantId
        ];

        if ($status) {
            $where['status'] = $status;
        }

        $this->merchant = Merchant::where($where)->first();

        if (2 === $this->merchant->getViewVersion()) {
            $this->viewTheme = 'payment.withdrawal.v2.post.';
            $this->merchant->viewVersion = 2;
        }

        return $this->merchant;
    }

    public function withdrawalPostAdd(Request $request)
    {
        $this->setLocaleByCurrency($request->input('currency'));
        $rules = [
            'merchant_id' => 'required|numeric',
            'amount' => 'required|numeric|gt:0|regex:/^\d*(\.\d{1,2})?$/',
            'order_id' => 'required',
            'signature' => 'required'
        ];

        $validator = Validator::make($request->input(), $rules, [], [
            'amount' => __('amount'),
            'merchant_id' => __('merchant_id'),
            'order_id' => __('order_id'),
            'signature' => __('signature')
        ]);

        if ($validator->fails()) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('Validation fails', [
                'errors' => $validator->errors()->toArray(),
                'inputs' => $request->input()
            ]);
            return view($this->viewTheme . 'add')
                ->withErrors($validator)
                ->with('error', __('Invalid parameters'))
                ->with('whiteLabelModel', new WhiteLabel);
        }

        $merchant = $this->getMerchant($request->input('merchant_id'), 1);

        if (!$merchant) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('merchant fail', [
                'inputs' => $request->input()
            ]);
            $validator->after(function ($validator) {
                $validator->errors()->add('merchant_id', __('Error: :error', ['error' => 'P10001']));
            });
        }
        $currency2currency = $exact_currency = false;
        if (!$request->input('currency')) {
            $currency = 'USD';
        } else {
            $currency = $request->input('currency');
            $currency2currency = MerchantProperty::getProperty($merchant->id, 'currency2currency', false);
            if($request->has('exact_currency')){
                $exact_currency = (bool)$request->input('exact_currency');
            }else{
                $exact_currency = (bool)MerchantProperty::getProperty($merchant->id, 'exact_currency', false);
            }
        }
        if (!$this->isAvalableCurrency($currency)) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('isAvailableCurrency fail', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $validator->after(function ($validator) {
                $validator->errors()->add('currency', __('Error: :error', ['error' => 'P10003']));
            });
        }

        if (!PaymentApiHelper::checkSignature($request->input(), $merchant->api5_key)) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('checkSignature fail', [
                'inputs' => $request->input(),
                'api5_key' => $merchant->api5_key,
                'merchant_id' => $merchant->id
            ]);
            $validator->after(function ($validator) {
                $validator->errors()->add('signature', __('Error: :error', ['error' => 'P10002']));
            });
        }

        $amount = floatval($request->input('amount'));
        $amount_usd = round($amount * $this->getCurrencyRate($currency), 2);
        if ($currency2currency) {
            $amount = 0;
        } else {
            $amount = $amount_usd;
        }

        $service_provider = $merchant->default_service_provider();
        $count_service_provider = $merchant->service_providers()->count();
        $min_invoice_amount = config('app.min_merchant_withdrawal_amount');
        if ($count_service_provider == 1) {
            $min_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_merchant_withdrawal_amount', $min_invoice_amount, $service_provider->id);
        } else {
            $props_min_amount = MerchantProperty::getPropertiesByName($merchant->id, 'min_merchant_withdrawal_amount');
            foreach ($props_min_amount as $value) {
                $min_invoice_amount = ($value < $min_invoice_amount) ? $value : $min_invoice_amount;
            }
        }

        if (!$available_coins = $this->getAvailableMerchantCoins($merchant->id)) {
            $validator->after(function ($validator) {
                $validator->errors()->add('currency', __('Error: :error', ['error' => 'I10002']));
            });
        }

        if (!$rates = $this->getCoinRates($available_coins)) {
            $validator->after(function ($validator) {
                $validator->errors()->add('currency', __('Error: :error', ['error' => 'I10004']));
            });
        }

        if ($validator->fails()) {
            return view($this->viewTheme . 'add')
                ->withErrors($validator)
                ->with('error', __('Invalid parameters'))
                ->with('whiteLabelModel', new WhiteLabel);
        }

        if ($invoiceExists = MerchantWithdrawalInvoice::where(['merchant_id' => $merchant->id, 'merchant_order_id' => $request->input('order_id')])->first()) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('order_id exists', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id,
                'invoiceExists' => $invoiceExists,
                'method' => 'POST'
            ]);
            return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $invoiceExists->id]);
        }

        if ($currency2currency) {
            $amount2pay = 0;
            $amount2service = 0;
            $amount2agent = 0;
            $amount2grow = 0;
            $commission_grow = null;
            $commission_agent = null;
            $commission_service = null;
        } else {
            if ($request->input('merchant_expense')) {
                $fee = $this->merchantModel->getRevertFeeCommission(
                    $merchant->id,
                    $amount,
                    MerchantWithdrawalInvoice::API_TYPE_5,
                    MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                    $service_provider->id
                );
                $amount_usd = $amount2pay = $amount;
                $amount += $fee['total'];
            } else {
                $fee = $this->merchantModel->getFeeCommission(
                    $merchant->id,
                    $amount,
                    MerchantWithdrawalInvoice::API_TYPE_5,
                    MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                    $service_provider->id
                );
                $amount2pay = $amount - $fee['total'];
            }
            $amount2service = $fee['service']['amount'];
            $amount2agent = $fee['agent']['amount'];
            $amount2grow = $fee['grow']['amount'];
            $commission_grow = $fee['grow']['commission'];
            $commission_agent = $fee['agent']['commission'];
            $commission_service = $fee['service']['commission'];
        }

        if ($amount_usd < $min_invoice_amount) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('min_merchant_withdrawal_amount fail', [
                'inputs' => $request->input(),
                'amount' => $amount_usd,
                'min_merchant_withdrawal_amount' => $min_invoice_amount,
                'merchant_id' => $merchant->id
            ]);
            return view($this->viewTheme . 'add')
                ->with('error', __('The minimum amount must be greater than or equal to :amount', ['amount' => $min_invoice_amount]))
                ->with('merchant', $merchant)
                ->with('whiteLabelModel', new WhiteLabel);
        }
        $merchantBalance = $merchant->balance_total();
        if ($merchantBalance <= 0) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('merchantBalance fail', [
                'inputs' => $request->input(),
                'amount' => $amount_usd,
                'merchantBalance' => $merchantBalance,
                'merchant_id' => $merchant->id
            ]);

            return view($this->viewTheme . 'add')
                ->with('error', __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10007']))
                ->with('merchant', $merchant)
                ->with('whiteLabelModel', new WhiteLabel);
        }

        if ($amount_usd > $merchantBalance) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('merchantBalance fail', [
                'inputs' => $request->input(),
                'amount' => $amount_usd,
                'merchantBalance' => $merchantBalance,
                'merchant_id' => $merchant->id
            ]);
            return view($this->viewTheme . 'add')
                ->with('error', __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10007']))
                ->with('merchant', $merchant)
                ->with('whiteLabelModel', new WhiteLabel);
        }

        if ($service_provider->name == ServiceProvider::CHATEX_PROVIDER) {
            $chatexProperties = ServiceProvidersProperty::getProperties(ChatexApi::OBJECT_NAME);
            $chatex = new ChatexApi($chatexProperties['base_url'], $chatexProperties['api_token']);

            foreach ($available_coins as $coin) {
                $wallet = $chatex->wallet($coin);
                $walletBalance = $wallet['amount'] ?? 0;
                if ($walletBalance < $amount_usd) {
                    Log::channel('api5_merchant_withdrawal_invoice')->info('walletBalance fail', [
                        'inputs' => $request->input(),
                        'amount' => $amount_usd,
                        'merchantBalance' => $merchantBalance,
                        'walletBalance' => $walletBalance,
                        'merchant_id' => $merchant->id
                    ]);

                    return view($this->viewTheme . 'add')
                        ->with('error', __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10008']))
                        ->with('merchant', $merchant)
                        ->with('whiteLabelModel', new WhiteLabel);
                }
            }
        }

        $invoice = MerchantWithdrawalInvoice::create([
            'merchant_id' => $merchant->id,
            'amount' => $amount,
            'amount2pay' => $amount2pay,
            'amount2service' => $amount2service,
            'amount2agent' => $amount2agent,
            'amount2grow' => $amount2grow,
            'commission_grow' => $commission_grow,
            'commission_agent' => $commission_agent,
            'commission_service' => $commission_service,
            'merchant_order_id' => $request->input('order_id'),
            'merchant_order_desc' => $request->input('order_desc'),
            'merchant_response_url' => $request->input('response_url'),
            'merchant_cancel_url' => $request->input('cancel_url'),
            'merchant_server_url' => $request->input('server_url'),
            'merchant_return_url' => $request->input('return_url'),
            'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC,
            'status' => MerchantWithdrawalInvoice::STATUS_CREATED,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_5,
            'merchant_expense' => $request->input('merchant_expense'),
            'input_currency' => $currency,
            'input_rate' => $this->getCurrencyRate($currency),
            'input_amount_value' => floatval($request->input('amount')),
            'currency2currency' => $currency2currency,
            'exact_currency' => $exact_currency
        ]);

        if ($invoice) {
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                0,
                InvoicesHistory::getStatus(1),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(3),
                NULL,
                'Create Invoice Record'
            );
            return redirect()->signedRoute('payment.withdrawal.post.checkout',
                [
                    'payment_withdrawal_id' => $invoice->id,
                ]);
        }

        return view($this->viewTheme . 'add')
            ->with('error', __('Invalid parameters'))
            ->with('merchant', $merchant)
            ->with('whiteLabelModel', new WhiteLabel);
    }

    public function withdrawalPostCheckout(Request $request, $payment_withdrawal_id = null)
    {
        if ($payment_withdrawal_id) {

            $invoice = MerchantWithdrawalInvoice::where([
                'id' => $payment_withdrawal_id
            ])
                ->firstOrFail();

            $this->setLocaleByCurrency($invoice->input_currency);

            if (in_array($invoice->status, [
                    MerchantWithdrawalInvoice::STATUS_CREATED,
                    MerchantWithdrawalInvoice::STATUS_IN_PROCESS,
                    MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT,
                    MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED,
                    MerchantWithdrawalInvoice::STATUS_USER_SELECTED
                ]) && !empty($invoice->payment_id)) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout.apply', $invoice->id);
            }

            if (MerchantWithdrawalInvoice::isCancelStatus($invoice->status)) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout.cancel', $invoice->id);
            }

            if ($invoice->status == MerchantWithdrawalInvoice::STATUS_PAYED) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout.complete', $invoice->id);
            }
            $error = $request->session()->pull('error');
            if (!empty($error)) {
                return view($this->viewTheme . 'checkout')
                    ->with('error', $error)
                    ->with('whiteLabelModel', new WhiteLabel);
            }

            $merchant = $this->getMerchant($invoice->merchant_id, 1);

            if (!$merchant) {
                return view($this->viewTheme . 'checkout')
                    ->with('error', __('Invalid parameters'))
                    ->with('whiteLabelModel', new WhiteLabel);
            }

            $amount = $invoice->amount;

            if (!$available_coins = $this->getAvailableMerchantCoins($merchant->id)) {
                return view($this->viewTheme . 'checkout')
                    ->with('error', __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002']))
                    ->with('merchant', $merchant)
                    ->with('whiteLabelModel', new WhiteLabel);
            }

            if (!$rates = $this->getCoinRates($available_coins)) {
                return view($this->viewTheme . 'checkout')
                    ->with('error', __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10004']))
                    ->with('merchant', $merchant)
                    ->with('whiteLabelModel', new WhiteLabel);
            }

            $buy_coins = $this->adsService->getAdsByMerchant($merchant, [
                'amount' => $invoice->amount,
                'amount2pay' => $invoice->amount2pay,
                'currency' => $invoice->input_currency,
                'input_amount' => $invoice->input_amount_value,
                'currency2currency' => $invoice->currency2currency,
                'expense' => $invoice->merchant_expense,
                'type' => TraderAd::TYPE_BUY,
                'api' => MerchantWithdrawalInvoice::API_TYPE_5,
                'exact_currency' => $invoice->exact_currency
            ]);

            if (!$buy_coins) {
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                    0,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(2),
                    NULL,
                    'There are no payment systems available'
                );
                return view($this->viewTheme . 'checkout', [
                    'invoice' => $invoice,
                    'merchant' => $merchant,
                    'error' => __('There are no payment systems available.'),
                    'canCancel' => true
                ])->with('whiteLabelModel', new WhiteLabel);
            }
            $currencies = array_keys($buy_coins);

            $defaultFiat = $invoice->input_currency != 'USD' ? $invoice->input_currency : null;
            $defaultFiat = old('currency', $defaultFiat);
            $defaultFiatKey = array_search($defaultFiat, $currencies);
            if ($defaultFiatKey === false) {
                $defaultFiat = null;
                $defaultFiatKey = 0;
            }
            $warning = $request->session()->pull('warning');
            return view($this->viewTheme . 'checkout',
                [
                    'invoice' => $invoice,
                    'currencies' => $currencies,
                    'buy_coins' => $buy_coins,
                    'amount' => $amount,
                    'payment_method' => ChatexApiLBC::PM_NAME,
                    'merchant' => $merchant,
                    'fmt' => new NumberFormatter('en_US', NumberFormatter::CURRENCY),
                    'defaultFiat' => $defaultFiat,
                    'defaultFiatKey' => $defaultFiatKey,
                    'defaultAd' => $buy_coins[$currencies[$defaultFiatKey]][0] ?? null
                ])->with('whiteLabelModel', new WhiteLabel)->with('warning', $warning);
        } else {
            $error = $request->session()->pull('error', __('Invalid parameters'));
            return view($this->viewTheme . 'checkout')
                ->with('error', $error)
                ->with('whiteLabelModel', new WhiteLabel);
        }
    }

    public function withdrawalPostCheckoutConfirm(Request $request, $payment_withdrawal_id = null)
    {

        $data['payment_system_id'] = $request->input('payment_system_id');
        $data['currency'] = $request->input('currency');
        $data['message'] = $request->input('message');
        $data['payment_info'] = $request->input('payment_info');
        $rules = [
            'payment_system_id' => 'required',
            'currency' => 'required',
            'message' => ['required']
        ];

        $validator = Validator::make($data, $rules, [], [
            'currency' => __('currency'),
            'payment_system_id' => __('payment system'),
            'message' => __('')
        ]);

        if ($validator->fails()) {
            return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])
                ->withErrors($validator)
                ->withInput();
        }

        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $payment_withdrawal_id,
        ])
            ->where(['status' => MerchantWithdrawalInvoice::STATUS_CREATED])
            ->whereNull('payment_id')
            ->first();

        if (!$invoice) {
            return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('error',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10000'])
            );
        }
        $invoice->status = MerchantWithdrawalInvoice::STATUS_IN_PROCESS;
        $invoice->save();

        $merchant = $this->getMerchant($invoice->merchant_id, 1);

        if (!$merchant) {
            return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('error',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10009'])
            );
        }

        $ad = $this->adsService->getAd($data['payment_system_id'], $invoice);
        if (!$ad || $ad['type'] != 'BUY') {
            $invoice->status = MerchantWithdrawalInvoice::STATUS_CREATED;
            $invoice->save();
            Log::channel('chatex_lbc_requests')->info('BUY|AD|NOT_FOUND|ERROR|MERCHANT_ID|' . $merchant->id, ['data' => $data, 'ad' => $ad ?? '[]']);
            return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('warning',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10001'])
            );
        }

        if ($ad['payment_system_type'] == PaymentSystemType::CARD_NUMBER) {
            $data['message'] = str_replace(' ', '', $request->input('message'));
            $rules['message'][] = 'regex:/^\d{16,19}$/';
        } elseif ($ad['payment_system_type'] == PaymentSystemType::IBAN) {
            $data['message'] = str_replace(' ', '', $request->input('message'));
            $rules['message'][] = 'min:15';
            $rules['message'][] = 'max:40';
            $rules['payment_info'][] = 'required';
        }

        $validator = Validator::make($data, $rules, [], [
            'currency' => __('currency'),
            'payment_system_id' => __('payment system'),
            'message' => __(''),
            'payment_info' => __('')
        ]);
        if ($validator->fails()) {
            $invoice->status = MerchantWithdrawalInvoice::STATUS_CREATED;
            $invoice->save();
            return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])
                ->withErrors($validator)
                ->withInput();
        }

        $amount = floatval($invoice->amount);
        $isAmountWasEmpty = false;
        if (!$amount) {
            $amount = round($invoice->input_amount_value * $this->getCurrencyRate($invoice->input_currency), 2);
            $isAmountWasEmpty = true;
        }

        if ($invoice->currency2currency) {
            $currencyRate = $this->getCurrencyRate($invoice->input_currency);
            $percentage_currency_rate = ServiceProvidersProperty::getProperty($ad['service_provider_type'], $ad['service_provider_id'], 'percentage_currency_rate', 0);
            $maxCurrencyRate = round(1 / $currencyRate * (1 + $percentage_currency_rate / 100), 2);
            $minCurrencyRate = round(1 / $currencyRate * (1 - $percentage_currency_rate / 100), 2);

            if ($ad['temp_price'] < $minCurrencyRate || $ad['temp_price'] > $maxCurrencyRate) {
                Log::channel($ad['log'])->info('BUY|CURRENCY_RATE|ERROR|MERCHANT_ID|' . $merchant->id, [
                    'invoice_id' => $invoice->id, 'ad' => $ad, 'currencyRate' => 1 / $currencyRate,
                    'maxCurrencyRate' => $maxCurrencyRate, 'minCurrencyRate' => $minCurrencyRate
                ]);
                return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_invoice_id' => $invoice->id])->with('warning',
                    __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10005'])
                );
            }
            $currency_amount = round($invoice->input_amount_value, 2);
            $amount = $currency_amount / $ad['temp_price'];
        }
        if ($isAmountWasEmpty || $invoice->currency2currency) {
            if ($invoice->merchant_expense) {
                $fee = $this->merchantModel->getRevertFeeCommission(
                    $merchant->id,
                    $amount,
                    MerchantWithdrawalInvoice::API_TYPE_5,
                    MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                    $ad['service_provider_id']
                );
                $amount += $fee['total'];
            }

            $merchantBalance = $merchant->balance_total();
            if ($merchantBalance <= 0) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('error',
                    __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10007'])
                );
            }

            if ($amount > $merchantBalance) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('error',
                    __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10007'])
                );
            }
        }

        $min_invoice_amount = MerchantProperty::getProperty(
            $merchant->id,
            'min_merchant_withdrawal_amount',
            config('app.min_merchant_withdrawal_amount'),
            $ad['service_provider_id']
        );

        if ($amount < $min_invoice_amount) {
            return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('warning',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'P10004'])
            );
        }

        $coin = strtolower($ad['coin']);
        $available_coins = $this->getAvailableMerchantCoins($merchant->id);
        if (!$this->isAvailableCoins($ad, $available_coins, $invoice)) {
            return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('error',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002'])
            );
        }

        if (!$this->isAvalableCoinRate($ad, $invoice)) {
            return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('error',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10004'])
            );
        }
        $coin_rate = $this->getCoinRate($coin);

        if ($ad['service_provider_type'] == ServiceProvider::CHATEX_PROVIDER) {
            $chatexProperties = ServiceProvidersProperty::getProperties(ChatexApi::OBJECT_NAME);
            $chatex = new ChatexApi($chatexProperties['base_url'], $chatexProperties['api_token']);
            $wallet = $chatex->wallet($coin);
            $walletBalance = $wallet['amount'] ?? 0;
            if ($walletBalance < $amount) {
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                    2,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(3),
                    json_encode($walletBalance),
                    'Mismatch Balance'
                );

                return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('error',
                    __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10008'])
                );
            }
        }

        $fee = $this->merchantModel->getFeeCommission(
            $merchant->id,
            $amount,
            MerchantWithdrawalInvoice::API_TYPE_5,
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            $ad['service_provider_id']
        );
        $amount2pay = $amount - $fee['total'];
        $coin_amount = $amount2pay * $coin_rate;

        $currency_amount = round($ad['temp_price'] * $coin_amount, 2);
        Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|REQUEST|MERCHANT_ID|' . $merchant->id, ['ad' => $data['payment_system_id'], 'amount' => $currency_amount, 'message' => $data['message'], 'invoice_id' => $invoice->id]);

        $contract = $this->contractManager->contractCreate($ad, $currency_amount, $merchant, $invoice, $data['message'], $data['payment_info']);
        if ($contract) {
            $contractInfo = $this->contractManager->contractInfoByContract($contract);
            if ($contractInfo) {
                $ps_amount = $contractInfo['amount_btc'] ?? null;
                $ps_currency = $ad['coin'] ?? null;
                $fiat_currency = $contractInfo['currency'] ?? null;

                $invoice->amount = $amount;
                $invoice->amount2pay = $amount - $fee['total'];
                $invoice->amount2service = $fee['service']['amount'];
                $invoice->amount2agent = $fee['agent']['amount'];
                $invoice->amount2grow = $fee['grow']['amount'];
                $invoice->commission_grow = $fee['grow']['commission'];
                $invoice->commission_agent = $fee['agent']['commission'];
                $invoice->commission_service = $fee['service']['commission'];

                $invoice->payment_id = $contractInfo['id'];
                $invoice->ps_amount = $ps_amount;
                $invoice->ps_currency = strtolower($ps_currency);
                $invoice->fiat_amount = $contractInfo['amount'] ?? null;
                $invoice->fiat_currency = strtolower($fiat_currency);
                $invoice->account_info = $contractInfo['account_info'] ?? null;
                $invoice->addition_info = json_encode([
                    'contact' => $contractInfo,
                    'ad' => $ad
                ]);
                $invoice->service_provider_id = $ad['service_provider_id'];
                $invoice->status = MerchantWithdrawalInvoice::STATUS_USER_SELECTED;
                $invoice->save();
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                    2,
                    InvoicesHistory::getStatus(1),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(2),
                    json_encode($contractInfo),
                    'Select Trader Successful'
                );
                return redirect()->signedRoute('payment.withdrawal.post.checkout.apply', $invoice->id);
            }
        }

        $invoice->status = MerchantWithdrawalInvoice::STATUS_CREATED;
        $invoice->save();

        Log::channel($ad['log'])->info('BUY|CONTACT_CREATE|CREATE|ERROR|MERCHANT_ID|' . $merchant->id, ['data' => $data, 'payment_invoice_id' => $invoice->id, 'amount' => $currency_amount]);

        return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('warning',
            __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
        );
    }

    public function withdrawalPostCheckoutApply(Request $request, $payment_withdrawal_id)
    {
        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $payment_withdrawal_id,
            'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_5
        ])
            ->whereIn('status', [
                MerchantWithdrawalInvoice::STATUS_CREATED,
                MerchantWithdrawalInvoice::STATUS_IN_PROCESS,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED,
                MerchantWithdrawalInvoice::STATUS_USER_SELECTED,

            ])
            ->first();

        if (!$invoice) {
            return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('error',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10000'])
            );
        }

        if ($invoice->status == MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT) {
            MerchantWithdrawalInvoiceAttachment::addAttachments($this->contractManager, $invoice);
        }

        $attachments = MerchantWithdrawalInvoiceAttachment::where(['invoice_id' => $invoice->id])->orderBy('id')->get();

        $deal_accepted = $payment_completed = false;
        $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
        $channel = $this->getLogChannel($invoice);
        if ($contractInfo) {
            Log::channel($channel)->info('BUY|CONTACT_CONFIRM|CHECK|MERCHANT_ID|' . $invoice->merchant_id . '|WITHDRAWAL|' . $invoice->id, ['contact_res' => $contractInfo]);
            if ($this->isContractCancel($contractInfo, $invoice)) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout.cancel', ['payment_withdrawal_id' => $payment_withdrawal_id]);
            }

            if ($this->isContractComplete($contractInfo, $invoice)) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout.cancel', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('error',
                    __('The withdrawal has been successfully completed')
                );
            }

            if ($this->isContractClosed($contractInfo, $invoice)) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('error',
                    __('The withdrawal closed')
                );
            }

            if (!empty($contractInfo['funded_at'])) {
                $deal_accepted = true;
            }

            if ($this->isContractPaymentCompleted($contractInfo, $invoice)) {
                $payment_completed = true;
                if (in_array($invoice->status, [MerchantWithdrawalInvoice::STATUS_CREATED, MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED])
                ) {
                    $invoice->status = MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT;
                    $invoice->save();
                }
            }
            $addition_info = json_decode($invoice['addition_info'], true);
            $merchant = $this->getMerchant($invoice->merchant_id, 1);
            return view($this->viewTheme . 'confirm', [
                'id' => $invoice->id,
                'invoice' => $invoice,
                'merchant' => $merchant,
                'attachments' => $attachments,
                'deal_accepted' => $deal_accepted,
                'payment_completed' => $payment_completed,
                'amount' => $contractInfo['amount'],
                'bank_name' => $addition_info['ad']['bank_name'] ?? null,
                'currency' => $addition_info['ad']['currency'] ?? null,
            ])->with('whiteLabelModel', new WhiteLabel);
        } else {
            Log::channel($channel)->info('BUY|CONTACT_CONFIRM_SHOW|ERROR|MERCHANT_ID|' . $invoice->merchant_id . '|WITHDRAWAL|' . $invoice->id, ['contact_res' => $contractInfo]);
        }
        InvoicesHistory::addHistoryRecord(
            __METHOD__,
            $invoice->id,
            AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
            2,
            InvoicesHistory::getStatus(0),
            InvoicesHistory::getSource(0),
            InvoicesHistory::getSource(2),
            json_encode($contractInfo),
            'Chatex response incorrect'
        );
        return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $payment_withdrawal_id])->with('error',
            __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
        );
    }

    public function withdrawalPostCheckoutStatus(Request $request)
    {
        $rules = [
            'id' => 'required|numeric',
            'action' => 'required|numeric'
        ];

        $validator = Validator::make($request->only(['id', 'action']), $rules);

        if ($validator->fails()) {
            redirect()->signedRoute('payment.withdrawal.post.checkout.apply', ['payment_withdrawal_id' => $request->input('id')])->withErrors($validator);
        }

        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $request->input('id'),
            'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_5
        ])
            ->whereIn('status', [
                MerchantWithdrawalInvoice::STATUS_CREATED,
                MerchantWithdrawalInvoice::STATUS_IN_PROCESS,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT,
                MerchantWithdrawalInvoice::STATUS_USER_SELECTED
            ])
            ->first();

        if (!$invoice) {
            return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $request->input('id')])->with('error',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10000'])
            );
        }
        if ($request->input('action') == 1) {
            $attachments = MerchantWithdrawalInvoiceAttachment::where(['invoice_id' => $invoice->id])->count();
            if (!$attachments) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout.apply', ['payment_withdrawal_id' => $invoice->id])->with('error',
                    __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10010'])
                );
            }
        }
        $channel = $this->getLogChannel($invoice);
        $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
        if ($contractInfo) {
            Log::channel($channel)->info('BUY|CONTACT_CONFIRM|CHECK|MERCHANT_ID|' . $invoice->merchant_id . '|WITHDRAWAL|' . $invoice->id, ['contact_res' => $contractInfo]);
            if ($this->isContractCancel($contractInfo, $invoice)) {
                return redirect()->signedRoute('payment.withdrawal.post.cancel', ['payment_withdrawal_id' => $invoice->id]);
            }
            if ($this->isContractComplete($contractInfo, $invoice)) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $invoice->id])->with('error',
                    __('The withdrawal has been successfully completed')
                );
            }
            if ($this->isContractClosed($contractInfo, $invoice)) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $invoice->id])->with('error',
                    __('The withdrawal closed')
                );
            }
            if ($request->input('action') == 0) {
                if (empty($contractInfo['funded_at'])) {
                    $res = $this->contractManager->contractCancelByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                    if ($res) {
                        $invoice->status = MerchantWithdrawalInvoice::STATUS_CANCELED;
                        $invoice->save();
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                            4,
                            InvoicesHistory::getStatus(1),
                            InvoicesHistory::getSource(0),
                            InvoicesHistory::getSource(2),
                            json_encode($res),
                            'Cancel Invoice'
                        );
                        Log::channel($channel)->info('BUY|CONTACT_CONFIRM|CANCEL|MERCHANT_ID|' . $invoice->merchant_id . '|WITHDRAWAL|' . $invoice->id, ['res' => $res]);
                        return redirect()->signedRoute('payment.withdrawal.post.checkout.cancel', ['payment_withdrawal_id' => $invoice->id]);
                    } else {
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                            4,
                            InvoicesHistory::getStatus(0),
                            InvoicesHistory::getSource(0),
                            InvoicesHistory::getSource(2),
                            json_encode($res['data']),
                            'Chatex response incorrect'
                        );
                        Log::channel($channel)->info('BUY|CONTACT_CONFIRM|CANCEL|ERROR||MERCHANT_ID|' . $request->user()->id . '|WITHDRAWAL|' . $invoice->id, ['res' => $res]);
                        return redirect()->signedRoute('payment.withdrawal.post.checkout.apply', ['payment_withdrawal_id' => $invoice->id])->with('error',
                            __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
                        );
                    }
                } else {
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                        4,
                        InvoicesHistory::getStatus(0),
                        InvoicesHistory::getSource(0),
                        InvoicesHistory::getSource(2),
                        null,
                        "You can't cancel the withdrawal because the request is already confirmed"
                    );
                    return redirect()->signedRoute('payment.withdrawal.post.checkout.apply', ['payment_withdrawal_id' => $invoice->id])->with('status',
                        __("You can't cancel the withdrawal because the request is already confirmed")
                    );
                }
            }

            if (empty($contractInfo['funded_at'])) {
                return redirect()->signedRoute('payment.withdrawal.post.checkout.apply', ['payment_withdrawal_id' => $invoice->id])->with('status',
                    __('Request processing ...')
                );
            }

            if ($request->input('action') == 1) {
                $res = $this->contractManager->contactReleaseByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($res) {
                    $invoice->status = MerchantWithdrawalInvoice::STATUS_PAYED;
                    $invoice->payed = now();
                    $invoice->save();
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                        6,
                        InvoicesHistory::getStatus(1),
                        InvoicesHistory::getSource(0),
                        InvoicesHistory::getSource(2),
                        json_encode($res),
                        'Payed'
                    );
                    $message = __('The withdrawal has been successfully completed');
                    Log::channel($channel)->info('BUY|CONTACT_CONFIRM|RELEASE|SUCCESS|MERCHANT_ID|' . $invoice->merchant_id . '|WITHDRAWAL|' . $invoice->id, ['res' => $res]);
                } else {
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                        6,
                        InvoicesHistory::getStatus(0),
                        InvoicesHistory::getSource(0),
                        InvoicesHistory::getSource(2),
                        json_encode($res),
                        'Chatex response incorrect'
                    );
                    Log::channel($channel)->info('BUY|CONTACT_CONFIRM|RELEASE|ERROR|MERCHANT_ID|' . $invoice->merchant_id . '|WITHDRAWAL|' . $invoice->id, ['res' => $res]);
                    return back()->with('status',
                        __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
                    );
                }
            }

            if (empty($message)) {
                $message = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10099']);
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                    8,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(3),
                    json_encode($contractInfo),
                    'Error'
                );
            }

            return redirect()->signedRoute('payment.withdrawal.post.checkout.complete', ['payment_withdrawal_id' => $invoice->id])->with('status', $message);
        }
        InvoicesHistory::addHistoryRecord(
            __METHOD__,
            $invoice->id,
            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
            8,
            InvoicesHistory::getStatus(0),
            InvoicesHistory::getSource(0),
            InvoicesHistory::getSource(2),
            json_encode($contractInfo),
            'Chatex response incorrect'
        );
        Log::channel($channel)->info('BUY|CONTACT_CONFIRM|ERROR|USER_ID|' . $invoice->merchant_id . '|WITHDRAWAL|' . $invoice->id, ['contact_res' => $contractInfo]);
        return redirect()->signedRoute('payment.withdrawal.post.checkout.apply', ['payment_withdrawal_id' => $invoice->id])->with('status',
            __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
        );
    }

    public function withdrawalPostCheckoutStatusCheck(Request $request)
    {
        $result = ['status' => 0];
        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $request->input('id'),
            'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_5,
        ])
            ->whereIn('status', [
                MerchantWithdrawalInvoice::STATUS_CREATED,
                MerchantWithdrawalInvoice::STATUS_IN_PROCESS,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED,
                MerchantWithdrawalInvoice::STATUS_USER_SELECTED,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT,
                MerchantWithdrawalInvoice::STATUS_CANCELED,
                MerchantWithdrawalInvoice::STATUS_CANCELED_BY_TIMEOUT,
            ])
            ->first();

        if ($invoice) {
            if (MerchantWithdrawalInvoice::isCancelStatus($invoice->status)) {
                $result['status'] = -1;
            } else {
                $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($contractInfo) {
                    if ($this->isContractCancel($contractInfo, $invoice)) {
                        $result['status'] = -1;
                    } else if ($this->isContractPaymentCompleted($contractInfo, $invoice)) {
                        $result['status'] = 2;
                        if (in_array($invoice->status, [MerchantWithdrawalInvoice::STATUS_CREATED, MerchantWithdrawalInvoice::STATUS_IN_PROCESS, MerchantWithdrawalInvoice::STATUS_USER_SELECTED, MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED])) {
                            $invoice->status = MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT;
                            $invoice->save();
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                                8,
                                InvoicesHistory::getStatus(0),
                                InvoicesHistory::getSource(0),
                                InvoicesHistory::getSource(2),
                                json_encode($contractInfo),
                                'Payment Confirmed'
                            );
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                                6,
                                InvoicesHistory::getStatus(1),
                                InvoicesHistory::getSource(2),
                                InvoicesHistory::getSource(3),
                                json_encode($contractInfo),
                                'Confirm Trader Payment'
                            );
                        }
                    } else if ($this->isContractFunded($contractInfo, $invoice)) {
                        $result['status'] = 1;
                    }
                }
                $new_attachments = [];
                if ($invoice->status == MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT) {
                    foreach (MerchantWithdrawalInvoiceAttachment::addAttachments($this->contractManager, $invoice) as $attachment) {
                        $new_attachments[] = URL::signedRoute('payment.withdrawal.post.attachment', ['id' => $attachment->id], now()->addMonth());
                    }
                }
                $result['attachments'] = MerchantWithdrawalInvoiceAttachment::where(['invoice_id' => $invoice->id])->orderBy('id')->get()->pluck('id');
                $result['new_attachments'] = $new_attachments;
            }
        }
        return response()->json($result);
    }

    public function withdrawalPostCheckoutComplete(Request $request, $payment_withdrawal_id)
    {
        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $payment_withdrawal_id
        ])
            ->firstOrFail();

        $isCompleted = false;
        $merchant = $this->getMerchant($invoice->merchant_id);
        if ($invoice->status == MerchantWithdrawalInvoice::STATUS_PAYED) {
            $isCompleted = true;
            if ($invoice->merchant_response_url) {
                return redirect()->away((is_null(parse_url($invoice->merchant_response_url, PHP_URL_HOST)) ? '//' : '') . $invoice->merchant_response_url);
            }
        }

        return view($this->viewTheme . 'checkout_complete', [
            'merchant' => $merchant,
            'invoice' => $invoice,
            'isCompleted' => $isCompleted
        ])->with('whiteLabelModel', new WhiteLabel);
    }

    public function withdrawalPostCheckoutDecline(Request $request, $payment_withdrawal_id)
    {
        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $payment_withdrawal_id
        ])
            ->whereIn('status', [
                MerchantWithdrawalInvoice::STATUS_CREATED,
                MerchantWithdrawalInvoice::STATUS_IN_PROCESS,
                MerchantWithdrawalInvoice::STATUS_CANCELED,
                MerchantWithdrawalInvoice::STATUS_CANCELED_BY_TIMEOUT,
            ])
            ->firstOrFail();

        $merchant = $this->getMerchant($invoice->merchant_id);

        if (!MerchantWithdrawalInvoice::isCancelStatus($invoice->status)) {
            $invoice->status = MerchantWithdrawalInvoice::STATUS_CANCELED;
            $invoice->save();
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                4,
                InvoicesHistory::getStatus(1),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(3),
                NULL,
                'Cancel Invoice'
            );
        }

        if ($invoice->merchant_cancel_url) {
            return redirect()->away((is_null(parse_url($invoice->merchant_cancel_url, PHP_URL_HOST)) ? '//' : '') . $invoice->merchant_cancel_url);
        }

        return view($this->viewTheme . 'checkout_complete', [
            'merchant' => $merchant,
            'invoice' => $invoice,
            'isCanceled' => true
        ])->with('whiteLabelModel', new WhiteLabel);
    }

    public function attachment(Request $request, $id)
    {
        $attachment = MerchantWithdrawalInvoiceAttachment::where(['id' => $id])
            ->firstOrFail();

        return response(file_get_contents($attachment->url), 200)->header('Content-type', $attachment->type);
    }
}
