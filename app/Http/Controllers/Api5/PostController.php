<?php

namespace App\Http\Controllers\Api5;

use App\Lib\PaymentApiHelper;
use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\Merchant;
use App\Http\Controllers\Controller;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\PaymentInvoice;
use App\Models\PaymentSystemType;
use App\Models\Property;
use App\Models\WhiteLabel;
use App\Models\ServiceProvidersProperty;
use App\Models\TraderAd;
use App\Services\AdsSelector;
use App\Services\ContractManager;
use App\Traits\Contract;
use App\Traits\Invoice;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use NumberFormatter;

class PostController extends Controller
{
    use Invoice;
    use Contract;

    private $adsService;
    private $merchantModel;
    protected $contractManager = null;
    private $viewTheme = 'payment.invoice.post.';
    private $viewVersion = 1;

    /** @var  Merchant */
    private $merchant = null;

    protected $chatexLBCProperties;
    protected $serviceProviderModel;
    protected $chatexLBC = null;
    private $service_provider;
    private $count_service_provider;

    public function __construct(
        Merchant $merchantModel,
        AdsSelector $adsService,
        ContractManager $contractManager
    )
    {
        $this->merchantModel = $merchantModel;
        $this->adsService = $adsService;
        $this->contractManager = $contractManager;
    }

    private function getMerchant($merchantId = null, $status = null)
    {
        $where = [
            'id' => $merchantId
        ];

        if ($status) {
            $where['status'] = $status;
        }

        $this->merchant = Merchant::where($where)->first();

        if (2 === $this->merchant->getViewVersion()) {
            $this->viewTheme = 'payment.invoice.v2.post.';
            $this->merchant->viewVersion = 2;
        }

        return $this->merchant;
    }

    public function invoicePostAdd(Request $request)
    {
        $this->setLocaleByCurrency($request->input('currency'));
        $merchant = $this->getMerchant($request->input('merchant_id'), 1);

        if($merchant){
            $this->service_provider = $merchant->default_service_provider();
            $this->count_service_provider = $merchant->service_providers()->count();
        }

        $validRequest = $this->getRequestAddValid($request, $merchant);
        if ($validRequest instanceof \Illuminate\Contracts\Validation\Validator) {
            return view($this->viewTheme . 'add')
                ->withErrors($validRequest)
                ->with('error', __('Invalid parameters'))
                ->with('whiteLabelModel', new WhiteLabel);
        }
        if (!$validRequest) {
            return view($this->viewTheme . 'add')
                ->with('error', __('Invalid parameters'))
                ->with('whiteLabelModel', new WhiteLabel);
        }

        if ($invoiceExists = PaymentInvoice::where(['merchant_id' => $merchant->id, 'merchant_order_id' => $request->input('order_id')])->first()) {
            Log::channel('api5_payment_invoice')->info('order_id exists', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id,
                'invoiceExists' => $invoiceExists,
                'method' => 'POST'
            ]);
            return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoiceExists->id]);
        }

        $currency2currency = $exact_currency = false;
        $amount = floatval($request->input('amount'));
        if (!$request->input('currency')) {
            $currency = 'USD';
        } else {
            $currency = $request->input('currency');
            $currency2currency = MerchantProperty::getProperty($merchant->id, 'currency2currency', false);
            if($request->has('exact_currency')){
                $exact_currency = (bool)$request->input('exact_currency');
            }else{
                $exact_currency = (bool)MerchantProperty::getProperty($merchant->id, 'exact_currency', false);
            }
        }

        $amount_usd = round($amount * $this->getCurrencyRate($currency), 2);

        if ($currency2currency) {
            $amount = 0;
        } else {
            $amount = $amount_usd;
        }

        $grow_token_amount = null;
        if ($request->input('grow_token')) {
            $rate = Property::getProperties('grow_token_usd2token');
            $coin_rate = $rate['coin_rate'] ?? 0;
            $correction = $rate['correction'] ?? 0;
            $grow_rate = round($coin_rate + ($coin_rate * $correction / 100), 8);
            $grow_token_amount = $amount_usd / $grow_rate;
        }

        if ($currency2currency) {
            $amount2pay = 0;
            $amount2service = 0;
            $amount2agent = 0;
            $amount2grow = 0;
            $commission_grow = null;
            $commission_agent = null;
            $commission_service = null;
        } else {
            $fee = $this->merchantModel->getFeeCommission(
                $merchant->id,
                $amount,
                PaymentInvoice::API_TYPE_5,
                MerchantCommission::PROPERTY_TYPE_COMMISSION,
                $this->service_provider->id

            );
            $amount2pay = $amount - $fee['total'];
            $amount2service = $fee['service']['amount'];
            $amount2agent = $fee['agent']['amount'];
            $amount2grow = $fee['grow']['amount'];
            $commission_grow = $fee['grow']['commission'];
            $commission_agent = $fee['agent']['commission'];
            $commission_service = $fee['service']['commission'];
        }

        $invoice = PaymentInvoice::create([
            'invoice_number' => PaymentInvoice::generateOrderNumber(),
            'merchant_id' => $merchant->id,
            'amount' => $amount,
            'amount2pay' => $amount2pay,
            'amount2service' => $amount2service,
            'amount2agent' => $amount2agent,
            'amount2grow' => $amount2grow,
            'commission_grow' => $commission_grow,
            'commission_agent' => $commission_agent,
            'commission_service' => $commission_service,
            'merchant_order_id' => $request->input('order_id'),
            'merchant_order_desc' => $request->input('order_desc'),
            'merchant_response_url' => $request->input('response_url'),
            'merchant_cancel_url' => $request->input('cancel_url'),
            'merchant_server_url' => $request->input('server_url'),
            'merchant_return_url' => $request->input('return_url'),
            'status' => PaymentInvoice::STATUS_CREATED,
            'input_currency' => $currency,
            'input_rate' => $this->getCurrencyRate($currency),
            'input_amount_value' => floatval($request->input('amount')),
            'api_type' => PaymentInvoice::API_TYPE_5,
            'grow_token_amount' => $grow_token_amount,
            'currency2currency' => $currency2currency,
            'exact_currency' => $exact_currency
        ]);

        if ($invoice) {
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                0,
                InvoicesHistory::getStatus(1),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(3),
                NULL,
                'Create Invoice Record'
            );
            return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id]);
        }

        return view($this->viewTheme . 'add')
            ->with('error', __('Invalid parameters'))
            ->with('merchant', $merchant)
            ->with('whiteLabelModel', new WhiteLabel);
    }

    public function invoicePostCheckout(Request $request, $payment_invoice_id = null)
    {
        if ($payment_invoice_id) {
            $invoice = PaymentInvoice::where([
                'id' => $payment_invoice_id
            ])
                ->firstOrFail();

            $this->setLocaleByCurrency($invoice->input_currency);

            if (($invoice->status == PaymentInvoice::STATUS_USER_SELECTED) || ($invoice->status == PaymentInvoice::STATUS_TRADER_CONFIRMED)) {
                return redirect()->signedRoute('payment.invoice.post.checkout.apply', $invoice->id);
            }

            if (PaymentInvoice::isCancelStatus($invoice->status)) {
                return redirect()->signedRoute('payment.invoice.post.checkout.cancel', $invoice->id);
            }

            if ($invoice->status == PaymentInvoice::STATUS_PAYED || $invoice->status == PaymentInvoice::STATUS_USER_CONFIRMED) {
                return redirect()->signedRoute('payment.invoice.post.checkout.complete', $invoice->id);
            }
            $merchant = $this->getMerchant($invoice->merchant_id);

            $error = $request->session()->pull('error');
            if (!empty($error)) {
                return view($this->viewTheme . 'checkout', [
                    'invoice' => $invoice,
                    'merchant' => $this->merchant,
                    'error' => $error
                ])->with('whiteLabelModel', new WhiteLabel);
            }

            $sell_coins = $this->adsService->getAdsByMerchant($this->merchant, [
                'amount' => $invoice->amount,
                'amount2pay' => $invoice->amount2pay,
                'currency' => $invoice->input_currency,
                'input_amount' => $invoice->input_amount_value,
                'currency2currency' => $invoice->currency2currency,
                'expense' => $invoice->merchant_expense,
                'type' => TraderAd::TYPE_SELL,
                'api' => PaymentInvoice::API_TYPE_5,
                'exact_currency' => $invoice->exact_currency
            ]);
            if (!$sell_coins) {
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                    0,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(2),
                    NULL,
                    'There are no payment systems available'
                );
                return view($this->viewTheme . 'checkout', [
                    'invoice' => $invoice,
                    'merchant' => $this->merchant,
                    'error' => __('There are no payment systems available.')
                ])->with('whiteLabelModel', new WhiteLabel);
            }

            $currencies = array_keys($sell_coins);

            $defaultFiat = $invoice->input_currency != 'USD' ? $invoice->input_currency : null;
            $defaultFiatKey = array_search($defaultFiat, $currencies);
            if ($defaultFiatKey === false) {
                $defaultFiat = null;
                $defaultFiatKey = 0;
            }
            $warning = $request->session()->pull('warning');
            return view($this->viewTheme . 'checkout', [
                'invoice' => $invoice,
                'merchant' => $this->merchant,
                'fmt' => new NumberFormatter('en_US', NumberFormatter::CURRENCY),
                'currencies' => $currencies,
                'sell_coins' => $sell_coins,
                'defaultFiat' => $defaultFiat,
                'defaultFiatKey' => $defaultFiatKey,
                'defaultAd' => $sell_coins[$currencies[$defaultFiatKey]][0] ?? null

            ])->with('whiteLabelModel', new WhiteLabel)
                ->with('warning', $warning);

        } else {
            $error = $request->session()->pull('error', __('Invalid parameters'));
            return view($this->viewTheme . 'checkout')
                ->with('error', $error)
                ->with('whiteLabelModel', new WhiteLabel);
        }
    }

    public function invoicePostCheckoutConfirm(Request $request)
    {

        $data['payment_system_id'] = $request->input('payment_system_id');
        $data['currency'] = $request->input('currency');

        $validator = Validator::make(
            $request->input(),
            [
                'id' => 'required|integer',
                'payment_system_id' => 'required|string',
                'currency' => 'required',
            ]
        );

        $validator->validate();

        $invoice = PaymentInvoice::where([
            'id' => $request->input('id')
        ])
            ->where('status', '!=', PaymentInvoice::STATUS_PAYED)
            ->whereNull('payment_id')
            ->first();

        if (!$invoice) {
            return back()->with('error',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10000'])
            );
        }
        $this->getMerchant($invoice->merchant_id, 1);
        $merchant = $this->merchant;

        $amount = $data['amount'] = $invoice->amount;
        $ad = $this->adsService->getAd($data['payment_system_id'], $invoice);
        if (!$ad || $ad['type'] != 'SELL') {
            Log::channel('payment_invoice_requests')->info('AD|NOT_FOUND|ERROR|MERCHANT_ID|' . $merchant->id, ['data' => $data, 'ad' => $ad]);
            return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('warning',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10001'])
            );
        }

        $available_coins = $this->getAvailableMerchantCoins($merchant->id);
        if (!$this->isAvailableCoins($ad, $available_coins, $invoice)) {
            return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('error',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002'])
            );
        }

        if(!$this->isValidCardNumber($ad, $invoice)){
            return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('warning',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10003'])
            );
        }

        if(!$this->isAvalableCoinRate($ad, $invoice)){
            return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('error',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10004'])
            );
        }

        if ($invoice->currency2currency) {
            $currencyRate = $this->getCurrencyRate($invoice->input_currency);
            $percentage_currency_rate = ServiceProvidersProperty::getProperty($ad['service_provider_type'], $ad['service_provider_id'], 'percentage_currency_rate', 0);
            $maxCurrencyRate = round(1/$currencyRate * (1 + $percentage_currency_rate / 100), 2);
            $minCurrencyRate = round(1/$currencyRate * (1 - $percentage_currency_rate / 100), 2);

            if ($ad['temp_price'] < $minCurrencyRate || $ad['temp_price'] > $maxCurrencyRate) {
                Log::channel($ad['log'])->info('AD|CURRENCY_RATE_PERCENTAGE|ERROR|MERCHANT_ID|' . $merchant->id, [
                    'invoice_id' => $invoice->id, 'ad' => $ad, 'currencyRate' => 1 / $currencyRate
                ]);
                return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('warning',
                    __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10005'])
                );
            }
            $currency_amount = round($invoice->input_amount_value, 2);
        } else {
            $coin_amount = $data['amount'] * $this->getCoinRate(strtolower($ad['coin']));
            $rounding_input_amount = MerchantProperty::getProperty($merchant->id, 'rounding_input_amount');
            if ($rounding_input_amount) {
                $currency_amount = ceil($ad['temp_price'] * $coin_amount);
            } else {
                $currency_amount = round($ad['temp_price'] * $coin_amount, 2);
            }
        }
        Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|REQUEST|MERCHANT_ID|' . $merchant->id, ['ad' => $data['payment_system_id'], 'amount' => $currency_amount, 'invoice_id' => $invoice->id]);
        $contract = $this->contractManager->contractCreate($ad, $currency_amount, $merchant, $invoice);
        if ($contract) {
            $contractInfo = $this->contractManager->contractInfoByContract($contract);
            if ($contractInfo) {
                $ps_amount = $contractInfo['amount_btc'] ?? null;
                $account_info = $contractInfo['account_info'] ?? null;
                if(!$account_info){
                    $account_info = $ad['account_info'] ?? null;
                }
                if ($invoice->currency2currency && $ps_amount) {
                    $amount = round($ps_amount, 2);
                }
                $fee = $this->merchantModel->getFeeCommission(
                    $merchant->id,
                    $amount,
                    PaymentInvoice::API_TYPE_5,
                    MerchantCommission::PROPERTY_TYPE_COMMISSION,
                    $ad['service_provider_id']
                );
                $invoice->amount = $amount;
                $invoice->amount2pay = $amount - $fee['total'];
                $invoice->amount2service = $fee['service']['amount'];
                $invoice->amount2agent = $fee['agent']['amount'];
                $invoice->amount2grow = $fee['grow']['amount'];
                $invoice->commission_grow = $fee['grow']['commission'];
                $invoice->commission_agent = $fee['agent']['commission'];
                $invoice->commission_service = $fee['service']['commission'];
                $invoice->payment_id = $contractInfo['id'];
                $invoice->ps_amount = $ps_amount;
                $invoice->ps_currency = strtolower($ad['coin']);
                $invoice->fiat_amount = $contractInfo['amount'];
                $invoice->fiat_currency = $contractInfo['currency'];
                $invoice->addition_info = json_encode([
                    'contact' => $contractInfo,
                    'ad' => $ad
                ]);
                $invoice->account_info = $account_info;
                $invoice->service_provider_id = $ad['service_provider_id'];
                $invoice->status = PaymentInvoice::STATUS_USER_SELECTED;
                $invoice->save();
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                    1,
                    InvoicesHistory::getStatus(1),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(2),
                    json_encode($contractInfo),
                    'Select Trader Successful'
                );
                return redirect()->signedRoute('payment.invoice.post.checkout.apply', $invoice->id);
            }
        }
        Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|ERROR|MERCHANT_ID|' . $merchant->id, ['data' => $data, 'payment_invoice_id' => $invoice->id, 'amount' => $currency_amount]);
        return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('warning',
            __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
        );
    }

    public function invoicePostCheckoutApply(Request $request, $payment_invoice_id)
    {
        $invoice = PaymentInvoice::where([
            'id' => $payment_invoice_id,
        ])
            ->whereIn('status', [PaymentInvoice::STATUS_USER_SELECTED, PaymentInvoice::STATUS_TRADER_CONFIRMED])
            ->first();

        if (!$invoice) {
            return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $payment_invoice_id])->with('error',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10000'])
            );
        }

        $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);

        $deal_accepted = false;
        if ($contractInfo) {
            if ($this->isContractCancel($contractInfo, $invoice)) {
                return redirect()->signedRoute('payment.invoice.post.checkout.cancel', $invoice->id);
            }

            if ($this->isContractComplete($contractInfo, $invoice)){
                return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('error',
                    __('Payment closed successfully')
                );
            }

            if ($this->isContractClosed($contractInfo, $invoice)) {
                return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('error',
                    __('Payment closed')
                );
            }
            if (!empty($contractInfo['funded_at'])) {
                try {
                    $expired_at = Carbon::parse($contractInfo['funded_at']);
                    if(isset($contractInfo['expired_at']) && $contractInfo['expired_at']){
                        $expired_at = $contractInfo['expired_at'];
                    }else{
                        $expired_at = $expired_at->addSeconds(config('app.chatex_deal_expiry'));
                    }

                    $expired = now()->diffInSeconds($expired_at, false);
                    if ($expired < 0) {
                        $expired = 0;
                    }
                } catch (Exception $e) {
                    $expired = 0;
                }
                $deal_accepted = true;
            }
            $addition_info = json_decode($invoice['addition_info'], true);

            $this->getMerchant($invoice->merchant_id);

            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                2,
                InvoicesHistory::getStatus(1),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(3),
                json_encode($contractInfo),
                'Response Apply Page'
            );

            $template = 'confirm_chatex_lbc';

            return view($this->viewTheme . $template, [
                'merchant' => $this->merchant,
                'id' => $invoice->id,
                'deal_accepted' => $deal_accepted,
                'amount' => $contractInfo['amount'],
                'address' => $contractInfo['address'] ?? null,
                'clean_card_number' => $contractInfo['account_info'] ?? null,
                'account_info' => $invoice->account_info ?? null,
                'bank_name' => $addition_info['ad']['bank_name'] ?? null,
                'currency' => $addition_info['ad']['currency'] ?? null,
                'payment_system_type' => $addition_info['ad']['payment_system_type'] ?? PaymentSystemType::CARD_NUMBER,
                'expired' => $expired ?? 0
            ])->with('whiteLabelModel', new WhiteLabel);
        } else {
            $channel = $this->getLogChannel($invoice);
            Log::channel($channel)->info('CONTACT_CONFIRM_SHOW|ERROR|INPUT_INVOICE|' . $invoice->id, ['contact_res' => $contractInfo]);
        }
        InvoicesHistory::addHistoryRecord(
            __METHOD__,
            $invoice->id,
            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
            2,
            InvoicesHistory::getStatus(0),
            InvoicesHistory::getSource(0),
            InvoicesHistory::getSource(2),
            json_encode($contractInfo),
            'Service Provider response incorrect'
        );
        return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('error',
            __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
        );
    }

    public function invoicePostCheckoutStatus(Request $request)
    {
        $messages = [
            'document.max' => __('The maximum file size is over 2Mb.'),
            'document.uploaded' => __('The maximum file size is over 2Mb.')
        ];

        $rules = [
            'id' => 'required|numeric',
            'action' => 'required|numeric',
            'document' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048'
        ];

        $validator = Validator::make($request->only(['id', 'action', 'document']), $rules, $messages);
        if ($validator->fails()) {
            Log::channel('api5_payment_invoice')->info('Validation fails', [
                'errors' => $validator->errors()->toArray(),
                'inputs' => $request->input()
            ]);
            if ($request->hasFile('document')) {
                Log::channel('api5_payment_invoice')->info('Validation fails. Document', [
                    'document' => optional($request->file('document'))->getClientOriginalName() ?? null
                ]);
            }
            return back()->withErrors($validator);
        }

        $invoice = PaymentInvoice::where([
            'id' => $request->input('id'),
        ])->whereIn('status',[PaymentInvoice::STATUS_TRADER_CONFIRMED])
            ->first();

        if (!$invoice) {
            return redirect()->signedRoute('payment.invoice.post.checkout')->with('error',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10000'])
            );
        }
        $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
        $channel = $this->getLogChannel($invoice);
        if ($contractInfo) {
            Log::channel($channel)->info('CONTACT_CONFIRM|CHECK|INPUT_INVOICE|' . $invoice->id, ['contact_res' => $contractInfo]);
            if ($this->isContractCancel($contractInfo, $invoice)) {
                return redirect()->signedRoute('payment.invoice.post.checkout.cancel', $invoice->id);
            }

            if ($this->isContractComplete($contractInfo, $invoice)) {
                return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('error',
                    __('Payment closed successfully')
                );
            }

            if ($this->isContractClosed($contractInfo, $invoice)) {
                return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('error',
                    __('Payment closed')
                );
            }

            if ($request->input('action') == 0) {
                $res = $this->contractManager->contractCancelByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($res) {
                    $invoice->status = PaymentInvoice::STATUS_CANCELED;
                    $invoice->save();
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                        4,
                        InvoicesHistory::getStatus(1),
                        InvoicesHistory::getSource(0),
                        InvoicesHistory::getSource(2),
                        json_encode($res),
                        'Cancel Invoice'
                    );
                    Log::channel($channel)->info('CONTACT_CONFIRM|CANCEL|INPUT_INVOICE|' . $invoice->id, ['res' => $res]);
                    return redirect()->signedRoute('payment.invoice.post.checkout.cancel', $invoice->id);
                } else {
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                        4,
                        InvoicesHistory::getStatus(0),
                        InvoicesHistory::getSource(0),
                        InvoicesHistory::getSource(2),
                        json_encode($contractInfo),
                        'Service Provider response incorrect'
                    );
                    Log::channel($channel)->info('CONTACT_CONFIRM|CANCEL|ERROR|INPUT_INVOICE|' . $invoice->id, ['res' => $res]);
                }
            }

            if (!$this->isContractFunded($contractInfo)) {
                return back()->with('status',
                    __('Request processing ...')
                );
            }

            if ($request->input('action') == 1) {
                $res = $this->contractManager->contactMarkAsPaidByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($res) {
                    $invoice->status = PaymentInvoice::STATUS_USER_CONFIRMED;
                    $invoice->save();
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                        6,
                        InvoicesHistory::getStatus(1),
                        InvoicesHistory::getSource(0),
                        InvoicesHistory::getSource(2),
                        json_encode($res),
                        'User Confirm'
                    );
                    $message = __('Payment in progress ...');
                    Log::channel($channel)->info('CONTACT_CONFIRM|MARK_AS_PAID|SUCCESS|INPUT_INVOICE|' . $invoice->id, ['res' => $res]);
                    if ($request->hasFile('document')) {
                        $requestMessage = $this->contractManager->contactMessagePostByProvider([
                                'id' => $invoice['payment_id'],
                                'service_provider_id' => $invoice['service_provider_id'],
                                'document' => $request->file('document'),
                                'user_id' =>  $invoice->merchant_id
                        ]);
                        $requestMessageResult = $requestMessage ? 1 : 0;
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                            7,
                            InvoicesHistory::getStatus($requestMessageResult),
                            InvoicesHistory::getSource(3),
                            InvoicesHistory::getSource(2),
                            json_encode($requestMessage),
                            'Attachment'
                        );
                    }
                } else {
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                        6,
                        InvoicesHistory::getStatus(0),
                        InvoicesHistory::getSource(0),
                        InvoicesHistory::getSource(2),
                        json_encode($res),
                        'Service Provider response incorrect'
                    );
                    Log::channel($channel)->info('CONTACT_CONFIRM|MARK_AS_PAID|ERROR|INPUT_INVOICE|' . $invoice->id, ['res' => $res]);
                    return back()->with('status',
                        __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
                    );
                }
            }
            if (empty($message)) {
                $message = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10099']);
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                    8,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(2),
                    json_encode($contractInfo),
                    'Error'
                );
            }

            return redirect()->signedRoute('payment.invoice.post.checkout.complete', ['payment_invoice_id' => $invoice->id])->with('status', $message);
        }
        InvoicesHistory::addHistoryRecord(
            __METHOD__,
            $invoice->id,
            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
            8,
            InvoicesHistory::getStatus(0),
            InvoicesHistory::getSource(0),
            InvoicesHistory::getSource(2),
            json_encode($contractInfo),
            'Service Provider response incorrect'
        );
        Log::channel($channel)->info('CONTACT_CONFIRM|ERROR|INPUT_INVOICE|' . $invoice->id, ['contact_info' => $contractInfo]);
        return redirect()->signedRoute('payment.invoice.post.checkout', ['payment_invoice_id' => $invoice->id])->with('error',
            __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
        );
    }

    public function invoicePostCheckoutStatusCheck(Request $request)
    {
        $result = ['status' => 0];
        $invoice_id = $request->input('invoice_id');
        $invoice = PaymentInvoice::where([
            'id' => $invoice_id,
        ])
            ->whereIn('status', [
                PaymentInvoice::STATUS_PAYED,
                PaymentInvoice::STATUS_CREATED,
                PaymentInvoice::STATUS_CANCELED,
                PaymentInvoice::STATUS_CANCELED_BY_TIMEOUT,
                PaymentInvoice::STATUS_CANCELED_BY_REVERT,
                PaymentInvoice::STATUS_TRADER_CONFIRMED,
                PaymentInvoice::STATUS_USER_SELECTED,
            ])
            ->first();

        if ($invoice) {
            if (PaymentInvoice::isCancelStatus($invoice->status)) {
                $result['status'] = -1;
            } else {
                $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($contractInfo) {
                    if ($this->isContractCancel($contractInfo)) {
                        $result['status'] = -1;
                    }
                    if (!empty($contractInfo['funded_at']) && !$this->isContractCancel($contractInfo)) {
                        try {
                            $expired_at = Carbon::parse($contractInfo['funded_at']);
                            if(isset($contractInfo['expired_at']) && $contractInfo['expired_at']){
                                $expired_at = $contractInfo['expired_at'];
                            }else{
                                $expired_at = $expired_at->addSeconds(config('app.chatex_deal_expiry'));
                            }
                            $expired = now()->diffInSeconds($expired_at, false);
                            if ($expired < 0) {
                                $expired = 0;
                            }
                        } catch (Exception $e) {
                            $expired = null;
                        }
                        $result['status'] = 1;
                        $result['expired'] = $expired;
                        $addition_info = json_decode($invoice['addition_info'], true);
                        if(isset($addition_info['ad']['payment_system_type']) && $addition_info['ad']['payment_system_type'] == PaymentSystemType::CRYPTO){
                            $result['address_amount'] = $contractInfo['address_amount'];
                        }
                        if($contractInfo['account_info'] ?? null){
                            $payment_system_type = $addition_info['ad']['payment_system_type'] ?? PaymentSystemType::CARD_NUMBER;
                            if($payment_system_type == PaymentSystemType::IBAN || $payment_system_type == PaymentSystemType::CARD_NUMBER){
                                $result['account_info'] = implode('<span>&nbsp;</span>', str_split($contractInfo['account_info'], 4));
                            }else{
                                $result['account_info'] = $contractInfo['account_info'];
                            }
                        }
                    }
                    if ($this->isContractComplete($contractInfo)) {
                        $addition_info = json_decode($invoice['addition_info'], true);
                        if(isset($addition_info['ad']['payment_system_type']) && $addition_info['ad']['payment_system_type'] == PaymentSystemType::CRYPTO){
                            $result['status'] = 2;
                        }


                    }
                }
            }
        }
        return response()->json($result);
    }

    public function invoicePostCheckoutComplete(Request $request, $payment_invoice_id)
    {
        $invoice = PaymentInvoice::where([
            'id' => $payment_invoice_id
        ])
            ->firstOrFail();
        $isCompleted = false;
        $this->getMerchant($invoice->merchant_id);


        if (PaymentInvoice::isCancelStatus($invoice->status)) {
            return redirect()->signedRoute('payment.invoice.post.checkout.cancel', $invoice->id);
        }

        if ($invoice->status == PaymentInvoice::STATUS_PAYED) {
            $isCompleted = true;
            if ($invoice->merchant_response_url) {
                return redirect()->away((is_null(parse_url($invoice->merchant_response_url, PHP_URL_HOST)) ? '//' : '') . $invoice->merchant_response_url);
            }
        }

        return view($this->viewTheme . 'checkout_complete', [
            'merchant' => $this->merchant,
            'invoice' => $invoice,
            'isCompleted' => $isCompleted
        ])->with('whiteLabelModel', new WhiteLabel);
    }

    public function invoicePostCheckoutDecline(Request $request, $payment_invoice_id)
    {
        $invoice = PaymentInvoice::where([
            'id' => $payment_invoice_id
        ])
            ->firstOrFail();

        $this->getMerchant($invoice->merchant_id);

        if (!PaymentInvoice::isCancelStatus($invoice->status)) {
            $invoice->status = PaymentInvoice::STATUS_CANCELED;
            $invoice->save();
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                4,
                InvoicesHistory::getStatus(1),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(3),
                NULL,
                'Cancel Invoice'
            );
        }

        if ($invoice->merchant_cancel_url) {
            return redirect()->away((is_null(parse_url($invoice->merchant_cancel_url, PHP_URL_HOST)) ? '//' : '') . $invoice->merchant_cancel_url);
        }

        return view($this->viewTheme . 'checkout_complete', [
            'merchant' => $this->merchant,
            'invoice' => $invoice,
            'isCanceled' => true
        ])->with('whiteLabelModel', new WhiteLabel);
    }

    private function getRequestAddValid(Request $request, $merchant)
    {
        $rules = [
            'merchant_id' => 'required|numeric',
            'amount' => 'required|numeric|gt:0|regex:/^\d*(\.\d{1,2})?$/',
            'order_id' => 'required',
            'signature' => 'required'
        ];
        $validator = Validator::make($request->input(), $rules, [], [
            'amount' => __('amount'),
            'merchant_id' => __('merchant_id'),
            'order_id' => __('order_id'),
            'signature' => __('signature')
        ]);
        if ($validator->fails()) {
            Log::channel('api5_payment_invoice')->info('Validation fails', [
                'errors' => $validator->errors()->toArray(),
                'inputs' => $request->input()
            ]);
            return $validator;
        }
        if (!$merchant) {
            Log::channel('api5_payment_invoice')->info('merchant fail', [
                'inputs' => $request->input()
            ]);
            $validator->after(function ($validator) {
                $validator->errors()->add('merchant_id', __('Error: :error', ['error' => 'P10001']));
            });
        }
        if ($validator->fails()) {
            return $validator;
        }
        if (!PaymentApiHelper::checkSignature($request->input(), $merchant->api5_key)) {
            Log::channel('api5_payment_invoice')->info('checkSignature fail', [
                'inputs' => $request->input(),
                'api5_key' => $merchant->api5_key,
                'merchant_id' => $merchant->id
            ]);
            $validator->after(function ($validator) {
                $validator->errors()->add('signature', __('Error: :error', ['error' => 'P10002']));
            });
        }
        if (!$request->input('currency')) {
            $currency = 'USD';
        } else {
            $currency = $request->input('currency');
        }
        if (!$this->isAvalableCurrency($currency)) {
            Log::channel('api5_payment_invoice')->info('isAvailableCurrency fail', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $validator->after(function ($validator) {
                $validator->errors()->add('currency', __('Error: :error', ['error' => 'P10003']));
            });
        }

        $amount = floatval($request->input('amount'));
        $amount = round($amount * $this->getCurrencyRate($currency), 2);

        $min_invoice_amount = config('app.min_payment_invoice_amount');
        if($this->count_service_provider == 1){
            $min_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_payment_invoice_amount', $min_invoice_amount, $this->service_provider->id);
        }else{
            $props_min_amount = MerchantProperty::getPropertiesByName($merchant->id, 'min_payment_invoice_amount');
            foreach ($props_min_amount as $value) {
                $min_invoice_amount = ($value < $min_invoice_amount) ? $value : $min_invoice_amount;
            }
        }

        if ($amount < $min_invoice_amount) {
            Log::channel('api5_payment_invoice')->info('min_payment_invoice_amount fail', [
                'inputs' => $request->input(),
                'amount' => $amount,
                'min_payment_invoice_amount' => $min_invoice_amount,
                'merchant_id' => $merchant->id
            ]);

            $validator->after(function ($validator) {
                $validator->errors()->add('amount', __('Error: :error', ['error' => 'P10004']));
            });
        }
        if ($validator->fails()) {
            return $validator;
        }

        return true;
    }

    public function getQr(Request $request, $id)
    {
        $invoice = PaymentInvoice::where([
            'id' => $id,
        ])
            ->with('crypto_address')
            ->firstOrFail();

        if(empty($invoice->crypto_address->address)){
            abort(404);
        }
        $size = 200;
        $imageBackEnd = new ImagickImageBackEnd();
        $renderer = new ImageRenderer(
            (new RendererStyle($size))->withSize($size),
            $imageBackEnd
        );

        $bacon = new Writer($renderer);

        $data = $bacon->writeString(
            $invoice->crypto_address->address,
            'utf-8'
        );

        $response = Response::make($data, 200);
        $response->header("Content-Type", "image/png");
        return $response;
    }

}
