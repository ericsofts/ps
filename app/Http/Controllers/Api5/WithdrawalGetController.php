<?php

namespace App\Http\Controllers\Api5;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\PaginationCollection;
use App\Lib\Chatex\ChatexApi;
use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use App\Traits\Invoice;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class WithdrawalGetController extends Controller
{
    use Invoice;

    private Merchant $merchantModel;

    public function __construct(Merchant $merchantModel)
    {
        $this->merchantModel = $merchantModel;
    }

    public function invoices(Request $request)
    {
        $rules = [
            'from_date' => ['sometimes', 'required', 'integer'],
            'to_date' => ['sometimes', 'required', 'integer'],
            'status' => ['sometimes', 'required', Rule::in([-1, 0, 1, 2, 3])]
        ];

        $response['status'] = 0;
        $merchant = $request->input('merchant');
        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            $response['error'] = $validator->errors();
            return $response;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');
        if (!empty($from_date)) {
            try {
                $from_date = Carbon::createFromTimestamp($from_date);
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::createFromTimestamp($to_date);
            } catch (Exception $e) {
                $to_date = null;
            }
        }

        if ($from_date && $from_date->isBefore(now()->subDays(30))) {
            $validator->after(function ($validator) {
                $validator->errors()->add('from_date', __('from_date cannot be greater than 30 days'));
            });
        }

        if ($to_date && $from_date->isBefore(now()->subDays(30))) {
            $validator->after(function ($validator) {
                $validator->errors()->add('to_date', __('to_date cannot be greater than 30 days'));
            });
        }

        if ($validator->fails()) {
            $response['error'] = $validator->errors();
            return $response;
        }

        $query = MerchantWithdrawalInvoice::where([
            'merchant_id' => $merchant->id
        ])
            ->select([
                'id as invoice_id',
                'status',
                'amount',
                'amount2pay as merchant_amount',
                'input_amount_value as amount_currency',
                'input_currency as currency',
                'merchant_order_id as order_id',
                'merchant_order_desc as order_desc',
                'merchant_response_url as response_url',
                'merchant_cancel_url as cancel_url',
                'merchant_server_url as server_url',
                //'api_type',
                'account_info',
                'created_at'
            ])
            ->orderBy('created_at', 'DESC');

        if (!is_null($status)) {
            if ($status == -1) {
                $status = MerchantWithdrawalInvoice::getCancelStatuses();
            } else {
                $status = [$status];
            }
            $query->whereIn('status', $status);
        }

        if (!$from_date) {
            $from_date = now()->subDays(30);
        }
        $query->where('created_at', '>=', $from_date);
        if ($to_date) {
            $query->where('created_at', '<=', $to_date);
        }

        $invoices = new PaginationCollection($query->paginate(config('app.default.merchant_pagination_limit')));

        return array_merge(['status' => 1], $invoices->toArray());
    }

    public function withdrawalStatus(Request $request)
    {
        $response['status'] = 0;
        $merchant = $request->input('merchant');

        $rules = [
            'invoice_id' => 'required_without:order_id|numeric',
            'order_id' => 'required_without:invoice_id'
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            $response['error'] = $validator->errors();
            return $response;
        }

        $query = MerchantWithdrawalInvoice::where([
            'merchant_id' => $merchant->id
        ]);

        if ($invoice_id = $request->input('invoice_id')) {
            $query->where('id', '=', $invoice_id);
        } elseif ($order_id = $request->input('order_id')) {
            $query->where('merchant_order_id', '=', $order_id);
        }

        $invoice = $query->first();

        if (!$invoice) {
            $response['error']['invoice_id'][] = __('Payment not found');
            return $response;
        }
        $status = [];
        switch ($invoice->status) {
            case MerchantWithdrawalInvoice::STATUS_CREATED:
            case MerchantWithdrawalInvoice::STATUS_IN_PROCESS:
            case MerchantWithdrawalInvoice::STATUS_USER_SELECTED:
                $status = [
                    'status' => 0,
                    'message' => __('Payment is being processed')
                ];
                break;
            case MerchantWithdrawalInvoice::STATUS_CANCELED:
            case MerchantWithdrawalInvoice::STATUS_CANCELED_BY_TIMEOUT:
                $status = [
                    'status' => -1,
                    'message' => __('Payment canceled')
                ];
                break;
            case MerchantWithdrawalInvoice::STATUS_PAYED:
                $status = [
                    'status' => 1,
                    'message' => __('Payment paid'),
                ];
                break;
            case MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED:
                $status = [
                    'status' => 3,
                    'message' => __('Payment has been approved')
                ];
                break;
            case MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT:
                $status = [
                    'status' => 2,
                    'message' => __('Payment completed')
                ];
                break;
        }

        $response['status'] = 1;
        $response['data'] = $status;
        $response['data']['invoice'] = [
            'invoice_id' => $invoice->id,
            'merchant_id' => $invoice->merchant_id,
            'order_id' => $invoice->merchant_order_id,
            'amount' => round($invoice->amount, 2),
            'order_desc' => $invoice->merchant_order_desc,
            'merchant_amount' => round($invoice->amount2pay, 2),
            'account_info' => mask_credit_card($invoice->account_info)
        ];

        return $response;
    }

    public function withdrawalGetAdd(Request $request)
    {

        $response = [
            'status' => 0,
            'message' => 'Invalid parameters'
        ];

        $rules = [
            'amount' => 'required|numeric|gt:0|regex:/^\d*(\.\d{1,2})?$/',
            'order_id' => 'required'
        ];

        $validator = Validator::make($request->input(), $rules, [], [
            'amount' => __('amount'),
            'order_id' => __('order_id')
        ]);

        $amount = floatval($request->input('amount'));
        $merchant = $request->input('merchant');

        if ($validator->fails()) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('Validation fails', [
                'errors' => $validator->errors()->toArray(),
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id ?? null
            ]);
            $response['errors'] = $validator->errors();
            return $response;
        }

        if (!$merchant) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('merchant fail', [
                'inputs' => $request->input()
            ]);
            $validator->after(function ($validator) {
                $validator->errors()->add('merchant_id', 'P10001');
            });
            $validator->fails();
            $response['errors'] = $validator->errors();
            return $response;
        }

        $currency2currency = $exact_currency = false;
        if (!$request->input('currency')) {
            $currency = 'USD';
        } else {
            $currency = $request->input('currency');
            $currency2currency = MerchantProperty::getProperty($merchant->id, 'currency2currency', false);
            if($request->has('exact_currency')){
                $exact_currency = (bool)$request->input('exact_currency');
            }else{
                $exact_currency = (bool)MerchantProperty::getProperty($merchant->id, 'exact_currency', false);
            }
        }
        if (!$this->isAvalableCurrency($currency)) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('isAvailableCurrency fail', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $validator->after(function ($validator) {
                $validator->errors()->add('currency', 'P10003');
            });
            $validator->fails();
            $response['errors'] = $validator->errors();
            return $response;
        }

        $amount_usd = round($amount * $this->getCurrencyRate($currency), 2);
        if ($currency2currency) {
            $amount = 0;
        } else {
            $amount = $amount_usd;
        }

        $service_provider = $merchant->default_service_provider();
        $count_service_provider = $merchant->service_providers()->count();

        $min_invoice_amount = config('app.min_merchant_withdrawal_amount');
        if($count_service_provider == 1){
            $min_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_merchant_withdrawal_amount', $min_invoice_amount, $service_provider->id);
        }else{
            $props_min_amount = MerchantProperty::getPropertiesByName($merchant->id, 'min_merchant_withdrawal_amount');
            foreach ($props_min_amount as $value) {
                $min_invoice_amount = ($value < $min_invoice_amount) ? $value : $min_invoice_amount;
            }
        }

        if (!$available_coins = $this->getAvailableMerchantCoins($merchant->id)) {
            return [
                'status' => 0,
                'message' => __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002'])
            ];
        }

        if (!$rates = $this->getCoinRates($available_coins)) {
            return [
                'status' => 0,
                'message' => __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10004'])
            ];
        }

        if($invoiceExists = MerchantWithdrawalInvoice::where(['merchant_id' => $merchant->id, 'merchant_order_id' => $request->input('order_id')])->first()){
            Log::channel('api5_merchant_withdrawal_invoice')->info('order_id exists', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id,
                'invoiceExists' => $invoiceExists,
                'method' => 'GET'
            ]);
            $response['errors']['order_id'][] = __('Error: :error', ['error' => 'P10006']);
            return $response;
        }
        if ($currency2currency) {
            $amount2pay = 0;
            $amount2service = 0;
            $amount2agent = 0;
            $amount2grow = 0;
            $commission_grow = null;
            $commission_agent = null;
            $commission_service = null;
        }else{
            if ($request->input('merchant_expense')) {
                $fee = $this->merchantModel->getRevertFeeCommission(
                    $merchant->id,
                    $amount,
                    MerchantWithdrawalInvoice::API_TYPE_5,
                    MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                    $service_provider->id
                );
                $amount2pay = $amount;
                $amount += $fee['total'];
                $amount_usd = $amount;
            } else {
                $fee = $this->merchantModel->getFeeCommission(
                    $merchant->id,
                    $amount,
                    MerchantWithdrawalInvoice::API_TYPE_5,
                    MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                    $service_provider->id
                );
                $amount2pay = $amount - $fee['total'];
            }
            $amount2service = $fee['service']['amount'];
            $amount2agent = $fee['agent']['amount'];
            $amount2grow = $fee['grow']['amount'];
            $commission_grow = $fee['grow']['commission'];
            $commission_agent = $fee['agent']['commission'];
            $commission_service = $fee['service']['commission'];
        }

        if ($amount_usd < $min_invoice_amount) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('min_merchant_withdrawal_amount fail', [
                'inputs' => $request->input(),
                'amount' => $amount_usd,
                'min_merchant_withdrawal_amount' => $min_invoice_amount,
                'merchant_id' => $merchant->id
            ]);
            return [
                'status' => 0,
                'message' => __('The minimum amount must be greater than or equal to :amount', ['amount' => $min_invoice_amount])
            ];
        }
        $merchantBalance = $merchant->balance_total();
        if ($merchantBalance <= 0) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('merchantBalance fail', [
                'inputs' => $request->input(),
                'amount' => $amount_usd,
                'merchantBalance' => $merchantBalance,
                'merchant_id' => $merchant->id
            ]);
            return [
                'status' => 0,
                'message' => __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10007'])
            ];
        }

        if ($amount_usd > $merchantBalance) {
            Log::channel('api5_merchant_withdrawal_invoice')->info('merchantBalance fail', [
                'inputs' => $request->input(),
                'amount' => $amount_usd,
                'merchantBalance' => $merchantBalance,
                'merchant_id' => $merchant->id
            ]);
            return [
                'status' => 0,
                'message' => __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10007'])
            ];
        }

        if($service_provider->name == ServiceProvider::CHATEX_PROVIDER){
            $chatexProperties = ServiceProvidersProperty::getProperties(ChatexApi::OBJECT_NAME);
            $chatex = new ChatexApi($chatexProperties['base_url'], $chatexProperties['api_token']);

            foreach ($available_coins as $coin) {
                $wallet = $chatex->wallet($coin);
                $walletBalance = $wallet['amount'] ?? 0;
                if ($walletBalance < $amount_usd) {
                    Log::channel('api5_merchant_withdrawal_invoice')->info('walletBalance fail', [
                        'inputs' => $request->input(),
                        'amount' => $amount_usd,
                        'merchantBalance' => $merchantBalance,
                        'walletBalance' => $walletBalance,
                        'merchant_id' => $merchant->id
                    ]);
                    return [
                        'status' => 0,
                        'message' => __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10008'])
                    ];
                }
            }
        }

        $invoice = MerchantWithdrawalInvoice::create([
            'merchant_id' => $merchant->id,
            'amount' => $amount,
            'amount2pay' => $amount2pay,
            'amount2service' => $amount2service,
            'amount2agent' => $amount2agent,
            'amount2grow' => $amount2grow,
            'commission_grow' => $commission_grow,
            'commission_agent' => $commission_agent,
            'commission_service' => $commission_service,
            'merchant_order_id' => $request->input('order_id'),
            'merchant_order_desc' => $request->input('order_desc'),
            'merchant_response_url' => $request->input('response_url'),
            'merchant_cancel_url' => $request->input('cancel_url'),
            'merchant_server_url' => $request->input('server_url'),
            'merchant_return_url' => $request->input('return_url'),
            'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC,
            'status' => MerchantWithdrawalInvoice::STATUS_CREATED,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_5,
            'merchant_expense' => $request->input('merchant_expense'),
            'input_currency' => $currency,
            'input_rate' => $this->getCurrencyRate($currency),
            'input_amount_value' => floatval($request->input('amount')),
            'currency2currency' => $currency2currency,
            'exact_currency' => $exact_currency
        ]);

        if ($invoice) {
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                0,
                InvoicesHistory::getStatus(1),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(3),
                NULL,
                'Create Invoice Record'
            );
            $response = [
                'url' => URL::signedRoute('payment.withdrawal.post.checkout', ['payment_withdrawal_id' => $invoice->id]),
                'merchant_id' => $merchant->id,
                'invoice_id' => $invoice->id,
                'order_id' => $invoice->merchant_order_id,
                'amount' => round($invoice->amount, 2),
                'order_desc' => $invoice->merchant_order_desc,
                'merchant_amount' => round($invoice->amount2pay, 2),
                'status' => 1
            ];
            return $response;
        }

        return $response;
    }

}
