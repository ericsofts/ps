<?php

namespace App\Http\Controllers;

use App\Models\Merchant;
use App\Models\Otp;
use App\Models\PaymentInvoice;
use App\Notifications\Otp as notifyOTP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OTPController extends Controller
{
    public function resend(Request $request)
    {
        $result['status'] = 0;
        $result['message'] = __('Invalid parameters');

        $rules = [
            'invoice_id' => 'required|numeric',
            'invoice_type' => 'required|numeric',
        ];

        $validator = Validator::make($request->input(), $rules, [],);
        if ($validator->fails()) {
            //$result['error'] = $validator->errors();
            return $result;
        }

        $invoice = PaymentInvoice::where([
            'id' => $request->input('invoice_id'),
            'user_id' => $request->user()->id,
        ])
            ->where('status', '!=', PaymentInvoice::STATUS_PAYED)
            ->first();

        if(!$invoice){
            return $result;
        }

        Otp::where([
            'user_id' => $request->user()->id,
            'invoice_id' => $request->input('invoice_id'),
            'invoice_type' => $request->input('invoice_type'),
            'status' => 1,
        ])->update(['status' => 0]);

        $otp = Otp::create([
            'user_id' => $request->user()->id,
            'merchant_id' => $invoice->merchant_id,
            'invoice_id' => $invoice->id,
            'invoice_type' => 1,
            'status' => 1,
            'expired_at' => now()->addMinutes(config('app.otp.time')),
            'code' => Otp::generateCode()
        ]);

        $merchant = Merchant::where([
            'id' => $invoice->merchant_id
        ])->first();

        $request->user()->notify(new notifyOTP($invoice, $merchant, $otp->code, config('app.otp.time')));
        $result['status'] = 1;
        $result['message'] = __('The OTP code is resent');
        return response()->json($result);
    }
}
