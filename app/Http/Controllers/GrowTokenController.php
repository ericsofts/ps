<?php

namespace App\Http\Controllers;

use App\Http\Libs\Api\BankBalance;
use App\Lib\Coinsbit;
use App\Models\BalanceQueue;
use App\Models\GrowTokenInvoice;
use App\Models\Property;
use App\Traits\Invoice;
use Illuminate\Http\Request;

class GrowTokenController extends Controller
{

    public function exchange(Request $request)
    {
        $rate = Property::getProperties('grow_token_usd2token');
        $total = $request->user()->balance_total();
        $totalGrow = $request->user()->balance_grow_token_total();
        $coin_rate = $rate['coin_rate'] ?? 0;

        return view('grow_token.exchange', [
            'balance_total' => $total,
            'balance_total_grow' => $totalGrow,
            'rate' => round($coin_rate + ($coin_rate * $rate['correction'] / 100), 8)
        ]);
    }

    public function confirm(Request $request)
    {
        $total = $request->user()->balance_total();
        $rules = [
            'amount' => ['required', 'numeric', 'gt:0', 'lte:' . $total, 'regex:/^\d+(\.\d{1,2})?$/']
        ];

        $request->validate($rules, [], [
            'amount' => __('amount'),
        ]);
        $amount = $request->input('amount');
        $rate = Property::getProperties('grow_token_usd2token');
        $coin_rate = round($rate['coin_rate'] + ($rate['coin_rate'] * $rate['correction'] / 100), 8);
        $grow_amount = round($amount / $coin_rate, 8);

        $description = "Account Charge - GROW Token " . $grow_amount;
        BalanceQueue::addToBalanceQueue(
            $request->user()->id,
            -$amount,
            BalanceQueue::TYPE_DEBIT,
            $description,
            null,
            null,
            null,
            BalanceQueue::STATUS_WAITING,
            BalanceQueue::CURRENCY_USD
        );

        $description = "Account Top-Up - GROW Token " . $grow_amount;
        BalanceQueue::addToBalanceQueue(
            $request->user()->id,
            $grow_amount,
            BalanceQueue::TYPE_CREDIT,
            $description,
            null,
            null,
            null,
            BalanceQueue::STATUS_WAITING,
            BalanceQueue::CURRENCY_GROW_TOKEN
        );

        return redirect()->route('dashboard')->with('status',
            __('The payment is complete')
        );
    }

    public function withdrawal(Request $request)
    {
        $totalGrow = $request->user()->balance_grow_token_total();
        return view('grow_token.withdrawal', [
            'balance_total_grow' => $totalGrow
        ]);

    }

    public function withdrawal_confirm(Request $request)
    {
        $total = $request->user()->balance_grow_token_total();
        $min_invoice_amount = Property::getProperty('grow_token', 0, 'min_value', 0);

        $rules = [
            'address' => [
                'required',
                'regex:/^(0x)?[0-9a-f]{40,42}$/i'

            ],
            'amount' => ['required', 'numeric', 'gt:0', 'lte:' . $total, 'gte:' . $min_invoice_amount, 'regex:/^\d+(\.\d{1,8})?$/']
        ];

        $request->validate($rules, [], [
            'address' => __('address'),
            'amount' => __('amount'),
        ]);

        $amount = $request->input('amount');
        $commission = Property::getProperty('users', 0, 'withdrawal_commission', 0);
        $amount2commission = $amount / 100 * $commission;
        $amount2pay = $amount - $amount2commission;
        GrowTokenInvoice::create([
            'user_id' => $request->user()->id,
            'amount' => $amount,
            'address' => $request->input('address'),
            'amount2pay'=>$amount2pay,
            'amount2commission'=>$amount2commission,
            'commission'=>$commission,
            'status' => GrowTokenInvoice::STATUS_CREATED
        ]);
        return redirect()->route('grow_token.withdrawal.index')->with('status',
            __('The payment is complete')
        );
    }

    public function withdrawal_cancel(Request $request, $id)
    {
        $invoice = GrowTokenInvoice::where([
            'id' => $id,
            'user_id' => $request->user()->id,
            'status' => GrowTokenInvoice::STATUS_CREATED
        ])
            ->firstOrFail();

        $invoice->status = GrowTokenInvoice::STATUS_CANCELED;
        $invoice->save();
        return redirect()->route('grow_token.withdrawal.index');
    }

    public function withdrawal_index(Request $request)
    {
        $totalGrow = $request->user()->balance_grow_token_total();
        $invoices = $request->user()
            ->grow_token_invoices()
            ->orderBy('id', 'desc')
            ->paginate(20);

        return view('grow_token.withdrawal_index', [
            'balance_total_grow' => $totalGrow,
            'invoices' => $invoices
        ]);
    }
}
