<?php

namespace App\Http\Controllers\Api6;


use App\Lib\Crypto\Cryptoprocessing;
use App\Models\CryptoCurrency;
use App\Models\InvoiceCryptoExtend;
use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\PaymentInvoice;
use App\Rules\CryptoAddress;
use App\Services\AdsSelector;
use App\Services\ContractManager;
use Illuminate\Http\Request;

class PaymentController extends \App\Http\Controllers\Api3\PaymentController
{
    protected $cryptoprocessing;

    protected $percent = null;

    public function __construct(
        Request $request,
        Merchant $merchantModel,
        AdsSelector $adsService,
        ContractManager $contractManager,
        Cryptoprocessing $cryptoprocessing
    )
    {
        parent::__construct(
            $request,
            $merchantModel,
            $adsService,
            $contractManager
        );
        $this->logChannel = 'api6_payment_invoice';
        $this->api_type = PaymentInvoice::API_TYPE_6;
        $this->cryptoprocessing = $cryptoprocessing;
    }

    protected function _prepareRequestRule(){
        return [
            'amount' => 'required|numeric|gt:0|lt:10000|regex:/^\d*(\.\d{1,2})?$/',
            'coin' => 'required|string'
        ];
    }

    protected function _prepareRequestCustomAttribute(){
        return [
            'amount' => __('amount'),
            'coin' => __('coin')
        ];
    }

    protected function _prepareCreateRule(){

        return [
            'ad_id' => 'required',
            'amount' => 'required|numeric|gt:0|lt:10000|regex:/^\d*(\.\d{1,2})?$/',
            'address' => [new CryptoAddress($this->cryptoprocessing)],
            'percent' => 'nullable|numeric|gt:0|lt:100',
            'coin' => 'required|string'
        ];
    }

    protected function _additionVerify($ad){
        if($this->request->input('percent')){
            $this->percent = $this->request->input('percent');
            return true;
        }else{
            $merchant = $this->request->input('merchant');
            $percent = MerchantProperty::getProperty($merchant->id,'api6_to_address_percent', false);
            if($percent){
                $this->percent = $percent;
                return true;
            }
        }
        return false;
    }

    protected function _getAmount2Pay($amount, $fee){
        return ($amount - $fee['total']) / 100 * (100 - $this->percent);
    }

    protected function _afterCreateInvoice($invoice){
        $coin = strtoupper($this->request->input('coin'));
        $currency = CryptoCurrency::where('asset', $coin)->first();
        $rate = $this->getCoinRate(strtolower($coin));
        if($currency){
            InvoiceCryptoExtend::create([
                'invoice_id' => $invoice->id,
                'invoice_type'=> InvoiceCryptoExtend::getInvoiceTypeByInstance($invoice),
                'crypto_address'=> $this->request->input('address'),
                'coin'=> $currency->crypto_asset,
                'status'=> InvoiceCryptoExtend::STATUS_CREATED,
                'currency_id'=> $currency->id,
                'amount'=> (($invoice->amount - ($invoice->amount2grow + $invoice->amount2service + $invoice->amount2agent)) / 100 * $this->percent) / $rate,
            ]);
            return true;
        }
        return false;
    }

    protected function _getRequestResult($currency2currency, $merchant, $amount, $service_provider, $sell_coins){
        $merchant_amount = 0;
        if (!$currency2currency) {
            $fee = $this->merchantModel->getFeeCommission(
                $merchant->id,
                $amount,
                $this->api_type,
                MerchantCommission::PROPERTY_TYPE_COMMISSION,
                $service_provider->id
            );
            $merchant_amount = round($amount - $fee['total'], 2);
        }
        $result['merchant_amount'] = $merchant_amount;
        $result['data'] = $sell_coins;
        $result['status'] = 1;
        return $result;
    }

    protected function _getStausIsContractCompleteResult($invoice){
        $invoiceCryptoExtend = InvoiceCryptoExtend::where('invoice_type', InvoiceCryptoExtend::getInvoiceTypeByInstance($invoice))
            ->where('invoice_id', $invoice->id)
            ->first();
        switch ($invoiceCryptoExtend->status){
            case InvoiceCryptoExtend::STATUS_CREATED:
                $cryptoMessage = __('Payment created');
                break;
            case InvoiceCryptoExtend::STATUS_PAYED:
                $cryptoMessage = __('Payment paid');
                break;
            case InvoiceCryptoExtend::STATUS_CANCELED:
                $cryptoMessage = __('Payment cancel');
                break;
           default:
                $cryptoMessage = __('Payment in process');
                break;
        }

        return [
            'id' => 1,
            'message' => __('Payment paid'),
            'crypto_message' => $cryptoMessage,
            'merchant_amount' => round($invoice->amount2pay, 2),
            'amount' => round($invoice->amount, 6),
            'address_amount' => round($invoiceCryptoExtend->amount, 6),
            'address_amount_current' => round($invoiceCryptoExtend->address_amount, 6)
        ];
    }

}
