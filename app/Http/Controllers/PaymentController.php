<?php

namespace App\Http\Controllers;

use App\Jobs\PaymentInvoiceSendResponse;
use App\Lib\PaymentApiHelper;
use App\Models\BalanceQueue;
use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\MerchantProperty;
use App\Models\Otp;
use App\Models\PaymentInvoice;
use App\Notifications\Otp as notifyOTP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use NumberFormatter;

class PaymentController extends Controller
{
    private $merchantModel;

    public function __construct(Merchant $merchantModel)
    {
        $this->merchantModel = $merchantModel;
    }

    public function checkout_redirect(Request $request)
    {

        $rules = [
            'merchant_id' => 'required|numeric',
            'amount' => 'required|numeric|gt:0|regex:/^\d*(\.\d{1,2})?$/',
            'order_id' => 'required',
            'signature' => 'required',
            'response_url' => 'required'
        ];

        $validator = Validator::make($request->input(), $rules);

        if ($validator->fails()) {
            return view('payment.checkout_redirect')
                ->withErrors($validator)
                ->with('error', __('Invalid parameters'));
        }

        $merchant = Merchant::where([
            'id' => $request->input('merchant_id'),
            'status' => 1
        ])->first();

        if (!$merchant) {
            return view('payment.checkout_redirect')
                ->with('error', __('Invalid parameters'));
        }

        if (!PaymentApiHelper::checkSignature($request->input(), $merchant->payment_key)) {
            return view('payment.checkout_redirect')
                ->with('error', __('Invalid parameters'));
        }

        $amount = $request->input('amount');
        $min_payment_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_payment_invoice_amount', config('app.min_payment_invoice_amount'));
        if ($amount < $min_payment_invoice_amount) {
            return view('payment.checkout_redirect')
                ->with('error', __('Invalid parameters'));
        }
        $fee = $this->merchantModel->getFeeCommission($merchant->id, $amount, PaymentInvoice::API_TYPE_2);
        $amount2pay = $amount - $fee['total'];
        if (empty($request->user()->id)) {
            session()->put('checkout_input', $request->input());
            return redirect()->route('payment.checkout');
        }

        $invoice = PaymentInvoice::create([
            'invoice_number' => PaymentInvoice::generateOrderNumber(),
            'user_id' => $request->user()->id,
            'merchant_id' => $merchant->id,
            'amount' => $amount,
            'amount2pay' => $amount2pay,
            'amount2service' => $fee['service']['amount'],
            'amount2agent' => $fee['agent']['amount'],
            'amount2grow' => $fee['grow']['amount'],
            'commission_grow' => $fee['grow']['commission'],
            'commission_agent' => $fee['agent']['commission'],
            'commission_service' => $fee['service']['commission'],
            'merchant_order_id' => $request->input('order_id'),
            'merchant_order_desc' => $request->input('order_desc'),
            'merchant_response_url' => $request->input('response_url'),
            'merchant_server_url' => $request->input('server_url'),
            'status' => PaymentInvoice::STATUS_CREATED,
            'api_type' => PaymentInvoice::API_TYPE_2
        ]);

        if ($invoice) {
            Otp::where([
                'user_id' => $request->user()->id,
                'status' => 1,
            ])->update(['status' => 0]);

            $otp = Otp::create([
                'user_id' => $request->user()->id,
                'merchant_id' => $merchant->id,
                'invoice_id' => $invoice->id,
                'invoice_type' => 1,
                'status' => 1,
                'expired_at' => now()->addMinutes(config('app.otp.time')),
                'code' => Otp::generateCode()
            ]);

            if ($otp) {
                $request->user()->notify(new notifyOTP($invoice, $merchant, $otp->code, config('app.otp.time')));
            }

            return redirect()->route('payment.checkout', ['payment_invoice_id' => $invoice->id]);
        }

        return view('payment.checkout_redirect')
            ->with('error', __('Invalid parameters'));

    }

    public function checkout(Request $request, $payment_invoice_id = null)
    {
        if ($payment_invoice_id) {
            $invoice = PaymentInvoice::where([
                'id' => $payment_invoice_id,
                'user_id' => $request->user()->id,
                'status' => PaymentInvoice::STATUS_CREATED
            ])
                ->firstOrFail();
        } else {
            $input = session()->pull('checkout_input');
            if (!$input) {
                return view('payment.checkout')
                    ->with('error', __('Invalid parameters'));
            }
            $merchant = Merchant::where([
                'id' => $input['merchant_id'],
                'status' => 1
            ])->first();
            if (!$merchant) {
                return view('payment.checkout')
                    ->with('error', __('Invalid parameters'));
            }
            $amount = $input['amount'];
            $min_payment_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_payment_invoice_amount', config('app.min_payment_invoice_amount'));
            if ($amount < $min_payment_invoice_amount) {
                return view('payment.checkout_redirect')
                    ->with('error', __('Invalid parameters'));
            }

            $fee = $this->merchantModel->getFeeCommission($merchant->id, $amount, PaymentInvoice::API_TYPE_2);
            $amount2pay = $amount - $fee['total'];

            $invoice = PaymentInvoice::create([
                'invoice_number' => PaymentInvoice::generateOrderNumber(),
                'user_id' => $request->user()->id,
                'merchant_id' => $merchant->id,
                'amount' => $amount,
                'amount2pay' => $amount2pay,
                'amount2service' => $fee['service']['amount'],
                'amount2agent' => $fee['agent']['amount'],
                'amount2grow' => $fee['grow']['amount'],
                'commission_grow' => $fee['grow']['commission'],
                'commission_agent' => $fee['agent']['commission'],
                'commission_service' => $fee['service']['commission'],
                'merchant_order_id' => $input['order_id'] ?? null,
                'merchant_order_desc' => $input['order_desc'] ?? null,
                'merchant_response_url' => $input['response_url'] ?? null,
                'merchant_server_url' => $input['server_url'] ?? null,
                'status' => PaymentInvoice::STATUS_CREATED,
                'api_type' => PaymentInvoice::API_TYPE_2
            ]);

            if ($invoice) {
                $otp = Otp::create([
                    'user_id' => $request->user()->id,
                    'merchant_id' => $merchant->id,
                    'invoice_id' => $invoice->id,
                    'invoice_type' => 1,
                    'status' => 1,
                    'expired_at' => now()->addMinutes(config('app.otp.time')),
                    'code' => Otp::generateCode()
                ]);
                if ($otp) {
                    $request->user()->notify(new notifyOTP($invoice, $merchant, $otp->code, config('app.otp.time')));
                }

                return redirect()->route('payment.checkout', $invoice->id);
            }
            return view('payment.checkout')
                ->with('error', __('Invalid parameters'));
        }

        $merchant = Merchant::where([
            'id' => $invoice->merchant_id
        ])->first();

        $balance = $request->user()->balance()->amount ?? 0;
        $balanceDebitQueue = BalanceQueue::where([
            'user_id' => $request->user()->id,
            'currency' => BalanceQueue::CURRENCY_USD,
            'type' => BalanceQueue::TYPE_DEBIT,
        ])
            ->where('status', '!=', BalanceQueue::STATUS_DONE)
            ->sum('amount');

        $total = round($balance + $balanceDebitQueue, 6);

        $enoughFunds = true;
        if ($total < $invoice->amount) {
            $enoughFunds = false;
        }

        return view('payment.checkout', [
            'invoice' => $invoice,
            'merchant' => $merchant,
            'fmt' => new NumberFormatter('en_US', NumberFormatter::CURRENCY),
            'balanceTotal' => $total,
            'enoughFunds' => $enoughFunds
        ]);
    }

    public function checkout_confirm(Request $request)
    {
        $validator = Validator::make(
            $request->input(),
            [
                'otp' => 'required',
                'id' => 'required|integer',
                'action' => 'required'
            ]
        );

        $validator->validate();

        $invoice = PaymentInvoice::where([
            'id' => $request->input('id'),
            'user_id' => $request->user()->id
        ])
            ->where('status', '!=', PaymentInvoice::STATUS_PAYED)
            ->first();

        if (!$invoice) {
            return back()->with('error',
                __('Invalid parameters')
            );
        }

        $otp = Otp::where([
            'status' => 1,
            'code' => $request->input('otp'),
            'user_id' => $request->user()->id,
            'merchant_id' => $invoice->merchant_id,
            'invoice_id' => $invoice->id,
            'invoice_type' => 1,
        ])
            ->first();

        if (!$otp) {
            $validator->after(function ($validator) {
                $validator->errors()->add('otp', __('The one-time password is not valid'));
            });
            $validator->validate();
        }
        $otp->status = 0;
        $otp->save();

        if ($otp->expired_at < now()) {
            $validator->after(function ($validator) {
                $validator->errors()->add('otp', __('The one-time password has expired'));
            });
            $validator->validate();
        }

        $balance = $request->user()->balance()->amount ?? 0;
        $balanceDebitQueue = BalanceQueue::where([
            'user_id' => $request->user()->id,
            'currency' => BalanceQueue::CURRENCY_USD,
            'type' => BalanceQueue::TYPE_DEBIT,
        ])
            ->where('status', '!=', BalanceQueue::STATUS_DONE)
            ->sum('amount');

        $total = round($balance + $balanceDebitQueue, 6);

        if ($total < $invoice->amount) {
            return back()->with('error',
                __('Not enough funds')
            );
        }

        $merchant = Merchant::where([
            'id' => $invoice->merchant_id
        ])->first();

        $description = "Account Charge - {$merchant->name}";
        $resultBalanceQueue = BalanceQueue::addToBalanceQueue(
            $invoice->user_id,
            -$invoice->amount,
            BalanceQueue::TYPE_DEBIT,
            $description,
            null,
            $invoice->id,
            $merchant->id
        );

        $invoice->status = PaymentInvoice::STATUS_PAYED;
        $invoice->payed = now();
        $invoice->save();

        return redirect()->route('payment.checkout_complete', $invoice->id);
    }

    public function checkout_complete(Request $request, $payment_invoice_id)
    {
        $invoice = PaymentInvoice::where([
            'id' => $payment_invoice_id,
            'user_id' => $request->user()->id
        ])
            ->where('status', '=', PaymentInvoice::STATUS_PAYED)
            ->firstOrFail();

        if(!$invoice->merchant_response_at){
            $invoice->merchant_response_at = now();
            $invoice->save();
        }

        $merchant = Merchant::where([
            'id' => $invoice->merchant_id
        ])->first();

        $response = [
            'invoice_id' => $invoice->id,
            'merchant_id' => $invoice->merchant_id,
            'order_id' => $invoice->merchant_order_id,
            'amount' => price_format($invoice->amount),
            'order_desc' => $invoice->merchant_order_desc,
            'merchant_amount' => price_format($invoice->amount2pay),
            'status' => 1
        ];

        $response['signature'] = PaymentApiHelper::generateSignature($response, $merchant->payment_key);

        if($invoice->merchant_server_url){
           PaymentInvoiceSendResponse::dispatch($invoice, $response);
        }

        if($invoice->merchant_response_url){
//            $url = $invoice->merchant_response_url;
//            $query = parse_url($url, PHP_URL_QUERY);
//            if ($query) {
//                $url .= '&';
//            } else {
//                $url .= '?';
//            }
//            $query = http_build_query($response);
//            $url .= $query;
            return redirect()->to($invoice->merchant_response_url);
        }

        return view('payment.checkout_complete', [
            'invoice' => $invoice,
            'response' => $response
        ]);
    }

}
