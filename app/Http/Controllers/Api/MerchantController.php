<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class MerchantController extends Controller
{

    public function balance(Request $request)
    {
        $merchant = $request->input('merchant');
        return [
            'status' => 1,
            'data' => ['balance' => $merchant->balance_total()]
        ];
    }

}
