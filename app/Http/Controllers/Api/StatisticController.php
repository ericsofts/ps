<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;


class StatisticController extends Controller
{

    public function pendingFees(Request $request)
    {
        $now = Carbon::now();
//        $monday = Carbon::now(config('app.default.timezone'));
//        $monday = $monday->startOfWeek(Carbon::MONDAY);
        $monday = Carbon::parse('2021-08-23');
        $sum = 0;
        $ps = PaymentInvoice::where([
            'status' => PaymentInvoice::STATUS_PAYED
        ])
            ->where('created_at', '>=', $monday)
            ->where('created_at', '<=', $now)
            ->select(
                DB::raw('SUM(amount) as total_amount'),
            )->first();

        if (!empty($ps->total_amount)) {
            $sum += $ps->total_amount;
        }

        $mwi = MerchantWithdrawalInvoice::where([
            'status' => MerchantWithdrawalInvoice::STATUS_PAYED
        ])
            ->where('created_at', '>=', $monday)
            ->where('created_at', '<=', $now)
            ->where('payment_system', '=', MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC)
            ->select(
                DB::raw('SUM(amount) as total_amount'),
            )->first();

        if (!empty($mwi->total_amount)) {
            $sum += $mwi->total_amount;
        }

        return response()->json([
            'status' => 1,
            'message' => 'OK',
            'result' => $sum * 0.001
        ]);
    }

}
