<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BalanceQueue;
use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\MerchantProperty;
use App\Models\PaymentInvoice;
use App\Models\User;
use App\Models\Otp;
use App\Notifications\Otp as notifyOTP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Google2FALaravel\Facade as Google2FA;

class PaymentController extends Controller
{

    private $merchantModel;

    public function __construct(Merchant $merchantModel)
    {
        $this->merchantModel = $merchantModel;
    }

    public function request(Request $request)
    {
        $result['status'] = 0;

        $rules = [
            'amount' => 'required|numeric|gt:0|regex:/^\d*(\.\d{1,2})?$/',
            'account_id' => 'required|numeric',
            'tfa' => 'required',
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules, [],[
            'amount' => __('amount')
        ]);

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }

        $amount = $request->input('amount');
        $tfa = $request->input('tfa');
        $merchant = $request->input('merchant');

        $user = $this->checkUser($request->input('account_id'), $tfa);
        if (!$user instanceof User) {
            return $result + $user;
        }

        $checkBalance = $this->checkBalance($user, $amount);
        if ($checkBalance !== true) {
            return $result + $checkBalance;
        }

        $min_payment_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_payment_invoice_amount', config('app.min_payment_invoice_amount'));
        if($amount < $min_payment_invoice_amount){
            $result['error']['amount'][] = __('The minimum amount must be greater than or equal to :amount', ['amount' => $min_payment_invoice_amount]);
            return $result;
        }


        $fee = $this->merchantModel->getFeeCommission($merchant->id, $amount, PaymentInvoice::API_TYPE_1);
        $amount2pay = $amount - $fee['total'];

        $paymentInvoice = PaymentInvoice::create([
            'invoice_number' => PaymentInvoice::generateOrderNumber(),
            'user_id' => $user->id,
            'merchant_id' => $merchant->id,
            'amount' => $amount,
            'amount2pay' => $amount2pay,
            'amount2service' => $fee['service']['amount'],
            'amount2agent' => $fee['agent']['amount'],
            'amount2grow' => $fee['grow']['amount'],
            'commission_grow' => $fee['grow']['commission'],
            'commission_agent' => $fee['agent']['commission'],
            'commission_service' => $fee['service']['commission'],
            'status' => PaymentInvoice::STATUS_CREATED,
            'api_type' => PaymentInvoice::API_TYPE_1
        ]);

        $otp = Otp::create([
            'user_id' => $user->id,
            'merchant_id' => $merchant->id,
            'invoice_id' => $paymentInvoice->id,
            'invoice_type' => 1,
            'status' => 1,
            'expired_at' => now()->addMinutes(config('app.otp.time')),
            'code' => Otp::generateCode()
        ]);

        $user->notify(new notifyOTP($paymentInvoice, $merchant, $otp->code, config('app.otp.time')));

        $result['data'] = [
            'otp_expired_at' => $otp->expired_at,
            'invoice' => $paymentInvoice->invoice_number
        ];

        $result['status'] = 1;
        return $result;
    }

    public function charge(Request $request)
    {
        $result['status'] = 0;

        $rules = [
            'account_id' => 'required|numeric',
            'invoice' => 'required|numeric',
            'otp' => 'required',
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules, [],);

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }

        $otp_code = $request->input('otp');
        $invoice_number = $request->input('invoice');
        $merchant = $request->input('merchant');

        $user = $this->checkUser($request->input('account_id'), false);
        if (!$user instanceof User) {
            return $result + $user;
        }

        $invoice = PaymentInvoice::where([
            'invoice_number' => $invoice_number,
            'user_id' => $user->id,
            'merchant_id' => $merchant->id
        ])
            ->where('status', '!=', PaymentInvoice::STATUS_PAYED)
            ->first();

        if (!$invoice) {
            $result['error']['invoice_id'][] = __('The invoice is not valid');
            return $result;
        }

        $otp = Otp::where([
            'status' => 1,
            'code' => $otp_code,
            'user_id' => $user->id,
            'merchant_id' => $merchant->id,
            'invoice_id' => $invoice->id,
            'invoice_type' => 1,
        ])
            ->first();

        if (!$otp) {
            $result['error']['otp'][] = __('The one-time password is not valid');
            return $result;
        }

        $otp->status = 0;
        $otp->save();

        if ($otp->expired_at < now()) {
            $result['error']['otp'][] = __('The one-time password has expired');
            return $result;
        }

        $checkBalance = $this->checkBalance($user, $invoice->amount);
        if ($checkBalance !== true) {
            return $result + $checkBalance;
        }

        $description = "Account Charge - {$merchant->name}";
        $resultBalanceQueue = BalanceQueue::addToBalanceQueue(
            $invoice->user_id,
            -$invoice->amount,
            BalanceQueue::TYPE_DEBIT,
            $description,
            null,
            $invoice->id,
            $merchant->id
        );

        $invoice->status = PaymentInvoice::STATUS_PAYED;
        $invoice->payed = now();
        $invoice->save();

        $result['status'] = 1;

        return $result;
    }

    private function checkUser($account_id, $tfa = false)
    {
        $user = User::where([
            'account_id' => $account_id
        ])->first();

        if (!$user) {
            $result['error']['user'][] = __('The user does not exist');
            return $result;
        }

        if ($user->status == User::STATUS_BLOCKED) {
            $result['error']['user'][] = __('The user is blocked');
            return $result;
        }

        if ($user->status == User::STATUS_BLOCKED) {
            $result['error']['user'][] = __('The user is blocked');
            return $result;
        }

        if (!$user->enable_2fa || !$user->secret_2fa) {
            $result['error']['tfa'][] = __('Two Factor Authentication is disabled');
            return $result;
        }

        if ($tfa !== false && !Google2FA::verifyKey($user->secret_2fa, $tfa)) {
            $result['error']['tfa'][] = __('The 2FA code is not valid');
            return $result;
        }

        return $user;
    }

    private function checkBalance($user, $amount)
    {

        $balance = $user->balance()->amount ?? 0;

        $balanceDebitQueue = BalanceQueue::where([
            'user_id' => $user->id,
            'currency' => BalanceQueue::CURRENCY_USD,
            'type' => BalanceQueue::TYPE_DEBIT,
        ])
            ->where('status', '!=', BalanceQueue::STATUS_DONE)
            ->sum('amount');

        $total = round($balance + $balanceDebitQueue, 6);

        if ($total < $amount) {
            $result['error']['amount'][] = __('Not enough funds');
            return $result;
        }
        return true;
    }

}
