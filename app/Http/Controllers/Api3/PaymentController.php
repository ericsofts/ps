<?php

namespace App\Http\Controllers\Api3;

use App\Http\Controllers\Controller;
use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\PaymentInvoice;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use App\Models\TraderAd;
use App\Services\AdsSelector;
use App\Services\ContractManager;
use App\Traits\Contract;
use App\Traits\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    use Invoice;
    use Contract;

    protected $logChannel;
    protected $adsService;
    protected Merchant $merchantModel;
    protected $contractManager = null;
    protected $api_type;
    protected $request;

    public function __construct(
        Request $request,
        Merchant $merchantModel,
        AdsSelector $adsService,
        ContractManager $contractManager
    )
    {
        $this->merchantModel = $merchantModel;
        $this->adsService = $adsService;
        $this->contractManager = $contractManager;
        $this->logChannel = 'api3_payment_invoice';
        $this->api_type = PaymentInvoice::API_TYPE_3;
        $this->request = $request;
    }


    public function request(Request $request)
    {
        $result['status'] = 0;
        app()->setLocale($request->input('locale', 'en'));
        $validator = Validator::make(
            $request->input(),
            $this->_prepareRequestRule(),
            $this->_prepareRequestMessage(),
            $this->_prepareRequestCustomAttribute());

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }

        $merchant = $request->input('merchant');
        $service_provider = $merchant->default_service_provider();
        $count_service_provider = $merchant->service_providers()->count();
        $currency2currency = $exact_currency = false;
        if (!$request->input('currency')) {
            $currency = 'USD';
        } else {
            $currency = $request->input('currency');
            $currency2currency = MerchantProperty::getProperty($merchant->id, 'currency2currency', false);
            if($request->has('exact_currency')){
                $exact_currency = (bool)$request->input('exact_currency');
            }else{
                $exact_currency = (bool)MerchantProperty::getProperty($merchant->id, 'exact_currency', false);
            }
        }

        if (!$this->isAvalableCurrency($currency)) {
            Log::channel($this->logChannel)->info('isAvailableCurrency fail', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $result['error']['currency'][] = __('The currency is not available');
            return $result;
        }

        $input_amount = floatval($request->input('amount'));
        $currencyRate = $this->getCurrencyRate($currency);
        if (!$currencyRate) {
            Log::channel($this->logChannel)->info('currencyRate fail', [
                'currencyRate' => $currencyRate,
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $result['error']['currency'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10011']);
            return $result;
        }
        $amount = round($input_amount * $currencyRate, 2);

        $min_invoice_amount = config('app.min_payment_invoice_amount');
        if ($count_service_provider == 1) {
            $min_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_payment_invoice_amount', $min_invoice_amount, $service_provider->id);
        } else {
            $props_min_amount = MerchantProperty::getPropertiesByName($merchant->id, 'min_payment_invoice_amount');
            foreach ($props_min_amount as $value) {
                $min_invoice_amount = ($value < $min_invoice_amount) ? $value : $min_invoice_amount;
            }
        }

        if ($amount < $min_invoice_amount) {
            $result['error']['amount'][] = __('The minimum amount must be greater than or equal to :amount', ['amount' => round($min_invoice_amount, 2)]);
            return $result;
        }

        if (!$available_coins = $this->getAvailableMerchantCoins($merchant->id)) {
            $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002']);
            return $result;
        }

        if($request->input('coin') && !in_array($request->input('coin'), $available_coins)){
            $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002']);
            return $result;
        }

        if (!$rates = $this->getCoinRates($available_coins)) {
            $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10004']);
            return $result;
        }
        $ads = $this->adsService->getAdsByMerchant($merchant, [
            'amount' => $amount,
            'amount2pay' => 0,
            'currency' => $currency,
            'input_amount' => $input_amount,
            'currency2currency' => $currency2currency,
            'expense' => 0,
            'type' => TraderAd::TYPE_SELL,
            'api' => $this->api_type,
            'coin' => $request->input('coin'),
            'exact_currency' => $exact_currency
        ]);

        $sell_coins = [];
        foreach ($ads as $c) {
            foreach ($c as $ad) {
                $sell_coins[] = [
                    'ad_id' => $ad['ad_id'],
                    'currency' => $ad['currency'],
                    'bank_name' => $ad['bank_name'],
                    'temp_amount' => $ad['gps_temp_amount'],
                    'priority' => $ad['gps_priority']
                ];
            }
        }

        $result = $this->_getRequestResult($currency2currency, $merchant, $amount, $service_provider, $sell_coins);

        return $result;
    }

    public function create(Request $request)
    {
        $result['status'] = 0;

        $rules = [
            'ad_id' => 'required',
            'amount' => 'required|numeric|gt:0|regex:/^\d*(\.\d{1,2})?$/'
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make(
            $request->input(),
            $this->_prepareCreateRule(),
            $this->_prepareCreateMessage(),
            $this->_prepareCreateCustomAttribute()
        );

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }
        $currency2currency = false;
        $merchant = $request->input('merchant');
        $ad_id = $request->input('ad_id');
        if (!$request->input('currency')) {
            $currency = 'USD';
        } else {
            $currency = $request->input('currency');
            $currency2currency = MerchantProperty::getProperty($merchant->id, 'currency2currency', false);
        }

        if (!$this->isAvalableCurrency($currency)) {
            Log::channel($this->logChannel)->info('isAvailableCurrency fail', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $result['error']['currency'][] = __('The currency is not available');
            return $result;
        }

        $input_amount = floatval($request->input('amount'));
        $currencyRate = $this->getCurrencyRate($currency);

        if (!$currencyRate) {
            Log::channel($this->logChannel)->info('currencyRate fail', [
                'currencyRate' => $currencyRate,
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $result['error']['currency'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10011']);
            return $result;
        }

        $ad = $this->adsService->getAd($ad_id);
        if (!$ad || $ad['type'] != 'SELL') {
            Log::channel('api3_chatex_lbc_requests')->info('AD|NOT_FOUND|ERROR|MERCHANT_ID|' . $merchant->id, ['inputs' => $request->only(['amount', 'ad_id']), 'ad' => $ad]);
            $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10001']);
            return $result;
        }

        if ($currency2currency) {
            $percentage_currency_rate = ServiceProvidersProperty::getProperty($ad['service_provider_type'], $ad['service_provider_id'], 'percentage_currency_rate', 0);
            $maxCurrencyRate = round(1 / $currencyRate * (1 + $percentage_currency_rate / 100), 2);
            $minCurrencyRate = round(1 / $currencyRate * (1 - $percentage_currency_rate / 100), 2);

            if ($ad['temp_price'] < $minCurrencyRate || $ad['temp_price'] > $maxCurrencyRate) {
                Log::channel($this->logChannel)->info('AD|CURRENCY_RATE_PERCENTAGE|ERROR|MERCHANT_ID|' . $merchant->id, [
                    'inputs' => $request->input(), 'ad' => $ad, 'currencyRate' => 1 / $currencyRate
                ]);
                $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10005']);
                return $result;
            }
            $amount = $input_amount / $ad['temp_price'];
        } else {
            $amount = round($input_amount * $currencyRate, 2);
        }

        $min_invoice_amount = MerchantProperty::getProperty(
            $merchant->id,
            'min_payment_invoice_amount',
            config('app.min_payment_invoice_amount'),
            $ad['service_provider_id']
        );

        if ($amount < $min_invoice_amount) {
            $result['error']['amount'][] = __('The minimum amount must be greater than or equal to :amount', ['amount' => round($min_invoice_amount, 2)]);
            return $result;
        }

        if (!$available_coins = $this->getAvailableMerchantCoins($merchant->id)) {
            $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002']);
            return $result;
        }


        if($request->input('coin') && !in_array($request->input('coin'), $available_coins)){
            $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002']);
            return $result;
        }

        $coin = strtolower($ad['coin']);

        if($request->input('coin') && ($coin != strtolower($request->input('coin')))){
            $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002']);
            return $result;
        }

        if (!$coin || !$available_coins || !in_array($coin, $available_coins)) {
            $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002']);
            Log::channel('api3_chatex_lbc_requests')->info('AD|COIN|ERROR|MERCHANT_ID|' . $merchant->id, ['ad' => $ad, 'available_coins' => $available_coins]);
            return $result;
        }

        if (!$coin_rate = $this->getCoinRate($coin)) {
            $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10004']);
            Log::channel($ad['log'])->info('AD|COIN_RATE|ERROR|MERCHANT_ID|' . $merchant->id, ['ad' => $ad, 'coin_rate' => $coin_rate]);
            return $result;
        }

        if (!$this->_additionVerify($ad)) {
            Log::channel($ad['log'])->info('CONTACT_CREATE|VERIFY|ERROR|MERCHANT_ID|' . $merchant->id, ['inputs' => $request->input(), 'ad' => $ad]);
            $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I100097']);
            return $result;
        }

        $coin_amount = $amount * $coin_rate;
        $rounding_input_amount = MerchantProperty::getProperty($merchant->id, 'rounding_input_amount');
        if ($rounding_input_amount) {
            $currency_amount = ceil($ad['temp_price'] * $coin_amount);
        } else {
            $currency_amount = round($ad['temp_price'] * $coin_amount, 2);
        }
        Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|REQUEST|MERCHANT_ID|' . $merchant->id, ['ad' => $ad_id, 'amount' => $currency_amount]);

        $contract = $this->contractManager->contractCreate($ad, $currency_amount, $merchant);

        if (!$contract) {
            Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|ERROR|MERCHANT_ID|' . $merchant->id, ['inputs' => $request->only(['amount', 'ad_id']), 'res' => $contract]);
            $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006']);
            return $result;
        }
        $contractInfo = $this->contractManager->contractInfoByContract($contract);
        if (!$contractInfo) {
            Log::channel($ad['log'])->info('CONTACT_CREATE|INFO|ERROR|MERCHANT_ID|' . $merchant->id, ['inputs' => $request->only(['amount', 'ad_id']), 'contact_res' => $contractInfo]);
            $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006']);
            return $result;
        }

        $ps_amount = $contractInfo['amount_btc'] ?? null;
        $ps_currency = $ad['coin'] ?? null;

        $account_info = $contractInfo['account_info'] ?? null;
        if (!$account_info) {
            $account_info = $ad['account_info'] ?? null;
        }

        if ($currency2currency && $ps_amount) {
            $amount = round($ps_amount, 2);
        }

        $fee = $this->merchantModel->getFeeCommission(
            $merchant->id,
            $amount,
            $this->api_type,
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            $ad['service_provider_id']
        );

        $amount2pay = $this->_getAmount2Pay($amount, $fee);

        $invoice = PaymentInvoice::create([
            'invoice_number' => PaymentInvoice::generateOrderNumber(),
            'merchant_id' => $merchant->id,
            'amount' => $amount,
            'amount2pay' => $amount2pay,
            'amount2service' => $fee['service']['amount'],
            'amount2agent' => $fee['agent']['amount'],
            'amount2grow' => $fee['grow']['amount'],
            'commission_grow' => $fee['grow']['commission'],
            'commission_agent' => $fee['agent']['commission'],
            'commission_service' => $fee['service']['commission'],
            'payment_id' => $contractInfo['id'],
            'ps_amount' => $ps_amount,
            'ps_currency' => strtolower($ps_currency),
            'fiat_amount' => $contractInfo['amount'],
            'fiat_currency' => strtolower($contractInfo['currency']),
            'addition_info' => json_encode([
                'contact' => $contractInfo,
                'ad' => $ad
            ]),
            'account_info' => $account_info,
            'status' => PaymentInvoice::STATUS_USER_SELECTED,
            'input_currency' => $currency,
            'input_rate' => $currencyRate,
            'input_amount_value' => $input_amount,
            'api_type' => $this->api_type,
            'service_provider_id' => $ad['service_provider_id'],
            'currency2currency' => $currency2currency
        ]);

        if (!$invoice) {
            $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10099']);
            return $result;
        }else{
            $afterCreate = $this->_afterCreateInvoice($invoice);
            if(!$afterCreate){
                Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|AFTER|ERROR|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['contractInfo' => $contractInfo]);
                $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I100098']);
                return $result;
            }
        }
        Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['contractInfo' => $contractInfo]);
        $result['data']['invoice_id'] = $invoice->id;
        $result['status'] = 1;
        InvoicesHistory::addHistoryRecord(
            __METHOD__,
            $invoice->id,
            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
            0,
            InvoicesHistory::getStatus(1),
            InvoicesHistory::getSource(0),
            InvoicesHistory::getSource(3),
            json_encode($contractInfo),
            'Create Invoice Record'
        );
        return $result;
    }

    public function check_approved(Request $request)
    {
        $result['status'] = 0;

        $rules = [
            'invoice_id' => 'required|numeric'
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules, []);

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }

        $merchant = $request->input('merchant');

        $invoice = PaymentInvoice::where([
            'id' => $request->input('invoice_id'),
            'user_id' => null,
            'merchant_id' => $merchant->id,
            'api_type' => $this->api_type
        ])->first();

        if ($invoice) {
            $channel = $this->getLogChannel($invoice);
            if ($invoice->status == PaymentInvoice::STATUS_CANCELED) {
                $result['error']['invoice_id'][] = __('Payment canceled');
                $result['error_code'][] = 'PI003';
            } elseif ($invoice->status == PaymentInvoice::STATUS_PAYED) {
                $result['error']['invoice_id'][] = __('Payment closed successfully');
                $result['error_code'][] = 'PI001';
            } else {
                $addition_info = json_decode($invoice['addition_info'], true);
                $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($contractInfo) {
                    Log::channel($channel)->info('CONTACT_CREATE|CHECK_APPROVED|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['contractInfo' => $contractInfo]);
                    if ($this->isContractCancel($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment canceled');
                        $result['error_code'][] = 'PI003';
                    } elseif ($this->isContractComplete($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment closed successfully');
                        $result['error_code'][] = 'PI001';
                    } elseif ($this->isContractClosed($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment closed');
                        $result['error_code'][] = 'PI002';
                    } elseif ($this->isContractFunded($contractInfo, $invoice)) {
                        $result['status'] = 1;
                        $result['data'] = [
                            'amount' => $contractInfo['amount'],
                            'card_number' => $contractInfo['account_info'] ?? null,
                            'bank_name' => $addition_info['ad']['bank_name'] ?? null,
                            'currency' => $addition_info['ad']['currency'] ?? null,
                        ];
                        if(in_array($invoice->service_provider_id, ServiceProvider::where('type', ServiceProvider::CRYPTO_PROVIDER)->pluck('id')->toArray())){
                            $result['data']['address_qr_url'] = URL::signedRoute('payment.invoice.post.checkout.qr', $invoice->id);
                            $result['data']['address'] = $contractInfo['address'];
                        }
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                            8,
                            InvoicesHistory::getStatus(1),
                            InvoicesHistory::getSource(0),
                            InvoicesHistory::getSource(2),
                            json_encode($contractInfo),
                            'Check Invoice'
                        );
                    } else {
                        $result['error']['invoice_id'][] = __('Payment has not been approved yet');
                        $result['error_code'][] = 'PI000';
                    }
                } else {
                    Log::channel($channel)->info('CONTACT_CREATE|CHECK_APPROVED|ERROR|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['contractInfo' => $contractInfo]);
                }
            }
        } else {
            $result['error']['invoice_id'][] = __('Payment not found');
            $result['error_code'][] = 'PI099';
        }
//        if (!empty($result['error'])) {
//            InvoicesHistory::addHistoryRecord(
//                __METHOD__,
//                $invoice->id,
//                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
//                8,
//                InvoicesHistory::getStatus(0),
//                InvoicesHistory::getSource(0),
//                InvoicesHistory::getSource(2),
//                json_encode($result['error']),
//                'Check Invoice'
//            );
//        }

        return $result;
    }

    public function cancel(Request $request)
    {
        $result['status'] = 0;

        $rules = [
            'invoice_id' => 'required|numeric'
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules, []);

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }

        $merchant = $request->input('merchant');

        $invoice = PaymentInvoice::where([
            'id' => $request->input('invoice_id'),
            'user_id' => null,
            'merchant_id' => $merchant->id,
            'api_type' => $this->api_type
        ])->first();
        if ($invoice) {
            $channel = $this->getLogChannel($invoice);
            if ($invoice->status == PaymentInvoice::STATUS_CANCELED) {
                $result['error']['invoice_id'][] = __('Payment canceled');
            } elseif ($invoice->status == PaymentInvoice::STATUS_PAYED) {
                $result['error']['invoice_id'][] = __('Payment closed successfully');
            } else {
                $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($contractInfo) {
                    Log::channel($channel)->info('CONTACT_CONFIRM|CHECK|INPUT_INVOICE|' . $invoice->id, ['contact_res' => $contractInfo]);
                    if ($this->isContractCancel($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment canceled');
                    } elseif ($this->isContractComplete($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment closed successfully');
                    } elseif ($this->isContractClosed($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment closed');
                    } else {
                        Log::channel($channel)->info('CONTACT_CONFIRM|CANCEL_INFO|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['contractInfo' => $contractInfo]);
                        $res = $this->contractManager->contractCancelByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                        if ($res) {
                            $invoice->status = PaymentInvoice::STATUS_CANCELED;
                            $invoice->save();
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                                4,
                                InvoicesHistory::getStatus(1),
                                InvoicesHistory::getSource(0),
                                InvoicesHistory::getSource(2),
                                json_encode($res),
                                'Cancel Invoice'
                            );
                            $result['status'] = 1;
                            Log::channel($channel)->info('CONTACT_CONFIRM|CANCEL|SUCCESS|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['res' => $res]);
                        } else {
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                                4,
                                InvoicesHistory::getStatus(0),
                                InvoicesHistory::getSource(0),
                                InvoicesHistory::getSource(2),
                                json_encode($contractInfo),
                                'Chatex response incorrect'
                            );
                            Log::channel($channel)->info('CONTACT_CONFIRM|CANCEL|ERROR|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['res' => $res]);
                        }
                    }
                } else {
                    $result['error']['invoice_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006']);
                    Log::channel($channel)->info('CONTACT_CONFIRM|CANCEL|ERROR|USER_ID|MERCHANT_ID|' . $merchant->id . '|INPUT_INVOICE|' . $invoice->id, ['contractInfo' => $contractInfo]);
                }
            }
        } else {
            $result['error']['invoice_id'][] = __('Payment not found');
        }
        if (!empty($invoice)) {
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                4,
                InvoicesHistory::getStatus(0),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(3),
                json_encode($result),
                'Error'
            );
        }
        return $result;
    }

    public function confirm(Request $request)
    {
        $result['status'] = 0;
        $messages = [
            'document.max' => __('The maximum file size is over 2Mb.'),
            'document.uploaded' => __('The maximum file size is over 2Mb.')
        ];
        $rules = [
            'invoice_id' => 'required|numeric',
            'document' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048'
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }
        $merchant = $request->input('merchant');
        $invoice = PaymentInvoice::where([
            'id' => $request->input('invoice_id'),
            'user_id' => null,
            'merchant_id' => $merchant->id,
            'api_type' => $this->api_type
        ])->first();

        if ($invoice) {
            $channel = $this->getLogChannel($invoice);
            if ($invoice->status == PaymentInvoice::STATUS_CANCELED) {
                $result['error']['invoice_id'][] = __('Payment canceled');
            } elseif ($invoice->status == PaymentInvoice::STATUS_PAYED) {
                $result['error']['invoice_id'][] = __('Payment closed successfully');
            } else {
                $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($contractInfo) {
                    if ($this->isContractCancel($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment canceled');
                    } elseif ($this->isContractComplete($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment closed successfully');
                    } elseif ($this->isContractClosed($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment closed');
                    } elseif ($this->isContractPaymentCompleted($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment confirmed');
                    } elseif (empty($contractInfo['funded_at'])) {
                        $result['error']['invoice_id'][] = __('Payment has not been approved yet');
                    }elseif (
                        in_array($invoice->service_provider_id, ServiceProvider::where('type', ServiceProvider::CRYPTO_PROVIDER)->pluck('id')->toArray()) &&
                        $this->isContractFunded($contractInfo, $invoice)
                    ) {
                        $result['error']['invoice_id'][] = __('Payment confirmed and is being processed');
                    } else {
                        Log::channel($channel)->info('CONTACT_CONFIRM|MARK_AS_PAID|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['contractInfo' => $contractInfo]);
                        $res = $this->contractManager->contactMarkAsPaidByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                        if ($res) {
                            $invoice->status = PaymentInvoice::STATUS_USER_CONFIRMED;
                            $invoice->save();
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                                6,
                                InvoicesHistory::getStatus(1),
                                InvoicesHistory::getSource(0),
                                InvoicesHistory::getSource(2),
                                json_encode($res),
                                'User Confirm'
                            );
                            $result['status'] = 1;
                            Log::channel($channel)->info('CONTACT_CONFIRM|MARK_AS_PAID|SUCCESS|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['res' => $res]);
                            if ($request->hasFile('document')) {
                                $requestMessage = $this->contractManager->contactMessagePostByProvider([
                                    'id' => $invoice['payment_id'],
                                    'service_provider_id' => $invoice['service_provider_id'],
                                    'document' => $request->file('document'),
                                    'user_id' => $invoice->merchant_id
                                ]);
                                $requestMessageResult = $requestMessage ? 1 : 0;
                                InvoicesHistory::addHistoryRecord(
                                    __METHOD__,
                                    $invoice->id,
                                    AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                                    7,
                                    InvoicesHistory::getStatus($requestMessageResult),
                                    InvoicesHistory::getSource(3),
                                    InvoicesHistory::getSource(2),
                                    json_encode($requestMessage),
                                    'Attachment'
                                );
                            }
                        } else {
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                                6,
                                InvoicesHistory::getStatus(0),
                                InvoicesHistory::getSource(0),
                                InvoicesHistory::getSource(2),
                                json_encode($res),
                                'Chatex response incorrect'
                            );
                            Log::channel($channel)->info('CONTACT_CONFIRM|MARK_AS_PAID|ERROR|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['res' => $res]);
                        }
                    }
                } else {
                    $result['error']['invoice_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006']);
                    Log::channel($channel)->info('CONTACT_CONFIRM|MARK_AS_PAID|ERROR|USER_ID|MERCHANT_ID|' . $merchant->id . '|INPUT_INVOICE|' . $invoice->id, ['contractInfo' => $contractInfo]);
                }
            }
        } else {
            $result['error']['invoice_id'][] = __('Payment not found');
        }
        return $result;
    }

    public function check_status(Request $request)
    {
        $result['status'] = 0;

        $rules = [
            'invoice_id' => 'required|numeric'
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules, [],);

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }
        $merchant = $request->input('merchant');
        $invoice = PaymentInvoice::where([
            'id' => $request->input('invoice_id'),
            'user_id' => null,
            'merchant_id' => $merchant->id,
            'api_type' => $this->api_type
        ])->first();

        if ($invoice) {
            $channel = $this->getLogChannel($invoice);
            $result['status'] = 1;
            if ($invoice->status == PaymentInvoice::STATUS_CANCELED) {
                $result['data']['status'] = [
                    'id' => -1,
                    'message' => __('Payment canceled')
                ];
            } elseif ($invoice->status == PaymentInvoice::STATUS_PAYED) {
                $result['data']['status'] = $this->_getStausIsContractCompleteResult($invoice);
            } else {
                $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($contractInfo) {
                    Log::channel($channel)->info('CONTACT_CHECK|USER_ID|MERCHANT_ID|' . $merchant->id . '|INPUT_INVOICE|' . $invoice->id, ['contractInfo' => $contractInfo]);
                    if ($this->isContractCancel($contractInfo)) {
                        $result['data']['status'] = [
                            'id' => -1,
                            'message' => __('Payment canceled')
                        ];
                    } elseif ($this->isContractComplete($contractInfo, $invoice)) {
                        $result['data']['status'] = $this->_getStausIsContractCompleteResult($invoice);
                    } elseif ($this->isContractClosed($contractInfo, $invoice)) {
                        $result['data']['status'] = [
                            'id' => -1,
                            'message' => __('Payment closed')
                        ];
                    } elseif ($this->isContractPaymentCompleted($contractInfo, $invoice)) {
                        $result['data']['status'] = [
                            'id' => 2,
                            'message' => __('Payment confirmed')
                        ];
                    } elseif ($this->isContractFunded($contractInfo, $invoice)) {
                        $result['data']['status'] = [
                            'id' => 0,
                            'message' => __('Payment confirmed')
                        ];
                        if(in_array($invoice->service_provider_id, ServiceProvider::where('type', ServiceProvider::CRYPTO_PROVIDER)->pluck('id')->toArray())){
                            $result['data']['status']['amount'] = (float) $contractInfo['amount'];
                            $result['data']['status']['amount_received'] = (float) $contractInfo['address_amount'];
                            $result['data']['status']['amount_to_pay'] = max(0, $contractInfo['amount'] - $contractInfo['address_amount']);
                            $result['data']['status']['status'] = $contractInfo['status'];
                            $result['data']['status']['expired_at'] = $contractInfo['expired_at'];
                        }
                    }elseif ($invoice->status == PaymentInvoice::STATUS_USER_CONFIRMED) {
                        $result['data']['status'] = [
                            'id' => 2,
                            'message' => __('Payment confirmed')
                        ];
                    }else {
                        $result['data']['status'] = [
                            'id' => 0,
                            'message' => __('Payment is being processed')
                        ];
                    }
                } else {
                    $result['error']['invoice_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006']);
                    Log::channel($channel)->info('CONTACT_CHECK|ERROR|USER_ID|MERCHANT_ID|' . $merchant->id . '|INPUT_INVOICE|' . $invoice->id, ['contractInfo' => $contractInfo]);
                }
            }
        } else {
            $result['error']['invoice_id'][] = __('Payment not found');
        }
        return $result;
    }

    protected function _prepareRequestRule(){
        return [
            'amount' => 'required|numeric|gt:0|regex:/^\d*(\.\d{1,2})?$/'
        ];
    }

    protected function _prepareRequestMessage(){
        return [];
    }

    protected function _prepareRequestCustomAttribute(){
        return [
            'amount' => __('amount')
        ];
    }

    protected function _prepareCreateRule(){
        return [
            'ad_id' => 'required',
            'amount' => 'required|numeric|gt:0|regex:/^\d*(\.\d{1,2})?$/'
        ];
    }

    protected function _prepareCreateMessage(){
        return [];
    }

    protected function _prepareCreateCustomAttribute(){
        return [];
    }

    protected function _getAmount2Pay($amount, $fee){
        return $amount - $fee['total'];
    }

    protected function _afterCreateInvoice($invoice){
        return true;
    }

    protected function _getRequestResult($currency2currency, $merchant, $amount, $service_provider, $sell_coins){
        $result = [];
        $merchant_amount = 0;
        if (!$currency2currency) {
            $fee = $this->merchantModel->getFeeCommission(
                $merchant->id,
                $amount,
                $this->api_type,
                MerchantCommission::PROPERTY_TYPE_COMMISSION,
                $service_provider->id
            );
            $merchant_amount = round($amount - $fee['total'], 2);
        }
        $result['merchant_amount'] = $merchant_amount;
        $result['data'] = $sell_coins;
        $result['status'] = 1;
        return $result;
    }

    protected function _getStausIsContractCompleteResult($invoice){
         return [
            'id' => 1,
            'message' => __('Payment paid'),
            'merchant_amount' => round($invoice->amount2pay, 2),
            'amount' => round($invoice->amount, 2)
        ];
    }

    protected function _additionVerify($ad){
        return true;
    }
}
