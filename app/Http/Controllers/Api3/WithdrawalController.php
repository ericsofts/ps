<?php

namespace App\Http\Controllers\Api3;

use App\Http\Controllers\Controller;
use App\Lib\Chatex\ChatexApi;
use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\MerchantWithdrawalInvoiceAttachment;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use App\Models\TraderAd;
use App\Services\AdsSelector;
use App\Services\ContractManager;
use App\Traits\Contract;
use App\Traits\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class WithdrawalController extends Controller
{
    use Invoice;
    use Contract;

    private AdsSelector $adsService;
    private Merchant $merchantModel;
    protected ContractManager $contractManager;

    public function __construct(
        Merchant $merchantModel,
        AdsSelector $adsService,
        ContractManager $contractManager
    )
    {
        $this->merchantModel = $merchantModel;
        $this->adsService = $adsService;
        $this->contractManager = $contractManager;
    }

    public function request(Request $request)
    {
        $result['status'] = 0;

        $rules = [
            'amount' => 'required|numeric|gt:0|regex:/^\d*(\.\d{1,2})?$/'
        ];
        app()->setLocale($request->input('locale', 'en'));

        $validator = Validator::make($request->input(), $rules, [], [
            'amount' => __('amount')
        ]);

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }

        $merchant = $request->input('merchant');
        $service_provider = $merchant->default_service_provider();
        $count_service_provider = $merchant->service_providers()->count();
        $currency2currency = $exact_currency = false;
        if (!$request->input('currency')) {
            $currency = 'USD';
        } else {
            $currency = $request->input('currency');
            //$currency2currency = MerchantProperty::getProperty($merchant->id, 'currency2currency', false);
            if($request->has('exact_currency')){
                $exact_currency = (bool)$request->input('exact_currency');
            }else{
                $exact_currency = (bool)MerchantProperty::getProperty($merchant->id, 'exact_currency', false);
            }
        }

        if (!$this->isAvalableCurrency($currency)) {
            Log::channel('api3_merchant_withdrawal_invoice')->info('isAvailableCurrency fail', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $result['error']['currency'][] = __('The currency is not available');
            return $result;
        }

        $input_amount = floatval($request->input('amount'));
        $currencyRate = $this->getCurrencyRate($currency);
        if(!$currencyRate){
            Log::channel('api3_merchant_withdrawal_invoice')->info('currencyRate fail', [
                'currencyRate' => $currencyRate,
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $result['error']['currency'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10011']);
            return $result;
        }
        $amount = round($input_amount * $currencyRate, 2);

        $min_invoice_amount = config('app.min_merchant_withdrawal_amount');
        if($count_service_provider == 1){
            $min_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_merchant_withdrawal_amount', $min_invoice_amount, $service_provider->id);
        }else{
            $props_min_amount = MerchantProperty::getPropertiesByName($merchant->id, 'min_merchant_withdrawal_amount');
            foreach ($props_min_amount as $value) {
                $min_invoice_amount = ($value < $min_invoice_amount) ? $value : $min_invoice_amount;
            }
        }

        if (!$available_coins = $this->getAvailableMerchantCoins($merchant->id)) {
            $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support');
            return $result;
        }

        if (!$rates = $this->getCoinRates($available_coins)) {
            $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10004']);
            return $result;
        }

        $merchant_expense = $request->input('merchant_expense');
        if($merchant_expense){
            $fee = $this->merchantModel->getRevertFeeCommission(
                $merchant->id,
                $amount,
                MerchantWithdrawalInvoice::API_TYPE_3,
                MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                $service_provider->id
            );
            $amount2pay = $amount;
            $amount += $fee['total'];
        }else{
            $fee = $this->merchantModel->getFeeCommission(
                $merchant->id,
                $amount,
                MerchantWithdrawalInvoice::API_TYPE_3,
                MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                $service_provider->id
            );
            $amount2pay = $amount - $fee['total'];
        }

        if ($amount < $min_invoice_amount) {
            $result['error']['amount'][] = __('The minimum amount must be greater than or equal to :amount', ['amount' => round($min_invoice_amount, 2)]);
            return $result;
        }
        $merchantBalance = $merchant->balance_total();
        if ($merchantBalance <= 0) {
            $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10007']);
            return $result;
        }

        if ($amount > $merchantBalance) {
            $result['error']['amount'][] = __('The maximum amount must be less than or equal to :amount', ['amount' => $merchantBalance]);
            return $result;
        }

        if($service_provider->name == ServiceProvider::CHATEX_PROVIDER){
            $chatexProperties = ServiceProvidersProperty::getProperties(ChatexApi::OBJECT_NAME);
            $chatex = new ChatexApi($chatexProperties['base_url'], $chatexProperties['api_token']);
            foreach ($available_coins as $coin) {
                $wallet = $chatex->wallet($coin);
                $walletBalance = $wallet['amount'] ?? 0;
                if ($walletBalance < $amount) {
                    $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10008']);
                    return $result;
                }
            }
        }

        $ads = $this->adsService->getAdsByMerchant($merchant, [
            'amount' => $amount,
            'amount2pay' => $amount2pay,
            'currency' => $currency,
            'input_amount' => $input_amount,
            'currency2currency' => $currency2currency,
            'expense' => $merchant_expense,
            'type' => TraderAd::TYPE_BUY,
            'api' => MerchantWithdrawalInvoice::API_TYPE_3,
            'exact_currency' => $exact_currency
        ]);

        $buy_ads = [];
        foreach ($ads as $c) {
            foreach ($c as $ad) {
                $buy_ads[] = [
                    'ad_id' => $ad['ad_id'],
                    'currency' => $ad['currency'],
                    'bank_name' => $ad['bank_name'],
                    'temp_amount' => $ad['gps_temp_amount'],
                    'priority' => $ad['gps_priority']
                ];
            }
        }

        $result['data'] = [
            'merchant_amount' => round($amount2pay, 2),
            'merchant_balance' => $merchantBalance,
            'ads' => $buy_ads

        ];
        $result['status'] = 1;
        return $result;
    }

    public function create(Request $request)
    {
        $result['status'] = 0;

        $rules = [
            'ad_id' => 'required',
            'amount' => 'required|numeric|gt:0|regex:/^\d+(\.\d{1,2})?$/',
            'message' => 'required|regex:/^\d{16,19}$/',
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules, [],);

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }

        $message = $request->input('message');
        $merchant = $request->input('merchant');
        $ad_id = $request->input('ad_id');

        $currency2currency = false;
        if (!$request->input('currency')) {
            $currency = 'USD';
        } else {
            $currency = $request->input('currency');
            //$currency2currency = MerchantProperty::getProperty($merchant->id, 'currency2currency', false);
        }

        if (!$this->isAvalableCurrency($currency)) {
            Log::channel('api3_merchant_withdrawal_invoice')->info('isAvailableCurrency fail', [
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $result['error']['currency'][] = __('The currency is not available');
            return $result;
        }

        $input_amount = floatval($request->input('amount'));
        $currencyRate = $this->getCurrencyRate($currency);

        if(!$currencyRate){
            Log::channel('api3_merchant_withdrawal_invoice')->info('currencyRate fail', [
                'currencyRate' => $currencyRate,
                'inputs' => $request->input(),
                'merchant_id' => $merchant->id
            ]);
            $result['error']['currency'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10011']);
            return $result;
        }

        $amount = round($input_amount * $currencyRate, 2);

        $ad = $this->adsService->getAd($ad_id);
        if (!$ad || $ad['type'] != 'BUY') {
            Log::channel('api3_chatex_lbc_requests')->info('BUY|AD|NOT_FOUND|ERROR|MERCHANT_ID|' . $merchant->id, ['inputs' => $request->only(['amount', 'ad_id']), 'ad' => $ad]);
            $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10001']);
            return $result;
        }

        $min_invoice_amount = MerchantProperty::getProperty(
            $merchant->id,
            'min_merchant_withdrawal_amount',
            config('app.min_merchant_withdrawal_amount'),
            $ad['service_provider_id']
        );

        if ($amount < $min_invoice_amount) {
            $result['error']['amount'][] = __('The minimum amount must be greater than or equal to :amount', ['amount' => round($min_invoice_amount,2)]);
            return $result;
        }

        if($request->input('merchant_expense')){
            $fee = $this->merchantModel->getRevertFeeCommission(
                $merchant->id,
                $amount,
                MerchantWithdrawalInvoice::API_TYPE_3,
                MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                $ad['service_provider_id']
            );
            $amount2pay = $amount;
            $amount += $fee['total'];
        }else{
            $fee = $this->merchantModel->getFeeCommission(
                $merchant->id,
                $amount,
                MerchantWithdrawalInvoice::API_TYPE_3,
                MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                $ad['service_provider_id']
            );
            $amount2pay = $amount - $fee['total'];
        }

        $merchantBalance = $merchant->balance_total();
        if ($merchantBalance <= 0) {
            $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10007']);
            return $result;
        }

        if ($amount > $merchantBalance) {
            $result['error']['amount'][] = __('The maximum amount must be less than or equal to :amount', ['amount' => $merchantBalance]);
            return $result;
        }

        $coin = strtolower($ad['coin']);
        $available_coins = $this->getAvailableMerchantCoins($merchant->id);
        if (!$coin || !$available_coins || !in_array($coin, $available_coins)) {
            $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002']);
            Log::channel('api3_chatex_lbc_requests')->info('AD|COIN|ERROR|MERCHANT_ID|' . $merchant->id, ['ad' => $ad, 'available_coins' => $available_coins]);
            return $result;
        }

        if (!$coin_rate = $this->getCoinRate($coin)) {
            $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10004']);
            Log::channel('api3_chatex_lbc_requests')->info('AD|COIN_RATE|ERROR|MERCHANT_ID|' . $merchant->id, ['ad' => $ad, 'coin_rate' => $coin_rate]);
            return $result;
        }
        if($ad['service_provider_type'] == ServiceProvider::CHATEX_PROVIDER){
            $chatexProperties = ServiceProvidersProperty::getProperties(ChatexApi::OBJECT_NAME);
            $chatex = new ChatexApi($chatexProperties['base_url'], $chatexProperties['api_token']);
            $wallet = $chatex->wallet($coin);
            $walletBalance = $wallet['amount'] ?? 0;
            if ($walletBalance < $amount) {
                $result['error']['amount'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10008']);
                return $result;
            }
        }

        $coin_amount = $amount2pay * $coin_rate;
        $currency_amount = round($ad['temp_price'] * $coin_amount, 2);
        Log::channel('api3_chatex_lbc_requests')->info('CONTACT_CREATE|CREATE|REQUEST|MERCHANT_ID|' . $merchant->id, ['ad' => $ad_id, 'amount' => $currency_amount, 'message' => $message]);
        $contract = $this->contractManager->contractCreate($ad, $currency_amount, $merchant, false, $message);
        if (!$contract) {
            Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CREATE|CREATE|ERROR|MERCHANT_ID|' . $merchant->id, ['inputs' => $request->only(['amount', 'ad_id']), 'res' => $contract]);
            $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006']);
            return $result;
        }
        $contractInfo = $this->contractManager->contractInfoByContract($contract);
        if (!$contractInfo) {
            Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CREATE|INFO|ERROR|MERCHANT_ID|' . $merchant->id, ['inputs' => $request->only(['amount', 'ad_id']), 'contractInfo' => $contractInfo]);
            $result['error']['ad_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006']);
            return $result;
        }

        $ps_amount = $contractInfo['amount_btc'] ?? null;
        $ps_currency = $ad['coin'] ?? null;
        $fiat_currency = $contractInfo['currency'] ?? null;

        $invoice = MerchantWithdrawalInvoice::create([
            'merchant_id' => $merchant->id,
            'amount' => $amount,
            'amount2pay' => $amount2pay,
            'amount2service' => $fee['service']['amount'],
            'amount2agent' => $fee['agent']['amount'],
            'amount2grow' => $fee['grow']['amount'],
            'commission_grow' => $fee['grow']['commission'],
            'commission_agent' => $fee['agent']['commission'],
            'commission_service' => $fee['service']['commission'],
            'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC,
            'payment_id' => $contractInfo['id'],
            'ps_amount' => $ps_amount,
            'ps_currency' => strtolower($ps_currency),
            'fiat_amount' => $contact_res['data']['amount'] ?? null,
            'fiat_currency' => strtolower($fiat_currency),
            'account_info' => $contact_res['data']['account_info'] ?? null,
            'addition_info' => json_encode([
                'contact' => $contractInfo,
                'ad' => $ad
            ]),
            'status' => MerchantWithdrawalInvoice::STATUS_USER_SELECTED,
            'input_currency' => $currency,
            'input_rate' => $currencyRate,
            'input_amount_value' => $input_amount,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_3,
            'merchant_expense' => $request->input('merchant_expense'),
            'service_provider_id' => $ad['service_provider_id'],
            'currency2currency' => $currency2currency
        ]);

        if (!$invoice) {
            $result['error']['ad_id'][] = __('Whoops! Something went wrong.');
            return $result;
        }
        Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CREATE|CREATE|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['contract' => $contract]);
        $result['data']['invoice_id'] = $invoice->id;
        $result['data']['amount'] = $contractInfo['amount'];
        $result['data']['currency'] = $ad['currency'] ?? null;
        $result['status'] = 1;
        InvoicesHistory::addHistoryRecord(
            __METHOD__,
            $invoice->id,
            AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
            0,
            InvoicesHistory::getStatus(1),
            InvoicesHistory::getSource(0),
            InvoicesHistory::getSource(3),
            json_encode($contractInfo),
            'Create Invoice Record'
        );
        return $result;
    }

    public function cancel(Request $request)
    {
        $result['status'] = 0;

        $rules = [
            'invoice_id' => 'required|numeric'
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules, [],);

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }

        $merchant = $request->input('merchant');

        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $request->input('invoice_id'),
            'merchant_id' => $merchant->id,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_3
        ])->first();

        if ($invoice) {
            $channel = $this->getLogChannel($invoice);
            if ($invoice->status == MerchantWithdrawalInvoice::STATUS_CANCELED) {
                $result['error']['invoice_id'][] = __('Payment canceled');
            } elseif ($invoice->status == MerchantWithdrawalInvoice::STATUS_PAYED) {
                $result['error']['invoice_id'][] = __('Payment closed successfully');
            } else {
                $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($contractInfo) {
                    Log::channel($channel)->info('BUY|CONTACT_CONFIRM|CHECK|INPUT_INVOICE|' . $invoice->id, ['contact_res' => $contractInfo]);
                    if ($this->isContractCancel($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment canceled');
                    } elseif ($this->isContractComplete($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment closed successfully');
                    } elseif ($this->isContractClosed($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment closed');
                    } elseif (!empty($contractInfo['funded_at'])) {
                        $result['error']['invoice_id'][] = __('Payment has been approved');
                    } else {
                        $res = $this->contractManager->contractCancelByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                        if ($res) {
                            $invoice->status = MerchantWithdrawalInvoice::STATUS_CANCELED;
                            $invoice->save();
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                                4,
                                InvoicesHistory::getStatus(1),
                                InvoicesHistory::getSource(0),
                                InvoicesHistory::getSource(2),
                                json_encode($res['data']),
                                'Cancel Invoice'
                            );
                            $result['status'] = 1;
                            Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CONFIRM|CANCEL|SUCCESS|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['res' => $res]);
                        } else {
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                                4,
                                InvoicesHistory::getStatus(0),
                                InvoicesHistory::getSource(0),
                                InvoicesHistory::getSource(2),
                                json_encode($res),
                                'Chatex response incorrect'
                            );
                            Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CONFIRM|CANCEL|ERROR|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['res' => $res]);
                        }
                    }
                } else {
                    $result['error']['invoice_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006']);
                    Log::channel('api3_chatex_lbc_requests')->info('CONTACT_CONFIRM|CANCEL|ERROR|USER_ID|MERCHANT_ID|' . $merchant->id . '|INPUT_INVOICE|' . $invoice->id, ['contractInfo' => $contractInfo]);
                }
            }
        } else {
            $result['error']['invoice_id'][] = __('Payment not found');
        }
        if(!empty($result['error']['invoice_id'])){
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                4,
                InvoicesHistory::getStatus(0),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(3),
                json_encode($result['error']['invoice_id']),
                'Error'
            );
        }
        return $result;
    }

    public function check_status(Request $request)
    {
        $result['status'] = 0;

        $rules = [
            'invoice_id' => 'required|numeric'
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->input(), $rules, [],);

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }
        $merchant = $request->input('merchant');
        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $request->input('invoice_id'),
            'merchant_id' => $merchant->id,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_3
        ])->first();

        if ($invoice) {
            $result['status'] = 1;
            if ($invoice->status == MerchantWithdrawalInvoice::STATUS_CANCELED) {
                $result['data']['status'] = [
                    'id' => -1,
                    'message' => __('Payment canceled')
                ];
            } elseif ($invoice->status == MerchantWithdrawalInvoice::STATUS_PAYED) {
                $result['data']['status'] = [
                    'id' => 1,
                    'message' => __('Payment paid')
                ];
            } else {
                $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($contractInfo) {
                    Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CHECK|USER_ID|MERCHANT_ID|' . $merchant->id . '|INPUT_INVOICE|' . $invoice->id, ['contractInfo' => $contractInfo]);
                    if ($this->isContractCancel($contractInfo)) {
                        $result['data']['status'] = [
                            'id' => -1,
                            'message' => __('Payment canceled')
                        ];
                    } elseif ($this->isContractComplete($contractInfo, $invoice)) {
                        $result['data']['status'] = [
                            'id' => 1,
                            'message' => __('Payment paid')
                        ];
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                            8,
                            InvoicesHistory::getStatus(1),
                            InvoicesHistory::getSource(0),
                            InvoicesHistory::getSource(2),
                            json_encode($contractInfo),
                            'Payment paid'
                        );
                    } elseif ($this->isContractClosed($contractInfo, $invoice)) {
                        $result['data']['status'] = [
                            'id' => -1,
                            'message' => __('Payment closed')
                        ];
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                            8,
                            InvoicesHistory::getStatus(1),
                            InvoicesHistory::getSource(0),
                            InvoicesHistory::getSource(2),
                            json_encode($contractInfo),
                            'Payment closed'
                        );
                    } else if ($this->isContractPaymentCompleted($contractInfo, $invoice) ||
                        $invoice->status == MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT) {
                        if (in_array($invoice->status, [MerchantWithdrawalInvoice::STATUS_CREATED, MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED, MerchantWithdrawalInvoice::STATUS_USER_SELECTED,])) {
                            $invoice->status = MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT;
                            $invoice->save();
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                                8,
                                InvoicesHistory::getStatus(0),
                                InvoicesHistory::getSource(0),
                                InvoicesHistory::getSource(2),
                                json_encode($contractInfo),
                                'Payment Confirmed'
                            );
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                                6,
                                InvoicesHistory::getStatus(1),
                                InvoicesHistory::getSource(2),
                                InvoicesHistory::getSource(3),
                                json_encode($contractInfo),
                                'Confirm Trader Payment'
                            );
                        }
                        $result['data']['status'] = [
                            'id' => 2,
                            'message' => __('Payment completed')
                        ];
                        $attachments = [];
                        if ($invoice->status == MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT) {
                            MerchantWithdrawalInvoiceAttachment::addAttachments($this->contractManager, $invoice);
                            foreach (MerchantWithdrawalInvoiceAttachment::where(['invoice_id' => $invoice->id])->orderBy('id')->get() as $attachment) {
                                $attachments[] = URL::signedRoute('withdrawal.attachment', ['id' => $attachment->id], now()->addMonth());
                            }
                        }
                        $result['data']['attachments'] = $attachments;

                    } else if ($this->isContractFunded($contractInfo, $invoice)) {
                        $result['data']['status'] = [
                            'id' => 3,
                            'message' => __('Payment has been approved')
                        ];
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                            6,
                            InvoicesHistory::getStatus(1),
                            InvoicesHistory::getSource(2),
                            InvoicesHistory::getSource(3),
                            json_encode($contractInfo),
                            'Payment has been approved'
                        );
                    } else {
                        $result['data']['status'] = [
                            'id' => 0,
                            'message' => __('Payment has not been approved yet')
                        ];
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                            6,
                            InvoicesHistory::getStatus(1),
                            InvoicesHistory::getSource(2),
                            InvoicesHistory::getSource(3),
                            json_encode($contractInfo),
                            'Payment has not been approved yet'
                        );
                    }
                } else {
                    $result['error']['invoice_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006']);
                    Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CHECK|ERROR|USER_ID|MERCHANT_ID|' . $merchant->id . '|INPUT_INVOICE|' . $invoice->id, ['contractInfo' => $contractInfo]);
                }
            }
        } else {
            $result['error']['invoice_id'][] = __('Payment not found');
        }

        return $result;
    }

    public function confirm(Request $request)
    {
        $result['status'] = 0;

        $rules = [
            'invoice_id' => 'required|numeric'
        ];

        $locale = $request->input('locale', 'en');
        app()->setLocale($locale);

        $validator = Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $result['error'] = $validator->errors();
            return $result;
        }
        $merchant = $request->input('merchant');
        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $request->input('invoice_id'),
            'merchant_id' => $merchant->id,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_3
        ])->first();

        if ($invoice) {
            if ($invoice->status == MerchantWithdrawalInvoice::STATUS_CANCELED) {
                $result['error']['invoice_id'][] = __('Payment canceled');
            } elseif ($invoice->status == MerchantWithdrawalInvoice::STATUS_PAYED) {
                $result['error']['invoice_id'][] = __('Payment closed successfully');
            } else {
                $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($contractInfo) {
                    Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CONFIRM|CHECK|MERCHANT_ID|' . $invoice->merchant_id . '|WITHDRAWAL|' . $invoice->id, ['contractInfo' => $contractInfo]);
                    if ($this->isContractCancel($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment canceled');
                    } elseif ($this->isContractComplete($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment closed successfully');
                    } elseif ($this->isContractClosed($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment closed');
                    } elseif (empty($contractInfo['funded_at'])) {
                        $result['error']['invoice_id'][] = __('Payment has not been approved yet');
                    } elseif (!$this->isContractPaymentCompleted($contractInfo, $invoice)) {
                        $result['error']['invoice_id'][] = __('Payment has not been paid yet');
                    } else {
                        Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CONFIRM|RELEASE|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['contractInfo' => $contractInfo]);
                        $res = $this->contractManager->contactReleaseByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                        if ($res) {
                            $invoice->status = MerchantWithdrawalInvoice::STATUS_PAYED;
                            $invoice->payed = now();
                            $invoice->save();
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                                6,
                                InvoicesHistory::getStatus(1),
                                InvoicesHistory::getSource(0),
                                InvoicesHistory::getSource(2),
                                json_encode($res),
                                'Payed'
                            );
                            $result['status'] = 1;
                            Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CONFIRM|RELEASE|SUCCESS|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['res' => $res]);
                        } else {
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                                6,
                                InvoicesHistory::getStatus(0),
                                InvoicesHistory::getSource(0),
                                InvoicesHistory::getSource(2),
                                json_encode($res),
                                'Chatex response incorrect'
                            );
                            Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CONFIRM|RELEASE|ERROR|MERCHANT_ID|' . $merchant->id . '|PAYMENT_INVOICE_ID|' . $invoice->id, ['res' => $res]);
                        }
                    }
                } else {
                    $result['error']['invoice_id'][] = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006']);
                    Log::channel('api3_chatex_lbc_requests')->info('BUY|CONTACT_CONFIRM|RELEASE|ERROR|USER_ID|MERCHANT_ID|' . $merchant->id . '|INPUT_INVOICE|' . $invoice->id, ['contractInfo' => $contractInfo]);
                }
            }
        } else {
            $result['error']['invoice_id'][] = __('Payment not found');
        }
        if(!empty($result['error']['invoice_id'])){
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                6,
                InvoicesHistory::getStatus(0),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(3),
                json_encode($result['error']['invoice_id']),
                'Error'
            );
        }
        return $result;
    }

    public function attachment(Request $request, $id)
    {
        $attachment = MerchantWithdrawalInvoiceAttachment::where(['id' => $id])
            ->firstOrFail();

        return response(file_get_contents($attachment->url), 200)->header('Content-type', $attachment->type);
    }
}
