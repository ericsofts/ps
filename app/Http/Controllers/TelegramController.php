<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Exception;

class TelegramController extends Controller
{
    public function webhook(Request $request)
    {
        $message = json_decode($request->getContent());
        if (!$message) {
            throw new Exception("Not valid message");
        }
        Log::channel('telegram_noty')->info('message', ['message' => $message]);
        $message = $message->message;

        App::setLocale($message->from->language_code ?? 'en');

        $text = __("Your ID") . ": `{$message->from->id}`";

        $reply_markup = Keyboard::make([
            'resize_keyboard' => true,
            'one_time_keyboard' => false
        ])
            ->row(['text' => __("My ID")]);

        if (preg_match('/start|ID/', $message->text)) {
            Telegram::sendMessage([
                'chat_id' => $message->from->id,
                'text' => $text,
                'parse_mode' => 'Markdown',
                'reply_markup' => $reply_markup
            ]);
        }

        return response()->json([
            'ok' => true
        ]);
    }
}
