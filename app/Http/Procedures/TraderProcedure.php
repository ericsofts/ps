<?php

declare(strict_types=1);

namespace App\Http\Procedures;

use App\Models\TraderDeal;
use App\Models\TraderDealHistory;
use Illuminate\Http\Request;
use Sajya\Server\Exceptions\InvalidParams;
use Sajya\Server\Procedure;

class TraderProcedure extends Procedure
{

    public static string $name = 'trader';

    public function setTraderDealStatus(Request $request): array|InvalidParams
    {
        $deal_id = $request->input('deal_id');
        $customer_type = $request->input('customer_type');
        $customer_id = $request->input('customer_id');
        $status = $request->input('status');
        $description = $request->input('description', false);
        if (empty($deal_id)) {
            return new InvalidParams(['deal_id' => 'Is required']);
        }

        if (empty($status)) {
            return new InvalidParams(['status' => 'Is required']);
        }

        if (empty($customer_type)) {
            return new InvalidParams(['customer_type' => 'Is required']);
        }

        if (empty($customer_id)) {
            return new InvalidParams(['customer_id' => 'Is required']);
        }

        $deal = TraderDeal::find($deal_id);
        if (!$deal) {
            return new InvalidParams(['deal_id' => 'Is unknown']);
        }

        $deal->status = $status;
        $deal->description = $description;
        $deal->customer_type = $customer_type;
        $deal->customer_id = $customer_id;
        try{
            $deal->save();
        }catch (\Exception $exception){
            return [
                'success' => false,
                'reason' => $exception->getMessage()
            ];
        }


//        if($description){
//            $dealHistory = TraderDealHistory::where('deal_id', $deal_id)->orderBy('created_at', 'desc')->first();
//            if($dealHistory){
//                $dealHistory->description = $description;
//                $dealHistory->save();
//            }
//        }

        return [
            'success' => true,
        ];
    }

    public function getTraderDealStatus(Request $request)
    {
        return [
            'success' => true,
            'statuses' => TraderDeal::getStatuses()
        ];
    }

    public function getTraderDealAvalableStatus(Request $request)
    {
        return [
            'success' => true,
            'statuses' => TraderDeal::getAvalableStatuses()
        ];
    }

    public function getCustomerType(Request $request)
    {
        return [
            'success' => true,
            'types' => TraderDeal::getCustomerType()
        ];
    }

}
