<?php

declare(strict_types=1);

namespace App\Http\Procedures;


use App\Models\AccountsCurrency;
use App\Models\Balance;
use App\Models\BalanceHistory;
use App\Models\BalanceQueue;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Sajya\Server\Exceptions\InvalidParams;
use Sajya\Server\Procedure;

class UserProcedure extends Procedure
{

    public static string $name = 'user';

    public function getUserById(Request $request)
    {
        $user_id = $request->input('user_id');
        if (empty($user_id)) {
            return new InvalidParams(['user_id' => 'Is required']);
        }

        $user = User::find($user_id);
        if (!$user) {
            return new InvalidParams(['user_id' => 'Is unknown']);
        }

        $data = $user;

        return [
            'success' => true,
            'data' => $data
        ];
    }

    public function searchUser(Request $request)
    {
        $user_search = $request->input('user');
        if (empty($user_search)) {
            return new InvalidParams(['user' => 'Is required']);
        }

        $users = User::where('name', 'LIKE' , '%'.$user_search.'%')
            ->orWhere('email', 'LIKE' , '%'.$user_search.'%')->get();
        if (!$users) {
            return new InvalidParams(['user' => 'Is unknown']);
        }

        $data = $users;

        return [
            'success' => true,
            'data' => $data
        ];
    }

    public function getAllUsersBalances(Request $request)
    {
        $data = Balance::orderBy('id', 'desc')->get();
        return [
            'success' => true,
            'data' => $data
        ];
    }

    public function getAllUsersBalanceSum(Request $request)
    {
        $accounts = AccountsCurrency::orderBy('id', 'asc')->get();
        foreach ($accounts as $accountItem) {
            $data[$accountItem->asset] = Balance::where('currency', $accountItem->asset)->sum('amount');
            $balanceDebitQueue = BalanceQueue::where([
                'currency' => $accountItem->asset,
                'type' => BalanceQueue::TYPE_DEBIT,
            ])->whereNotIn('status', [BalanceQueue::STATUS_DONE, BalanceQueue::STATUS_FAILED])
                ->sum('amount');
            $data[$accountItem->asset] = round($data[$accountItem->asset] + $balanceDebitQueue, 8);
        }

        return [
            'success' => true,
            'data' => $data
        ];
    }

    public function getUserBalance(Request $request)
    {
        $user_id = $request->input('user_id');
        if (empty($user_id)) {
            return new InvalidParams(['user_id' => 'Is required']);
        }

        $user = User::find($user_id);
        if (!$user) {
            return new InvalidParams(['user_id' => 'Is unknown']);
        }

        $accounts = AccountsCurrency::orderBy('id', 'asc')->get();
        foreach ($accounts as $accountItem) {
            if($accountItem->asset == 'GRT'){
                $data['GRT'] = $user->balance_grow_token_total();
            }else{
                $data[$accountItem->asset] =  $user->balance_total($accountItem->asset);
            }

        }

        return [
            'success' => true,
            'data' => $data
        ];
    }

    public function getUserBalanceHistory(Request $request)
    {
        $user_id = $request->input('user_id');
        $from = $request->input('from', false);
        $to = $request->input('to', false);
        $type = $request->input('type', false);
        $currency = $request->input('currency', false);
        if (empty($user_id)) {
            return new InvalidParams(['user_id' => 'Is required']);
        }

        $user = User::find($user_id);
        if (!$user) {
            return new InvalidParams(['user_id' => 'Is unknown']);
        }

        $balanceHistory = BalanceHistory::where('user_id', '=', $user_id)->orderBy('id','desc');
        if($from){
            $balanceHistory->where('created_at', '>=', $from);
        }

        if($to){
            $balanceHistory->where('created_at', '<=', $to);
        }

        if($type !== false){
            $balanceHistory->where('type', '=', $type);
        }

        if($currency){
            $balanceHistory->where('currency', '=', $currency);
        }

        $data = $balanceHistory->get();

        return [
            'success' => true,
            'data' => $data
        ];
    }

    public function getUserTransaction(Request $request)
    {
        $user_id = $request->input('user_id');
        $from = $request->input('from', false);
        $to = $request->input('to', false);
        $type = $request->input('type', false);
        $currency = $request->input('currency', false);
        $status = $request->input('status', false);
        $fiat = $request->input('fiat', false);
        if (empty($user_id)) {
            return new InvalidParams(['user_id' => 'Is required']);
        }

        $user = User::find($user_id);
        if (!$user) {
            return new InvalidParams(['user_id' => 'Is unknown']);
        }

        $balanceTransactions = BalanceQueue::where('user_id', '=', $user_id);
        if($from){
            $balanceTransactions->where('created_at', '>=', $from);
        }

        if($to){
            $balanceTransactions->where('created_at', '<=', $to);
        }

        if($type){
            $balanceTransactions->where('type', '=', $type);
        }

        if($fiat !== false){
            $balanceTransactions->whereIn('currency', $fiat);
        }

        if($currency){
            $balanceTransactions->where('currency', '=', $currency);
        }

        if($status){
            $balanceTransactions->where('status', '=', $status);
        }

        $accounts = AccountsCurrency::orderBy('id', 'asc')->get();
        $count = $balanceTransactions->selectRaw('count(*) as total');

        foreach ($accounts as $accountItem) {
            $balanceTransactions->selectRaw("count(case when currency = '" . $accountItem->asset . "' then 1 end) as \"" . $accountItem->asset ."\"");
        }

        $data = $this->getTansactonDataByCurreny($accounts,  $count->first());

        return [
            'success' => true,
            'data' => $data
        ];
    }

    public function getUserTransactionList(Request $request)
    {
        $user_id = $request->input('user_id');
        $from = $request->input('from', false);
        $to = $request->input('to', false);
        $type = $request->input('type', false);
        $currency = $request->input('currency', false);
        $status = $request->input('status', false);
        $fiat = $request->input('fiat', false);

        if (empty($user_id)) {
            return new InvalidParams(['user_id' => 'Is required']);
        }

        $user = User::find($user_id);
        if (!$user) {
            return new InvalidParams(['user_id' => 'Is unknown']);
        }

        $balanceTransactions = BalanceQueue::where('user_id', '=', $user_id)->orderBy('id', 'desc');
        if($from){
            $balanceTransactions->where('created_at', '>=', $from);
        }

        if($to){
            $balanceTransactions->where('created_at', '<=', $to);
        }

        if($type !== false){
            $balanceTransactions->where('type', '=', $type);
        }

        if($fiat !== false){
            $balanceTransactions->whereIn('currency', $fiat);
        }

        if($currency){
            $balanceTransactions->where('currency', '=', $currency);
        }

        if($status !== false){
            $balanceTransactions->where('status', '=', $status);
        }


        $data = $balanceTransactions->get();

        return [
            'success' => true,
            'data' => $data
        ];
    }

    public function getTotalTransaction(Request $request)
    {

        $from = $request->input('from', false);
        $to = $request->input('to', false);
        $type = $request->input('type', false);
        $currency = $request->input('currency', false);
        $status = $request->input('status', false);


        $balanceTransactions = BalanceQueue::query();
        if($from){
            $balanceTransactions->where('created_at', '>=', $from);
        }

        if($to){
            $balanceTransactions->where('created_at', '<=', $to);
        }

        if($type){
            $balanceTransactions->where('type', '=', $type);
        }

        if($currency){
            $balanceTransactions->where('currency', '=', $currency);
        }

        if($status){
            $balanceTransactions->where('status', '=', $status);
        }

        $accounts = AccountsCurrency::orderBy('id', 'asc')->get();
        $count = $balanceTransactions->selectRaw('count(*) as total');

        foreach ($accounts as $accountItem) {
            $balanceTransactions->selectRaw("count(case when currency = '" . $accountItem->asset . "' then 1 end) as \"" . $accountItem->asset ."\"");
        }
        $data = $this->getTansactonDataByCurreny($accounts,  $count->first());

        return [
            'success' => true,
            'data' => $data
        ];
    }

    public function changeUserBalance(Request $request){
        $type = $request->input('type', false);
        $currency = $request->input('currency', false);
        $amount = $request->input('amount', false);
        $user_id = $request->input('user_id', false);
        $description = $request->input('description', '');
        if(($type !== false) && $currency && $user_id){
            $result = BalanceQueue::addToBalanceQueue(
                $user_id,
                $amount,
                $type,
                $description,
                null,
                null,
                null,
                BalanceQueue::STATUS_WAITING,
                $currency
            );
            return [
                'success' => $result,
                'data' => []
            ];
        }
        return [
            'success' => false,
            'data' => []
        ];
    }

    protected function getTansactonDataByCurreny($accounts, $data){
        foreach ($accounts as $accountItem) {
            $countAccounts[$accountItem->asset]['name'] = $accountItem->name;
            $countAccounts[$accountItem->asset]['type'] = $accountItem->type;
            $countAccounts[$accountItem->asset]['count'] = $data->{$accountItem->asset};
        }
        $countAccounts['total']['name'] = __('Total');
        $countAccounts['total']['count'] = $data->total;
        $countAccounts['total']['type'] = 'total';
        return $countAccounts;

    }

    public function getUserAccountsBalancesToDay(Request $request)
    {
        $user_id = $request->input('user_id');
        $to = $request->input('to');

        if (empty($user_id)) {
            return new InvalidParams(['user_id' => 'Is required']);
        }

        if (empty($to)) {
            return new InvalidParams(['to' => 'Is required']);
        }

        $user = User::find($user_id);
        if (!$user) {
            return new InvalidParams(['user_id' => 'Is unknown']);
        }


        $accounts = AccountsCurrency::orderBy('id', 'asc')->get();
        foreach ($accounts as $accountItem) {
            if($accountItem->asset == 'GRT'){
                $data['GRT'] = $user->balance_grow_to_day($to);
            }else{
                $data[$accountItem->asset] =  $user->balance_to_day($to, $accountItem->asset);
            }

        }

        return [
            'success' => true,
            'data' => $data
        ];
    }

    public function getUserAccountsBalancesDayChange(Request $request)
    {
        $user_id = $request->input('user_id');
        $from = $request->input('from');
        $to = $request->input('to');
        if (empty($user_id)) {
            return new InvalidParams(['user_id' => 'Is required']);
        }

        if (empty($to)) {
            return new InvalidParams(['to' => 'Is required']);
        }

        if (empty($from)) {
            return new InvalidParams(['from' => 'Is required']);
        }

        $user = User::find($user_id);
        if (!$user) {
            return new InvalidParams(['user_id' => 'Is unknown']);
        }

        $accounts = AccountsCurrency::orderBy('id', 'asc')->get();
        foreach ($accounts as $accountItem) {
            if($accountItem->asset == 'GRT'){
                $data['GRT'] = $user->balance_grow_change($from, $to);
            }else{
                $data[$accountItem->asset] =  $user->balance_change($from, $to, $accountItem->asset);
            }

        }

        return [
            'success' => true,
            'data' => $data
        ];
    }
}
