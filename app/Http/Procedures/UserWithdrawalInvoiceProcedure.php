<?php

declare(strict_types=1);

namespace App\Http\Procedures;

use App\Models\AccountingEntrie;
use App\Models\GrowTokenInvoice;
use App\Models\InvoicesHistory;
use Illuminate\Http\Request;
use Sajya\Server\Exceptions\InvalidParams;
use Sajya\Server\Procedure;

class UserWithdrawalInvoiceProcedure extends Procedure
{

    public static string $name = 'uwi';

    public function setStatusPayed(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return new InvalidParams(['id' => 'Is required']);
        }

        $invoice = GrowTokenInvoice::where(['id' => $id])
            ->where('status', '<>', GrowTokenInvoice::STATUS_PAYED)
            ->first();
        if (!$invoice) {
            return new InvalidParams(['id' => 'Is unknown']);
        }

        $invoice->status = GrowTokenInvoice::STATUS_PAYED;
        $invoice->save();

        return [
            'success' => true
        ];
    }

    public function setStatusCancel(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return new InvalidParams(['id' => 'Is required']);
        }

        $invoice = GrowTokenInvoice::where(['id' => $id])
            ->where('status', '<>', GrowTokenInvoice::STATUS_CANCELED)
            ->first();
        if (!$invoice) {
            return new InvalidParams(['id' => 'Is unknown']);
        }

        $invoice->status = GrowTokenInvoice::STATUS_CANCELED;
        $invoice->save();

        return [
            'success' => true
        ];
    }
}
