<?php

declare(strict_types=1);

namespace App\Http\Procedures;

use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\PaymentInvoice;
use Illuminate\Http\Request;
use Sajya\Server\Exceptions\InvalidParams;
use Sajya\Server\Procedure;

class PaymentInvoiceProcedure extends Procedure
{

    public static string $name = 'pi';

    public function setStatusPayed(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return new InvalidParams(['id' => 'Is required']);
        }

        $invoice = PaymentInvoice::where(['id' => $id])
            ->where('status', '<>', PaymentInvoice::STATUS_PAYED)
            ->first();
        if (!$invoice) {
            return new InvalidParams(['id' => 'Is unknown']);
        }

        $invoice->status = PaymentInvoice::STATUS_PAYED;
        $invoice->save();

        InvoicesHistory::addHistoryRecord(
            __METHOD__,
            $invoice->id,
            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
            3,
            InvoicesHistory::getStatus(1),
            InvoicesHistory::getSource(5),
            InvoicesHistory::getSource(3),
            NULL,
            'Change Status to Payed by Admin(api)'
        );

        return [
            'success' => true
        ];
    }

    public function setStatusRevert(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return new InvalidParams(['id' => 'Is required']);
        }

        $invoice = PaymentInvoice::where(['id' => $id])
            ->where('status', PaymentInvoice::STATUS_PAYED)
            ->first();
        if (!$invoice) {
            return new InvalidParams(['id' => 'Is unknown']);
        }

        $invoice->status = PaymentInvoice::STATUS_CANCELED_BY_REVERT;
        $invoice->save();

        InvoicesHistory::addHistoryRecord(
            __METHOD__,
            $invoice->id,
            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
            3,
            InvoicesHistory::getStatus(1),
            InvoicesHistory::getSource(5),
            InvoicesHistory::getSource(3),
            NULL,
            'Change Status to Revert by Admin(api)'
        );

        return [
            'success' => true
        ];
    }
}
