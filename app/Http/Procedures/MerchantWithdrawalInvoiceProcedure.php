<?php

declare(strict_types=1);

namespace App\Http\Procedures;

use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\MerchantWithdrawalInvoice;
use Illuminate\Http\Request;
use Sajya\Server\Exceptions\InvalidParams;
use Sajya\Server\Procedure;

class MerchantWithdrawalInvoiceProcedure extends Procedure
{

    public static string $name = 'mwi';

    public function setStatusPayed(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return new InvalidParams(['id' => 'Is required']);
        }

        $invoice = MerchantWithdrawalInvoice::where(['id' => $id])
            ->where('status', '<>', MerchantWithdrawalInvoice::STATUS_PAYED)
            ->first();
        if (!$invoice) {
            return new InvalidParams(['id' => 'Is unknown']);
        }

        $invoice->status = MerchantWithdrawalInvoice::STATUS_PAYED;
        $invoice->save();

        InvoicesHistory::addHistoryRecord(
            __METHOD__,
            $invoice->id,
            AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
            3,
            InvoicesHistory::getStatus(1),
            InvoicesHistory::getSource(5),
            InvoicesHistory::getSource(3),
            NULL,
            'Change Status to Payed by Admin(api)'
        );

        return [
            'success' => true
        ];
    }
}
