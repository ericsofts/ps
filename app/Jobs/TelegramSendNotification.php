<?php

namespace App\Jobs;

use App\Models\Property;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class TelegramSendNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $api_url = 'https://api.telegram.org/bot';
    protected string $text;

    /**
     * Create a new job instance.
     *
     * @param $text
     */
    public function __construct($text)
    {
        $this->text = $text;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pt = Property::getProperties('telegram');
        if (!empty($pt['api_token'])) {
            $response = Http::get($this->api_url . $pt['api_token'] . "/sendMessage", [
                'chat_id' => $pt['chat_id'],
                'text' => $this->text
            ]);
            Log::channel('telegram')->info($this->text, ['status' => $response->status(), 'body' => $response->body()]);
        }
    }
}
