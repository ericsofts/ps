<?php

namespace App\Jobs;

use App\Models\MerchantWithdrawalInvoice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PaymentWithdrawalSendResponse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 20;

    protected $paymentWithdrawal;
    protected $response2server;

    /**
     * Create a new job instance.
     *
     * @param MerchantWithdrawalInvoice $paymentWithdrawal
     * @param $response2server
     */
    public function __construct(MerchantWithdrawalInvoice $paymentWithdrawal, $response2server)
    {
        $this->paymentWithdrawal = $paymentWithdrawal;
        $this->response2server = $response2server;
    }

    public function backoff()
    {
        return [10, 30, 30, 60];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->paymentWithdrawal->merchant_server_url){
            $validator = Validator::make(['url' => $this->paymentWithdrawal->merchant_server_url], [
                'url' => 'required|url'
            ]);
            if (!$validator->fails()) {
                try{
                    $response = Http::withoutVerifying()->asForm()->post($this->paymentWithdrawal->merchant_server_url, $this->response2server);
                    Log::channel('merchant_withdrawal_invoice_server_response')->info('INVOICE_ID|' . $this->paymentWithdrawal->id.'|STATUS|' . $response->status(),
                        ['response' => $response->json(), 'response2server' => $this->response2server, 'attempt' => $this->attempts()]);
                    if($response->ok()){
                        $this->paymentWithdrawal->merchant_response_at = now();
                        $this->paymentWithdrawal->save();
                    }else{
                        $this->release($this->backoff()[$this->attempts()] ?? 60);
                    }
                }catch (\Exception $e){
                    Log::channel('merchant_withdrawal_invoice_server_response')->info('INVOICE_ID|' . $this->paymentWithdrawal->id.'|EXCEPTION|STATUS|' .$e->getCode(),
                        ['response' => $e->getMessage(), 'response2server' => $this->response2server, 'attempt' => $this->attempts()]);
                    $this->release($this->backoff()[$this->attempts()] ?? 60);
                }

            }else{
                Log::channel('merchant_withdrawal_invoice_server_response')->info('INVOICE_ID|' . $this->paymentWithdrawal->id.'|ERROR|',
                ['errors' => $validator->errors()->toArray(), 'response2server' => $this->response2server]);
            }
        }
    }
}
