<?php

namespace App\Console\Commands;

use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use Illuminate\Console\Command;

class UpdateInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:update_amounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    private Merchant $merchantModel;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Merchant $merchantModel)
    {
        parent::__construct();
        $this->merchantModel = $merchantModel;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $payment_invoices = PaymentInvoice::whereIn('api_type', [
                PaymentInvoice::API_TYPE_1,
                PaymentInvoice::API_TYPE_2,
                PaymentInvoice::API_TYPE_3,
                PaymentInvoice::API_TYPE_4
            ]
        )->get();
        foreach ($payment_invoices as $invoice){
            $fee = $this->merchantModel->getFeeCommission($invoice->merchant_id, $invoice->amount, $invoice->api_type);

            $amount2pay = $invoice->amount - $fee['total'];
            $invoice->amount2pay = $amount2pay;
            $invoice->amount2service = $fee['service']['amount'];
            $invoice->amount2agent = $fee['agent']['amount'];
            $invoice->amount2grow = $fee['grow']['amount'];
            $invoice->commission_grow = $fee['grow']['commission'];
            $invoice->commission_agent = $fee['agent']['commission'];
            $invoice->commission_service = $fee['service']['commission'];
            $invoice->save();
        }
        $invoices = MerchantWithdrawalInvoice::whereIn('api_type', [MerchantWithdrawalInvoice::API_TYPE_0, MerchantWithdrawalInvoice::API_TYPE_3])->get();

        foreach ($invoices as $invoice){

            $fee = $this->merchantModel->getFeeCommission($invoice->merchant_id, $invoice->amount, MerchantWithdrawalInvoice::API_TYPE_3, MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION);
            $amount2pay = $invoice->amount - $fee['total'];
            $invoice->amount2pay = $amount2pay;
            $invoice->amount2service = $fee['service']['amount'];
            $invoice->amount2agent = $fee['agent']['amount'];
            $invoice->amount2grow = $fee['grow']['amount'];
            $invoice->commission_grow = $fee['grow']['commission'];
            $invoice->commission_agent = $fee['agent']['commission'];
            $invoice->commission_service = $fee['service']['commission'];
            $invoice->save();
        }
        return 0;
    }
}
