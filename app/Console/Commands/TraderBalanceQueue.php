<?php

namespace App\Console\Commands;

use App\Models\TraderBalanceHistory;
use App\Models\TraderBalance;
use App\Models\TraderBalanceQueue as TraderBalanceQueueModel;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TraderBalanceQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trader_balance_queue:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check accounting queue';

    protected $isSleep = false;

    protected $traderBalanceQueue;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $this->info('Start TraderBalancegQueue');
        while (true) {
            $this->_main();
        }
    }

    private function _main()
    {
        $this->traderBalanceQueue = $traderBalanceQueue = TraderBalanceQueueModel::getTraderBalanceQueueWaiting();

        if (!$this->traderBalanceQueue) {
            sleep(config('app.trader_balance_queue_sleep_time'));
            if (!$this->isSleep) {
                $this->info('Sleep');
                $this->isSleep = true;
            }
            return;
        }
        $this->isSleep = false;
        $this->info('Working');
        $this->info(now()->toIso8601String() . '| traderBalanceQueue #' . $this->traderBalanceQueue->id);

        $this->logMessage('Start executing a request from the queue');

        try {
            DB::transaction(function () use ($traderBalanceQueue) {

                $this->logMessage('Opening a transaction');
                $this->changeStatus(TraderBalanceQueueModel::STATUS_PROCESSING, 'STATUS_PROCESSING');

                if ($traderBalanceQueue->type == TraderBalanceQueueModel::TYPE_DEBIT) {
                    $amount = $traderBalanceQueue->amount * (-1);
                } else {
                    $amount = $traderBalanceQueue->amount;
                }

                //
                $balance = TraderBalance::find($traderBalanceQueue->balance_id);

                if (!$balance) {
                    $balance = TraderBalance::firstOrCreate(
                        [
                            'type' => $traderBalanceQueue->balance_type,
                            'trader_id' => $traderBalanceQueue->trader_id,
                            'currency_id' => $traderBalanceQueue->currency_id,
                            'currency' => $traderBalanceQueue->currency
                        ]
                    );
                }
                $newBalanceAfter = $this->balanceUpdate($balance, $amount);
                //

                $balanceHistoryData = $this->getBalanceHistoryData($this->traderBalanceQueue->attributesToArray(), $newBalanceAfter);
                $balanceHistory = TraderBalanceHistory::create($balanceHistoryData);
                $this->logMessage('Update balance history.');

                $this->changeStatus(TraderBalanceQueueModel::STATUS_DONE, 'STATUS_DONE');
            }, 3);
        } catch (\Exception $exception) {

            $this->logMessage('An error occurred while executing a request from the queue.');
            $this->logMessage($exception);

            $this->changeStatus(TraderBalanceQueueModel::STATUS_FAILED, 'STATUS_FAILED');
        }
    }

    private function logMessage($message)
    {
        $message = $message . sprintf(' [id = %s]', $this->traderBalanceQueue->id);
        Log::channel('trader_balance_queue')->info($message);
    }

    private function changeStatus($statusValue, $statusName)
    {
        $this->traderBalanceQueue->status = $statusValue;
        $this->traderBalanceQueue->save();
        $this->logMessage(sprintf('Status of the request changed to %s(%s).', $statusName, $statusValue));
    }

    private function balanceUpdate($balance, $amount)
    {
        $balance->amount += $amount;
        $balance->save();
        $this->logMessage(sprintf('Update %s trader_balance.', $balance->id));
        return $balance->amount;
    }

    private function getBalanceHistoryData($data, $newBalanceAfter)
    {
        unset($data['id']);
        unset($data['created_at']);
        unset($data['updated_at']);
        $data['balance'] = $newBalanceAfter;
        $data['status'] = TraderBalanceQueueModel::STATUS_DONE;
        return $data;
    }
}
