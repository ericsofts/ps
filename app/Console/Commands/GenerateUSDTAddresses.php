<?php

namespace App\Console\Commands;

use App\Models\UsdtAddress;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class GenerateUSDTAddresses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'usdt:generate_addresses {qty}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $qty = $this->argument('qty');
        $xpub_key = Storage::get('usdt_xpk');

        if(!$xpub_key){
            $this->warn('Error! xpub does not configured!!!');
            return 0;
        }
        $count = UsdtAddress::where(['status' => UsdtAddress::STATUS_UNUSED])->count();
        if($count > $qty){
            return 0;
        }
        $start = 0;
        $addr_data = UsdtAddress::orderBy('addr_index', 'desc')->first();
        if(!empty($addr_data->addr_index)){
            $start = $addr_data->addr_index + 1;
        }
        exec( base_path('hd-wallet-derive').'/hd-wallet-derive.php -g --key=' . $xpub_key . ' --cols=index,address --coin=ETH --startindex=' . $start . ' --numderive=' . $qty . ' --format=json', $output, $retval);
        $res = json_decode($output[count($output) - 1], true);
        if (is_array($res)) {
            $data = [];
            foreach ($res as $tem){
                UsdtAddress::create(['usdt_address' => $tem['address'], 'addr_index' => $tem['index'], 'xpub_id' => 1]);
            }
        }

        return 0;
    }
}
