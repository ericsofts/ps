<?php

namespace App\Console\Commands;

use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use Illuminate\Console\Command;

class RecalculateMerchantBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merchant_balance:recalculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $merchants = Merchant::all();
        foreach ($merchants as $merchant){
            $balance = MerchantBalance::firstOrCreate(
                ['merchant_id' => $merchant->id]
            );
            $paymentSum = PaymentInvoice::where([
                'merchant_id' => $merchant->id,
                'status' => PaymentInvoice::STATUS_PAYED
            ])->sum('amount2pay');
            $withdrawalSum = MerchantWithdrawalInvoice::where([
                'merchant_id' => $merchant->id,
                'status' => MerchantWithdrawalInvoice::STATUS_PAYED
            ])->sum('amount');
            $balance->amount += $paymentSum - $withdrawalSum;
            $balance->save();
        }
        return 0;
    }
}
