<?php

namespace App\Console\Commands;

use App\Models\MerchantWithdrawalInvoice;
use App\Models\Property;
use Carbon\Carbon;
use CoinMarketCap\Features\Tools;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class FixWithdrawalAccountInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FixWithdrawalAccountInfo:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix Withdrawal Account Info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->updateAccountInfo();
        return 0;
    }

    public function updateAccountInfo()
    {
        $startDate = Carbon::parse('2021-10-26');
        $withdravalRecords = MerchantWithdrawalInvoice::where('created_at', '>=', $startDate)
            ->where('account_info','')
            ->get();
        foreach ($withdravalRecords as $record){
            $addition_info = json_decode($record->addition_info, true);
            $account_info = $addition_info['contact']['account_info'] ?? null;
            $record->account_info = $account_info;
            $record->save();
        }
    }
}
