<?php

namespace App\Console\Commands;

use App\Models\Property;
use Illuminate\Console\Command;
use WebSocket\Client;

class CoinsbitUpdateRateGROW2USDT extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coinsbit_update:grow_rate_usdt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var mixed
     */
    private $grow_eth;
    /**
     * @var mixed
     */
    private $eth_usd;
    /**
     * @var int|mixed
     */
    private $coin_rate;
    /**
     * @var int|mixed
     */
    private $grow_usdt;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $properties = Property::getProperties('grow_token_usd2token');
        $this->grow_eth = $properties['grow_eth'] ?? 0;
        $this->grow_usdt = $properties['grow_usdt'] ?? 0;
        $this->eth_usd = $properties['eth_usd'] ?? 0;
        $this->coin_rate = $properties['coin_rate'] ?? 0;
        $start_datetime = now();
        $client = new Client("wss://coinsbit.io/trade_ws");
        $params = ["GROW_USDT"];
        $client->send('{"method":"price.subscribe","params":["GROW_USDT"],"id": ' . time() . '}');
        $check = true;
        $this->info('Start ' . $start_datetime->toIso8601String());
        while ($check) {
            try {
                $message = $client->receive();
                $m = json_decode($message, true);
                if ($m && !empty($m['method']) && $m['method'] == 'price.update') {
                    if ($m['params'][0] == 'GROW_USDT') {
                        $this->grow_usdt = $m['params'][1];
                        $this->updateRate();
                    }
                    $this->info(now());
                    $this->info($message);
                } else {
                    if ($m && !empty($m['params']) && in_array($m['params'][0], $params)) {
                        $this->warn($message);
                    }
                }
            } catch (\WebSocket\ConnectionException $e) {
                $this->error($e->getMessage());
                $this->error($e->getCode());
                $client->close();
            }
            if ($start_datetime->addHours(1) < now()) {
                $client->close();
                $check = false;
            }
        }
        $this->info('Stop ' . now()->toIso8601String());
        return 0;
    }

    private function updateRate()
    {
        $properties = Property::getProperties('grow_token_usd2token');
        $usdt = Property::getProperties('coinmarketcap_usdt2usd');
        $grow_usdt = $properties['grow_eth'];
        if ($grow_usdt != $this->grow_usdt) {
            Property::setProperty('grow_token_usd2token', 0, 'grow_usdt', $this->grow_usdt);
        }
        if (!empty($usdt['coin_rate']) ?? $this->coin_rate != $usdt['coin_rate'] * $this->grow_usdt) {
            Property::setProperty('grow_token_usd2token', 0, 'coin_rate', $usdt['coin_rate'] * $this->grow_usdt);
        }
    }
}
