<?php

namespace App\Console\Commands;

use App\Models\TraderDeal;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class CheckerTraderDeals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trader_deals:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Trader Deals';

    protected $isSleep = false;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $this->info('Start Trader Deals Check');
        while (true) {
            $this->_main();
        }
    }


    private function _main()
    {

        $traderDeals = TraderDeal::getTraderDealsWaiting();

        if (!$traderDeals->count()) {
            sleep(config('app.trader_deals_sleep_time'));
            if (!$this->isSleep) {
                $this->info('Sleep');
                $this->isSleep = true;
            }

            return;
        }
        $this->isSleep = false;
        $this->info('Working');
        //$this->info(now()->toIso8601String() . '| TraderDealsChecker #' . $traderDeals->id);
        $this->info(now()->toIso8601String());

        foreach ($traderDeals as $traderDeal){
            Log::channel('trader_deals_checker')->info('--------------------------------------------------------');
            try {

                $message = __('Check Deal. [id = :id status = :status]', [
                    'id' => $traderDeal->id, 'status' => $traderDeal->status
                ]);
                Log::channel('trader_deals_checker')->info($message);
                $changed = false;

                switch ($traderDeal->status){
                    case TraderDeal::STATUS_CREATE:
                        $changed = $this->checkLiveTime(
                            $traderDeal,
                            TraderDeal::STATUS_CANCELED_BY_REPLY_TIMEOUT,
                            config('app.trader_deals_create_live_time'));
                        break;
                    case TraderDeal::STATUS_TRADER_FUNDED:
                        $changed = $this->checkLiveTime(
                            $traderDeal,
                            TraderDeal::STATUS_CANCELED_TIMEOUT,
                            config('app.trader_deals_funded_live_time')
                        );
                        break;
                }


                if($changed){
                    $message = __('Status of the deals changed to CANCELED. [id = :id status = :status]', [
                        'id' => $traderDeal->id, 'status' => $traderDeal->status]);
                    Log::channel('trader_deals_checker')->info($message);
                    Log::channel('trader_deals_checker')->info('--------------------------------------------------------');
                }else{
                    $message = __('Status of the deals not changed. [id = :id status = :status]', [
                        'id' => $traderDeal->id, 'status' => $traderDeal->status]);
                    Log::channel('trader_deals_checker')->info($message);
                    Log::channel('trader_deals_checker')->info('--------------------------------------------------------');
                }

            } catch (\Exception $exception) {
                $message = __('An error occurred while executing a deals check. [id = :id status = :status]', [
                    'id' => $traderDeal->id, 'status' => $traderDeal->status]);
                Log::channel('trader_deals_checker')->info($message);
                Log::channel('trader_deals_checker')->info($exception->getMessage());
                Log::channel('trader_deals_checker')->info('--------------------------------------------------------');
            }
            $traderDeal->checked_at = now();
            $traderDeal->save();
        }


    }

    private function checkLiveTime($traderDeal, $status, $time){
        if(isset($traderDeal->statusHistory)){
            $statusTime = Carbon::parse($traderDeal->statusHistory->created_at, 'Europe/Stockholm');
            if($statusTime->isBefore(now()->subMinutes($time))){
                $traderDeal->status = $status;
                $traderDeal->save();
                return true;
            }
        }
        return false;
    }
}
