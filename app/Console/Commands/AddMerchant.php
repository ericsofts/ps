<?php

namespace App\Console\Commands;

use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AddMerchant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merchant:add {name} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|iunique:merchants'
        ];

        $data = [
            'name' => $this->argument('name'),
            'email' => $this->argument('email')
        ];

        $validator = Validator::make($data, $rules, []);

        if ($validator->fails()) {
            dd($validator->errors()->getMessages());
        }
        $password = Str::random(8);
        $merchant = Merchant::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($password),
            'status' => 1,
            'token' => Str::random(128),
            'payment_key' => Merchant::generatePaymentKey(),
            'api3_key' => Merchant::generateApi3Key(),
            'api5_key' => Merchant::generateApi5Key(),
            'api6_key' => Merchant::generateApi6Key(),
            'email_verified_at' => now()
        ]);

        MerchantBalance::create([
            'merchant_id' => $merchant->id
        ]);

        //grow
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API1,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API2,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API4,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        //service
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API1,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API2,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API4,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        //agent
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API1,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API2,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API4,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);

        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_commission',
            'value' => config('app.commission.min_commission')
        ]);

        MerchantProperty::create([
            'merchant_id' => $merchant->id,
            'property' => 'min_payment_invoice_amount',
            'value' => config('app.min_payment_invoice_amount')
        ]);

        MerchantProperty::create([
            'merchant_id' => $merchant->id,
            'property' => 'available_coins',
            'value' => config('app.available_merchant_coins')
        ]);

        $this->info('User: ' . $merchant->name);
        $this->info('Email: ' . $merchant->email);
        $this->info('Password: ' . $password);
        $this->info('Token: ' . $merchant->token);
        return 0;
    }
}
