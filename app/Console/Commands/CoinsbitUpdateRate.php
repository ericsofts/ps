<?php

namespace App\Console\Commands;

use App\Models\Property;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use WebSocket\Client;

class CoinsbitUpdateRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coinsbit_update:grow_rate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var mixed
     */
    private $grow_eth;
    /**
     * @var mixed
     */
    private $eth_usd;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $properties = Property::getProperties('grow_token_usd2token');
        $this->grow_eth = $properties['grow_eth'] ?? 0;
        $this->eth_usd = $properties['eth_usd'] ?? 0;
        $start_datetime = now();
        $client = new Client("wss://coinsbit.io/trade_ws");
        $params = ["GROW_ETH", "ETH_USD", "USD_ETH"];
        $client->send('{"method":"price.subscribe","params":["GROW_ETH", "ETH_USD", "USD_ETH"],"id": ' . time() . '}');
        $check = true;
        $this->info('Start '. $start_datetime->toIso8601String());
        while ($check) {
            try {
                $message = $client->receive();
                $m = json_decode($message, true);
                if ($m && !empty($m['method']) && $m['method'] == 'price.update') {
                    if ($m['params'][0] == 'GROW_ETH') {
                        $this->grow_eth = $m['params'][1];
                    }
                    if ($m['params'][0] == 'ETH_USD') {
                        $this->eth_usd = $m['params'][1];
                    }
                    $this->info(now());
                    $this->info($message);
                    $this->updateRate();
                } else {
                    if ($m && !empty($m['params']) && in_array($m['params'][0], $params)) {
                        $this->warn($message);
                    }
                }
            } catch (\WebSocket\ConnectionException $e) {
                Log::channel('coinsbit_update')->info($e->getMessage(). " | " . $e->getCode(), $e->getTrace());
                $client->close();
                $check = false;
            }
            if($start_datetime->addHours(1) < now()){
                $client->close();
                $check = false;
            }
        }
        $this->info('Stop '. now()->toIso8601String());
        return 0;
    }

    private function updateRate()
    {
        $properties = Property::getProperties('grow_token_usd2token');
        $grow_eth = $properties['grow_eth'];
        $eth_usd = $properties['eth_usd'];
        if ($grow_eth != $this->grow_eth || $eth_usd != $this->eth_usd) {
            Property::setProperty('grow_token_usd2token', 0, 'coin_rate', $this->grow_eth * $this->eth_usd);
            Property::setProperty('grow_token_usd2token', 0, 'grow_eth', $this->grow_eth);
            Property::setProperty('grow_token_usd2token', 0, 'eth_usd', $this->eth_usd);
        }
    }
}
