<?php

namespace App\Console\Commands;

use App\Lib\Chatex\ChatexApiLBC;
use App\Models\BalanceQueue;
use App\Models\InputInvoice;
use App\Models\Property;
use App\Models\UsdtAddress;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class UsdtErc20InputInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'usdt_erc20_input_invoice:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $isSleep = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start USDT ERC20 checker');
        while (true) {
            $this->_main();
        }
        return 0;
    }

    private function _main()
    {
        $check_iterval = config('app.usdt_erc20_check_time', 30);
        $datetime = Carbon::now()->addSeconds(-$check_iterval);
        $inputInvoices = InputInvoice::where([
            'payment_system' => InputInvoice::PAYMENT_SYSTEM_USDT
        ])
            ->whereIn('status', [InputInvoice::STATUS_CREATED])
            ->where(function ($query) use ($datetime) {
                $query->where('checked_at', '<=', $datetime)
                    ->orWhereNull('checked_at');
            })
            ->orderBy('id', 'ASC')
            ->with('usdt_address')
            ->limit(1)
            ->get();
        if (!$inputInvoices->count()) {
            if (!$this->isSleep) {
                $this->info('Sleep');
                $this->isSleep = true;
            }
            sleep(config('app.usdt_erc20_check_sleep_time', 30));
            return;
        }

        $this->isSleep = false;
        $this->info('Working');

        $rate = Property::get('coinmarketcap_usdt2usd', 0, 'coin_rate');
        if (empty($rate->property_value)) {
            Log::channel('usdt_erc20_checker')->info('EMPTY_RATE|RATE|', $rate->toArray());
            return;
        }

        foreach ($inputInvoices as $inputInvoice) {
            $this->info(now()->toIso8601String() . '| Input_invoice #' . $inputInvoice->id);
            $res = Http::get(config('app.etherscan_url'), [
                'module' => 'account',
                'action' => 'tokenbalance',
                'contractaddress' => UsdtAddress::CONTRACT_ADDRESS,
                'address' => $inputInvoice->usdt_address->usdt_address,
                'tag' => 'latest',
                'apikey' => config('app.etherscan_api_key')
            ]);
            $data = $res->json();
            Log::channel('usdt_erc20_checker')->info('INPUT_INVOICE_ID|' . $inputInvoice->id, $data);
            if (!empty($data['result']) && !empty($data['status'])) {
                $total_received_funds = round($data['result'] / 1000000, 6);
                if ($total_received_funds) {
                    $amount = floor($rate->property_value * $total_received_funds * 100) / 100;
                    $inputInvoice->status = InputInvoice::STATUS_PAYED;
                    $inputInvoice->payed = now();
                    $inputInvoice->amount = $amount;
                    $inputInvoice->ps_amount = $total_received_funds;
                    $inputInvoice->usdt_address->total_received_funds = $total_received_funds;
                    $inputInvoice->usdt_address->status = UsdtAddress::STATUS_USED;

                }
            } else {
                $expired_at = $inputInvoice->created_at->addHours(config('app.usdt_erc20_input_invoice_expiry'));
                if ($expired_at < now()) {
                    $inputInvoice->status = InputInvoice::STATUS_CANCELED;
                    Log::channel('usdt_erc20_checker')->info('CANCEL|INPUT_INVOICE_ID|' . $inputInvoice->id);
                }
            }
            $inputInvoice->checked_at = now();
            $inputInvoice->push();
        }
    }
}
