<?php

namespace App\Console\Commands;

use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\MerchantInputInvoice;
use Illuminate\Console\Command;

class AddMerchantInputInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merchant_input_invoice:add {id} {amount} {comment}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('id');
        $amount = $this->argument('amount');
        $comment = $this->argument('comment');

        $merchant = Merchant::where(['id' => $id])->first();

        if(!$merchant){
            $this->info(__('Merchant ":id" not found', ['id' => $id]));
            return;
        }

        $invoice = MerchantInputInvoice::create([
            'merchant_id' => $id,
            'payment_system' => MerchantInputInvoice::PAYMENT_SYSTEM_MANUALLY,
            'amount' => $amount,
            'payed' => now(),
            'comment' => $comment,
            'status' => MerchantInputInvoice::STATUS_PAYED
        ]);

        if($invoice){
            $this->addAccountingQueue(
                $invoice,
                AccountingEntrie::USER_TYPE_MERCHANT,
                $invoice->merchant_id,
                $invoice->amount,
                AccountingEntrie::TYPE_CREDIT,
                AccountingEntrie::INVOICE_TYPE_MERCHANT_INPUT_INVOICE
            );

            $this->info(__('Done'));
        }

        return 0;
    }

    private function addAccountingQueue($invoice, $user_type, $user_id, $amount, $type, $invoice_type)
    {
        $merchantBalance = MerchantBalance::firstOrCreate(['merchant_id' => $invoice->merchant_id]);
        $balance_after = $merchantBalance->amount + $amount;
        $accounting = AccountingQueue::create([
            'type' => $type,
            'user_type' => $user_type,
            'user_id' => $user_id,
            'amount' => $amount,
            'balance_after' => $balance_after,
            'invoice_id' => $invoice->id,
            'invoice_type' => $invoice_type,
            'status' => AccountingEntrie::STATUS_CREATE,
            'created_at' => $invoice->created_at,
            'updated_at' => $invoice->updated_at
        ]);
    }

}
