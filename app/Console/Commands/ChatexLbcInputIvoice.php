<?php

namespace App\Console\Commands;

use App\Lib\Chatex\ChatexApiLBC;
use App\Models\BalanceQueue;
use App\Models\InputInvoice;
use App\Models\Property;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class ChatexLbcInputIvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chatex_lbc_input_invoice:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $isSleep = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start ChatexLbc checker');
        while (true) {
            $this->_main();
        }
        return 0;
    }

    private function _main()
    {
        $check_iterval = config('app.chatex_lbc_check_time');
        $datetime = Carbon::now()->addSeconds(-$check_iterval);
        $inputInvoices = InputInvoice::where([
            'payment_system' => InputInvoice::PAYMENT_SYSTEM_CHATEX_LBC
        ])
            ->whereIn('status', [InputInvoice::STATUS_CREATED, InputInvoice::STATUS_USER_CONFIRMED])
            ->where(function ($query) use ($datetime) {
                $query->where('checked_at', '<=', $datetime)
                    ->orWhereNull('checked_at');
            })
            ->orderBy('id', 'ASC')
            ->limit(10)
            ->get();
        if (!$inputInvoices->count()) {
            if (!$this->isSleep) {
                $this->info('Sleep');
                $this->isSleep = true;
            }
            sleep(config('app.chatex_lbc_check_sleep_time'));
            return;
        }

        $this->isSleep = false;
        $this->info('Working');
        //$paymentIds = $inputInvoices->pluck('payment_id');
        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        //$contact_res = $chatexLBC->contactInfo(implode(';', $paymentIds->toArray()));
        foreach ($inputInvoices as $inputInvoice) {
            $this->info(now()->toIso8601String() . '| Input_invoice #' . $inputInvoice->id);
            $contact_res = $chatexLBC->contactInfo($inputInvoice->payment_id);
            if (!empty($contact_res['data'])) {
                Log::channel('chatex_lbc_checker')->info('CONTACT_INFO|INPUT_INVOICE_ID|' . $inputInvoice->id, $contact_res);
                if (!empty($contact_res['data']['canceled_at'])) {
                    $inputInvoice->status = InputInvoice::STATUS_CANCELED;
                } elseif (!empty($contact_res['data']['released_at'])) {
                    $inputInvoice->status = InputInvoice::STATUS_PAYED;
                    $inputInvoice->payed = now();
                    $description = "Account Top-Up - Bank Card";
                    $result = BalanceQueue::addToBalanceQueue(
                        $inputInvoice->user_id,
                        $inputInvoice->amount,
                        BalanceQueue::TYPE_CREDIT,
                        $description,
                        $inputInvoice->id
                    );
                }
            } else {
                Log::channel('chatex_lbc_checker')->info('CONTACT_INFO|ERROR|INPUT_INVOICE_ID|' . $inputInvoice->id, $contact_res);
            }
            $inputInvoice->checked_at = now();
            $inputInvoice->save();
        }
    }
}
