<?php

namespace App\Console\Commands;

use App\Models\Property;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use SimpleXMLElement;

class CbrUpdateRateRub2Usd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cbr_update_rate:usd2rub';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::get('http://www.cbr.ru/scripts/XML_dynamic.asp', [
            'date_req1' => now()->format('d/m/Y'),
            'date_req2' => now()->format('d/m/Y'),
            'VAL_NM_RQ' => 'R01235'
        ]);
        try {
            $xml = new SimpleXMLElement($response->body());
            if(!empty($xml->Record->Value)){
                $rate = floatval(str_replace(',', '.', str_replace('.', '', $xml->Record->Value)));
                if($rate > 0){
                    $this->info($rate);
                    Property::setProperty('currency_rate', 0, 'rub2usd', round(1/$rate, 4));
                    Property::setProperty('currency_rate', 0, 'usd2rub', round($rate, 4));
                }
            }
        } catch (Exception $e) {
            Log::channel('cbr_update')->info($e->getMessage() . " | " . $e->getCode(), $e->getTrace());
        }
        return 0;
    }
}
