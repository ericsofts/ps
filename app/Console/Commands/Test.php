<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use WebSocket\Client;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $client = new Client("wss://coinsbit.io/trade_ws");
        $params = ["GROW_ETH", "ETH_USD", "USD_ETH"];
        $client->send('{"method":"price.subscribe","params":["GROW_ETH", "ETH_USD", "USD_ETH"],"id": 1}');

        while (true) {
            try {
                $message = $client->receive();
                $m = json_decode($message, true);
                if($m && !empty($m['method']) && $m['method'] == 'price.update'){
                    $this->info(now());
                    $this->info($message);
                }else{
                    if($m && !empty($m['params']) && in_array($m['params'][0], $params)){
                        $this->warn($message);
                    }
                }
            } catch (\WebSocket\ConnectionException $e) {
            }
        }
        $client->close();
        return 0;
    }
}
