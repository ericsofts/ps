<?php

namespace App\Console\Commands;

use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use Illuminate\Console\Command;

class MerchantWithdrawalInvoiceSetStatusPayed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merchant_withdrawal_invoice:set_status_payed {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('id');
        $invoice = MerchantWithdrawalInvoice::where(['id' => $id])->first();
        if(!$invoice){
            $this->info(__('Withdrawal invoice ":id" not found', ['id' => $id]));
            return;
        }
        $this->info('Current status: '. $invoice->status . ' - ' . payment_invoice_status($invoice->status));
        if($invoice->status == MerchantWithdrawalInvoice::STATUS_PAYED){
            return;
        }
        if ($this->confirm('Do you wish to continue?')) {
            $invoice->status = MerchantWithdrawalInvoice::STATUS_PAYED;
            $invoice->save();
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                3,
                InvoicesHistory::getStatus(1),
                InvoicesHistory::getSource(5),
                InvoicesHistory::getSource(3),
                NULL,
                'Change Status to Payed by Admin'
            );
            $this->info(__('Status changed'));
        }

        return 0;
    }
}
