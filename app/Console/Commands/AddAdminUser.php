<?php

namespace App\Console\Commands;

use App\Models\AdminUser;
use App\Models\Merchant;
use App\Models\MerchantProperty;
use App\Models\SdUser;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AddAdminUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin_user:add {name} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|iunique:admin_users'
        ];

        $data = [
            'name' => $this->argument('name'),
            'email' => $this->argument('email')
        ];

        $validator = Validator::make($data, $rules, []);

        if ($validator->fails()) {
            dd($validator->errors()->getMessages());
        }
        $password = Str::random(8);
        $user = AdminUser::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($password),
            'email_verified_at' => now(),
            'status' => 1
        ]);

        $this->info('User: ' . $user->name);
        $this->info('Email: ' . $user->email);
        $this->info('Password: ' . $password);
        return 0;
    }
}
