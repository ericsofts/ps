<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AddEmailDomainToBlock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blocked_email_domains:add_url {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = $this->argument('url');
        $content = file_get_contents($url);
        $domains = explode("\n", $content);
        if(!empty($domains)){
            foreach ($domains as $domain){
                $domain = strtolower($domain);
                $d = DB::table('blocked_email_domains')->where('domain', '=', $domain)->first();
                if(!$d){
                    DB::table('blocked_email_domains')->insert([
                        'domain' => $domain
                    ]);
                }
            }
        }
        return 0;
    }
}
