<?php

namespace App\Console\Commands;

use App\Jobs\TelegramSendNotification;
use App\Lib\Chatex\ChatexApiLBC;
use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\PaymentInvoice;
use App\Models\Property;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class ChatexLbcPaymentInvoiceApi3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chatex_lbc_payment_invoice_api3:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $isSleep = false;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start ChatexLbc checker (Payment invoice APIv3)');
        while (true) {
            $this->_main();
        }
        return 0;
    }

    private function _main()
    {
        $check_iterval = config('app.chatex_lbc_check_api3_time');
        $datetime = Carbon::now()->addSeconds(-$check_iterval);
        $invoices = PaymentInvoice::whereIn('api_type', [
             PaymentInvoice::API_TYPE_3,
            PaymentInvoice::API_TYPE_5,
            PaymentInvoice::API_TYPE_0,
        ])
            ->whereIn('status', [PaymentInvoice::STATUS_CREATED, PaymentInvoice::STATUS_USER_CONFIRMED])
            ->where(function ($query) use ($datetime) {
                $query->where('checked_at', '<=', $datetime)
                    ->orWhereNull('checked_at');
            })
            ->orderBy('id', 'ASC')
            ->limit(10)
            ->get();
        if (!$invoices->count()) {
            if (!$this->isSleep) {
                $this->info('Sleep');
                $this->isSleep = true;
            }
            sleep(config('app.chatex_lbc_check_api3_sleep_time'));
            return;
        }


        $this->isSleep = false;
        $this->info('Working');
        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $invoiceTimeOut = Property::getProperty('invoice', 0, 'payment_invoice_cancel_timeout', config('app.payment_invoice_cancel_timeout'));
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        foreach ($invoices as $invoice) {
            $this->info(now()->toIso8601String(). '| Payment_invoice #'.$invoice->id);
            if($invoice->payment_id){
                $contact_res = $chatexLBC->contactInfo($invoice->payment_id);
                if (!empty($contact_res['data'])) {
                    Log::channel('api3_chatex_lbc_checker')->info('CONTACT_INFO|PAYMENT_INVOICE_ID|'.$invoice->id, ['contact_res' => $contact_res]);
                    if (!empty($contact_res['data']['canceled_at'])) {
                        $invoice->status = PaymentInvoice::STATUS_CANCELED;
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                            3,
                            InvoicesHistory::getStatus(0),
                            InvoicesHistory::getSource(3),
                            InvoicesHistory::getSource(2),
                            json_encode($contact_res['data']),
                            'Canceled by Chatex'
                        );
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                            4,
                            InvoicesHistory::getStatus(1),
                            InvoicesHistory::getSource(2),
                            InvoicesHistory::getSource(3),
                            json_encode($contact_res['data']),
                            'Apply Cancel by Chatex'
                        );
                    } elseif (!empty($contact_res['data']['released_at'])) {
                        $invoice->status = PaymentInvoice::STATUS_PAYED;
                        $invoice->payed = now();
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                            3,
                            InvoicesHistory::getStatus(1),
                            InvoicesHistory::getSource(3),
                            InvoicesHistory::getSource(2),
                            json_encode($contact_res['data']),
                            'Payed'
                        );
                    }elseif (!empty($contact_res['data']['disputed_at'])){
                        $record = InvoicesHistory::where([
                            'invoice_id' => $invoice->id,
                            'invoice_type' => AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                            'comment' => 'Arbitrage Invoice'
                        ])->first();
                        if(!$record){
                            InvoicesHistory::addHistoryRecord(
                                __METHOD__,
                                $invoice->id,
                                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                                6,
                                InvoicesHistory::getStatus(0),
                                InvoicesHistory::getSource(3),
                                InvoicesHistory::getSource(2),
                                json_encode($contact_res['data']),
                                'Arbitrage Invoice'
                            );
                        }
                    }
                }else{
                    Log::channel('api3_chatex_lbc_checker')->info('CONTACT_INFO|ERROR|PAYMENT_INVOICE_ID|'.$invoice->id, ['contact_res' => $contact_res]);
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                        3,
                        InvoicesHistory::getStatus(0),
                        InvoicesHistory::getSource(3),
                        InvoicesHistory::getSource(2),
                        json_encode($contact_res),
                        'Chatex response incorrect'
                    );
                }
            }elseif($invoice->created_at < date('Y-m-d H:i:s',strtotime("-".$invoiceTimeOut." hour"))){
                $invoice->status = PaymentInvoice::STATUS_CANCELED_BY_TIMEOUT;
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                    3,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(3),
                    InvoicesHistory::getSource(3),
                    NULL,
                    'Invoice timeout'
                );
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                    4,
                    InvoicesHistory::getStatus(1),
                    InvoicesHistory::getSource(3),
                    InvoicesHistory::getSource(3),
                    NULL,
                    'Apply Cancel by timeout'
                );
            }
            $invoice->checked_at = now();
            $invoice->save();
        }
    }
}
