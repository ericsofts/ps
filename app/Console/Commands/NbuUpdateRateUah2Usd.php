<?php

namespace App\Console\Commands;

use App\Models\Property;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class NbuUpdateRateUah2Usd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nbu_update_rate:uah2usd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::get('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange', [
            'date' => now()->format('Ymd'),
            'valcode' => 'USD',
            'json'
        ]);

        if($response->ok()){
            $rate = $response->json('0.rate');
            if($rate){
                Property::setProperty('currency_rate', 0, 'uah2usd', round(1/$rate, 4));
                Property::setProperty('currency_rate', 0, 'usd2uah', round($rate, 4));
            }
        }
        Log::channel('nbu_update')->info('UAH2USD', ['res' => $response->json(), 'status' => $response->status()]);
        return 0;
    }
}
