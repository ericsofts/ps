<?php

namespace App\Console\Commands;

use App\Models\Balance;
use App\Models\BalanceHistory;
use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use Illuminate\Console\Command;
use App\Models\BalanceQueue as BalanceQueueModels;
use function React\Promise\all;

class RecalculateUserBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user_balance:recalculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->rewriteCurrency();
        $this->createNewUserBalance();
        $this->recalculateUserBalanceHistory();
        return 0;
    }

    protected function rewriteCurrency(){
        BalanceQueueModels::where('currency', 1)
            ->update(['currency' => BalanceQueueModels::CURRENCY_GROW_TOKEN]);
        BalanceQueueModels::where('currency', 0)
            ->update(['currency' => BalanceQueueModels::CURRENCY_USD]);
        BalanceHistory::where('currency', 1)
            ->update(['currency' => BalanceHistory::CURRENCY_GROW_TOKEN]);
        BalanceHistory::where('currency', 0)
            ->update(['currency' => BalanceHistory::CURRENCY_USD]);
    }

    protected function createNewUserBalance(){
        $userBalances = Balance::all();
        foreach ($userBalances as $balance){
            Balance::create([
                'user_id' => $balance->user_id,
                'amount' => $balance->grow_token_amount,
                'currency' => BalanceQueueModels::CURRENCY_GROW_TOKEN
            ]);
            $balance->currency = BalanceQueueModels::CURRENCY_USD;
            $balance->grow_token_amount = 0;
            $balance->save();

        }
    }

    protected function recalculateUserBalanceHistory(){
        $userBalances = Balance::all();
        foreach ($userBalances as $balance){
            $total = 0;
            $records = BalanceHistory::where('currency', $balance->currency)
                ->where('user_id', $balance->user_id)->orderBy('id', 'asc')->get();
            if(count($records)){
                foreach ($records as $record){
                    $total += $record->amount;
                    $record->balance = $total;
                    $record->balance_id = $balance->id;
                    $record->save();
                }
            }
        }
    }
}
