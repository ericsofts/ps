<?php

namespace App\Console\Commands;

use App\Lib\Chatex\ChatexApi;
use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\MerchantWithdrawalInvoiceAttachment;
use App\Models\Property;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use App\Services\ContractManager;
use App\Traits\Contract;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class CheckerWithdrawalInvoice extends Command
{

    use Contract;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'withdrawal_invoice:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $isSleep = false;
    protected $contractManager = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ContractManager $contractManager)
    {
        parent::__construct();
        $this->contractManager = $contractManager;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start Withdrawal Invoice Checker');
        while (true) {
            $this->_main();
        }
        return 0;
    }

    private function _main()
    {
        $check_iterval = config('app.invoice_check_time');
        $datetime = Carbon::now()->addSeconds(-$check_iterval);
        $invoice = MerchantWithdrawalInvoice::whereIn('api_type', [
            MerchantWithdrawalInvoice::API_TYPE_5,
            MerchantWithdrawalInvoice::API_TYPE_3,
            MerchantWithdrawalInvoice::API_TYPE_0
        ])
            ->whereIn('status', [
                MerchantWithdrawalInvoice::STATUS_CREATED,
                MerchantWithdrawalInvoice::STATUS_IN_PROCESS,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED,
                MerchantWithdrawalInvoice::STATUS_USER_SELECTED,
            ])
            ->where(function ($query) use ($datetime) {
                $query->where('checked_at', '<=', $datetime)
                    ->orWhereNull('checked_at');
            })
            ->orderBy('id', 'ASC')
            ->with('attachment')
            ->first();
        if (!$invoice) {
            if (!$this->isSleep) {
                $this->info('Sleep');
                $this->isSleep = true;
            }
            sleep(config('app.invoice_check_sleep_time'));
            return;
        }

        $this->isSleep = false;
        $this->info('Working');

        $invoiceTimeOut = Property::getProperty('invoice', 0, 'withdrawal_invoice_cancel_timeout', config('app.withdrawal_invoice_cancel_timeout'));
        $this->info(now()->toIso8601String() . '| Withdrawal_invoice #' . $invoice->id);
        if ($invoice->payment_id) {
            $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
            Log::channel('merchant_withdrawal_invoices_checker')->info('CONTACT_INFO|PAYMENT_INVOICE_ID|' . $invoice->id, ['contract' => $contractInfo]);
            if ($contractInfo) {
                $isContractCancel = $this->isContractCancel($contractInfo, $invoice);
                $isClosed = $this->isContractClosed($contractInfo, $invoice);
                if(!$isContractCancel && $isClosed){
                    $invoice->status = MerchantWithdrawalInvoice::STATUS_CANCELED_BY_TIMEOUT;
                }
                if (!$isContractCancel && !$isClosed) {
                    $isContractPaymentCompleted = $this->isContractPaymentCompleted($contractInfo, $invoice);
                    if ($isContractPaymentCompleted
                        && in_array($invoice->status, [MerchantWithdrawalInvoice::STATUS_CREATED, MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED, MerchantWithdrawalInvoice::STATUS_IN_PROCESS, MerchantWithdrawalInvoice::STATUS_USER_SELECTED])
                    ) {
                        $invoice->status = MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT;
                        InvoicesHistory::addHistoryRecord(
                            __METHOD__,
                            $invoice->id,
                            AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                            6,
                            InvoicesHistory::getStatus(1),
                            InvoicesHistory::getSource(3),
                            InvoicesHistory::getSource(2),
                            json_encode($contractInfo),
                            'Confirm Trader Payment'
                        );
                    }
                }
                if (!isset($isContractPaymentCompleted) || !$isContractPaymentCompleted) {
                    $this->isContractDisputed($contractInfo, $invoice);
                }
                if ($invoice->status == MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT) {
                    MerchantWithdrawalInvoiceAttachment::addAttachments($this->contractManager, $invoice);

                    if (isset($invoice->attachment->id)) {
                        $serviceProvider = ServiceProvider::where('id', $invoice->service_provider_id)->first();
                        if ($serviceProvider && ($serviceProvider->type == ChatexApi::OBJECT_NAME)) {
                            $info = json_decode($invoice->addition_info, JSON_FORCE_OBJECT);
                            $priorityTraders = json_decode(ServiceProvidersProperty::getProperty($serviceProvider->type, $serviceProvider->id, 'withdrawal_priority_traders', '[]'), true);
                            $priority_banks_traders = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, $serviceProvider->id, 'priority_banks_traders', '[]'), true);
                            if (
                                (count($priorityTraders) && isset($info['ad']['name']) && in_array($info['ad']['name'], $priorityTraders)) ||
                                ($priority_banks_traders && !empty($priority_banks_traders[$info['ad']['bank_name']]) && in_array($info['ad']['name'], $priority_banks_traders[$info['ad']['bank_name']]))
                            ) {

                                $lastOfImage = Carbon::parse($invoice->attachment->created_at);
                                $lastOfStatusChange = Carbon::parse($contractInfo['payment_completed_at']);
                                $checkedTime = $lastOfStatusChange->gt($lastOfImage) ? $lastOfStatusChange : $lastOfImage;

                                if ($checkedTime->isBefore(now()->subMinutes(config('app.chatex_autocomplete_live_time')))) {
                                    $res = $this->contractManager->contactReleaseByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                                    if ($res) {
                                        $invoice->status = MerchantWithdrawalInvoice::STATUS_PAYED;
                                        $invoice->payed = now();
                                        InvoicesHistory::addHistoryRecord(
                                            __METHOD__,
                                            $invoice->id,
                                            AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                                            6,
                                            InvoicesHistory::getStatus(1),
                                            InvoicesHistory::getSource(0),
                                            InvoicesHistory::getSource(2),
                                            json_encode($res),
                                            'Payed (auto-priority-trader)'
                                        );
                                        $message = __('Invoice Complete by Auto Complete. [id = :id status = :status]', [
                                            'id' => $invoice->id, 'status' => $invoice->status
                                        ]);
                                        Log::channel('merchant_withdrawal_invoices_checker')->info($message);

                                    } else {
                                        $message = __('Invoice Complete by Auto Complete Fail. [id = :id status = :status]', [
                                            'id' => $invoice->id, 'status' => $invoice->status
                                        ]);
                                        Log::channel('merchant_withdrawal_invoices_checker')->info($message);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                Log::channel('merchant_withdrawal_invoices_checker')->info('CONTACT_INFO|ERROR|PAYMENT_INVOICE_ID|' . $invoice->id, ['contract' => $contractInfo]);
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::getInvoiceTypeByInstance($invoice),
                    3,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(3),
                    InvoicesHistory::getSource(2),
                    json_encode($contractInfo),
                    'Service Provider response incorrect'
                );
            }

        } elseif ($invoice->created_at < date('Y-m-d H:i:s', strtotime("-" . $invoiceTimeOut . " hour"))) {
            $invoice->status = MerchantWithdrawalInvoice::STATUS_CANCELED_BY_TIMEOUT;
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                6,
                InvoicesHistory::getStatus(0),
                InvoicesHistory::getSource(3),
                InvoicesHistory::getSource(2),
                'Invoice timeout'
            );
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                4,
                InvoicesHistory::getStatus(1),
                InvoicesHistory::getSource(2),
                InvoicesHistory::getSource(3),
                'Apply Cancel by timeout'
            );
        }
        $invoice->checked_at = now();
        $invoice->save();

    }
}
