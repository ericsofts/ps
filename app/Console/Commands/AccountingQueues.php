<?php

namespace App\Console\Commands;

use App\Jobs\TelegramSendNotification;
use App\Models\AccountingEntrie;
use App\Models\AccountingQueue as AccountingQueueModel;
use App\Models\AgentBalance;
use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use App\Models\ServiceProvidersBalance;
use App\Models\SystemBalance;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AccountingQueues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounting_queue:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check accounting queue';

    protected $isSleep = false;

    protected $accountingQueue;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $this->info('Start AccountingQueue');
        while (true) {
            $this->_main();
        }
    }

    private function _main()
    {


        $this->accountingQueue = $accountingQueue = AccountingQueueModel::getAccountingQueueWaiting();

        if (!$this->accountingQueue) {
            sleep(config('app.accounting_queue_sleep_time'));
            if (!$this->isSleep) {
                $this->info('Sleep');
                $this->isSleep = true;
            }
            return;
        }
        $this->isSleep = false;
        $this->info('Working');
        $this->info(now()->toIso8601String() . '| accountingQueue #' . $this->accountingQueue->id);

        $this->logMessage('Start executing a request from the queue');

        try {
            DB::transaction(function () use ($accountingQueue) {

                $this->logMessage('Opening a transaction');
                $this->changeStatus(AccountingQueueModel::STATUS_PROCESSING, 'STATUS_PROCESSING');

                if ($accountingQueue->type == AccountingEntrie::TYPE_DEBIT) {
                    $amount = $accountingQueue->amount * (-1);
                } else {
                    $amount = $accountingQueue->amount;
                }

                switch ($accountingQueue->user_type) {
                    case AccountingEntrie::USER_TYPE_SERVICE:
                        $balance = ServiceProvidersBalance::firstOrCreate(
                            ['provider_id' => $accountingQueue->user_id]
                        );
                        $newBalanceAfter = $this->balanceUpdate($balance, $amount, 'service');
                        break;

                    case AccountingEntrie::USER_TYPE_AGENT:
                        $balance = AgentBalance::firstOrCreate(
                            ['agent_id' => $accountingQueue->user_id]
                        );
                        $newBalanceAfter = $this->balanceUpdate($balance, $amount, 'agent');
                        break;

                    case AccountingEntrie::USER_TYPE_GROW:
                        $balance = SystemBalance::firstOrCreate(
                            ['name' => SystemBalance::NAME_GROW]
                        );
                        $newBalanceAfter = $this->balanceUpdate($balance, $amount, 'grow');
                        break;

                    case AccountingEntrie::USER_TYPE_MERCHANT:
                        $balance = MerchantBalance::firstOrCreate(
                            ['merchant_id' => $accountingQueue->user_id]
                        );
                        $newBalanceAfter = $this->balanceUpdate($balance, $amount, 'merchant');
                        $this->sendTelegramNotify();
                        break;
                }

                $balanceHistoryData = $this->getBalanceHistoryData($this->accountingQueue->attributesToArray(), $newBalanceAfter);
                $balanceHistory = AccountingEntrie::create($balanceHistoryData);
                $this->logMessage('Update balance history.');

                $this->changeStatus(AccountingQueueModel::STATUS_DONE, 'STATUS_DONE');


            }, 3);
        } catch (\Exception $exception) {

            $this->logMessage('An error occurred while executing a request from the queue.');
            $this->logMessage($exception);

            $this->changeStatus(AccountingQueueModel::STATUS_FAILED, 'STATUS_FAILED');

        }
    }

    private function logMessage($message)
    {
        $message = $message . sprintf(' [id = %s]', $this->accountingQueue->id);
        Log::channel('accounting_queue')->info($message);
    }

    private function changeStatus($statusValue, $statusName)
    {
        $this->accountingQueue->status = $statusValue;
        $this->accountingQueue->save();
        $this->logMessage(sprintf('Status of the request changed to %s(%s).', $statusName, $statusValue));
    }

    private function balanceUpdate($balance, $amount, $name)
    {
        $balance->amount += $amount;
        $balance->save();
        $this->logMessage(sprintf('Update %s account.', $name));
        return $balance->amount;
    }

    private function sendTelegramNotify()
    {

        $datetime = now(config('app.default.timezone'))->format('d.m.Y H:i:s');
        $merchant = Merchant::where(['id' => $this->accountingQueue->user_id])->first();
        $channel = $this->getBaseInvoiceApi();
        $invoice = $this->getInvoice();
        if (!$invoice) {
            return;
        }
        Log::channel('accounting_queue')->info('INVOICE', ['i' => $invoice]);
        $sp_name = $invoice->service_provider->name ?? null;
        switch ($this->accountingQueue->invoice_type) {

            case AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE:
                $addition_info = json_decode($invoice->addition_info, true);
                $trader = $addition_info['ad']['name'] ?? null;
                $bank = $addition_info['ad']['bank_name'] ?? null;
                if($invoice->status == PaymentInvoice::STATUS_CANCELED_BY_REVERT){

                }
                $text = <<<EOT
                    Ввод (pi): $datetime
                    Мерчант: #{$this->accountingQueue->user_id} {$merchant->name}
                    Invoice: {$this->accountingQueue->invoice_id}
                    Сумма: {$invoice->amount}
                    Fiat amount: {$invoice->fiat_amount}
                    Fiat currency: {$invoice->fiat_currency}
                    Bank: {$bank}
                    Trader: {$trader}
                    Канал: {$channel}
                    SP: {$sp_name}
                    EOT;
                break;

            case AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL:

                $addition_info = json_decode($invoice->addition_info, true);
                $trader = $addition_info['ad']['name'] ?? null;
                $bank = $addition_info['ad']['bank_name'] ?? null;
                $text = <<<EOT
                    Вывод (mwi): $datetime
                    Мерчант: #{$this->accountingQueue->user_id} {$merchant->name}
                    Invoice: {$this->accountingQueue->invoice_id}
                    Сумма: {$invoice->amount}
                    Fiat amount: {$invoice->fiat_amount}
                    Fiat currency: {$invoice->fiat_currency}
                    Bank: {$bank}
                    Trader: {$trader}
                    Канал: {$channel}
                    SP: {$sp_name}
                    EOT;
                break;

            default:
                $channel = 'none';
                $text = <<<EOT
                    Вывод: $datetime
                    Мерчант: #{$this->accountingQueue->user_id} {$merchant->name}
                    Invoice: {$this->accountingQueue->invoice_id}
                    Сумма: {$invoice->amount}
                    Канал: {$channel}
                    EOT;
                break;
        }
        try {
            TelegramSendNotification::dispatch($text);
        } catch (\Exception $exception) {
            $this->logMessage(sprintf('%s Can not send telegram notification %s merchant-id, %s invoice-id, %s type. Accounting Queue.', $datetime, $merchant->id, $invoice->id, $channel));
        }

    }

    private function getBalanceHistoryData($data, $newBalanceAfter)
    {
        unset($data['id']);
        unset($data['created_at']);
        unset($data['updated_at']);
        $data['balance_after'] = $newBalanceAfter;
        $data['status'] = AccountingEntrie::STATUS_DONE;
        return $data;
    }

    private function getBaseInvoiceApi()
    {
        $type = 'none';
        switch ($this->accountingQueue->invoice_type) {
            case AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE:
                $instance = PaymentInvoice::where(['id' => $this->accountingQueue->invoice_id])->first();
                $type = $instance->api_type;
                break;
            case AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL:
                $instance = MerchantWithdrawalInvoice::where(['id' => $this->accountingQueue->invoice_id])->first();
                $type = $instance->api_type;
                break;
        }
        return $type;
    }

    private function getInvoice()
    {
        switch ($this->accountingQueue->invoice_type) {
            case AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE:
                $invoice = PaymentInvoice::where(['id' => $this->accountingQueue->invoice_id])->with(['service_provider'])->first();
                break;
            case AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL:
                $invoice = MerchantWithdrawalInvoice::where(['id' => $this->accountingQueue->invoice_id])->with(['service_provider'])->first();
                break;
            default:
                $invoice = null;
        }
        return $invoice;
    }

}
