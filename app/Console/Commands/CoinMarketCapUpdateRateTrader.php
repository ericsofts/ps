<?php

namespace App\Console\Commands;

use App\Models\TraderRate;
use App\Models\TraderRateSource;
use App\Models\CoinmarketcapKeys;
use App\Models\Property;
use CoinMarketCap\Features\Tools;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class CoinMarketCapUpdateRateTrader extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CoinMarketCapTrader:update_rate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update coin rate from CoinMarketCap for Trader';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->updateRate();
        return 0;
    }

    public function updateRate()
    {
        $cmcRow = TraderRateSource::where('name', 'coinmarketcap')->first();
        $rows = TraderRate::where('rate_source_id', $cmcRow->id)->with(['fiat', 'currency'])->orderBy('id', 'desc')->get();
        foreach ($rows as $i => $row) {
            try {
                $currency = preg_replace('/[^A-Z]/', '', $row->currency->asset);
                $fiat = strtoupper($row->fiat->asset);

                if (preg_match('/USDT/', $currency)) {
                    $currency = 'USDT';
                }

                $key = CoinmarketcapKeys::orderBy('updated_at', 'asc')->orderBy('uses', 'asc')->first();

                if ($fiat == 'TRY') {
                    $fiat = 2810;
                    $data = [
                        'amount' => 1,
                        'symbol' => $currency,
                        'convert_id' => $fiat
                    ];
                } else {
                    $data = [
                        'amount' => 1,
                        'symbol' => $currency,
                        'convert' => $fiat
                    ];
                }
                $response = Http::withHeaders([
                    'X-CMC_PRO_API_KEY' => $key->apikey,
                    'Accept' => 'application/json'
                ])
                    ->get('https://pro-api.coinmarketcap.com/v1/tools/price-conversion', $data);

                $key->uses++;
                $key->save();

                if ($response->ok()) {
                    $res = $response->json();

                    $rate = $res['data']['quote'][$fiat]['price'] ?? false;
                    if ($rate) {
                        $row->rate = $rate;
                        $row->save();

                        if ($fiat == 'INR') {
                            Property::setProperty('currency_rate', 0, 'inr2usd', round(1/$rate, 8));
                            Property::setProperty('currency_rate', 0, 'usd2inr', round($rate, 2));
                        }

                        if ($fiat == 2810) {
                            Property::setProperty('currency_rate', 0, 'try2usd', round(1/$rate, 8));
                            Property::setProperty('currency_rate', 0, 'usd2try', round($rate, 2));
                        }
                    } else {
                        Log::error('Курс CoinMarketCapTrader. пустой', $data);
                    }
                } else {
                    Log::error('Не удалось обновить курс CoinMarketCapTrader.', $data);
                }
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                //$this->info($exception->getMessage());
            }
        }
    }
}
