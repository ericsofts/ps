<?php

namespace App\Console\Commands;

use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\PaymentInvoice;
use App\Models\Property;
use App\Services\ContractManager;
use App\Traits\Contract;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class CheckerPaymentInvoice extends Command
{
    use Contract;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment_invoice:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $isSleep = false;
    protected $contractManager = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ContractManager $contractManager)
    {
        parent::__construct();
        $this->contractManager = $contractManager;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start Payment Invoice Checker');
        while (true) {
            $this->_main();
        }
        return 0;
    }

    private function _main()
    {
        $check_iterval = config('app.invoice_check_time');
        $datetime = Carbon::now()->addSeconds(-$check_iterval);
        $invoices = PaymentInvoice::whereIn('api_type', [
             PaymentInvoice::API_TYPE_3,
            PaymentInvoice::API_TYPE_5,
            PaymentInvoice::API_TYPE_0,
            PaymentInvoice::API_TYPE_6,
        ])
            ->whereIn('status', [
                PaymentInvoice::STATUS_CREATED,
                PaymentInvoice::STATUS_USER_CONFIRMED,
                PaymentInvoice::STATUS_TRADER_CONFIRMED,
                PaymentInvoice::STATUS_USER_SELECTED])
            ->where(function ($query) use ($datetime) {
                $query->where('checked_at', '<=', $datetime)
                    ->orWhereNull('checked_at');
            })
            ->orderBy('id', 'ASC')
            ->limit(1)
            ->get();
        if (!$invoices->count()) {
            if (!$this->isSleep) {
                $this->info('Sleep');
                $this->isSleep = true;
            }
            sleep(config('app.invoice_check_sleep_time'));
            return;
        }


        $this->isSleep = false;
        $this->info('Working');

        $invoiceTimeOut = Property::getProperty('invoice', 0, 'payment_invoice_cancel_timeout', config('app.payment_invoice_cancel_timeout'));

        foreach ($invoices as $invoice) {
            $this->info(now()->toIso8601String(). '| Payment_invoice #'.$invoice->id);
            if($invoice->payment_id){
                $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                Log::channel('payment_invoices_checker')->info('CONTACT_INFO|PAYMENT_INVOICE_ID|'.$invoice->id, ['contract' => $contractInfo]);
                if($contractInfo){
                    $isContractCancel = $this->isContractCancel($contractInfo, $invoice);
                    $isClosed = $this->isContractClosed($contractInfo, $invoice);
                    if(!$isContractCancel && $isClosed){
                        $invoice->status = PaymentInvoice::STATUS_CANCELED_BY_TIMEOUT;
                    }
                    if(!$isContractCancel && !$isClosed){
                        $isContractComplete = $this->isContractComplete($contractInfo, $invoice);
                        if($isContractComplete){
                            $invoice->status = PaymentInvoice::STATUS_PAYED;
                            $invoice->payed = now();
                        }
                    }
                    if(!isset($isContractComplete) || !$isContractComplete){
                        $this->isContractDisputed($contractInfo, $invoice);
                    }
                }else{
                    Log::channel('payment_invoices_checker')->info('CONTACT_INFO|ERROR|PAYMENT_INVOICE_ID|'.$invoice->id, ['contract' => $contractInfo]);
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::getInvoiceTypeByInstance($invoice),
                        3,
                        InvoicesHistory::getStatus(0),
                        InvoicesHistory::getSource(3),
                        InvoicesHistory::getSource(2),
                        json_encode($contractInfo),
                        'Service Provider response incorrect'
                    );
                }

            }elseif($invoice->created_at < date('Y-m-d H:i:s',strtotime("-".$invoiceTimeOut." hour"))){
                $invoice->status = PaymentInvoice::STATUS_CANCELED_BY_TIMEOUT;
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                    3,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(3),
                    InvoicesHistory::getSource(3),
                    NULL,
                    'Invoice timeout'
                );
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                    4,
                    InvoicesHistory::getStatus(1),
                    InvoicesHistory::getSource(3),
                    InvoicesHistory::getSource(3),
                    NULL,
                    'Apply Cancel by timeout'
                );
            }
            $invoice->checked_at = now();
            $invoice->save();
        }
    }
}
