<?php

namespace App\Console\Commands;

use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\AgentBalance;
use App\Models\Merchant;
use App\Models\MerchantBalance;
use App\Models\MerchantInputInvoice;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use App\Models\SystemBalance;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RecalculateBalances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'balance:recalculate_invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $result = DB::select(DB::raw("SELECT payment_invoices.id AS invoice_id,
    payment_invoices.merchant_id,
    payment_invoices.amount,
    payment_invoices.amount2pay,
    payment_invoices.amount2grow,
    payment_invoices.amount2service,
    payment_invoices.amount2agent,
    payment_invoices.status,
    payment_invoices.payed,
    payment_invoices.created_at,
    payment_invoices.api_type,
    'payment'::text AS invoice_type
   FROM payment_invoices
   WHERE (payment_invoices.status = 1)
UNION ALL
 SELECT merchant_withdrawal_invoices.id AS invoice_id,
    merchant_withdrawal_invoices.merchant_id,
    merchant_withdrawal_invoices.amount,
    merchant_withdrawal_invoices.amount2pay,
    merchant_withdrawal_invoices.amount2grow,
    merchant_withdrawal_invoices.amount2service,
    merchant_withdrawal_invoices.amount2agent,
    merchant_withdrawal_invoices.status,
    merchant_withdrawal_invoices.payed,
    merchant_withdrawal_invoices.created_at,
    merchant_withdrawal_invoices.api_type,
    'withdrawal'::text AS invoice_type
   FROM merchant_withdrawal_invoices
   WHERE (merchant_withdrawal_invoices.status = 1 )

UNION ALL
 SELECT merchant_input_invoices.id AS invoice_id,
    merchant_input_invoices.merchant_id,
    merchant_input_invoices.amount,
    NULL AS amount2pay,
    NULL AS amount2grow,
    NULL AS amount2service,
    NULL AS amount2agent,
    merchant_input_invoices.status,
    merchant_input_invoices.payed,
    merchant_input_invoices.created_at,
    NULL AS api_type,
    'merchant_input'::text AS invoice_type
   FROM merchant_input_invoices
   WHERE (merchant_input_invoices.status = 1)
order by created_at asc;"));


        foreach ($result as $item) {
            if ($item->invoice_type == 'payment') {
                $invoice = PaymentInvoice::where(['id' => $item->invoice_id])->first();
                $invoice_type = AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE;
            } elseif ($item->invoice_type == 'withdrawal') {
                $invoice = MerchantWithdrawalInvoice::where(['id' => $item->invoice_id])->first();
                $invoice_type = AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL;
            } elseif ($item->invoice_type == 'merchant_input') {
                $invoice = MerchantInputInvoice::where(['id' => $item->invoice_id])->first();
                $invoice_type = AccountingEntrie::INVOICE_TYPE_MERCHANT_INPUT_INVOICE;
                $this->addAccountingQueue(
                    $invoice,
                    AccountingEntrie::USER_TYPE_MERCHANT,
                    $invoice->merchant_id,
                    $invoice->amount,
                    AccountingEntrie::TYPE_CREDIT,
                    $invoice_type
                );
                continue;
            } else {
                continue;
            }

            if ($item->amount2service > 0) {
                $this->addAccountingQueue(
                    $invoice,
                    AccountingEntrie::USER_TYPE_SERVICE,
                    null,
                    $invoice->amount2service,
                    AccountingEntrie::TYPE_CREDIT,
                    $invoice_type
                );
            }
            if ($item->amount2agent > 0) {
                $merchant = Merchant::where('id', $invoice->merchant_id)->first();
                if (!empty($merchant->agent_id)) {
                    $this->addAccountingQueue(
                        $invoice,
                        AccountingEntrie::USER_TYPE_AGENT,
                        $merchant->agent_id,
                        $invoice->amount2agent,
                        AccountingEntrie::TYPE_CREDIT,
                        $invoice_type
                    );
                }
            }
            if ($item->amount2grow > 0) {
                $this->addAccountingQueue(
                    $invoice,
                    AccountingEntrie::USER_TYPE_GROW,
                    null,
                    $invoice->amount2grow,
                    AccountingEntrie::TYPE_CREDIT,
                    $invoice_type
                );
            }

            if($item->invoice_type == 'withdrawal'){
                $type = AccountingEntrie::TYPE_DEBIT;
                $amount = $invoice->amount;
            }else{
                $type = AccountingEntrie::TYPE_CREDIT;
                $amount = $invoice->amount2pay;
            }
            $this->addAccountingQueue(
                $invoice,
                AccountingEntrie::USER_TYPE_MERCHANT,
                $invoice->merchant_id,
                $amount,
                $type,
                $invoice_type
            );
        }
        return 0;
    }

    private function addAccountingQueue($invoice, $user_type, $user_id, $amount, $type, $invoice_type)
    {
        $accounting = AccountingQueue::create([
            'type' => $type,
            'user_type' => $user_type,
            'user_id' => $user_id,
            'amount' => $amount,
            'balance_after' => $this->getBalanceAfter($user_type, $invoice, $amount, $type),
            'invoice_id' => $invoice->id,
            'invoice_type' => $invoice_type,
            'status' => AccountingEntrie::STATUS_CREATE,
            'created_at' => $invoice->created_at,
            'updated_at' => $invoice->updated_at
        ]);
    }

    private function getBalanceAfter($user_type, $invoice, $amount, $type)
    {

        if ($type == AccountingEntrie::TYPE_DEBIT) {
            $amount = $amount * (-1);
        }

        switch ($user_type) {
            case AccountingEntrie::USER_TYPE_SERVICE:
                $serviceBalance = SystemBalance::firstOrCreate(['name' => SystemBalance::NAME_SERVICE]);
                $result = $serviceBalance->amount + $amount;
                break;
            case AccountingEntrie::USER_TYPE_AGENT:
                $merchant = Merchant::where(['id' => $invoice->merchant_id])->first();
                $agentBalance = AgentBalance::firstOrCreate(['agent_id' => $merchant->agent_id]);
                $result = $agentBalance->amount + $amount;
                break;
            case AccountingEntrie::USER_TYPE_GROW:
                $growBalance = SystemBalance::firstOrCreate(['name' => SystemBalance::NAME_GROW]);
                $result = $growBalance->amount + $amount;
                break;
            case AccountingEntrie::USER_TYPE_MERCHANT:
                $merchantBalance = MerchantBalance::firstOrCreate(['merchant_id' => $invoice->merchant_id]);
                $result = $merchantBalance->amount + $amount;
                break;
            default:
                $result = 0;
        }
        return $result;
    }

}
