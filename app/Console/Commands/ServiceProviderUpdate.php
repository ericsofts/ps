<?php

namespace App\Console\Commands;

use App\Lib\Chatex\ChatexApi;
use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use App\Models\Property;
use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantServiceProvider;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersBalance;
use App\Models\ServiceProvidersProperty;
use App\Models\SystemBalance;
use Illuminate\Console\Command;

class ServiceProviderUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'service_provider:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $providerChatex = ServiceProvider::firstOrCreate(
            [
                'name' => ChatexApi::OBJECT_NAME,
                'status' => ServiceProvider::STATUS_ACTIVE
            ]
        );
        $providerTrader = ServiceProvider::firstOrCreate(
            [
                'name' => 'trader',
                'status' => ServiceProvider::STATUS_ACTIVE
            ]
        );

        $oldBalance = SystemBalance::where('name', '=', SystemBalance::NAME_SERVICE)->first();
        $balance = ServiceProvidersBalance::where('provider_id', '=' ,$providerChatex->id)->first();
        if ($balance) {
            if ($oldBalance) {
                $balance->amount = $oldBalance->amount;
            }
            $balance->name = $providerChatex->name;
            $balance->save();
            if ($oldBalance) {
                $oldBalance->delete();
            }
        }

        MerchantCommission::where('service_provider_id', null)->update(['service_provider_id' => $providerChatex->id]);

        PaymentInvoice::whereIn('api_type', [PaymentInvoice::API_TYPE_0, PaymentInvoice::API_TYPE_3, PaymentInvoice::API_TYPE_5 ])->update(['service_provider_id' => $providerChatex->id]);
        MerchantWithdrawalInvoice::whereIn('payment_system', [MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC])->update(['service_provider_id' => $providerChatex->id]);
        AccountingQueue::whereIn('user_type', [AccountingEntrie::USER_TYPE_SERVICE])->update(['user_id' => $providerChatex->id]);
        AccountingEntrie::whereIn('user_type', [AccountingEntrie::USER_TYPE_SERVICE])->update(['user_id' => $providerChatex->id]);

        $merchants = Merchant::all();
        foreach ($merchants as $unit) {
            MerchantServiceProvider::firstOrCreate([
                'merchant_id' => $unit->id,
                'service_provider_id' => $providerChatex->id,
                'priority' => 0
            ]);
        }

        $chatexProp = Property::where('obj_name', '=', ChatexApi::OBJECT_NAME)->get();
        foreach ($chatexProp as $item){
            ServiceProvidersProperty::create([
                'obj_name' => $item->obj_name,
                'obj_id' => $item->obj_id,
                'property' => $item->property,
                'property_value' => $item->property_value
            ]);
        }
        Property::where('obj_name', '=', ChatexApi::OBJECT_NAME)->delete();
    }
}
