<?php

namespace App\Console\Commands;

use App\Models\BalanceQueue;
use App\Models\InputInvoice;
use App\Services\ContractManager;
use App\Traits\Contract;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class ChekerInputIvoice extends Command
{

    use Contract;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'input_invoice:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $isSleep = false;
    protected $contractManager = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ContractManager $contractManager)
    {
        parent::__construct();
        $this->contractManager = $contractManager;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start Input Invoice checker');
        while (true) {
            $this->_main();
        }
        return 0;
    }

    private function _main()
    {
        $check_iterval = config('app.invoice_check_time');
        $datetime = Carbon::now()->addSeconds(-$check_iterval);
        $inputInvoices = InputInvoice::whereIn('payment_system',[
            InputInvoice::PAYMENT_SYSTEM_CHATEX_LBC,
            InputInvoice::PAYMENT_SYSTEM_TRADER
        ])
            ->whereIn('status', [InputInvoice::STATUS_CREATED, InputInvoice::STATUS_USER_CONFIRMED, InputInvoice::STATUS_USER_SELECTED, InputInvoice::STATUS_TRADER_CONFIRMED])
            ->where(function ($query) use ($datetime) {
                $query->where('checked_at', '<=', $datetime)
                    ->orWhereNull('checked_at');
            })
            ->orderBy('id', 'ASC')
            ->limit(1)
            ->get();
        if (!$inputInvoices->count()) {
            if (!$this->isSleep) {
                $this->info('Sleep');
                $this->isSleep = true;
            }
            sleep(config('app.invoice_check_sleep_time'));
            return;
        }

        $this->isSleep = false;
        $this->info('Working');

        foreach ($inputInvoices as $inputInvoice) {
            $this->info(now()->toIso8601String() . '| Input_invoice #' . $inputInvoice->id);
            $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $inputInvoice['payment_id'], 'service_provider_id' => $inputInvoice['service_provider_id']]);
            Log::channel('invoice_deals_checker')->info('CONTACT_INFO|PAYMENT_INVOICE_ID|'.$inputInvoice->id, ['contract' => $contractInfo]);
            if($contractInfo){
                $isContractCancel = $this->isContractCancel($contractInfo, $inputInvoice);
                $isClosed = $this->isContractClosed($contractInfo, $inputInvoice);
                if(!$isContractCancel && $isClosed){
                    $inputInvoice->status = InputInvoice::STATUS_CANCELED;
                }
                if(!$isContractCancel){
                    $isContractComplete = $this->isContractComplete($contractInfo, $inputInvoice);
                    if($isContractComplete){
                        $inputInvoice->status = InputInvoice::STATUS_PAYED;
                        $inputInvoice->payed = now();
                    }
                }else {
                    Log::channel('invoice_deals_checker')->info('CONTACT_INFO|ERROR|INPUT_INVOICE_ID|' . $inputInvoice->id, $contractInfo);
                }
            }
            $inputInvoice->checked_at = now();
            $inputInvoice->save();
        }
    }
}
