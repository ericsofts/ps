<?php

namespace App\Console\Commands;

use App\Models\Property;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class NbkUpdateRateKzt2Usd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nbk_update_rate:kzt2usd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::get('https://nationalbank.kz/rss/rates_all.xml');

        if($response->ok()){
            $body = $response->body();
            $matches = [];
            preg_match('/>USD<\/title>\s*<pubDate>.+?<\/pubDate>\s*<description>(\d+(.\d+)*)</', $body, $matches);
            $rate = $matches[1] ?? false;

            if($rate){
                Property::setProperty('currency_rate', 0, 'kzt2usd', round(1/$rate, 8));
                Property::setProperty('currency_rate', 0, 'usd2kzt', round($rate, 2));
            }
        }
        Log::channel('nbk_update')->info('KZT2USD', ['res' => $response->body(), 'status' => $response->status()]);
        return 0;
    }
}
