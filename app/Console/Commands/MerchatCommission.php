<?php

namespace App\Console\Commands;

use App\Models\Merchant;
use App\Models\MerchantCommission;
use Illuminate\Console\Command;

class MerchatCommission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merchant:add_commission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $merchants = Merchant::all();
        foreach ($merchants as $merchant) {
            $this->info($merchant->id);
            $this->add_default_commission($merchant);
        }
        return 0;
    }

    private function add_default_commission($merchant)
    {
        //grow
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API1,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API2,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API4,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'grow',
            'value' => config('app.commission.grow')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_sum_grow',
            'value' => config('app.commission.min_sum_grow')
        ]);
        //service
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API1,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API2,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API4,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'service',
            'value' => config('app.commission.service')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_sum_service',
            'value' => config('app.commission.min_sum_service')
        ]);
        //agent
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API1,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API2,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API4,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'agent',
            'value' => config('app.commission.agent')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_sum_agent',
            'value' => config('app.commission.min_sum_agent')
        ]);
    }
}
