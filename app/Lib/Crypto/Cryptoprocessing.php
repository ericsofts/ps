<?php

namespace App\Lib\Crypto;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Cryptoprocessing
{
    const INVOICE_STATUS_NEW = 'new';
    const INVOICE_STATUS_DONE = 'done';
    const INVOICE_STATUS_EXPIRED = 'expired';
    const INVOICE_STATUS_CANCELLED = 'cancelled';

    private $httpClient;

    public function __construct($token = null)
    {
        if (is_null($token))
            $token = config('app.crypto_gate_token');
        $this->httpClient = Http::withToken($token)->withHeaders([
            'Accept' => 'application/json'
        ])->withoutVerifying()->log();
    }

    public function invoiceNew($params = [])
    {
        $response = $this->httpClient->post(
            config('app.crypto_gate_host'),
            $this->prepare_params('svc.quppy.new', $params)
        );

        if ($response->ok()) {
            $result = $response->json();
            if (!empty($result['result'])) {
                return $result['result'];
            }
        }
        Log::channel('crypto_processing')->info('invoiceNew', ['status' => $response->status(), 'result' => $response->json() ?? null, 'params' => $params]);
        return false;
    }

    public function invoiceCancel($params = [])
    {
        $response = $this->httpClient->post(
            config('app.crypto_gate_host'),
            $this->prepare_params('svc.quppy.cancel', $params)
        );

        if ($response->ok()) {
            $result = $response->json();
            if (!empty($result['result'])) {
                return $result['result'];
            }
        }
        Log::channel('crypto_processing')->info('invoiceCancel', ['status' => $response->status(), 'result' => $response->json() ?? null, 'params' => $params]);
        return false;
    }

    public function withdrawalInvoiceNew($params = [])
    {
        $response = $this->httpClient->post(
            config('app.crypto_gate_host'),
            $this->prepare_params('svc.quppyIP.transfer', $params)
        );

        if ($response->ok()) {
            $result = $response->json();
            if (!empty($result['result'])) {
                return $result['result'];
            }
        }
        Log::channel('crypto_processing')->info('withdrawalInvoiceNew', ['status' => $response->status(), 'result' => $response->json() ?? null, 'params' => $params]);
        return false;
    }

//    public function withdrawalInvoiceCancel($params = [])
//    {
//        $response = $this->httpClient->post(
//            config('app.crypto_gate_host'),
//            $this->prepare_params('svc.quppy.withdrawal.cancel', $params)
//        );
//
//        if ($response->ok()) {
//            $result = $response->json();
//            if (!empty($result['result'])) {
//                return $result['result'];
//            }
//        }
//        Log::channel('crypto_processing')->info('withdrawalInvoiceCancel', ['status' => $response->status(), 'result' => $response->json() ?? null, 'params' => $params]);
//        return false;
//    }

    public function checkAddress($params = [])
    {
        $response = $this->httpClient->post(
            config('app.crypto_gate_host'),
            $this->prepare_params('system.validateAddress', $params)
        );

        if ($response->ok()) {
            $result = $response->json();
            if (!empty($result['error'])) {
                return false;
            }
            return true;
        }
        Log::channel('crypto_processing')->info('checkAddress', ['status' => $response->status(), 'result' => $response->json() ?? null, 'params' => $params]);
        return false;
    }

    protected function prepare_params($method, $params = [])
    {
        return [
            'jsonrpc' => '2.0',
            'method' => $method,
            'params' => $params,
            'id' => time()
        ];
    }

}
