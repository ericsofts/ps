<?php


namespace App\Lib\Crypto;


use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class KrakenAPI
{
    private PendingRequest $httpClient;
    protected $url = '';

    public function __construct()
    {
        $this->url = config('app.kraken_host');
        $this->httpClient = Http::withHeaders([
            'Accept' => 'application/json'
        ])->withoutVerifying();
    }

    public function getTickerInformation($pair)
    {
        return Cache::remember('kraken_get_token_information'.$pair, config('app.kraken_cache', 1), function () use ($pair) {
            $response = $this->httpClient->get(
                $this->url. '/0/public/Ticker?',
                ['pair' => $pair]
            );
            if ($response->ok()) {
                $result = $response->json();
                if (!empty($result['result'])) {
                    $first = array_shift($result['result']);
                    return 1 / $first['c'][0] ?? false;
                }
            } else {
                return false;
            }
            return false;
        });

    }
}
