<?php


namespace App\Lib;


class PaymentApiHelper
{
    /**
     * @var string sign separator
     */
    const signatureSeparator = '|';

    /**
     * @param array $params
     * @param $secret_key
     * @return string
     */
    public static function generateSignature($params, $secret_key): string
    {
        $data = array_filter($params,
            function ($var) {
                return $var !== '' && $var !== null;
            });
        ksort($data);
        $sign_str = $secret_key;
        foreach ($data as $k => $v) {
            $sign_str .= self::signatureSeparator . $v;
        }

        $signature = sha1($sign_str);
        return strtolower($signature);
    }

    /**
     * @param $data
     * @param $secret_key
     * @return boolean
     */
    public static function checkSignature($data, $secret_key): bool
    {
        if (!array_key_exists('signature', $data)) {
            return false;
        }
        $signature = $data['signature'];
        unset($data['signature']);
        return $signature == self::generateSignature($data, $secret_key);
    }
}
