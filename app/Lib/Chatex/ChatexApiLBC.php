<?php


namespace App\Lib\Chatex;


use GuzzleHttp\Psr7\MultipartStream;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class ChatexApiLBC
{
    const OBJECT_NAME = 'chatex_lbc';
    const PM_NAME = 'bank_card';
    private $hmac_key;
    private $hmac_secret;
    private $server;

    public function __construct($server, $hmac_key)
    {
        $this->server = $server;
        $this->hmac_key = $hmac_key;
        $secret = Storage::get($hmac_key);

        if ($secret) {
            try {
                $hmac_secret = Crypt::decryptString($secret);
            } catch (DecryptException $e) {
                Log::channel('chatex_lbc')->info($e->getMessage());
                throw new \Exception(__('Whoops! Something went wrong.'), 500);
            }
        }
        if (empty($hmac_secret)) {
            Log::channel('chatex_lbc')->info('Secret file not found');
            throw new \Exception(__('Whoops! Something went wrong.'), 500);
        }
        $this->hmac_secret = $hmac_secret;
    }

    private function call($method, $url, $params = [])
    {
        for ($i = 1; $i <= 10; $i++) {
            $nonce = time();
            $message = $nonce . $this->hmac_key . $url;
            $params_encoded = '';
            if ($params) {
                $params_encoded = http_build_query($params);
            }
            $message .= $params_encoded;
            $signature = strtoupper(hash_hmac('sha256', $message, $this->hmac_secret));
            $headers['Apiauth-Key'] = $this->hmac_key;
            $headers['Apiauth-Nonce'] = $nonce;
            $headers['Apiauth-Signature'] = $signature;
            $http = Http::log()->withHeaders($headers);
            try {
                if ($method == 'POST') {
                    $response = $http->asForm()
                        ->post($this->server . $url, $params);
                } elseif ($method == 'GET') {
                    $response = $http->get($this->server . $url, $params);
                } else {
                    throw new MethodNotAllowedHttpException(['POST', 'GET'], 'Method not allowed');
                }

                $response_json = $response->json();
                if (!empty($response_json['error_code']) && $response_json['error_code'] == 42) {
                    Log::channel('chatex_lbc')->info(print_r($response_json));
                    sleep(0.1);
                    continue;
                }
                return $response_json;
            } catch (\Exception $e) {
                Log::channel('chatex_lbc')->info($e->getMessage());
                return $e;
            }
        }
    }

    public function adList($type)
    {
        return Cache::remember('chatex_lbc_sell_ad_list_'.$type, config('app.chatex_lbc_sell_ad_list_cache', 30), function () use ($type) {
            $ad_list = [];
            $hasPage = true;
            $maxPage = 50;
            $page = null;
            if ($type == 'usdt') {
                $method = 'sellUSDTOnline';
            } elseif ($type == 'usdt_trc20') {
                $method = 'sellUsdtTrc20Online';
            } elseif ($type == 'btc') {
                $method = 'sellBitcoinsOnline';
            } else {
                return [];
            }
            while ($hasPage && $maxPage >= $page) {
                $hasPage = false;
                $res_sell = $this->$method(null, null, $page);
                if (!empty($res_sell['data']['ad_list'])) {
                    $ad_list = array_merge($ad_list, $res_sell['data']['ad_list']);
                } else {
                    Log::channel('chatex_lbc_requests')->info('AD_LIST|ERROR|USER_ID|' . optional(auth()->user())->id . '|' . $method, ['res_sell' => $res_sell]);
                }
                if (!empty($res_sell['pagination']['next'])) {
                    $url_query = parse_url($res_sell['pagination']['next'], PHP_URL_QUERY);
                    $query = explode('=', $url_query);
                    if (!empty($query[1])) {
                        $page = $query[1];
                        $hasPage = true;
                    }
                }
            }
//            Log::channel('chatex_lbc_requests')->info('AD_LIST|FULL_RESULT|' , ['result' => $res_sell]);
            return $ad_list;
        });
    }

    public function adList2buy($type): array
    {
        return Cache::remember('chatex_lbc_buy_ad_list_'.$type, config('app.chatex_lbc_buy_ad_list_cache', 30), function () use ($type) {
            $ad_list = [];
            $hasPage = true;
            $maxPage = 50;
            $page = null;
            if ($type == 'usdt') {
                $method = 'buyUSDTOnline';
            }elseif ($type == 'usdt_trc20') {
                $method = 'buyUsdtTrc20Online';
            } else {
                return [];
            }
            while ($hasPage && $maxPage >= $page) {
                $hasPage = false;
                $res_sell = $this->$method(null, null, $page);
                if (!empty($res_sell['data']['ad_list'])) {
                    $ad_list = array_merge($ad_list, $res_sell['data']['ad_list']);
                } else {
                    Log::channel('chatex_lbc_requests')->info('AD_LIST2BUY|ERROR|USER_ID|' . optional(auth()->user())->id . '|' . $method, ['res_sell' => $res_sell]);
                }
                if (!empty($res_sell['pagination']['next'])) {
                    $url_query = parse_url($res_sell['pagination']['next'], PHP_URL_QUERY);
                    $query = explode('=', $url_query);
                    if (!empty($query[1])) {
                        $page = $query[1];
                        $hasPage = true;
                    }
                }
            }
            return $ad_list;
        });
    }

    public function contactCreate($ad_id, $amount, $message = null)
    {
        $data = ['amount' => $amount];
        if ($message) {
            $data['message'] = $message;
        }
        return $this->call('POST', "/api/contact_create/$ad_id/", $data);
    }

    public function contactInfo($contact_id)
    {
        return $this->call('GET', "/api/contact_info/$contact_id/");
    }

    public function adGet($ad_id)
    {
        return $this->call('GET', "/api/ad-get/$ad_id/");
    }

    public function contactMarkAsPaid($contact_id)
    {
        return $this->call('POST', "/api/contact_mark_as_paid/$contact_id/");
    }

    public function contactRelease($contact_id)
    {
        return $this->call('POST', "/api/contact_release/$contact_id/");
    }

    public function contactCancel($contact_id)
    {
        return $this->call('POST', "/api/contact_cancel/$contact_id/");
    }

    public function dashboard($type = null)
    {
        $url = '/api/dashboard/';
        if (!empty($type)) {
            $url .= $type . "/";
        }
        return $this->call('GET', $url);
    }

    public function contactMessagePost($contact_id, $msg, UploadedFile $file = null)
    {
        if ($file) {
            $url = "/api/contact_message_post/$contact_id/";
            $body = new MultipartStream([
                ['name' => 'document',
                    'contents' => fopen($file->path(), 'r'),
                    'filename' => strtolower($file->getClientOriginalName())
                ],
                ['name' => 'msg', 'contents' => $msg],
            ]);
            $headers['Content-Type'][] = 'multipart/form-data; boundary=' . $body->getBoundary();
            $nonce = time();
            $message = $nonce . $this->hmac_key . $url;
            $message .= $body->getContents();
            $signature = strtoupper(hash_hmac('sha256', $message, $this->hmac_secret));
            $headers['Apiauth-Key'] = $this->hmac_key;
            $headers['Apiauth-Nonce'] = $nonce;
            $headers['Apiauth-Signature'] = $signature;
            $request = new \GuzzleHttp\Psr7\Request('POST', $this->server . $url, $headers, $body);
            $client = new \GuzzleHttp\Client();
            try{
                $response = $client->send($request);
                $response_json = json_decode($response->getBody()->getContents(), true);
                return $response_json;
            }catch (\Exception $e){
                Log::channel('chatex_lbc_errors')->info('contactMessagePost', [
                    'contact_id' => $contact_id,
                    'msg' => $msg,
                    'file' => optional($file)->getClientOriginalName(),
                    'e-code' => $e->getCode(),
                    'e-message' => $e->getMessage()
                ]);
                return false;
            }


        }
        return $this->call('POST', "/api/contact_message_post/$contact_id/", ['msg' => $msg]);
    }

    public function contactMessages($contact_id)
    {
        return $this->call('GET', "/api/contact_messages/$contact_id/");
    }

    public function sellBitcoinsOnline($currency = null, $payment_method = null, $page = null)
    {
        return $this->sellCoinOnline('bitcoins', $currency, $payment_method, $page);
    }

    public function sellUSDTOnline($currency = null, $payment_method = null, $page = null)
    {
        return $this->sellCoinOnline('usdt', $currency, $payment_method, $page);
    }

    public function sellUsdtTrc20Online($currency = null, $payment_method = null, $page = null)
    {
        return $this->sellCoinOnline('usdt_trc20', $currency, $payment_method, $page);
    }

    public function sellCoinOnline($coin, $currency = null, $payment_method = null, $page = null)
    {
        $url = "/sell-{$coin}-online/.json";
        if (!empty($currency)) {
            $url = "/sell-{$coin}-online/{$currency}/.json";
            if (!empty($payment_method)) {
                $url = "/sell-{$coin}-online/{$currency}/{$payment_method}/.json";
            }
        }
        $params = [];
        if ($page) {
            $params['page'] = $page;
        }
        $params['limit'] = 500;
        return $this->call('GET', $url, $params);
    }

    public function buyUSDTOnline($currency = null, $payment_method = null, $page = null)
    {
        return $this->buyCoinOnline('usdt', $currency, $payment_method, $page);
    }

    public function buyUsdtTrc20Online($currency = null, $payment_method = null, $page = null)
    {
        return $this->buyCoinOnline('usdt_trc20', $currency, $payment_method, $page);
    }

    public function buyBitcoinsOnline($currency = null, $payment_method = null, $page = null)
    {
        return $this->buyCoinOnline('bitcoins', $currency, $payment_method, $page);
    }

    public function buyCoinOnline($coin, $currency = null, $payment_method = null, $page = null)
    {
        $url = "/buy-{$coin}-online/.json";
        if (!empty($currency)) {
            $url = "/buy-{$coin}-online/{$currency}/.json";
            if (!empty($payment_method)) {
                $url = "/buy-{$coin}-online/{$currency}/{$payment_method}/.json";
            }
        }
        $params = [];
        if ($page) {
            $params['page'] = $page;
        }
        $params['limit'] = 500;
        return $this->call('GET', $url, $params);
    }

}
