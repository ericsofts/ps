<?php


namespace App\Lib;


use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Coinsbit
{
    const OBJECT_NAME = 'сщштыише';
    private $key;
    private $secret;
    private $server = 'https://coinsbit.io';

    /**
     * Сoinsbit constructor.
     * @param $key
     * @param $secret
     */
    public function __construct($key, $secret)
    {
        $this->key = $key;
        $this->secret = $secret;
    }

    private function call($method, $url, $params = [])
    {

        $data = [
            'request' => $params,
            'nonce' => (string)Carbon::now()->timestamp,
        ];

        $dataJsonStr = json_encode($data, JSON_UNESCAPED_SLASHES);
        $payload = base64_encode($dataJsonStr);
        $signature = hash_hmac('sha512', $payload, $this->secret);

        $headers['X-TXC-APIKEY'] = $this->key;
        $headers['X-TXC-PAYLOAD'] = $payload;
        $headers['X-TXC-SIGNATURE'] = $signature;
        $http = Http::withHeaders($headers);
        try {
            if ($method == 'POST') {
                $response = $http->asJson()->post($this->server . $url, $data);
            } elseif ($method == 'GET') {
                $response = $http->get($this->server . $url, $params);
            } else {
                throw new MethodNotAllowedHttpException(['POST', 'GET'], 'Method not allowed');
            }
            $response_json = $response->json();
            return $response_json;
        } catch (\Exception $e) {
            Log::channel('coinsbit')->info($e->getMessage());
            return $e;
        }
    }

    public function symbols()
    {
        return $this->call('POST', '/api/v1/public/symbols');
    }

    public function ticker($market)
    {
        return $this->call('GET', '/api/v1/public/ticker', ['market' => $market]);
    }
}
