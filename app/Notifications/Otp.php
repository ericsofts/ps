<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;
use NumberFormatter;

class Otp extends Notification
{
    use Queueable;

    public $merchant;
    public $payment_order;
    public $time;
    public $passcode;

    /**
     * Create a new notification instance.
     *
     * @param $payment_order
     * @param $merchant
     * @param $passcode
     * @param $time
     */
    public function __construct($payment_order, $merchant, $passcode, $time)
    {
        $this->merchant = $merchant;
        $this->payment_order = $payment_order;
        $this->passcode = $passcode;
        $this->time = $time;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
        return (new MailMessage)
            ->subject(__('Grow Payment System: One-Time Passcode'))
            ->line(__('Your One Time Passcode for completing your transaction with :name (Invoice#:order_number :amount) is:', [
                'name' => $this->merchant->name,
                'order_number' => $this->payment_order->invoice_number,
                'amount' => $fmt->formatCurrency(price_format($this->payment_order->amount), "USD")
            ]))
            ->line(new HtmlString("<p style='text-align: center; font-weight: bold; font-size: 26px'>$this->passcode</p>"))
            ->line(__('Please use this Passcode to complete your transaction. Do not share this Passcode with anyone.'))
            ->line(__('The Passcode is valid for <b>:time minutes</b>.', ['time' => $this->time]))
            ->line(__("If you haven't requested for the Passcode just ignore this mail."));
    }

}
