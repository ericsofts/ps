<?php

namespace App\Traits;

use App\Models\AccountingEntrie;
use App\Models\CryptoCurrency;
use App\Models\InvoicesHistory;
use App\Models\PaymentSystemType;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use App\Models\TraderAd;
use Illuminate\Support\Facades\Log;

trait CryptoAds
{
    use Invoice;
    use ServiceProviderTrait;

    private function getCryptoAvalableAds()
    {
        if($this->type == TraderAd::TYPE_BUY){
            return [];
        }
        $adsList = $this->getCryptoAds();
        if (!empty($adsList)) {
            $ads_coin = $this->getCryptoCoins($adsList);
            if (!empty($ads_coin)) {
                return $ads_coin;
            }
        }
        return [];
    }

    private function getCryptoAds()
    {

        $available_currencies = json_decode(ServiceProvidersProperty::getProperty(ServiceProvider::CRYPTO_PROVIDER, $this->currentProvider->id, 'available_currencies', '[]'), true);

        $cryptoCurrencies = CryptoCurrency::active()->whereIn('asset', $available_currencies)->get();

        $ad_list = [];

        foreach ($cryptoCurrencies as $record){
            $ad_list[] = [
                'bank_name' => $this->currentProvider->name,
                'ad_id' => $record->id,
                'service_provider_id' => $this->currentProvider->id,
                'coin' => $record->asset,
                'currency' => $record->asset,
                'payment_system_type' => PaymentSystemType::CRYPTO,
                'name' => $this->currentProvider->name,
                'spread' => $record->spread,
            ];
        }

        return $ad_list;
    }

    private function getCryptoCoins($ad_list)
    {
        $ads_coins = [];
        if($this->type == TraderAd::TYPE_BUY) {
            return [];
        }
        if ($this->currency2currency) {
            $currencyRate = $this->getCurrencyRate($this->currency);
        }

        $rates = $this->getCoinRates($this->userType==self::TYPE_MERCHANT ? $this->getAvailableMerchantCoins($this->merchant->id) : $this->getAvailableUserCoins());

        foreach ($ad_list as $data) {
            $coin = strtolower($data['coin']);
            if (empty($rates[$coin]['coin_rate'])) {
                continue;
            }

            if ($this->currency2currency) {
                $coin_amount = round(floatval($this->input_amount) * $currencyRate, 2);
            }else {
                $coin_amount = $this->amount * $rates[$coin]['coin_rate'];
            }

//            $coin_amount = $this->amount * $rates[$coin]['coin_rate'];
            $c_amount = $coin_amount + ($coin_amount * $data['spread'] / 100);
            $data['gps_temp_amount'] = $c_amount;
            $data['gps_priority'] = 0;
            $ads_coins[$data['currency']][] = $data;
        }

        return $ads_coins;
    }


    private function getCryptoAd($id, $invoice = false)
    {
        $cryptoCurrencies = CryptoCurrency::active()->where('id', $id)->first();
        $serviceProvider = $this->getFirstProviderByType(PaymentSystemType::CRYPTO);
        if ($cryptoCurrencies) {
            return [
                'id' => $cryptoCurrencies->id,
                'coin' => $cryptoCurrencies->asset,
                'card_number' => false,
                'currency' => $cryptoCurrencies->asset,
                'crypto_asset' => $cryptoCurrencies->crypto_asset,
                'bank_name' => $serviceProvider->name,
                'name' => $serviceProvider->name,
                'service_provider_id' => $serviceProvider->id,
                'service_provider_name' => $serviceProvider->name,
                'service_provider_type' => $serviceProvider->type,
                'payment_system_type' => PaymentSystemType::CRYPTO,
                'temp_price' => (100 + $cryptoCurrencies->spread) / 100,
                'type' => 'SELL',
                'log' => 'crypto_requests',
                'auto_acceptance' => true,
                'payment_invoice_time_live' => $cryptoCurrencies->payment_invoice_time_live,
                'currency_id' => $cryptoCurrencies->id,
            ];
        } else if ($invoice) {
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::getInvoiceTypeByInstance($invoice),
                1,
                InvoicesHistory::getStatus(0),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(2),
                json_encode($cryptoCurrencies),
                'Crypto response incorrect'
            );
            Log::channel('crypto_requests')->info('AD|NOT_FOUND|ERROR|MERCHANT_ID|' . $invoice->merchant_id ?? '');
        }

        return null;
    }

}
