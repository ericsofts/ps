<?php

namespace App\Traits;

use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\PaymentSystemType;
use App\Models\Property;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use App\Models\TraderAd;
use App\Models\TraderBalance;
use App\Models\TraderRate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait TraderAds
{
    use Invoice;

    private function getTraderAvalableAds()
    {

        $adsList = $this->getTraderAds();
        if (!empty($adsList)) {
            $ads_coin = $this->getTraderCoins($adsList);

            if (!empty($ads_coin)) {
                $coins_filtered = $this->filterTraderCoin($ads_coin);
                return $coins_filtered;
            }
        }
        return [];
    }

    private function getTraderAds()
    {
        $blocked_traders = json_decode(ServiceProvidersProperty::getProperty(ServiceProvider::TRADER_PROVIDER, $this->currentProvider->id, 'blocked_traders', '[]'), true);
        $excluded_currencies = $this->userType==self::TYPE_MERCHANT ? json_decode(MerchantProperty::getProperty($this->merchant->id, 'excluded_currencies', '[]'), true) : [];

        $blocked_banks = json_decode(ServiceProvidersProperty::getProperty(ServiceProvider::TRADER_PROVIDER, $this->currentProvider->id, 'blocked_banks', '[]'), true);
        $available_currencies = json_decode(ServiceProvidersProperty::getProperty(ServiceProvider::TRADER_PROVIDER, $this->currentProvider->id, 'available_currencies', '[]'), true);

        $available_coins = $this->userType==self::TYPE_MERCHANT ? $this->getAvailableMerchantCoins($this->merchant->id) : $this->getAvailableUserCoins();
        if($this->coin){
            if(in_array($this->coin, $available_coins)){
                $available_coins = [$this->coin];
            }else{
                $available_coins = [];
            }
        }
        if(is_array($available_coins)){
            array_walk($available_coins, function (&$value) {
                $value = strtoupper($value);
            });
        }

        $ad_list = DB::table('trader_ads')
            ->join('trader_currencies as currencies', 'trader_ads.currency_id', '=', 'currencies.id')
            ->join('trader_payment_systems as payments', 'trader_ads.payment_system_id', '=', 'payments.id')
            ->join('traders', 'trader_ads.trader_id', '=', 'traders.id')
            ->join('trader_fiats as fiat', 'trader_ads.fiat_id', '=', 'fiat.id')
            ->join('payment_system_types', 'trader_ads.payment_system_type_id', '=', 'payment_system_types.id', 'left')
            ->join('trader_balances as balances', function ($join) {
                $join->on('trader_ads.trader_id', '=', 'balances.trader_id')
                    ->on('balances.currency_id', '=', 'trader_ads.currency_id')
                    ->where('balances.type', '=', TraderBalance::TYPE_NORMAL);
            })
            ->select(
                'trader_ads.*',
                'currencies.asset as coin',
                'payments.name as bank_name',
                'fiat.name as fiat_name',
                'fiat.asset as currency',
                'traders.name as name',
                'traders.service_provider_id as service_provider_id',
                'balances.amount as balance',
                'payment_system_types.obj_name as payment_system_type'
            )
            ->where('trader_ads.status', '=', TraderAd::STATUS_ACTIVE)
            ->where('trader_ads.type', '=', $this->type)
            ->where('traders.service_provider_id', '=', $this->currentProvider->id)
            ->whereNotIn('traders.name',  $blocked_traders)
            ->whereNotIn('payments.name',  $blocked_banks)
            ->whereNotIn('fiat.asset',  $excluded_currencies);

        if ($available_currencies) {
            $ad_list->whereIn('fiat.asset',  $available_currencies);
        }
        if ($this->currency2currency || $this->exact_currency) {
            $ad_list->where('fiat.asset', '=',  $this->currency);
        }
        if ($available_coins) {
            $ad_list->whereIn('currencies.asset', $available_coins);
        }

        $ad_list = $ad_list->get()->toArray();

        return $ad_list;
    }

    private function getTraderCoins($ad_list)
    {
        $ads_coins = [];
        if($this->type == TraderAd::TYPE_BUY || $this->currency2currency) {
            $currencyRate = $this->getCurrencyRate($this->currency);
        }
        if ($this->currency2currency) {
            $percentage_currency_rate = ServiceProvidersProperty::getProperty(ServiceProvider::TRADER_PROVIDER, $this->currentProvider->id, 'percentage_currency_rate', 0);
            $maxCurrencyRate = round(1 / $currencyRate * (1 + $percentage_currency_rate / 100), 2);
            $minCurrencyRate = round(1 / $currencyRate * (1 - $percentage_currency_rate / 100), 2);
        }

        $rates = $this->getCoinRates($this->userType==self::TYPE_MERCHANT ? $this->getAvailableMerchantCoins($this->merchant->id) : $this->getAvailableUserCoins());

        if ($this->type == TraderAd::TYPE_SELL) {
            $priority_traders = json_decode(ServiceProvidersProperty::getProperty(ServiceProvider::TRADER_PROVIDER, $this->currentProvider->id, 'priority_traders', '[]'), true);
        }
        if ($this->type == TraderAd::TYPE_BUY) {
            $priority_traders = json_decode(ServiceProvidersProperty::getProperty(ServiceProvider::TRADER_PROVIDER, $this->currentProvider->id, 'withdrawal_priority_traders', '[]'), true);
        }

        $priority_banks_traders = json_decode(ServiceProvidersProperty::getProperty(ServiceProvider::TRADER_PROVIDER, $this->currentProvider->id, 'priority_banks_traders', '[]'), true);
        if ($priority_banks_traders) {
            foreach ($priority_banks_traders as &$item) {
                $item = array_reverse($item);
            }
        }

        if ($this->type == TraderAd::TYPE_SELL) {
            $rounding_input_amount = $this->userType==self::TYPE_MERCHANT ? MerchantProperty::getProperty($this->merchant->id, 'rounding_input_amount'): false;
        }

        foreach ($ad_list as $data) {
            $data->temp_price = $this->getTempPrice($data);
            if ($this->currency2currency && ($data->temp_price < $minCurrencyRate || $data->temp_price > $maxCurrencyRate)) {
                continue;
            }

            $data->gps_priority = 0;
            if (!empty($data->name)) {
                if ($priority_banks_traders && !empty($priority_banks_traders[$data->bank_name]) && in_array($data->name, $priority_banks_traders[$data->bank_name])) {
                    $data->gps_priority = 2 + array_search($data->name, $priority_banks_traders[$data->bank_name]);
                } else if ($priority_traders && in_array($data->name, $priority_traders)) {
                    $data->gps_priority = 1;
                }
            }

            $coin = strtolower($data->coin);
            if (empty($rates[$coin]['coin_rate'])) {
                continue;
            }

            if ($this->type == TraderAd::TYPE_SELL) {
                if ($this->currency2currency) {
                    $c_amount = floatval($this->input_amount);
                    $coin_amount = $c_amount / $data->temp_price;;
                } else {
                    $coin_amount = $this->amount * $rates[$coin]['coin_rate'];
                    $c_amount = $coin_amount * $data->temp_price;
                }
            } elseif ($this->type == TraderAd::TYPE_BUY) {
                if ($this->currency2currency) {
                    $amount_usd = floatval($this->input_amount) / $data->temp_price;
                }else {
                    $amount_usd = round(floatval($this->input_amount) * $currencyRate, 2);
                }
                if ($this->expense) {
                    $amount2pay = $amount_usd;
                } else {
                    $fee = $this->merchant->getFeeCommission(
                        $this->merchant->id,
                        $amount_usd,
                        $this->api,
                        MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                        $this->currentProvider->id
                    );
                    $amount2pay = $amount_usd - $fee['total'];
                }
                $coin_amount = $amount2pay * $rates[$coin]['coin_rate'];
                $c_amount = $coin_amount * $data->temp_price;
            }

            if(($this->userType==self::TYPE_MERCHANT) && $this->filterAdByLimit($data, $c_amount)){
                continue;
            }

            if ((is_null($data->min_amount) || $c_amount >= $data->min_amount)
                && (is_null($data->max_amount) || $c_amount <= $data->max_amount)
            ) {
                if ($this->type == TraderAd::TYPE_SELL) {
                    if($coin_amount > $data->balance){
                        continue;
                    }
                    if ($rounding_input_amount) {
                        $data->gps_temp_amount = ceil($c_amount);
                    } else {
                        $data->gps_temp_amount = round($c_amount, 2);
                    }
                } elseif ($this->type == TraderAd::TYPE_BUY) {
                    $data->gps_temp_amount = round($c_amount, 2);
                }
                if(empty($data->payment_system_type)){
                    $data->payment_system_type = PaymentSystemType::CARD_NUMBER;
                }
                $ads_coins[$data->currency][] = $data;
            }
        }

        return $ads_coins;
    }

    private function filterTraderCoin($ads_coin)
    {
        foreach ($ads_coin as &$item) {
            if($this->type == TraderAd::TYPE_SELL){
                usort($item, function ($a, $b) {
                    if ($a->gps_priority && $b->gps_priority) {
                        if ($a->gps_priority == $b->gps_priority && $a->temp_price > $b->temp_price) {
                            return 1;
                        } else if ($a->gps_priority > $b->gps_priority) {
                            return -1;
                        } else {
                            return 1;
                        }
                    }
                    if (!$a->gps_priority && !$b->gps_priority && $a->temp_price == $b->temp_price) {
                        return 0;
                    }
                    if (!$a->gps_priority && !$b->gps_priority && $a->temp_price > $b->temp_price) {
                        return 1;
                    }
                    return $a->gps_priority < $b->gps_priority ? 1 : -1;
                });
            } elseif ($this->type == TraderAd::TYPE_BUY){
                usort($item, function ($a, $b) {
                    if($a->gps_priority && $b->gps_priority){
                        if ($a->gps_priority == $b->gps_priority && $a->temp_price < $b->temp_price) {
                            return 1;
                        }else if ($a->gps_priority > $b->gps_priority) {
                            return -1;
                        }else{
                            return 1;
                        }
                    }
                    if ($a->gps_priority && $b->gps_priority && $a->temp_price < $b->temp_price) {
                        return 1;
                    }
                    if (!$a->gps_priority && !$b->gps_priority && $a->temp_price == $b->temp_price) {
                        return 0;
                    }
                    if (!$a->gps_priority && !$b->gps_priority && $a->temp_price < $b->temp_price) {
                        return 1;
                    }
                    return $a->gps_priority < $b->gps_priority ? 1 : -1;
                });
            }
        }
        $coins_filtered = [];
        foreach ($ads_coin as $k => $v) {
            $ads = [];
            $banks = [];
            foreach ($v as $k1 => $ad) {
                if ($ad->gps_priority && !in_array(mb_strtolower($ad->bank_name), $banks)) {
                    $banks[] = mb_strtolower($ad->bank_name);
                    $ad->bank_name .= "*";
                    $ads[] =  json_decode(json_encode($ad), true);
                    unset($v[$k1]);
                }
            }
            foreach ($v as $ad) {
                if (!$ad->gps_priority && !in_array(mb_strtolower($ad->bank_name), $banks)) {
                    $ads[] = json_decode(json_encode($ad), true);
                    $banks[] = mb_strtolower($ad->bank_name);
                }
            }
            $coins_filtered[$k] = $ads;
        }
        return $coins_filtered;
    }

    private function getTempPrice($item)
    {
        if ($item->rate_type == TraderAd::RATE_PERCENTAGE) {
            // @TODO rate_id
            $rate_rate = TraderRate::where('rate_source_id', '=', $item->rate_source_id)
                ->where('currency_id', '=', $item->currency_id)
                ->where('fiat_id', '=', $item->fiat_id)->first();

            $item->rate = $rate_rate->rate * (1 + $item->rate / 100);

            if ((float) $item->rate_stop) {
                if ($item->type == TraderAd::TYPE_BUY) {
                    $item->rate = min($item->rate, $item->rate_stop);
                } else {
                    $item->rate = max($item->rate, $item->rate_stop);
                }
            }
        }

        return round($item->rate, 2);
    }

    private function getTraderAd($id, $invoice = false)
    {
        $ad = DB::table('trader_ads')
            ->join('trader_currencies as currencies', 'trader_ads.currency_id', '=', 'currencies.id')
            ->join('trader_payment_systems as payments', 'trader_ads.payment_system_id', '=', 'payments.id')
            ->join('traders', 'trader_ads.trader_id', '=', 'traders.id')
            ->join('trader_fiats as fiat', 'trader_ads.fiat_id', '=', 'fiat.id')
            ->join('service_providers as provider', 'traders.service_provider_id', '=', 'provider.id')
            ->join('payment_system_types', 'trader_ads.payment_system_type_id', '=', 'payment_system_types.id', 'left')

            ->select(
                'trader_ads.*',
                'currencies.asset as coin',
                'payments.name as bank_name',
                'fiat.name as fiat_name',
                'fiat.asset as currency',
                'traders.name as name',
                'traders.service_provider_id as service_provider_id',
                'provider.name as provider_name',
                'provider.type as provider_type',
                'payment_system_types.obj_name as payment_system_type'
            )
            ->where('trader_ads.id', '=', $id)->first();
        if ($ad) {
            return [
                'id' => $ad->id,
                'coin' => $ad->coin,
                'card_number' => $ad->card_number,
                'currency' => $ad->currency,
                'bank_name' => $ad->bank_name,
                'name' => $ad->name,
                'service_provider_id' => $ad->service_provider_id,
                'service_provider_name' => $ad->provider_name,
                'service_provider_type' => $ad->provider_type,
                'payment_system_type' => $ad->payment_system_type ?? PaymentSystemType::CARD_NUMBER,
                'temp_price' => $this->getTempPrice($ad),
                'type' => $this->traderAdType($ad),
                'log' => 'trader_requests',
                'auto_acceptance' => $ad->auto_acceptance,
            ];
        } else if ($invoice) {
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::getInvoiceTypeByInstance($invoice),
                1,
                InvoicesHistory::getStatus(0),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(2),
                json_encode($ad),
                'Deal response incorrect'
            );
            Log::channel('trader_requests')->info('AD|NOT_FOUND|ERROR|MERCHANT_ID|' . $invoice->merchant_id ?? '');
        }

        return null;
    }

    private function traderAdType($ad)
    {
        return match ($ad->type) {
            1 => 'BUY',
            0 => 'SELL',
            default => null,
        };
    }
}
