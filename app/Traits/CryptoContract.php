<?php

namespace App\Traits;

use App\Lib\Crypto\Cryptoprocessing;
use App\Models\AccountingEntrie;
use App\Models\CryptoCurrency;
use App\Models\CryptoPaymentInvoice;
use App\Models\InputInvoice;
use App\Models\InvoicesHistory;
use App\Models\PaymentInvoice;
use App\Models\ServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

trait CryptoContract
{

    private $invoice;

    public function createCryptoContract($ad, $currency_amount, $user = false, $invoice = false, $message = null, $payment_info = null)
    {
//        $rate = $this->krakenAPI->getTickerInformation($ad['currency'] . $ad['currency']);
//        if (!$rate) {
//            Log::channel($ad['log'])->info('AD|NOT_FOUND|ERROR|', ['data' => $ad]);
//        }

        $rate = 1;
        $currency = CryptoCurrency::find($ad['currency_id']);
        $rateCurrency = $rate + ($currency->spread / 100) * $rate;
        $amount = round($currency_amount * $rate, $currency->precision);

        $data = [
            'amount' => $amount,
            'currency_id' => $ad['currency_id'],
            'currency_rate' => $rateCurrency,
            'expired_at' => Carbon::now()->addSeconds($ad['payment_invoice_time_live']),
            'input_amount' => $currency_amount,
            'input_currency_id' => $ad['currency_id'],
            'input_rate' => $rate,
            'status' => CryptoPaymentInvoice::STATUS_CREATED,
        ];
        $cryptoContract = CryptoPaymentInvoice::create($data);

        $address = $this->cryptoprocessing->invoiceNew([
            $ad['crypto_asset'], "$cryptoContract->id", $amount, $ad['payment_invoice_time_live']
        ]);

        if ($address) {
            $cryptoContract->addition_info = json_encode([
                'address' => $address
            ]);
            $cryptoContract->address = $address;
            $cryptoContract->status = CryptoPaymentInvoice::STATUS_USER_CONFIRMED;
            $cryptoContract->save();
            return ['id' => $cryptoContract->id, 'service_provider_type' => $ad['service_provider_type']];
        } elseif ($invoice) {
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                1,
                InvoicesHistory::getStatus(0),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(2),
                json_encode($responce ?? null),
                'Service Provider response incorrect'
            );
            Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|ERROR|MERCHANT_ID|' . $invoice->merchant_id, ['deal' => $responce ?? null]);
        }
        $cryptoContract->delete();
        return false;
    }

    public function infoCryptoContract($id)
    {

        $contractInfo = CryptoPaymentInvoice::find($id);
        $funded_at = $contractInfo->created_at; // !!!!! or STATUS_FUNDED
        $payment_completed_at = null;
        $contractCurrency = CryptoCurrency::find($contractInfo->currency_id);
        if ($contractInfo) {
            if ($contractInfo->status == CryptoPaymentInvoice::STATUS_CANCELED) {
                $status = self::STATUS_CANCEL;
            } elseif ($contractInfo->status == CryptoPaymentInvoice::STATUS_PAYED) {
                $status = self::STATUS_COMPLETE;
            } elseif ($contractInfo->status == CryptoPaymentInvoice::STATUS_USER_CONFIRMED) {
                $contractInfo->status = CryptoPaymentInvoice::STATUS_TRADER_CONFIRMED;
                $contractInfo->save();
                $status = self::STATUS_FUNDED;
            } elseif ($contractInfo->status == CryptoPaymentInvoice::STATUS_TRADER_CONFIRMED) {
                $status = self::STATUS_FUNDED;
                $this->updateInvoiceStatusByCrypto($id);
            } else {
                $status = self::STATUS_DEFAULT;
            }


            return $this->getFormatedCryptoInfo($contractInfo, $status, $funded_at, $payment_completed_at, $contractCurrency);
        }
        return false;

    }

    public function cancelCryptoContract($id)
    {
        $cryptoInvoice = CryptoPaymentInvoice::find($id);
        if ($cryptoInvoice && !CryptoPaymentInvoice::isCancelStatus($cryptoInvoice->status)) {
            $cancel = $this->cryptoprocessing->invoiceCancel(["$cryptoInvoice->id"]);
            if ($cancel && $cancel['status'] == Cryptoprocessing::INVOICE_STATUS_CANCELLED) {
                $cryptoInvoice->address_amount = $cancel['value'];
                $cryptoInvoice->status = CryptoPaymentInvoice::STATUS_CANCELED;
                $cryptoInvoice->cancelation_reason = CryptoPaymentInvoice::CANCELED_BY_USER;
                $cryptoInvoice->save();
                return true;
            }
        }
        return false;
    }

    public function markAsPaidCryptoContract($id)
    {
//        $cryptoInvoice = CryptoPaymentInvoice::find($id);
//        if ($cryptoInvoice) {
//            $cryptoInvoice->status = CryptoPaymentInvoice::STATUS_USER_CONFIRMED;
//            $cryptoInvoice->save();
//            return true;
//        }
        return true;
    }

    public function releaseCryptoContract($id)
    {
//        $cryptoInvoice = CryptoPaymentInvoice::find($id);
//        if ($cryptoInvoice) {
//            $cryptoInvoice->status = CryptoPaymentInvoice::STATUS_PAYED;
//            $cryptoInvoice->save();
//            return true;
//        }
        return true;
    }

    public function messagePostCryptoContract($document)
    {
        return true;
    }

    public function contactMessagesCryptoContract($id)
    {
        $messages = [];
        return $messages;
    }

    public function getFormatedCryptoInfo($contractInfo, $status, $funded_at, $payment_completed_at, $contractCurrency){
        return [
            'id' => $contractInfo->id,
            'amount_btc' => $contractInfo->amount,
            'amount' => round($contractInfo->amount, 6),
            'address_amount' => round($contractInfo->address_amount, 6),
            'currency' => $contractCurrency->asset,
            'status' => $status,
            'address' => $contractInfo->address ?? null,
            'expired_at' => $contractInfo->expired_at ?? null,
            'funded_at' => $funded_at ?? null,
            'payment_completed_at' => $payment_completed_at ?? null,
            'account_info' => $contractInfo->addition_info,
            'payment_info' => null
        ];
    }

    public function getInvoiceToCrypto($id){
        if(!$this->invoice || ($this->invoice->payment_id != $id)){
            $serviceProviderIds = ServiceProvider::where('type', ServiceProvider::CRYPTO_PROVIDER)->pluck('id')->toArray();
            if(count($serviceProviderIds)){
                $this->invoice = PaymentInvoice::where('payment_id', $id)->whereIn('service_provider_id', $serviceProviderIds)->first();
                if(!$this->invoice){
                    $this->invoice = InputInvoice::where('payment_id', $id)->whereIn('service_provider_id', $serviceProviderIds)->first();
                }
            }
        }
        return $this->invoice;
    }

    public function updateInvoiceStatusByCrypto($id){
        $invoice = $this->getInvoiceToCrypto($id);
        if($invoice && ($invoice->status != $invoice::class::STATUS_TRADER_CONFIRMED) ) {
            $invoice->status = $invoice::class::STATUS_TRADER_CONFIRMED;
            $invoice->save();
        }
    }
}
