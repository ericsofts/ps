<?php

namespace App\Traits;

use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\PaymentSystemType;
use App\Services\ContractManager;

trait Contract
{
    use ServiceProviderTrait;

    protected function isContractComplete($contractInfo, $invoice = false){
        if ($contractInfo['status'] == ContractManager::STATUS_COMPLETE) {
            if($invoice){
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::getInvoiceTypeByInstance($invoice),
                    2,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(2),
                    json_encode($contractInfo),
                    'Payment closed successfully'
                );
            }
            return true;
        }
        return false;
    }

    protected function isContractClosed($contractInfo, $invoice = false){
        if ($contractInfo['status'] == ContractManager::STATUS_CLOSED) {
            if($invoice){
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::getInvoiceTypeByInstance($invoice),
                    2,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(2),
                    json_encode($contractInfo),
                    'Payment closed'
                );
            }
            return true;
        }
        return false;
    }

    protected function isContractCancel($contractInfo, $invoice = false){
        if ($contractInfo['status'] == ContractManager::STATUS_CANCEL) {
            if($invoice){
                $invoice->status = get_class($invoice)::STATUS_CANCELED;
                $invoice->save();
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::getInvoiceTypeByInstance($invoice),
                    2,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(2),
                    json_encode($contractInfo),
                    'Canceled by Service Provider'
                );
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::getInvoiceTypeByInstance($invoice),
                    4,
                    InvoicesHistory::getStatus(1),
                    InvoicesHistory::getSource(2),
                    InvoicesHistory::getSource(3),
                    json_encode($contractInfo),
                    'Apply Cancel by Service Provider'
                );
            }
            return true;
        }
        return false;
    }

    protected function isContractDisputed($contractInfo, $invoice = false){
        if ($contractInfo['status'] == ContractManager::STATUS_DISPUTED) {
            if($invoice){
                $record = InvoicesHistory::where([
                    'invoice_id' => $invoice->id,
                    'invoice_type' => AccountingEntrie::getInvoiceTypeByInstance($invoice),
                    'comment' => 'Arbitrage Invoice'
                ])->first();
                if(!$record){
                    InvoicesHistory::addHistoryRecord(
                        __METHOD__,
                        $invoice->id,
                        AccountingEntrie::getInvoiceTypeByInstance($invoice),
                        6,
                        InvoicesHistory::getStatus(0),
                        InvoicesHistory::getSource(3),
                        InvoicesHistory::getSource(2),
                        json_encode($contractInfo),
                        'Arbitrage Invoice'
                    );
                }
            }
            return true;
        }
        return false;
    }

    protected function isContractFunded($contractInfo, $invoice = false){
        if ($contractInfo['status'] == ContractManager::STATUS_FUNDED) {
            return true;
        }
        return false;
    }

    protected function isContractPaymentCompleted($contractInfo, $invoice = false){
        if ($contractInfo['status'] == ContractManager::STATUS_PAYMENT_COMPLETED) {
            if($invoice){
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::getInvoiceTypeByInstance($invoice),
                    2,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(2),
                    json_encode($contractInfo),
                    'Confirm Trader/user Payment'
                );
            }
            return true;
        }
        return false;
    }

    protected function isContractCrypto($contractInfo){
        if (isset($contractInfo['address']) && $contractInfo['address']) {
            return true;
        }
        return false;
    }

}
