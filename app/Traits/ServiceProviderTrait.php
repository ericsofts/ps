<?php

namespace App\Traits;


use App\Models\ServiceProvider;
use Illuminate\Support\Facades\DB;

trait ServiceProviderTrait
{

    public function getProviderByName($name){
        return ServiceProvider::where('name', '=' ,$name)->first();
    }

    public function getFirstProviderByType($type){
        return ServiceProvider::where('type', '=' ,$type)->first();
    }

    public function getProviderById($id){
        return ServiceProvider::where('id', '=' ,$id)->first();
    }

    public function getAvalableProviderQuery()
    {
        $providers = DB::table('merchant_service_providers')
            ->join('service_providers', 'service_providers.id', '=', 'merchant_service_providers.service_provider_id')
            ->join('service_providers_properties', function ($join) {
                $join->on('service_providers_properties.obj_name', '=', 'service_providers.type')
                    ->where('service_providers_properties.property', '=', 'ad_id_prefix');
            })
            ->select(
                'service_providers.id',
                'service_providers.name',
                'service_providers.type',
                'service_providers.status',
                'merchant_service_providers.priority',
                'service_providers_properties.property_value as prefix'
            )
            ->where('status', '=', ServiceProvider::STATUS_ACTIVE)
            ->orderBy('priority', 'ASC');
        return $providers;
    }

    public function getAvalableUserProviderQuery()
    {
        $providers = DB::table('service_providers')
            ->join('service_providers_properties', function ($join) {
                $join->on('service_providers_properties.obj_name', '=', 'service_providers.type')
                    ->where('service_providers_properties.property', '=', 'ad_id_prefix');
            })
            ->select(
                'service_providers.id',
                'service_providers.name',
                'service_providers.type',
                'service_providers.status',
                'service_providers_properties.property_value as prefix'
            )
            ->where('status', '=', ServiceProvider::STATUS_ACTIVE);
        return $providers;
    }

    public function getProviderTypeByAdId($adId){
        $currentPrefix = explode('_',$adId);
        $provider = $this->getAvalableProviderQuery()->where('property_value', '=', $currentPrefix[0])->first();
        return $provider->type;
    }

    public function getCleanAdId($adId){
        $currentPrefix = explode('_',$adId);
        return $currentPrefix[1];
    }
}
