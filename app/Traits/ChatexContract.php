<?php

namespace App\Traits;

use App\Lib\Chatex\ChatexApiLBC;
use App\Models\AccountingEntrie;
use App\Models\InputInvoice;
use App\Models\InvoicesHistory;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use App\Models\Property;
use App\Models\ServiceProvider;
use Illuminate\Support\Facades\Log;

trait ChatexContract
{

    use ChatexAds;

    private $invoice;

    public function getChatexLBCConnection(){
        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        return $chatexLBC;
    }

    public function createChatexContract($ad, $currency_amount, $user = false, $invoice = false, $message = null){
        $res = $this->getChatexLBCConnect()->contactCreate($ad['id'], $currency_amount, $message);
        Log::channel('chatex_lbc_requests_info')->info('CONTACT_CREATE|', ['ad' => $ad, 'res' => $res]);
        if (!empty($res['data']['contact_id'])) {
               return [ 'id' => $res['data']['contact_id'], 'service_provider_type' => $ad['service_provider_type']];
        }elseif($invoice){
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::getInvoiceTypeByInstance($invoice),
                1,
                InvoicesHistory::getStatus(0),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(2),
                json_encode($res),
                'Service Provider response incorrect'
            );
            Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|ERROR|MERCHANT_ID|' . $invoice->merchant_id, ['res' => $res, 'invoice_id' => $invoice->id]);
        }
        return false;
    }

    public  function infoChatexContract($id){
        $contractInfo = $this->getChatexLBCConnect()->contactInfo($id);
        Log::channel('chatex_lbc_requests_info')->info('CONTACT_INFO|', ['contact_id' => $id, 'contractInfo' => $contractInfo]);
        if(!empty($contractInfo['data'])){
            if(!empty($contractInfo['data']['canceled_at'])){
                $status = self::STATUS_CANCEL;
            }elseif (!empty($contractInfo['data']['released_at'])){
                $status = self::STATUS_COMPLETE;
            }elseif (!empty($contractInfo['data']['closed_at'])){
                $status = self::STATUS_CLOSED;
            }elseif (!empty($contractInfo['data']['payment_completed_at'])){
                $status = self::STATUS_PAYMENT_COMPLETED;
            }elseif (!empty($contractInfo['data']['funded_at'])){
                $status = self::STATUS_FUNDED;
                $this->updateInvoiceStatusByChatex($contractInfo, $status, $id);
            }elseif (!empty($contractInfo['data']['disputed_at'])){
                $status = self::STATUS_DISPUTED;
            }else{
                $status = self::STATUS_DEFAULT;
            }

            $invoice = $this->getInvoiceToChatex($id, $contractInfo);

            if($invoice){
                $addition_info = json_decode($invoice['addition_info'], true);
                $card_number = $addition_info['ad']['card_number'] ?? null;
            }
            if(empty($card_number)){
                $ad = $this->getChatexAd($contractInfo['data']['advertisement']['id']);
                $card_number = $ad['card_number'];
            }

            return $this->getFormatedChatexInfo($contractInfo, $status, $card_number);
        }
        return false;
    }

    public function cancelChatexContract($id){
        $contractCancel = $this->getChatexLBCConnect()->contactCancel($id);
        Log::channel('chatex_lbc_requests_info')->info('CONTACT_CANCEL|', ['contact_id' => $id, 'contractCancel' => $contractCancel]);
        if(!empty($contractCancel['data'])){
            return $contractCancel;
        }
        return false;
    }

    public function markAsPaidChatexContract($id){
        $contractMarkAsPaid = $this->getChatexLBCConnect()->contactMarkAsPaid($id);
        Log::channel('chatex_lbc_requests_info')->info('CONTACT_MARK_AS_PAID|', ['contact_id' => $id, 'contractMarkAsPaid' => $contractMarkAsPaid]);
        if(!empty($contractMarkAsPaid['data'])){
            return $contractMarkAsPaid;
        }
        return false;
    }

    public function releaseChatexContract($id){
        $contractRelease = $this->getChatexLBCConnect()->contactRelease($id);
        Log::channel('chatex_lbc_requests_info')->info('CONTACT_RELEASE|', ['contact_id' => $id, 'contractRelease' => $contractRelease]);
        if(!empty($contractRelease['data'])){
            return $contractRelease;
        }
        return false;
    }

    public function messagePostChatexContract($id, $document){
        $messagePost = $this->getChatexLBCConnect()->contactMessagePost(
            $id,
            null,
            $document
        );
        Log::channel('chatex_lbc_requests_info')->info('CONTACT_MESSAGE_POST|', ['contact_id' => $id, 'messagePost' => $messagePost]);
        if($messagePost){
            return true;
        }
        return false;
    }

    public function contactMessagesChatexContract($id){
        $result = $this->getChatexLBCConnect()->contactMessages($id);
        if(!empty($result['data']['message_list'])){
            return $result['data']['message_list'];
        }
        return false;
    }

    public function getFormatedChatexInfo($contractInfo, $status, $card_number){
        return [
            'id' => $contractInfo['data']['contact_id'],
            'amount_btc' => $contractInfo['data']['amount_btc'] ?? null,
            'amount' => $contractInfo['data']['amount'],
            'currency' => $contractInfo['data']['currency'],
            'status' => $status,
            'funded_at' => $contractInfo['data']['funded_at'] ?? null,
            'payment_completed_at' => $contractInfo['data']['payment_completed_at'] ?? null,
            'account_info' => $card_number ?:$contractInfo['data']['account_info'] ?? null,
        ];
    }

    public function updateInvoiceStatusByChatex($contractInfo, $status, $id){
        if(
            isset($contractInfo['data']['advertisement']['id']) &&
            $contractInfo['data']['advertisement']['id']
        ){
            $invoice = $this->getInvoiceToChatex($id, $contractInfo);
            if($invoice && ($invoice->status != $invoice::class::STATUS_TRADER_CONFIRMED) && !($invoice instanceof MerchantWithdrawalInvoice) ){
                $ad = $this->getChatexAd($contractInfo['data']['advertisement']['id']);
                $card_number = $ad['card_number'] ? : $ad['account_info'];
                $contract = $this->getFormatedChatexInfo($contractInfo, $status, $card_number);

                if($ad){
                    $invoice->addition_info = json_encode([
                        'contact' => $contract,
                        'ad' => $ad
                    ]);
                    if ($invoice->getConnection()
                        ->getSchemaBuilder()
                        ->hasColumn($invoice->getTable(), 'account_info')) {
                        $invoice->account_info = $card_number;
                    }
                    $invoice->status = $invoice::class::STATUS_TRADER_CONFIRMED;
                    $invoice->save();
                    $this->invoice = $invoice;
                }
            }elseif ($invoice && ($invoice->status != $invoice::class::STATUS_TRADER_CONFIRMED) && ($invoice instanceof MerchantWithdrawalInvoice) ){
                $invoice->status = $invoice::class::STATUS_TRADER_CONFIRMED;
                $invoice->save();
                $this->invoice = $invoice;
            }
        }
    }

    public function getInvoiceToChatex($id, $contractInfo){
        if(!$this->invoice || ($this->invoice->payment_id != $id)){
            $serviceProviderIds = ServiceProvider::where('type', ServiceProvider::CHATEX_PROVIDER)->pluck('id')->toArray();
            if(count($serviceProviderIds)){
                if(isset($contractInfo['data']['is_buying']) &&
                    $contractInfo['data']['is_buying']
                ){
                    $this->invoice = PaymentInvoice::where('payment_id', $id)->whereIn('service_provider_id', $serviceProviderIds)->first();
                    if(!$this->invoice){
                        $this->invoice = InputInvoice::where('payment_id', $id)->whereIn('service_provider_id', $serviceProviderIds)->first();
                    }
                }elseif (isset($contractInfo['data']['is_selling']) &&
                    $contractInfo['data']['is_selling']
                ){
                    $this->invoice = MerchantWithdrawalInvoice::where('payment_id', $id)->whereIn('service_provider_id', $serviceProviderIds)->first();
                }

            }
        }
        return $this->invoice;
    }

}
