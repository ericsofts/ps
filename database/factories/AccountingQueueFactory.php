<?php

namespace Database\Factories;

use App\Models\AccountingQueue;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccountingQueueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AccountingQueue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
