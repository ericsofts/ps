<?php

namespace Database\Factories;

use App\Models\MerchantCommission;
use Illuminate\Database\Eloquent\Factories\Factory;

class MerchantCommissionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MerchantCommission::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
