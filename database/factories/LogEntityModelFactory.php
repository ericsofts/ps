<?php

namespace Database\Factories;

use App\Models\LogEntityModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class LogEntityModelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LogEntityModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
