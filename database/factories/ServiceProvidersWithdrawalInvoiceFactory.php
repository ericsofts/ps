<?php

namespace Database\Factories;

use App\Models\ServiceProvidersWithdrawalInvoice;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceProvidersWithdrawalInvoiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ServiceProvidersWithdrawalInvoice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
