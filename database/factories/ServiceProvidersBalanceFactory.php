<?php

namespace Database\Factories;

use App\Models\ServiceProvidersBalance;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceProvidersBalanceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ServiceProvidersBalance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
