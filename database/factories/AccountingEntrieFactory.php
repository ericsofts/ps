<?php

namespace Database\Factories;

use App\Models\AccountingEntrie;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccountingEntrieFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AccountingEntrie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
