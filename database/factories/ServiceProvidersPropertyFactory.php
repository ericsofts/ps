<?php

namespace Database\Factories;

use App\Models\ServiceProvidersProperty;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceProvidersPropertyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ServiceProvidersProperty::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
