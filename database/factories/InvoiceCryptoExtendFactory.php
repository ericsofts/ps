<?php

namespace Database\Factories;

use App\Models\InvoiceCryptoExtend;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceCryptoExtendFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = InvoiceCryptoExtend::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
