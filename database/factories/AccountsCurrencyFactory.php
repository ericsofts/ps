<?php

namespace Database\Factories;

use App\Models\AccountsCurrency;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccountsCurrencyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AccountsCurrency::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
