<?php

namespace Database\Factories;

use App\Models\SystemBalance;
use Illuminate\Database\Eloquent\Factories\Factory;

class SystemBalanceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SystemBalance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
