<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCheckedAtToInputInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('input_invoices', function (Blueprint $table) {
            $table->timestamp('checked_at')->nullable()->after('payed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('input_invoices', function (Blueprint $table) {
            $table->dropColumn(['checked_at']);
        });
    }
}
