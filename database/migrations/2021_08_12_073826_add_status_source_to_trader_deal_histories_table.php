<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusSourceToTraderDealHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trader_deal_histories', function (Blueprint $table) {
            $table->bigInteger('customer_id')->nullable();
            $table->integer('customer_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trader_deal_histories', function (Blueprint $table) {
            $table->dropColumn(['customer_type']);
            $table->dropColumn(['customer_id']);
        });
    }
}
