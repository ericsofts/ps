<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RateStop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trader_ads', function (Blueprint $table) {
            $table->dropColumn(['rate_stop_min']);
            $table->dropColumn(['rate_stop_max']);
        });
        Schema::table('trader_ads', function (Blueprint $table) {
            $table->decimal('rate_stop', 22, 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trader_ads', function (Blueprint $table) {
            $table->decimal('rate_stop_min', 22, 4)->nullable();
            $table->decimal('rate_stop_max', 22, 4)->nullable();
        });
        Schema::table('trader_ads', function (Blueprint $table) {
            $table->dropColumn(['rate_stop']);
        });
    }
}
