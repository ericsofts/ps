<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantInputInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_input_invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('amount', 22,6);
            $table->integer('payment_system')->index()->comment('0-manually');
            $table->string('payment_id')->nullable();
            $table->integer('merchant_id')->index();
            $table->integer('sd_user_id')->nullable()->index();
            $table->integer('status')->default(1)->index()->comment('0-created, 1-payed, 99');
            $table->timestamp('payed')->nullable();
            $table->text('addition_info')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_input_invoices');
    }
}
