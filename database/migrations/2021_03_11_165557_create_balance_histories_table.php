<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalanceHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_id')->nullable();
            $table->integer('user_id')->index();
            $table->integer('merchant_id')->nullable()->index();
            $table->integer('input_invoice_id')->nullable()->index();
            $table->integer('payment_invoice_id')->nullable()->index();
            $table->decimal('amount', 22,6);
            $table->integer('type')->default(0)->index()->comment('0-credit, 1-debit');
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_histories');
    }
}
