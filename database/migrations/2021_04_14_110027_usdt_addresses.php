<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsdtAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usdt_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('usdt_address', 128)->index();
            $table->bigInteger('block')->default(0);
            $table->string('status')->index()->default('unused');
            $table->decimal('total_received_funds', 16, 8)->default(0.00000000);
            $table->decimal('amount_correction', 20, 8)->default(0.00000000);
            $table->timestamp('assigned_at')->nullable();
            $table->timestamp('joined_at')->nullable();
            $table->bigInteger('xpub_id')->default(0);
            $table->integer('addr_index')->default(0);
            $table->string('join_info', 16384)->index()->nullable();
            $table->decimal('join_value', 65,32)->default(0.00000000000000000000000000000000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usdt_addresses');
    }
}
