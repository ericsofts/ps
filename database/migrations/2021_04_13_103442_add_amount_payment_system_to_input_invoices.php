<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAmountPaymentSystemToInputInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('input_invoices', function (Blueprint $table) {
            $table->decimal('ps_amount', 22,8)->nullable();
            $table->string('ps_currency')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('input_invoices', function (Blueprint $table) {
            $table->dropColumn(['ps_amount', 'ps_currency']);
        });
    }
}
