<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCryptoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_payment_invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('amount', 22,8)->nullable();
            $table->integer('currency_id')->nullable();
            $table->decimal('currency_rate', 22, 8)->nullable();
            $table->string('address')->nullable();
            $table->decimal('address_amount', 22,8)->nullable();
            $table->integer('input_currency_id')->nullable();
            $table->decimal('input_amount', 22, 8)->nullable();
            $table->decimal('input_rate', 22, 8)->nullable();
            $table->integer('status')->default(0)->index();
            $table->integer('cancelation_reason')->nullable()->index();
            $table->timestamp('payed')->nullable();
            $table->text('addition_info')->nullable();
            $table->string('comment')->nullable()->index();
            $table->timestamp('checked_at')->nullable()->index();
            $table->timestamp('expired_at')->nullable()->index();
            $table->timestamps();
        });

        Schema::create('crypto_currencies', function (Blueprint $table) {
            $table->id();
            $table->string('asset');
            $table->string('name');
            $table->integer('type')->comment('0-crypto, 1-fiat');
            $table->integer('precision');
            $table->string('crypto_asset')->nullable();
            $table->string('kraken_asset')->nullable();
            $table->decimal('spread', 8,4)->nullable();
            $table->integer('payment_invoice_time_live')->default(0);
            $table->integer('status')->index()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_payment_invoices');
        Schema::dropIfExists('crypto_currencies');
    }
}
