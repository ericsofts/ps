<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TraderBalanceOperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trader_balance_operations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('trader_id');
            $table->bigInteger('balance_id');
            $table->decimal('amount', 22,8);
            $table->bigInteger('sd_user_id');
            $table->text('comment')->nullable();
            $table->integer('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trader_balance_operations');
    }
}
