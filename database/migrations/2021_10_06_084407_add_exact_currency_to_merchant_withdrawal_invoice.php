<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExactCurrencyToMerchantWithdrawalInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_withdrawal_invoices', function (Blueprint $table) {
            $table->boolean('exact_currency')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_withdrawal_invoices', function (Blueprint $table) {
            $table->dropColumn(['exact_currency']);
        });
    }
}
