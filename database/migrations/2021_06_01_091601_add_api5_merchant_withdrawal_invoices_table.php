<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApi5MerchantWithdrawalInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_withdrawal_invoices', function (Blueprint $table) {
            $table->string('merchant_order_id')->nullable()->index();
            $table->string('merchant_order_desc')->nullable()->index();
            $table->string('merchant_response_url')->nullable()->index();
            $table->string('merchant_server_url')->nullable()->index();
            $table->timestamp('merchant_response_at')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_withdrawal_invoices', function (Blueprint $table) {
            $table->dropColumn(['merchant_order_id', 'merchant_order_desc', 'merchant_response_url', 'merchant_server_url', 'merchant_response_at']);
        });
    }
}
