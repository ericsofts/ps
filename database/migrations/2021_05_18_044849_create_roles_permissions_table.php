<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sd_roles_permissions', function (Blueprint $table) {
            $table->unsignedInteger('sd_role_id');
            $table->unsignedInteger('sd_permission_id');

            $table->foreign('sd_role_id')->references('id')->on('sd_roles')->onDelete('cascade');
            $table->foreign('sd_permission_id')->references('id')->on('sd_permissions')->onDelete('cascade');
            $table->timestamps();
            $table->primary(['sd_role_id','sd_permission_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sd_roles_permissions');
    }
}
