<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TraderPaymentSystemsColor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trader_payment_systems', function (Blueprint $table) {
            $table->string('color')->nullable();
            $table->string('bg_color')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trader_payment_systems', function (Blueprint $table) {
            $table->dropColumn(['color']);
            $table->dropColumn(['bg_color']);
        });
    }
}
