<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMessageStatusTraderDealMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trader_deal_messages', function (Blueprint $table) {
            $table->boolean('viewed')->default(false);
            $table->dateTime('viewed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trader_deal_messages', function (Blueprint $table) {
            $table->dropColumn(['viewed']);
            $table->dropColumn(['viewed_at']);
        });
    }
}
