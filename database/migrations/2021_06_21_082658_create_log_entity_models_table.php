<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogEntityModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_entity_models', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('sd_user_id')->index();
            $table->string('model_instance')->nullable();
            $table->text('before')->nullable();
            $table->text('after')->nullable();
            $table->text('different')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_entity_models');
    }
}
