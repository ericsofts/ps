<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReturnUrlToMerchantWithdrawalInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_withdrawal_invoices', function (Blueprint $table) {
            $table->string('merchant_return_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_withdrawal_invoices', function (Blueprint $table) {
            $table->dropColumn(['merchant_return_url']);
        });
    }
}
