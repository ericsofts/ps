<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCurrencyToPaymentInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_invoices', function (Blueprint $table) {
            $table->string('input_currency')->nullable();
            $table->decimal('input_rate', 22, 4)->nullable();
            $table->decimal('input_amount_value', 22, 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_invoices', function (Blueprint $table) {
            $table->dropColumn(['input_currency']);
            $table->dropColumn(['input_rate']);
            $table->dropColumn(['input_amount_value']);
        });
    }
}
