<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TraderCurrenciesAddfields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trader_currencies', function (Blueprint $table) {
            $table->integer('can_get_address')->nullable();
            $table->string('crypto_asset')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trader_currencies', function (Blueprint $table) {
            $table->dropColumn(['can_get_address']);
            $table->dropColumn(['crypto_asset']);
        });
    }
}
