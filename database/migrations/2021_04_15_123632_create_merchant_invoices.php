<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('invoice_number')->unique()->index();
            $table->integer('user_id')->nullable()->index();
            $table->integer('merchant_id')->nullable()->index();
            $table->integer('admin_id')->nullable()->index();
            $table->decimal('amount', 22,6);
            $table->decimal('amount2pay', 22,6);
            $table->integer('status')->default(1)->index()->comment('0-created, 1-payed');
            $table->timestamp('payed')->nullable();
            $table->text('addition_info')->nullable();
            $table->string('merchant_order_id')->nullable()->index();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_invoices');
    }
}
