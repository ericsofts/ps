<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMerchantWithdrawalInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_withdrawal_invoices', function (Blueprint $table) {
            $table->decimal('fiat_amount', 22,8)->nullable();
            $table->string('fiat_currency')->nullable()->index();
            $table->string('account_info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_withdrawal_invoices', function (Blueprint $table) {
            $table->dropColumn(['fiat_amount', 'fiat_currency', 'account_info']);
        });
    }
}
