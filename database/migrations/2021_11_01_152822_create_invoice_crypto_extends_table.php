<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceCryptoExtendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_crypto_extends', function (Blueprint $table) {
            $table->id();
            $table->integer('invoice_id');
            $table->integer('invoice_type');
            $table->string('crypto_address')->nullable();
            $table->string('coin')->nullable();
            $table->integer('currency_id')->nullable();
            $table->decimal('address_amount', 22,8)->nullable();
            $table->decimal('amount', 22,8)->nullable();
            $table->integer('status')->default(0)->index();
            $table->integer('cancelation_reason')->nullable()->index();
            $table->timestamp('payed')->nullable();
            $table->text('addition_info')->nullable();
            $table->timestamp('checked_at')->nullable()->index();
            $table->string('comment')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_crypto_extends');
    }
}
