<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountingQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_queues', function (Blueprint $table) {
            $table->id();
            $table->integer('type')->index();
            $table->integer('user_type')->index();
            $table->bigInteger('user_id')->nullable()->index();
            $table->decimal('amount', 22, 8);
            $table->decimal('balance_after', 22, 8);
            $table->bigInteger('invoice_id')->index();
            $table->integer('invoice_type')->index();
            $table->integer('status')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting_queues');
    }
}
