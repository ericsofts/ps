<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceProvidersPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_providers_properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('obj_name')->nullable()->index();
            $table->bigInteger('obj_id')->index();
            $table->string('property')->index();
            $table->text('property_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_providers_properties');
    }
}
