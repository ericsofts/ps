<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices_histories', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('invoice_id')->index();
            $table->integer('invoice_type')->index();
            $table->integer('event_id')->index();
            $table->string('event')->index();
            $table->string('status')->nullable();
            $table->string('source_request')->nullable();
            $table->string('source_response')->nullable();
            $table->text('additional_info')->nullable();
            $table->string('instance');
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices_histories');
    }
}
