<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('account_id')->unique()->index();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->integer('status')->index()->default(0);
            $table->string('secret_2fa')->nullable();
            $table->boolean('enable_2fa')->default(false)->after('secret_2fa');
            $table->timestamps();
        });

        DB::statement('CREATE UNIQUE INDEX users_email_lower_unique on users (lower(email));');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');

//        Schema::table('users', function (Blueprint $table) {
//            $table->dropIndex("users_email_lower_unique");
//        });
    }
}
