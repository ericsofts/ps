<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToGrowTokenInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('grow_token_invoices', function (Blueprint $table) {
            $table->decimal('amount2commission', 22, 6)->nullable();
            $table->string('commission')->nullable();
            $table->decimal('amount2pay', 22,6)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('grow_token_invoices', function (Blueprint $table) {
            $table->dropColumn(['amount2commission']);
            $table->dropColumn(['commission']);
            $table->dropColumn(['amount2pay']);
        });
    }
}
