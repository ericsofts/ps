<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TraderInputInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trader_input_invoices', function (Blueprint $table) {
            $table->id();
            $table->integer('trader_id')->index();
            $table->integer('currency_id')->index();
            $table->decimal('amount', 22,6);
            $table->integer('address_id')->index();
            $table->integer('balance_id')->index();
            $table->text('additional_info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trader_input_invoices');
    }
}
