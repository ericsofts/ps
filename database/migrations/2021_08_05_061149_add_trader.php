<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddTrader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traders', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->integer('status')->index()->default(0);
            $table->timestamps();
        });

        DB::statement('CREATE UNIQUE INDEX traders_email_lower_unique on traders (lower(email));');

        Schema::create('trader_password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('trader_fiats', function (Blueprint $table) {
            $table->id();
            $table->string('asset')->index();
            $table->string('name');
            $table->integer('precision');
            $table->integer('status')->index()->default(0);
            $table->timestamps();
        });

        Schema::create('trader_currencies', function (Blueprint $table) {
            $table->id();
            $table->string('asset')->index();
            $table->string('name');
            $table->integer('precision');
            $table->integer('status')->index()->default(0);
            $table->timestamps();
        });

        Schema::create('trader_rate_sources', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('status')->index()->default(0);
            $table->timestamps();
        });

        Schema::create('trader_rates', function (Blueprint $table) {
            $table->id();
            $table->integer('fiat_id')->index();
            $table->integer('currency_id')->index();
            $table->integer('rate_source_id')->index();
            $table->decimal('rate', 22, 8)->default(0);
            $table->timestamps();
        });

        Schema::create('trader_payment_systems', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('status')->index()->default(0);
            $table->timestamps();
        });

        Schema::create('trader_fiats_payment_systems', function (Blueprint $table) {
            $table->id();
            $table->integer('fiat_id')->index();
            $table->integer('payment_system_id')->index();
            $table->timestamps();
        });

        Schema::create('trader_balances', function (Blueprint $table) {
            $table->id();
            $table->integer('trader_id')->index();
            $table->decimal('amount', 22, 8)->default(0);
            $table->string('currency')->index();
            $table->integer('currency_id')->index();
            $table->integer('type')->index()->comment('0-normal, 1-frozen');
            $table->timestamps();
        });

        Schema::create('trader_balance_histories', function (Blueprint $table) {
            $table->id();
            $table->integer('trader_id')->index();
            $table->decimal('amount', 22, 8)->default(0);
            $table->integer('type')->index()->comment('0 - credit, 1 - debit');
            $table->integer('balance_id')->index();
            $table->decimal('balance', 22, 8)->default(0);
            $table->string('currency')->index();
            $table->integer('currency_id')->index();
            $table->integer('invoice_id')->index();
            $table->integer('invoice_type')->index()->comment('0-deal, 1-input, 2-output');
            $table->integer('balance_type')->index()->comment('0-normal, 1-frozen');
            $table->text('description')->nullable();
            $table->integer('status')->index()->default(0);
            $table->timestamps();
        });

        Schema::create('trader_balance_queues', function (Blueprint $table) {
            $table->id();
            $table->integer('trader_id')->index();
            $table->decimal('amount', 22, 8)->default(0);
            $table->integer('type')->index()->comment('0 - credit, 1 - debit');
            $table->integer('balance_id')->index();
            $table->decimal('balance', 22, 8)->default(0);
            $table->string('currency')->index();
            $table->integer('currency_id')->index();
            $table->integer('invoice_id')->index();
            $table->integer('invoice_type')->index()->comment('0-deal, 1-input, 2-output');
            $table->integer('balance_type')->index()->comment('0-normal, 1-frozen');
            $table->text('description')->nullable();
            $table->integer('status')->index()->default(0);
            $table->timestamps();
        });

        Schema::create('trader_ads', function (Blueprint $table) {
            $table->id();
            $table->integer('trader_id')->index();
            $table->integer('type')->index()->comment('0 - sell, 1 - buy');
            $table->integer('fiat_id')->index();
            $table->integer('currency_id')->index();
            $table->integer('payment_system_id')->index();
            $table->string('card_number')->nullable();
            $table->decimal('min_amount', 22, 8)->default(0);
            $table->decimal('max_amount', 22, 8)->default(0);
            $table->integer('rate_type')->index()->comment('0 - fixed, 1 - percentage');
            $table->integer('rate_id')->index()->nullable();
            $table->decimal('rate', 22, 8)->default(0);
            $table->integer('status')->index()->default(0);
            $table->timestamps();
        });

        Schema::create('trader_deals', function (Blueprint $table) {
            $table->id();
            $table->integer('ad_id')->index();
            $table->integer('type')->index()->comment('0 - sell, 1 - buy');
            $table->integer('fiat_id')->index();
            $table->integer('currency_id')->index();
            $table->integer('payment_system_id')->index();
            $table->string('card_number')->nullable();
            $table->decimal('amount', 22, 8)->default(0);
            $table->decimal('amount_currency', 22, 8)->default(0);
            $table->decimal('rate', 22, 8)->default(0);
            $table->integer('status')->index()->default(0);
            $table->timestamps();
        });

        Schema::create('trader_deal_histories', function (Blueprint $table) {
            $table->id();
            $table->integer('deal_id')->index();
            $table->integer('status')->index()->default(0);
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('trader_deal_messages', function (Blueprint $table) {
            $table->id();
            $table->integer('deal_id')->index();
            $table->text('msg')->nullable();
            $table->integer('attachment_type')->index()->default(0);
            $table->integer('sender_type')->index()->default(0);
            $table->integer('sender_id')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traders');
        Schema::dropIfExists('trader_password_resets');
        Schema::dropIfExists('trader_fiats');
        Schema::dropIfExists('trader_currencies');
        Schema::dropIfExists('trader_rate_sources');
        Schema::dropIfExists('trader_rates');
        Schema::dropIfExists('trader_payment_systems');
        Schema::dropIfExists('trader_balances');
        Schema::dropIfExists('trader_balance_histories');
        Schema::dropIfExists('trader_balance_queues');
        Schema::dropIfExists('trader_ads');
        Schema::dropIfExists('trader_deals');
        Schema::dropIfExists('trader_deal_histories');
        Schema::dropIfExists('trader_deal_messages');
    }
}
