<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCurrencyFieldInBalanceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('balance_queues', function (Blueprint $table) {
            $table->string('currency', 10)->change();
        });
        Schema::table('balance_histories', function (Blueprint $table) {
            $table->string('currency', 10)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balance_queues', function (Blueprint $table) {
            $table->integer('currency')->default(0)->change();
        });
        Schema::table('balance_histories', function (Blueprint $table) {
            $table->integer('currency')->default(0)->change();
        });
    }
}
