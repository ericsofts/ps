<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TraderDealsFieldsRename extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trader_deals', function(Blueprint $table) {
            $table->renameColumn('amount_currency', 'amount_fiat');
            $table->renameColumn('amount', 'amount_currency');
            $table->renameColumn('json', 'ad_json');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trader_deals', function(Blueprint $table) {
            $table->renameColumn('amount_currency', 'amount');
            $table->renameColumn('amount_fiat', 'amount_currency');
            $table->renameColumn('ad_json', 'json');
        });
    }
}
