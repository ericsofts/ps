<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSdUsersPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sd_users_partners', function (Blueprint $table) {
            $table->unsignedInteger('sd_user_id');
            $table->unsignedInteger('partner_id');

            $table->timestamps();
            $table->primary(['sd_user_id','partner_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sd_users_partners');
    }
}
