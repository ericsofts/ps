<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBalanceHistoryQueue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('balance_queues', function (Blueprint $table) {
            $table->integer('currency')->default(0);
            $table->integer('balance_type')->default(0);
            $table->decimal('amount', 22,8)->change();
        });

        Schema::table('balance_histories', function (Blueprint $table) {
            $table->integer('currency')->default(0);
            $table->integer('balance_type')->default(0);
            $table->decimal('amount', 22,8)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balance_queues', function (Blueprint $table) {
            $table->dropColumn(['currency', 'balance_type']);
        });
        Schema::table('balance_histories', function (Blueprint $table) {
            $table->dropColumn(['currency', 'balance_type']);
        });
    }
}
