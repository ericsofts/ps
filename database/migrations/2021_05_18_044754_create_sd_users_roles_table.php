<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSdUsersRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sd_users_roles', function (Blueprint $table) {
            $table->unsignedInteger('sd_user_id');
            $table->unsignedInteger('sd_role_id');

            $table->foreign('sd_user_id')->references('id')->on('sd_users')->onDelete('cascade');
            $table->foreign('sd_role_id')->references('id')->on('sd_roles')->onDelete('cascade');
            $table->timestamps();
            $table->primary(['sd_user_id','sd_role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sd_users_roles');
    }
}
