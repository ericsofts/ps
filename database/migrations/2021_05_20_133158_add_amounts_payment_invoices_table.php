<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAmountsPaymentInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_invoices', function (Blueprint $table) {
            $table->decimal('amount2service', 22,8)->nullable();
            $table->decimal('amount2agent', 22, 8)->nullable();
            $table->decimal('amount2grow', 22, 8)->nullable();
            $table->string('commission_grow')->nullable();
            $table->string('commission_agent')->nullable();
            $table->string('commission_service')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_invoices', function (Blueprint $table) {
            $table->dropColumn(['amount2service']);
            $table->dropColumn(['amount2agent']);
            $table->dropColumn(['amount2grow']);
            $table->dropColumn(['commission_grow']);
            $table->dropColumn(['commission_agent']);
            $table->dropColumn(['commission_service']);
        });
    }
}
