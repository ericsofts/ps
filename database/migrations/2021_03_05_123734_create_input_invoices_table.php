<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInputInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input_invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->index();
            $table->decimal('amount', 22,6);
            $table->integer('payment_system')->index()->comment('1-chatex');
            $table->string('payment_id')->nullable();
            $table->text('addition_info')->nullable();
            $table->integer('status')->index()->comment('0-created, 1-payed');
            $table->timestamp('payed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_invoices');
    }
}
