<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantWithdrawalInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_withdrawal_invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('merchant_id')->nullable()->index();
            $table->integer('payment_system')->index()->comment('1-chatex');
            $table->decimal('amount', 22,6);
            $table->integer('status')->default(0)->index();
            $table->timestamp('payed')->nullable();
            $table->text('addition_info')->nullable();
            $table->string('payment_id')->nullable();
            $table->decimal('ps_amount', 22,8)->nullable();
            $table->string('ps_currency')->nullable()->index();
            $table->integer('api_type')->nullable()->index();
            $table->timestamp('checked_at')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_withdrawal_invoices');
    }
}
