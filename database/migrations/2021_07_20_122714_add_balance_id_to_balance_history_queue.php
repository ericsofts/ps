<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBalanceIdToBalanceHistoryQueue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('balance_queues', function (Blueprint $table) {
            $table->integer('balance_id')->nullable();
        });

        Schema::table('balance_histories', function (Blueprint $table) {
            $table->integer('balance_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balance_queues', function (Blueprint $table) {
            $table->dropColumn(['balance_id']);
        });
        Schema::table('balance_histories', function (Blueprint $table) {
            $table->dropColumn(['balance_id']);
        });
    }
}
