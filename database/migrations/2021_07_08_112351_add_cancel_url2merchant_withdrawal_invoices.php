<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCancelUrl2merchantWithdrawalInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_withdrawal_invoices', function (Blueprint $table) {
            $table->string('merchant_cancel_url')->nullable();
            $table->string('merchant_cancel_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_withdrawal_invoices', function (Blueprint $table) {
            $table->dropColumn(['merchant_cancel_url', 'merchant_cancel_at']);
        });
    }
}
