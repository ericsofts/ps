<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToInputInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('input_invoices', function (Blueprint $table) {
            $table->integer('service_provider_id')->nullable();
            $table->decimal('fiat_amount', 22,8)->nullable();
            $table->string('fiat_currency')->nullable()->index();
            $table->decimal('amount2service', 22,8)->nullable();
            $table->decimal('amount2grow', 22, 8)->nullable();
            $table->string('commission_grow')->nullable();
            $table->string('commission_service')->nullable();
            $table->decimal('amount2pay', 22,8)->nullable();
            $table->decimal('grow_token_amount', 22,8)->nullable();
            $table->decimal('grow_token_amount2pay', 22,8)->nullable();
            $table->decimal('grow_token_rate', 22, 8)->nullable();
            $table->string('input_currency')->nullable();
            $table->decimal('input_rate', 22, 4)->nullable();
            $table->decimal('input_amount_value', 22, 4)->nullable();
            $table->boolean('is_auto_grt')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('input_invoices', function (Blueprint $table) {
            $table->dropColumn(['service_provider_id']);
            $table->dropColumn(['fiat_amount']);
            $table->dropColumn(['fiat_currency']);
            $table->dropColumn(['amount2service']);
            $table->dropColumn(['amount2grow']);
            $table->dropColumn(['commission_grow']);
            $table->dropColumn(['commission_service']);
            $table->dropColumn(['amount2pay']);
            $table->dropColumn(['grow_token_amount']);
            $table->dropColumn(['grow_token_rate']);
            $table->dropColumn(['input_currency']);
            $table->dropColumn(['input_rate']);
            $table->dropColumn(['input_amount_value']);
        });
    }
}
