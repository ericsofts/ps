<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PaymentSystemTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_system_types', function (Blueprint $table) {
            $table->id();
            $table->string('obj_name')->index();
            $table->string('name')->index();
            $table->timestamps();
        });

        Schema::table('trader_ads', function (Blueprint $table) {
            $table->integer('payment_system_type_id')->after('payment_system_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_system_types');

        Schema::table('trader_ads', function (Blueprint $table) {
            $table->dropColumn(['payment_system_type_id']);
        });
    }
}
