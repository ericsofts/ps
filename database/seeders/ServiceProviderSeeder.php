<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_providers')->insert(['name' => 'chatex', 'priority' => 0, 'status' => 1]);
        DB::table('service_providers')->insert(['name' => 'trader', 'priority' => 0, 'status' => 1]);
    }
}
