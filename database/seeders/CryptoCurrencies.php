<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CryptoCurrencies extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('crypto_currencies')->insert([
            'asset' => 'USDT_TRC20',
            'name' => 'USDT_TRC20',
            'type' => 0,
            'precision' => 6,
            'crypto_asset' => 'TRX:USDT',
            'status' => 1,
            'kraken_asset' => 'USDT'
        ]);

    }
}
