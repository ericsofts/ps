<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoinmarketcapKeys extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date('Y-m-d H:i:s');

        DB::table('coinmarketcap_keys')->insert(['email' => 'ss1-gps@mailinator.com', 'apikey' => 'dda40c0a-efe2-4b93-8850-61f313c72ecc', 'uses' => 0, 'created_at' => $now, 'updated_at' => $now]);
        DB::table('coinmarketcap_keys')->insert(['email' => 'ss2-gps@mailinator.com', 'apikey' => '5ed29a94-26be-4921-8bdd-7d71c1bf42ce', 'uses' => 0, 'created_at' => $now, 'updated_at' => $now]);
    }
}
