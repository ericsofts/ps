<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertyUSDT2USD extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->insert(['obj_name' => 'coinmarketcap_usdt2usd', 'obj_id' => '0', 'property' => 'coin', 'property_value' => 'USD']);
        DB::table('properties')->insert(['obj_name' => 'coinmarketcap_usdt2usd', 'obj_id' => '0', 'property' => 'symbol', 'property_value' => 'USDT']);
        DB::table('properties')->insert(['obj_name' => 'coinmarketcap_usdt2usd', 'obj_id' => '0', 'property' => 'coin_rate', 'property_value' => 0]);
        DB::table('properties')->insert(['obj_name' => 'coinmarketcap_usdt2usd', 'obj_id' => '0', 'property' => 'key', 'property_value' => '8b0dc5d1-0cb5-48d4-8885-8a47d15cad9e']);
    }
}
