<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertyGROWTokenUSDT extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->insert(['obj_name' => 'grow_token_usd2token', 'obj_id' => '0', 'property' => 'grow_usdt', 'property_value' => '0']);

    }
}
