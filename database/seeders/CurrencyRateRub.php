<?php

namespace Database\Seeders;

use App\Models\Property;
use Illuminate\Database\Seeder;

class CurrencyRateRub extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Property::firstOrCreate([
            'obj_name' => 'currency_rate',
            'obj_id' => 0,
            'property' => 'usd2rub'
        ]);

        Property::firstOrCreate([
            'obj_name' => 'currency_rate',
            'obj_id' => 0,
            'property' => 'rub2usd'
        ]);

        Property::firstOrCreate([
            'obj_name' => 'currency_rate',
            'obj_id' => 0,
            'property' => 'usd2uah'
        ]);

        Property::firstOrCreate([
            'obj_name' => 'currency_rate',
            'obj_id' => 0,
            'property' => 'uah2usd'
        ]);

        Property::firstOrCreate([
            'obj_name' => 'currency_rate',
            'obj_id' => 0,
            'property' => 'usd2kzt'
        ]);

        Property::firstOrCreate([
            'obj_name' => 'currency_rate',
            'obj_id' => 0,
            'property' => 'kzt2usd'
        ]);

        Property::firstOrCreate([
            'obj_name' => 'currency_rate',
            'obj_id' => 0,
            'property' => 'usd2inr'
        ]);

        Property::firstOrCreate([
            'obj_name' => 'currency_rate',
            'obj_id' => 0,
            'property' => 'inr2usd'
        ]);

        Property::firstOrCreate([
            'obj_name' => 'currency',
            'obj_id' => 0,
            'property' => 'available_currency',
            'property_value' => json_encode(['USD','RUB','UAH','KZT', 'INR'])
        ]);
    }
}
