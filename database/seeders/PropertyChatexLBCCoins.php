<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertyChatexLBCCoins extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->insert(['obj_name' => 'system', 'obj_id' => '0', 'property' => 'available_chantex_lbc_coins', 'property_value' => '["usdt"]']);
    }
}
