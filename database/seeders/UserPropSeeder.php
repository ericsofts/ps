<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserPropSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->insert(['obj_name' => 'users', 'obj_id' => '0', 'property' => 'user_commission', 'property_value' => '{"service":"2", "grow":"2"}']);
        DB::table('properties')->insert(['obj_name' => 'users', 'obj_id' => '0', 'property' => 'user_available_service_providers', 'property_value' => '["chatex"]']);
        DB::table('properties')->insert(['obj_name' => 'users', 'obj_id' => '0', 'property' => 'user_available_coins', 'property_value' => '["usdt_trc20"]']);
        DB::table('properties')->insert(['obj_name' => 'uniswap', 'obj_id' => '0', 'property' => 'rate', 'property_value' => '0']);
        DB::table('properties')->insert(['obj_name' => 'users', 'obj_id' => '0', 'property' => 'withdrawal_commission', 'property_value' => '5']);
    }
}
