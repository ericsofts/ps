<?php

namespace Database\Seeders;

use App\Models\Merchant;
use App\Models\MerchantCommission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertyMerchant extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


//grow Finance Boutique

        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API2,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":8}}'
        );
        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":6.5}}'
        );
        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API4,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":8}}'
        );
//agent Finance Boutique

        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API2,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API4,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
//service Finance Boutique

        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API2,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":1.5}}'
        );
        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API4,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );

//grow Monotrade

        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API2,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":8}}'
        );
        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":6.5}}'
        );
        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API4,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":8}}'
        );
//agent Monotrade

        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API2,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API4,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
//service Monotrade

        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API2,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":1.5}}'
        );
        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API4,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );

//grow ToTheMoney

        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API2,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":2.7}}'
        );
        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0.5}}'
        );
        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API4,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":2.7}}'
        );
//agent ToTheMoney

        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API2,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );
        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );
        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API4,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );
//service ToTheMoney

        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API2,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":2.2}}'
        );
        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API4,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );

//grow ZelwinGames

        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API2,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":8}}'
        );
        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":6.5}}'
        );
        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API4,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":8}}'
        );
//agent ZelwinGames

        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API2,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API4,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
//service ZelwinGames

        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API2,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":1.5}}'
        );
        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API4,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );

//grow Teletrade

        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API2,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":8}}'
        );
        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":4.5}}'
        );
        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API4,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":8}}'
        );
//agent Teletrade

        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API2,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API4,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
//service Teletrade

        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API2,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":1.5}}'
        );
        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API4,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );

//grow AlexF TEST

        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API2,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":8}}'
        );
        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":6.5}}'
        );
        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API4,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":8}}'
        );
//agent AlexF TEST

        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API2,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API4,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
//service AlexF TEST

        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API2,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":1.5}}'
        );
        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API4,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );

//grow fx-data

        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API2,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":2.7}}'
        );
        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0.5}}'
        );
        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API4,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":2.7}}'
        );
//agent fx-data

        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API2,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );
        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );
        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API4,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );
//service fx-data

        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API2,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":2.2}}'
        );
        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API4,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );

//grow Alikassa

        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API2,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":2.7}}'
        );
        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0.5}}'
        );
        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API4,
            'grow',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":2.7}}'
        );
//agent Alikassa

        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API2,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );
        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );
        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API4,
            'agent',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );
//service Alikassa

        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API2,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":2.2}}'
        );
        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API4,
            'service',
            MerchantCommission::PROPERTY_TYPE_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );

////withdrawal Teletrade

        //grow Finance Boutique

        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":6.5}}'
        );
//agent Finance Boutique

        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
//service Finance Boutique

        $this->setCommission(
            1286,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":1.5}}'
        );

//grow Monotrade

        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":6.5}}'
        );
//agent Monotrade

        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
//service Monotrade

        $this->setCommission(
            1287,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":1.5}}'
        );
//grow ToTheMoney

        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":0.5}}'
        );
//agent ToTheMoney

        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );
//service ToTheMoney

        $this->setCommission(
            1291,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":2.2}}'
        );

//grow ZelwinGames
        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":6.5}}'
        );
//agent ZelwinGames

        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
//service ZelwinGames

        $this->setCommission(
            1289,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":1.5}}'
        );


//grow AlexF TEST

        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":6.5}}'
        );
//agent AlexF TEST

        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":10}}'
        );
        //service AlexF TEST

        $this->setCommission(
            1288,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":1.5}}'
        );

//grow fx-data

        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":0.5}}'
        );

//agent fx-data

        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );

//service fx-data

        $this->setCommission(
            1292,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":2.2}}'
        );

//grow Alikassa

        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":0.5}}'
        );

//agent Alikassa

        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":5.3}}'
        );

//service Alikassa

        $this->setCommission(
            1293,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":2.2}}'
        );


///

//grow Teletrade

        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API3,
            'grow',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":2.5}}'
        );
//agent Teletrade

        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API3,
            'agent',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":0}}'
        );
//service Teletrade

        $this->setCommission(
            1290,
            MerchantCommission::API_TYPE_API3,
            'service',
            MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            '{"0":{"type":"percentage","value":1.5}}'
        );

        $this->setMinSums();
    }

    private function setMinSums()
    {
        $merchants = Merchant::all();
        foreach($merchants as $merchant) {
            $this->setMinSum(MerchantCommission::API_TYPE_API2, $merchant->id, '10', MerchantCommission::PROPERTY_TYPE_COMMISSION);
            $this->setMinSum(MerchantCommission::API_TYPE_API3, $merchant->id, '10', MerchantCommission::PROPERTY_TYPE_COMMISSION);
            $this->setMinSum(MerchantCommission::API_TYPE_API4, $merchant->id, '10', MerchantCommission::PROPERTY_TYPE_COMMISSION);
            $this->setMinSum(MerchantCommission::API_TYPE_API3, $merchant->id, '10', MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION);
        }

    }

    private function setMinSum($api_type, $id, $value, $type)
    {
        DB::table('merchant_commissions')->insert([
            'api_type' => $api_type,
            'merchant_id' => $id,
            'property_type' => $type,
            'property' => 'min_commission',
            'value' => $value
        ]);
    }

    private function setCommission($id, $api_type, $propery, $propertyType, $value){
        DB::table('merchant_commissions')->insert([
            'api_type' => $api_type,
            'merchant_id' => $id,
            'property_type' => $propertyType,
            'property' => $propery,
            'value' => $value
        ]);
    }

}
