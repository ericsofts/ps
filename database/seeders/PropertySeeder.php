<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('truncate table properties');

        DB::table('properties')->insert(['obj_name' => 'system', 'obj_id' => '0', 'property' => 'available_payment_systems', 'property_value' => '["bank_card"]']);

        DB::table('properties')->insert(['obj_name' => 'chatex', 'obj_id' => '0', 'property' => 'api_token', 'property_value' => 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxOTdlMTM0My0zMDBjLTQwZDItODRjNi0yNWNlYmYzNTdkYTIiLCJpYXQiOjE2MDI4ODA5MDcsImlzcyI6ImNoYXRleC1pZCIsInN1YiI6InVzZXIiLCJ1aWQiOjE0MSwidmVyIjoxLCJyZXMiOlsxXSwidHlwIjoxfQ.kj0EwdQ62td4c2JZqJETQQYDaCDKJnAXpVNTKeC4J_9KhCh-Y342UQsN4gCL8OPEaK-olv-saQc5fLTuLJW6cw']);
        DB::table('properties')->insert(['obj_name' => 'chatex', 'obj_id' => '0', 'property' => 'base_url', 'property_value' => 'https://api.staging.iserverbot.ru/v1/']);
        DB::table('properties')->insert(['obj_name' => 'chatex', 'obj_id' => '0', 'property' => 'callback_url', 'property_value' => '/chatex/callback']);
        DB::table('properties')->insert(['obj_name' => 'chatex', 'obj_id' => '0', 'property' => 'coin', 'property_value' => 'USDT']);

        DB::table('properties')->insert(['obj_name' => 'chatex_lbc', 'obj_id' => '0', 'property' => 'hmac_key', 'property_value' => 'YBFdFjDOCoLhrxDUo7JT']);
        //DB::table('properties')->insert(['obj_name' => 'chatex_lbc', 'obj_id' => '0', 'property' => 'hmac_secret', 'property_value' => 'QBeBLdEstQrHXG66rZOODZUiqUCS6h']);
        DB::table('properties')->insert(['obj_name' => 'chatex_lbc', 'obj_id' => '0', 'property' => 'server', 'property_value' => 'https://lbc.iserverbot.ru']);
        DB::table('properties')->insert(['obj_name' => 'chatex_lbc', 'obj_id' => '0', 'property' => 'coin', 'property_value' => 'BTC']);

        DB::table('properties')->insert(['obj_name' => 'coinmarketcap', 'obj_id' => '0', 'property' => 'coin', 'property_value' => 'USDT']);
        DB::table('properties')->insert(['obj_name' => 'coinmarketcap', 'obj_id' => '0', 'property' => 'symbol', 'property_value' => 'USD']);
        DB::table('properties')->insert(['obj_name' => 'coinmarketcap', 'obj_id' => '0', 'property' => 'coin_rate', 'property_value' => 0]);
        DB::table('properties')->insert(['obj_name' => 'coinmarketcap', 'obj_id' => '0', 'property' => 'key', 'property_value' => '8b0dc5d1-0cb5-48d4-8885-8a47d15cad9e']);

        DB::table('properties')->insert(['obj_name' => 'coinmarketcap2', 'obj_id' => '0', 'property' => 'coin', 'property_value' => 'BTC']);
        DB::table('properties')->insert(['obj_name' => 'coinmarketcap2', 'obj_id' => '0', 'property' => 'symbol', 'property_value' => 'USD']);
        DB::table('properties')->insert(['obj_name' => 'coinmarketcap2', 'obj_id' => '0', 'property' => 'coin_rate', 'property_value' => 0]);
        DB::table('properties')->insert(['obj_name' => 'coinmarketcap2', 'obj_id' => '0', 'property' => 'key', 'property_value' => '8b0dc5d1-0cb5-48d4-8885-8a47d15cad9e']);
    }
}
