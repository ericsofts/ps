<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertyInvoiceTimeout extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->insert(['obj_name' => 'invoice', 'obj_id' => '0', 'property' => 'payment_invoice_cancel_timeout', 'property_value' => '1']);
        DB::table('properties')->insert(['obj_name' => 'invoice', 'obj_id' => '0', 'property' => 'withdrawal_invoice_cancel_timeout', 'property_value' => '1']);
    }
}
