<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountsCurrencies extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accounts_currencies')->insert(['asset' => 'USD', 'name' => 'US Dollar', 'type' => 'fiat', 'precision' => '2']);
        DB::table('accounts_currencies')->insert(['asset' => 'USDT', 'name' => 'USDT ERC20', 'type' => 'crypto', 'precision' => '8']);
        DB::table('accounts_currencies')->insert(['asset' => 'GRT', 'name' => 'Grow Token', 'type' => 'crypto', 'precision' => '8']);
        DB::table('accounts_currencies')->insert(['asset' => 'BTC', 'name' => 'Bitcoin', 'type' => 'crypto', 'precision' => '8']);
        DB::table('accounts_currencies')->insert(['asset' => 'ETH', 'name' => 'Ethereum', 'type' => 'crypto', 'precision' => '8']);
        DB::table('accounts_currencies')->insert(['asset' => 'EUR', 'name' => 'Euro', 'type' => 'fiat', 'precision' => '2']);
    }
}
