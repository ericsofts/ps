<?php

namespace Database\Seeders;

use App\Models\Merchant;
use App\Models\MerchantCommission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertyMerchantApi5 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $merchants = Merchant::all();
        foreach($merchants as $merchant) {
            $mercantCommisions = MerchantCommission::getProperties($merchant->id, MerchantCommission::API_TYPE_API3, MerchantCommission::PROPERTY_TYPE_COMMISSION);
            $mercantWithdrawalCommisions = MerchantCommission::getProperties($merchant->id, MerchantCommission::API_TYPE_API3, MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION);
            foreach ($mercantCommisions as $key => $value){
                $this->setCommission($merchant->id, MerchantCommission::API_TYPE_API5, $key, MerchantCommission::PROPERTY_TYPE_COMMISSION,$value);
            }
            foreach ($mercantWithdrawalCommisions as $key => $value){
                $this->setCommission($merchant->id, MerchantCommission::API_TYPE_API5, $key, MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION, $value);
            }
        }
    }

    private function setCommission($id, $api_type, $propery, $propertyType, $value){
        DB::table('merchant_commissions')->insert([
            'api_type' => $api_type,
            'merchant_id' => $id,
            'property_type' => $propertyType,
            'property' => $propery,
            'value' => $value
        ]);
    }

}
