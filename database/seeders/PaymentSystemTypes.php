<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentSystemTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_system_types')->insert(['obj_name' => 'card_number', 'name' => 'Card Number']);
        DB::table('payment_system_types')->insert(['obj_name' => 'iban', 'name' => 'IBAN']);
        DB::table('payment_system_types')->insert(['obj_name' => 'upi', 'name' => 'UPI']);
        DB::table('payment_system_types')->insert(['obj_name' => 'qiwi', 'name' => 'Qiwi']);
    }
}
