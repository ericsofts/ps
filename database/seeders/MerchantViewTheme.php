<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MerchantViewTheme extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('merchant_view_themes')->insert(['name' => 'Old Theme']);
        DB::table('merchant_view_themes')->insert(['name' => 'New Theme']);
    }
}
