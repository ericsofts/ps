<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertyTelegram extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->insert(['obj_name' => 'telegram', 'obj_id' => '0', 'property' => 'api_token', 'property_value' => '1719953219:AAEgbpu7RxGmjF5gN0AgB9H4A9tjHGiEyXA']);
        DB::table('properties')->insert(['obj_name' => 'telegram', 'obj_id' => '0', 'property' => 'chat_id', 'property_value' => '-1001287081062']);
    }
}
