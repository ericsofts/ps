<?php

namespace Database\Seeders;

use App\Models\SystemBalance;
use Illuminate\Database\Seeder;

class SystemBalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SystemBalance::create([
            'name' => 'grow'
        ]);
        SystemBalance::create([
            'name' => 'service'
        ]);
    }
}
