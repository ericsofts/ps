<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertyGROWToken extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->insert(['obj_name' => 'grow_token_usd2token', 'obj_id' => '0', 'property' => 'coin', 'property_value' => 'GROWTOKEN']);
        DB::table('properties')->insert(['obj_name' => 'grow_token_usd2token', 'obj_id' => '0', 'property' => 'symbol', 'property_value' => 'USD']);
        DB::table('properties')->insert(['obj_name' => 'grow_token_usd2token', 'obj_id' => '0', 'property' => 'coin_rate', 'property_value' => '0.033333333']);
        DB::table('properties')->insert(['obj_name' => 'grow_token_usd2token', 'obj_id' => '0', 'property' => 'correction', 'property_value' => '2']);
        DB::table('properties')->insert(['obj_name' => 'grow_token_usd2token', 'obj_id' => '0', 'property' => 'grow_eth', 'property_value' => '0']);
        DB::table('properties')->insert(['obj_name' => 'grow_token_usd2token', 'obj_id' => '0', 'property' => 'eth_usd', 'property_value' => '0']);

        DB::table('properties')->insert(['obj_name' => 'grow_token', 'obj_id' => '0', 'property' => 'min_value', 'property_value' => '500']);
    }
}
