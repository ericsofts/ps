<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 14,
        ],

        'http_client' => [
            'driver' => 'single',
            'path' => storage_path('logs/http_client.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'accounting_queue' => [
            'driver' => 'single',
            'path' => storage_path('logs/accounting_queue.log'),
            'level' => 'debug',
        ],

        'trader_balance_queue' => [
            'driver' => 'single',
            'path' => storage_path('logs/trader_balance_queue.log'),
            'level' => 'debug',
        ],

        'trader_input_invoice' => [
            'driver' => 'single',
            'path' => storage_path('logs/trader_input_invoice.log'),
            'level' => 'debug',
        ],

        'after-request' => [
            'driver' => 'single',
            'path' => storage_path('logs/requests.log'),
            'level' => 'debug',
        ],

        'payment_invoice' => [
            'driver' => 'single',
            'path' => storage_path('logs/payment_invoice.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'chatex' => [
            'driver' => 'single',
            'path' => storage_path('logs/chatex.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'chatex_lbc' => [
            'driver' => 'single',
            'path' => storage_path('logs/chatex_lbc.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'chatex_lbc_errors' => [
            'driver' => 'single',
            'path' => storage_path('logs/chatex_lbc_errors.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'chatex_lbc_checker' => [
            'driver' => 'single',
            'path' => storage_path('logs/chatex_lbc_checker.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'api3_chatex_lbc_checker' => [
            'driver' => 'single',
            'path' => storage_path('logs/api3_chatex_lbc_checker.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'chatex_lbc_requests' => [
            'driver' => 'single',
            'path' => storage_path('logs/chatex_lbc_requests.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'chatex_lbc_requests_info' => [
            'driver' => 'single',
            'path' => storage_path('logs/chatex_lbc_requests_info.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'payment_invoice_requests' => [
            'driver' => 'single',
            'path' => storage_path('logs/payment_invoice_requests.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'api3_chatex_lbc_requests' => [
            'driver' => 'single',
            'path' => storage_path('logs/api3_chatex_lbc_requests.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'payment_invoice_server_response' => [
            'driver' => 'single',
            'path' => storage_path('logs/payment_invoice_server_response.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'merchant_withdrawal_invoice_server_response' => [
            'driver' => 'single',
            'path' => storage_path('logs/merchant_withdrawal_invoice_server_response.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'usdt_erc20_checker' => [
            'driver' => 'single',
            'path' => storage_path('logs/usdt_erc20_checker.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'callback_chatex' => [
            'driver' => 'single',
            'path' => storage_path('logs/callback_chatex.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'balance_queue' => [
            'driver' => 'single',
            'path' => storage_path('logs/balance_queue.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'trader_deals_checker' => [
            'driver' => 'single',
            'path' => storage_path('logs/trader_deals_checker.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'payment_invoices_checker' => [
            'driver' => 'single',
            'path' => storage_path('logs/payment_invoices_checker.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'merchant_withdrawal_invoices_checker' => [
            'driver' => 'single',
            'path' => storage_path('logs/merchant_withdrawal_invoices_checker.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'invoice_deals_checker' => [
            'driver' => 'single',
            'path' => storage_path('logs/invoice_deals_checker.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'telegram_noty' => [
            'driver' => 'single',
            'path' => storage_path('logs/telegram_noty.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'cbr_update' => [
            'driver' => 'single',
            'path' => storage_path('logs/cbr_update.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'nbu_update' => [
            'driver' => 'single',
            'path' => storage_path('logs/nbu_update.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'nbk_update' => [
            'driver' => 'single',
            'path' => storage_path('logs/nbk_update.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'coinsbit_update' => [
            'driver' => 'single',
            'path' => storage_path('logs/coinsbit_update.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'api5_payment_invoice' => [
            'driver' => 'single',
            'path' => storage_path('logs/api5_payment_invoice.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'api5_merchant_withdrawal_invoice' => [
            'driver' => 'single',
            'path' => storage_path('logs/api5_merchant_withdrawal_invoice.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'api3_payment_invoice' => [
            'driver' => 'single',
            'path' => storage_path('logs/api3_payment_invoice.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'api6_payment_invoice' => [
            'driver' => 'single',
            'path' => storage_path('logs/api6_payment_invoice.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'api3_merchant_withdrawal_invoice' => [
            'driver' => 'single',
            'path' => storage_path('logs/api3_merchant_withdrawal_invoice.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'telegram' => [
            'driver' => 'single',
            'path' => storage_path('logs/telegram.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'api_internal_ip_security' => [
            'driver' => 'single',
            'path' => storage_path('logs/api_internal_ip_security.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'internal_api_request' => [
            'driver' => 'single',
            'path' => storage_path('logs/internal_api_request.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => env('LOG_LEVEL', 'critical'),
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
        ],

        'emergency' => [
            'path' => storage_path('logs/laravel.log'),
        ],

        'ip_security' => [
            'driver' => 'single',
            'path' => storage_path('logs/ip_security.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'trader_requests' => [
            'driver' => 'single',
            'path' => storage_path('logs/trader_requests.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'trader_ads_shutdown' => [
            'driver' => 'single',
            'path' => storage_path('logs/trader_ads_shutdown.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'ads_filter_limit' => [
            'driver' => 'single',
            'path' => storage_path('logs/ads_filter_limit.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'crypto_callback' => [
            'driver' => 'single',
            'path' => storage_path('logs/crypto_callback.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'crypto_processing' => [
            'driver' => 'single',
            'path' => storage_path('logs/crypto_processing.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
    ],

];
